
<?php 
	global $post, $woocommerce, $product, $attributes;
	$product = get_product( $post->ID );
	$alt = 1;
	$attributes = $product->get_attributes();

	if ( has_post_thumbnail() ) {
		$img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large');	
	} else {
		// product placeholder image
		$img = array(get_stylesheet_directory_uri()."/img/modules/4xDDR-DIMM---184pin.png");
	}
	
	$product->upgrade_size = array_shift(woocommerce_get_product_terms( $product->id, $attributes["pa_upgrade-size"]["name"], 'names' ));
	$product->upgrade_configuration = array_shift(woocommerce_get_product_terms( $product->id, $attributes["pa_upgrade-configuration"]["name"], 'names' ));
	$product->oem_part_number = array_shift(woocommerce_get_product_terms( $product->id, $attributes["pa_oem-part-number"]["name"], 'names' ));
	$product->sku = $product->get_sku();
	
	$filename = 'DDR-DIMM-184pin';
	//print_r($product);
?>
<script type="text/javascript">
// Hide the elements initially  

// When some anchor tag is clicked. (Being super generic here)         
jQuery(document).ready(function($) {
	var lis = $('.images li').hide();
	var i = 0;  
	displayImages();
	function displayImages() {  
		lis.eq(i++).fadeIn(500, displayImages);
	}
	
	$img_container = $(".enlarged-img");
	$("body").prepend($img_container);
	
	$(".enlarge").click(function() {
		$(".enlarged-img").addClass("active");
		$(".enlarged-img").fadeIn();
		/*
		$(".enlarged-img img").rotate({
            angle: 0,
            center: ["50%", "50%"], 
            animateTo:90
		});
		*/
		
		
		
		$(this).bind("clickoutside", hideEnlargedImg);
		
		
	});

	var hideEnlargedImg = function() {
		if ($(".enlarged-img").is(":visible")) { $(".enlarged-img").fadeOut(); }
	}
});
</script>
<div class="enlarged-img" style="display: none;"><div class="pad"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/modules/<?php echo $filename; ?>-2x.png" alt="1xDDR-DIMM---184pin%2Bmeas" /></div></div>
<?php do_action( 'woocommerce_before_single_product' ); ?>
<div id="logo">
		<a href="<?php echo site_url(); ?>"><?php 
		if ( function_exists( 'get_option_tree' ) ) {
		$logopath = get_option_tree( 'vn_toplogo' );
		} ?><img src="<?php echo $logopath; ?>" alt="logo" /></a>
	</div><!-- End of logo -->

<div class="hero-wrapper">
	
	<?php if (1==2) :?><div class="hero-img" style="background-image: url('<?php echo $img[0]; ?>');"><img src="<?php echo $img[0]; ?>" /></div><?php endif; ?>
	
	<div class="hero-img">
		<ul class="images quad expanded">
			<li class="first"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/modules/<?php echo $filename; ?>-meas.png" alt="1xDDR-DIMM---184pin%2Bmeas" /></li>
			
			<li class="second"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/modules/<?php echo $filename; ?>.png" alt="1xDDR-DIMM---184pin-iso" /></li>
			<li class="third"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/modules/<?php echo $filename; ?>.png" alt="1xDDR-DIMM---184pin-iso" /></li>
			<li class="last"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/modules/<?php echo $filename; ?>.png" alt="1xDDR-DIMM---184pin-iso" /></li>
			<?php if (!empty($product->upgrade_size)) : ?><li class="upgrade-size"><strong><?php print $product->upgrade_size; ?></strong>
					<span><?php print $product->upgrade_configuration; ?></span></li><?php endif; ?>
			<span class="icon enlarge"></span>
		</ul>
	</div>
	
		<?php while ( have_posts() ) : the_post(); ?>
		<div class="header_right_column">
			<?php if ($product->is_on_sale()) : ?>
				<div class="onsale rounded darken">Currently on Sale!</div>
			<?php endif; ?>
				<?php edit_post_link( 'Edit this Product','<div class="edit darken" style="display: inline-block;">','</div>'); ?>
			
			<h1><?php the_title(); ?></h1>
			<ul class="product-inventory-details">
				<?php if (!empty($product->sku)) : ?><li>SKU: <?php print $product->sku; ?></li><?php endif; ?>
				<?php if (!empty($product->oem_part_number)) : ?><li>OEM Part #: <?php print $product->oem_part_number; ?></li><?php endif; ?>
			</ul>
			<div class="the-content"><?php the_content(); ?></div>
				
			<div class="cart-block darken rounded<?php if ($product->is_on_sale()) print ' onsalex';?>">
				<div class="pad">
					<div class="price-block">
						<p itemprop="price" class="price"><?php echo $product->get_price_html(); ?></p>
						<meta itemprop="priceCurrency" content="<?php echo get_woocommerce_currency(); ?>" />
						<link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />
					</div>
					
					<div class="add-to-cart-block">
					
					
						<?php do_action('woocommerce_before_add_to_cart_form'); ?>
					
						<?php do_action( 'woocommerce_' . $product->product_type . '_add_to_cart'  ); ?>
					
						<?php do_action('woocommerce_after_add_to_cart_form'); ?>
					
					</div><!-- /.add-to-cart-block -->
					
				<div class="clearfix"></div>
				</div><!-- /.pad -->
			</div><!-- /.cart-block -->
			
			
			
			
			
		</div><!-- /.header_right_column -->
		<?php endwhile; ?>
		
	<!-- Clear Fix --><div class="clear"></div>
	
</div><!-- /.hero-wrapper -->