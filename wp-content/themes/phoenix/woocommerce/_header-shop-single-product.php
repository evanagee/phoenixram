
<?php 
	global $post, $woocommerce, $product, $attributes;
	$product = get_product( $post->ID );
	$alt = 1;
	$attributes = $product->get_attributes();

	if ( has_post_thumbnail() ) {
		$img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large');	
	} else {
		// product placeholder image
		$img = array(get_stylesheet_directory_uri()."/img/modules/4xDDR-DIMM---184pin.png");
	}
	
	$product->memory_technology = array_shift(woocommerce_get_product_terms( $product->id, $attributes["pa_technology"]["name"], 'names' ));
	$product->dimm_type = array_shift(woocommerce_get_product_terms( $product->id, $attributes["pa_dimm-type"]["name"], 'names' ));
	$product->height = array_shift(woocommerce_get_product_terms( $product->id, $attributes["pa_height"]["name"], 'names' ));
	$product->speed = array_shift(woocommerce_get_product_terms( $product->id, $attributes["pa_module-speed"]["name"], 'names' ));
	$product->upgrade_size = array_shift(woocommerce_get_product_terms( $product->id, $attributes["pa_upgrade-size"]["name"], 'names' ));
	$product->upgrade_configuration = array_shift(woocommerce_get_product_terms( $product->id, $attributes["pa_upgrade-configuration"]["name"], 'names' ));
	$product->oem_part_number = array_shift(woocommerce_get_product_terms( $product->id, $attributes["pa_oem-part-number"]["name"], 'names' ));
	$product->sku = $product->get_sku();
	
	// Determine DIMM Type
	if (strpos($product->memory_technology,'2-')) { 
		// It's ddr2 or ddr3
		$filename = 'DDR2';
	} else if (strpos($product->memory_technology,'3-')) {
		$filename = 'DDR3';
	} else {
		// ddr
		$filename = 'DDR';
	}
	
	if ($product->height == 'VLP') {
		$filename .= '-VLP';
	}
	
	$filename .= '-'.$product->dimm_type;
	$has_image = true;
	$has_large_image = false;
	//print_r($_SERVER);
	//echo ''.get_template_directory().'/img/modules/'.$filename.'-meas.png'.'';
	if (!file_exists(get_template_directory().'/img/modules/'.$filename.'-meas.png')) {
		$filename = 'default';
		$has_image = false;
	}
	
	if (file_exists(get_template_directory().'/img/modules/'.$filename.'-2x.png')) {
		$has_large_image = true;
	}
	
	$kit_count = preg_match('/^(\d+)x/',$product->upgrade_configuration, $match);
	$kit_count = $match[1];
	
	if ($kit_count == 4) {
		$kit_class = 'quad';
	} else if ($kit_count == 2) {
		$kit_class = 'double';
	} else if ($kit_count == 1) {
		$kit_class = 'single';
	}
	
	//$filename = 'DDR-DIMM-184pin';
	//print_r($product);
?>
<script type="text/javascript">     
jQuery(document).ready(function($) {
	var lis = $('.images li').hide();
	var i = 0;  
	displayImages();
	function displayImages() { 
		lis.eq(i++).fadeIn(500, displayImages);
	}
	
	$img_container = $(".enlarged-img");
	$("body").prepend($img_container);
	
	$(".enlarge").click(function() {
		$(".enlarged-img").addClass("active");
		$(".enlarged-img").fadeIn();		
		$(this).bind("clickoutside", hideEnlargedImg);
	});

	var hideEnlargedImg = function() {
		if ($(".enlarged-img").is(":visible")) { $(".enlarged-img").fadeOut(); }
	}
});
</script>

<!-- Start of header wrapper -->
<div id="header_wrapper">

<!-- Start of content wrapper -->
<div class="content_wrapper <?php if ($cat_type == 'host' || $cat_type == 'parent') { print 'category-'.$cat_type; } else if ($post->post_type == 'product' && !is_shop()) { print 'single-product'; } else if (is_shop()) { print 'shop-index'; } ?>">

<?php if ($has_large_image) : ?>
<div class="enlarged-img" style="display: none;">
	<div class="pad"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/modules/<?php echo $filename; ?>-2x.png" alt="Module Image" /></div>
</div>
<?php endif; ?>

<?php
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked woocommerce_show_messages - 10
	 */
	 do_action( 'woocommerce_before_single_product' );
?>

<div id="logo">
	<a href="<?php echo site_url(); ?>"><?php 
	if ( function_exists( 'get_option_tree' ) ) {
	$logopath = get_option_tree( 'vn_toplogo' );
	} ?><img src="<?php echo $logopath; ?>" alt="logo" /></a>
</div><!-- End of logo -->


<div class="hero-wrapper">
	<div class="hero-img">
		<ul class="images <?php print $kit_class; ?> expanded">
			
			<?php if ($kit_count >= 1) : ?><li class="first" style="display: none;"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/modules/<?php echo $filename; ?>-meas.png" alt="Module Image" /></li><?php endif; ?>
			<?php if ($kit_count >= 2) : ?><li class="second" style="display: none;"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/modules/<?php echo $filename; ?>.png" alt="Module Image" /></li><?php endif; ?>
			<?php if ($kit_count >= 4) : ?><li class="third" style="display: none;"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/modules/<?php echo $filename; ?>.png" alt="Module Image" /></li>
			<li class="last" style="display: none;"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/modules/<?php echo $filename; ?>.png" alt="Module Image" /></li><?php endif; ?>
			
			
			<?php if (!empty($product->upgrade_size)) : ?><li class="upgrade-size"><strong><?php print $product->upgrade_size; ?></strong>
					<span><?php print $product->upgrade_configuration; ?></span></li><?php endif; ?>
			<?php if ($has_large_image) : ?><span class="icon enlarge"></span><?php endif; ?>
		</ul>
	</div>
	
		<div class="header_right_column">
			<div itemscope itemtype="http://schema.org/Product" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
			
			<?php
				/**
				 * woocommerce_show_product_images hook
				 *
				 * @hooked woocommerce_show_product_sale_flash - 10
				 * @hooked woocommerce_show_product_images - 20
				 */
				//do_action( 'woocommerce_before_single_product_summary' );
				do_action('woocommerce_show_product_sale_flash');
			?>
			
			<?php edit_post_link( 'Edit this Product','<div class="edit darken" style="display: inline-block;">','</div>'); ?>
			
			<?php woocommerce_template_single_title(); ?>
			
			<ul class="product-inventory-details">
				<?php if (!empty($product->sku)) : ?><li>SKU: <?php print $product->sku; ?></li><?php endif; ?>
				<?php if (!empty($product->oem_part_number)) : ?><li>OEM Part #: <?php print $product->oem_part_number; ?></li><?php endif; ?>
			</ul>
			
			<div class="the-content"><?php woocommerce_template_single_excerpt(); ?></div>
				
			
				
			<?php woocommerce_template_single_add_to_cart(); ?>
			
			<?php //woocommerce_template_single_sharing(); ?>
			
			</div><!-- #product-<?php the_ID(); ?> -->
		</div><!-- /.header_right_column -->
		
	<!-- Clear Fix --><div class="clear"></div>
</div><!-- /.hero-wrapper -->
<?php do_action( 'woocommerce_after_single_product' ); ?>

</div><!-- End of content wrapper -->

<?php $product->list_attributes(); ?>
<!-- Clear Fix --><div class="clear"></div>
</div><!-- End of header wrapper -->