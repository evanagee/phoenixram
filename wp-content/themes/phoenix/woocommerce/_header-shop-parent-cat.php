<?php
	global $cat, $featured_image, $cat_id, $cat_type;
?>

<!-- Start of header wrapper -->
<div id="header_wrapper">

<!-- Start of content wrapper -->
<div class="content_wrapper <?php if ($cat_type == 'host' || $cat_type == 'parent') { print 'category-'.$cat_type; } ?>">

<?php if (!empty($featured_image)) : ?><div class="hero-img" style="background-image: url('<?php echo $featured_image; ?>');"></div><?php endif; ?>

<div class="header_right_column">
	<!-- Start of logo -->
	<div id="logo">
		<a href="<?php echo site_url(); ?>"><?php 
		if ( function_exists( 'get_option_tree' ) ) {
		$logopath = get_option_tree( 'vn_toplogo' );
		} ?><img src="<?php echo $logopath; ?>" alt="logo" /></a>
	</div><!-- End of logo -->
	
	<?php if( current_user_can( 'administrator' ) && 1==2 ) : ?>
		<button class="edit button"><a href="/wp-admin/edit-tags.php?action=edit&taxonomy=product_cat&tag_ID=<?php print $cat_id; ?>&post_type=product">Edit this Category</a></button>
	<?php endif; ?>
	
	<h1><?php print $cat['banner_title']; ?></h1>
	
	<?php if ($cat['body_content'] !== '') : ?>
		<div class="the-content"><?php print $cat['body_content']; ?></div><!-- /.the-content -->
	<?php endif; ?>
	
	<div class="darken rounded">
		<div class="pad">
		<strong><?php print $cat['banner_text']; ?></strong>
		<?php 
		$args = array(
			'child_of'		=> $cat_id,
			'menu_order'	=> 'ASC',
			'hide_empty'	=> 1,
			'hierarchical'	=> 1,
			'taxonomy'		=> 'product_cat',
			'pad_counts'	=> 1
		);
		$product_categories = get_categories( $args );
		//print_r($product_categories);
		?>
		<h3 class="dropdown-header rounded"><?php print $cat['subcategory_dropdown_default_text']; ?></h3>
		<ul class="dropdown-menu rounded">
			<?php foreach ($product_categories as $c) : 
				$thumbnail_id = get_woocommerce_term_meta( $c->term_id, 'thumbnail_id', true ); 
			    // get the image URL
			    $c->image = wp_get_attachment_image_src( $thumbnail_id, 'medium' );
			    if ($c->parent == $cat_id) :
			?>
				<li style="background-image: url(<?php echo $c->image[0]; ?>);"><a href="<?php print get_term_link( $c->slug, 'product_cat' ); ?>"><?php print $c->name; ?></a></li>
			<?php 
			endif;
			endforeach; ?>
		</ul>
		
		<div class="clearfix"></div>
		</div><!-- /.pad -->
	</div><!-- /.darken -->
	<?php //woocommerce_template_single_sharing(); ?>
</div><!-- /.header_right_column -->

<!-- Clear Fix --><div class="clear"></div>
</div><!-- End of content wrapper -->

<!-- Clear Fix --><div class="clear"></div>
</div><!-- End of header wrapper -->