<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
get_header(); // generic shop header
get_template_part('woocommerce/_header','shop-single-product');
include('_shop-before-wrap.php');

//print_r(get_the_terms( $post->ID, 'product_cat' ));

$product_cats = reset(get_the_terms( $post->ID, 'product_cat' ));
$args = array( 'post_type' => 'product', 'posts_per_page' => 10, 'product_cat' => $product_cats->slug, 'orderby' => 'rand' );
$loop = new WP_Query( $args );
?>
	<?php
		/**
		 * woocommerce_before_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action('woocommerce_output_content_wrapper', 10);
	?>

		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

			<h2 class="page-title">
				<span>Related Products</span>
				Shop more <?php print $product->memory_technology; ?> <?php print $product->speed; ?> <?php print $product_cats->name; ?> Upgrades.
			</h2>

		<?php endif; ?>

		<?php do_action( 'woocommerce_archive_description' ); 
			//get_sidebar('page');
			
		?>

		<?php if ( $loop->have_posts() ) : ?>

			<?php
				/**
				 * woocommerce_before_shop_loop hook
				 *
				 * @hooked woocommerce_result_count - 20
				 * @hooked woocommerce_catalog_ordering - 30
				 */
				do_action( 'woocommerce_before_shop_loop' );
			?>

			<?php woocommerce_product_loop_start(); ?>


				<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

					<?php woocommerce_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; // end of the loop. ?>
				<div class="clear"></div>
			<?php woocommerce_product_loop_end(); ?>

			<?php
				/**
				 * woocommerce_after_shop_loop hook
				 *
				 * @hooked woocommerce_pagination - 10
				 */
				do_action( 'woocommerce_after_shop_loop' );
				/*
echo '<pre>';
				print_r(woocommerce_default_product_tabs());
				echo '</pre>';
*/
			?>

		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

			<?php woocommerce_get_template( 'loop/no-products-found.php' ); ?>

		<?php endif; ?>
		
		<?php 
		if (function_exists( 'get_option_tree')) $display_product_tabs = get_option_tree( 'vn_woocommerce_display_product_tabs' );
		?>
		<?php if (!empty($display_product_tabs)) : ?>
		<?php woocommerce_output_product_data_tabs(); ?>
		<?php endif; ?>
	<?php
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action('woocommerce_after_main_content');
		
		//do_action('woocommerce_sidebar');
	?>

	<div class="clear"></div>
	<?php //echo $product->get_categories( ', ', '<span class="posted_in">' . _n( 'Category:', 'Categories:', sizeof( get_the_terms( $post->ID, 'product_cat' ) ), 'woocommerce' ) . ' ', '.</span>' ); ?>
<?php 
include('_shop-after-wrap.php'); 
?>
