<?php 
global $attributes, $product, $woocommerce, $cat_type;
if (!empty($cat_type)) {
	$cat_class = 'category-'.$cat_type;
}
?>

<?php if (!empty($cat['featured_text'])) :?>
<section id="header_postscript" class="white">
	<div class="content_wrapper">
	<?php print $cat['featured_text']; ?>
	</div>
</section>
<?php endif; ?>
<?php if (is_shop() && function_exists( 'get_option_tree' )) :?>
<section id="header_postscript" class="white">
	<div class="content_wrapper">
	<?php print get_option_tree( 'vn_home_page_featured_text' ); ?>
	</div>
</section>
<?php endif; ?>
<?php if (is_shop()) :?>
<section id="recent_blog_posts">
	<div class="content_wrapper">
	<!-- List most recent blog post -->
	<div class="recent_posts">
		<?php $recentpost = new WP_Query("showposts=3"); while($recentpost->have_posts()) : $recentpost->the_post(); ?>
		<div class="recent_post">
			<div class="post-image"><?php the_post_thumbnail('thumbnail'); ?></div>
			<div class="content">
				<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
				<?php the_excerpt(); ?>
				<a href="<?php the_permalink(); ?>" class="button rounded red">Read More</a>
			</div>
		</div>
		<?php endwhile; ?>
		<div class="clearfix"></div>
	</div>
	
	</div><!-- /.content_wrapper -->
</section>
<?php endif; ?>
<!-- Start of content wrapper -->
<div id="contentwrapper" class="<?php print $cat_class; ?>">

<!-- Start of content wrapper -->
<div class="content_wrapper">