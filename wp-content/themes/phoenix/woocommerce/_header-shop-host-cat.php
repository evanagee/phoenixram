<?php
	global $cat, $cats, $cat_id, $featured_image, $cat_type;
/*	
	echo '<pre>';
	print_r($cat);
	echo '</pre>';
*/

?>
<!-- Start of header wrapper -->
<div id="header_wrapper">

<!-- Start of content wrapper -->
<div class="content_wrapper <?php if ($cat_type == 'host' || $cat_type == 'parent') { print 'category-'.$cat_type; } ?>">

<div id="logo">
		<a href="<?php echo site_url(); ?>"><?php 
		if ( function_exists( 'get_option_tree' ) ) {
		$logopath = get_option_tree( 'vn_toplogo' );
		} ?><img src="<?php echo $logopath; ?>" alt="logo" /></a>
	</div><!-- End of logo -->

<h1><?php print $cat['banner_title']; ?></h1>

<div class="hero-wrapper">
	<?php if (!empty($featured_image)) : ?><div class="hero-img" style="background-image: url('<?php echo $featured_image; ?>');"></div><?php endif; ?>
	
	<div class="header_right_column">
	<!-- Start of logo -->
	<?php if( current_user_can( 'administrator' ) && 1==2) : ?>
		<div class="edit darken" style="display: inline-block;"><a class="post-edit-link" href="/wp-admin/edit-tags.php?action=edit&taxonomy=product_cat&tag_ID=<?php print $cat_id; ?>&post_type=product">Edit this Category</a></div>
	<?php endif; ?>
	
	<?php if ($cat['body_content'] !== '') : ?>
		<div class="the-content"><?php print $cat['body_content']; ?></div><!-- /.the-content -->
	<?php endif; ?>
	<div class="spec-list-wrapper">
		<ul class="spec-list">
			<?php if(!empty($cat["max_memory_capacity"])) : ?>
			<li class="icon text">
				<h4><?php print $cat["max_memory_capacity"]; ?></h4>
				MAX Memory Capacity
			</li>
			<?php endif; ?>
			<?php if(!empty($cat["memory_technology"])) : ?>
			<li class="icon info">
				<h4><?php print $cat["memory_technology"]; ?></h4>
				Memory Technology
			</li>
			<?php endif; ?>
			<?php if(!empty($cat["memory_speed"])) : ?>
			<li class="icon clock2">
				<h4><?php print $cat["memory_speed"]; ?></h4>
				Memory Speed
			</li>
			<?php endif; ?>
			<?php if(!empty($cat["dimm_type"])) : ?>
			<li class="icon tools">
				<h4><?php print $cat["dimm_type"]; ?></h4>
				DIMM Type
			</li>
			<?php endif; ?>
			<?php if(!empty($cat["pin_count"])) : ?>
			<li class="icon grid">
				<h4><?php print $cat["pin_count"]; ?></h4>
				PIN Count
			</li>
			<?php endif; ?>
			<?php if(!empty($cat["error_correction"])) : ?>
			<li class="icon iphone">
				<h4><?php print $cat["error_correction"]; ?></h4>
				Error Correction
			</li>
			<?php endif; ?>
			<div class="clear"></div>
		</ul>
	</div>
	
	<div class="scroll_to_continue">
		<strong>You're getting warmer, <span>peek below...</span></strong>
	</div>
	
	<?php //woocommerce_template_single_sharing(); ?>
</div><!-- /.header_right_column -->
	
<div class="clear"></div>
</div><!-- /.hero-wrapper -->

</div><!-- End of content wrapper -->

<div class="clear"></div>
</div><!-- End of header wrapper -->