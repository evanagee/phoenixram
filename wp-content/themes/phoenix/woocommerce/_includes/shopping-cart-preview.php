<?php
	// INCLUDE: Used to display a small shopping cart preview
	global $woocommerce;
?>
<div id="shopping-cart-preview" style="">
	<h4 class="icon icon-stack">Your Shopping Cart</h4>
			<?php if (count($woocommerce->cart->cart_contents) >= 1) : ?>
			<ul class="product-list">
			<?php foreach ($woocommerce->cart->cart_contents as $cart_item_key => $cart_item) :
				$i++;
				if ( $i == 1 ) :				
					$rowclass = ' class="cart_oddrow rounded"';			
				else :
					$rowclass = ' class="cart_evenrow rounded"';
					$i = 0;
				endif;
		
				$_product = $cart_item['data'];
				
				if ($_product->exists() && $cart_item['quantity']>0) :
					echo '<li'.$rowclass.'>';
					
					echo '<div class="dropdowncartimage">';
					echo '<a href="'.get_permalink($cart_item['product_id']).'">';				
					if (has_post_thumbnail($cart_item['product_id'])) :					
						echo get_the_post_thumbnail($cart_item['product_id'], 'shop_thumbnail'); 
					else :					 
						echo '<img src="'.$woocommerce->plugin_url(). '/assets/images/placeholder.png" alt="Placeholder" width="'.$woocommerce->get_image_size('shop_thumbnail_image_width').'" height="'.$woocommerce->get_image_size('shop_thumbnail_image_height').'" />'; 				
					endif;				
					echo '</a>';
					echo '</div>';
					
					echo '<div class="dropdowncartproduct">';
					echo '<a href="'.get_permalink($cart_item['product_id']).'">';				
					echo apply_filters('woocommerce_cart_widget_product_title', $_product->get_title(), $_product).'</a>';				
					if ($_product instanceof woocommerce_product_variation && is_array($cart_item['variation'])) :
	        			echo woocommerce_get_formatted_variation( $cart_item['variation'] );
	   				endif;
					echo '</a>';
					echo '</div>';
					echo '<div class="clearfix"></div>';
					
					echo '<div class="dropdowncartremove">';	
					echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf('<a href="%s" class="remove" title="%s">&times;</a>', esc_url( $woocommerce->cart->get_remove_url( $cart_item_key ) ), __( 'Remove this item', 'woocommerce' ) ), $cart_item_key );
					echo '</div>';
					
					echo '<div class="dropdowncartquantity">';				
					echo '<span class="quantity">' .$cart_item['quantity'].' &times; '.woocommerce_price($_product->get_price()).'</span>';
					echo '</div>';
					echo '<div class="clearfix"></div>';
					
					echo '</li>';
					
				endif;
			endforeach; ?>
			</ul>
			<?php
			echo '<div class="buttons">
			  		<a class="button rounded" href="'.$woocommerce->cart->get_cart_url().'">'.__('View Cart', 'woothemes').'</a> 
			  		<a class="button rounded checkout" href="'.$woocommerce->cart->get_checkout_url().'">'.__('Checkout &rarr;', 'woothemes').'</a>
			  		<div class="clear"></div>
			  	  </div>';
			?>
			<?php else: // No cart items?>
			<div class="no-cart-items">
				<strong>No items in your cart</strong>
			</div>
			<?php endif; ?>
		</div>