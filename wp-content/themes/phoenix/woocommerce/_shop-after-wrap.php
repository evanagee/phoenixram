</div><!-- /.content_wrapper -->
<?php if (is_shop() && 1==2) : ?>
<section id="phone_feature" class="parallax"><div id="phone_feature_img" style="background-image: url(<?php bloginfo('stylesheet_directory'); ?>/img/hands-with-phone.png);"></section>
<?php endif; ?>

<?php if (($cat_type == 'host' || $cat_type == 'parent') && !empty($cat['lead_generation_1_headline'])) : ?>
<section id="cta1">
	<div class="content_wrapper">
		<aside class="rounded">
			<ul class="feature-list">
				<?php if (!empty($cat['feature_item_1_title'])) :?>
				<li class="icon circle <?php print $cat['feature_item_1_icon']; ?>">
					<h4><?php print $cat['feature_item_1_title']; ?></h4>
					<?php print $cat['feature_item_1_text']; ?>
				</li>
				<?php endif; ?>
				<?php if (!empty($cat['feature_item_2_title'])) :?>
				<li class="icon circle <?php print $cat['feature_item_2_icon']; ?>">
					<h4><?php print $cat['feature_item_2_title']; ?></h4>
					<?php print $cat['feature_item_2_text']; ?>
				</li>
				<?php endif; ?>
				<?php if (!empty($cat['feature_item_3_title'])) :?>
				<li class="icon circle <?php print $cat['feature_item_3_icon']; ?>">
					<h4><?php print $cat['feature_item_3_title']; ?></h4>
					<?php print $cat['feature_item_3_text']; ?>
				</li>
				<?php endif; ?>
			</ul>
		</aside>
		<div class="content">
			<?php if (!empty($cat['lead_generation_1_headline'])) :?><h2><?php print $cat['lead_generation_1_headline']; ?></h2><?php endif; ?>
			<?php if (!empty($cat['lead_generation_1_sub_headline'])) :?><h3><?php print $cat['lead_generation_1_sub_headline']; ?></h3><?php endif; ?>
			<?php if (!empty($cat['lead_generation_1_body'])) :?><p><?php print $cat['lead_generation_1_body']; ?></p><?php endif; ?>
			<?php if (!empty($cat['lead_generation_1_button_label'])) :?><a href="#" class="button rounded red"><?php print $cat['lead_generation_1_button_label']; ?></a><?php endif; ?>
		</div>
		<div class="clear"></div>
	</div><!-- /.content_wrapper -->
</section>
<?php endif; ?>

<?php 
	if ( function_exists( 'ot_get_option') ) {
		$cta = new stdClass;
		$cta->title = ot_get_option( 'vn_call_to_action_title' );
		$cta->subtitle = ot_get_option( 'vn_call_to_action_subtitle' );
		$cta->content = ot_get_option( 'vn_call_to_action_content' );
		$cta->button_label = ot_get_option( 'vn_call_to_action_button_label' );
		$cta->button_target = get_permalink(ot_get_option( 'vn_call_to_action_button_target' ));
	}
?>
<?php if (!empty($cta->title)) :?>
<section id="cta2">
	<div class="content_wrapper">
		
		<img src="<?php bloginfo('stylesheet_directory'); ?>/img/ipad-logo.png" />
		<div class="content">
			<?php if (!empty($cta->title)) :?><h2><?php print $cta->title; ?></h2><?php endif; ?>
			<?php if (!empty($cta->subtitle)) :?><h3><?php print $cta->subtitle; ?></h3><?php endif; ?>
			<?php if (!empty($cta->content)) :?><p><?php print $cta->content; ?></p><?php endif; ?>
			<?php if (!empty($cta->button_target)) :?><a href="<?php print $cta->button_target; ?>" class="button rounded red"><?php print $cta->button_label; ?></a><?php endif; ?>
		</div>
		<div class="clear"></div>
	</div><!-- /.content_wrapper -->
</section>
<?php endif; ?>

<!-- Clear Fix --><div class="clear"></div>

<?php get_footer(); ?>