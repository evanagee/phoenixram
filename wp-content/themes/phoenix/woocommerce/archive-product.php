<?php
/**
 * The Template for displaying TOP LEVEL CATEGORIES (A)
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $cat, $cats, $cat_id, $featured_image, $cat_type;

// verify that this is a product category page
if (is_product_category()){
    global $wp_query, $cat;
    // get the query object
    $cat = $wp_query->get_queried_object();
    // get the thumbnail id user the term_id
    $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true ); 
    // get the image URL
    $featured_image = wp_get_attachment_url( $thumbnail_id ); 
    
    //print_r($cat);
    
    //$cat_id = get_term_by('slug',"'".$cat->slug."'",'product_cat');
    $cat_id = $cat->term_id;
    
    // Check to see if this category has products
	$postsInCat = get_term_by('id', $cat_id, 'product_cat');
	$postsInCat = $postsInCat->count;
	
	if (is_shop()) {
		$cat_type = NULL;
	} else if ($postsInCat > 0) {
		$cat_type = 'host';
	} else {
		$cat_type = 'parent';
	}

	$params = array( 'where' => 'id = "'.$cat_id.'"' );
	$cats = pods( 'product_cat', $params );
	$cat = $cats->fetch();
}

get_header(); // generic shop header
if ($cat_type == 'parent') {
	get_template_part('woocommerce/_header','shop-parent-cat'); // parent specific header
} else if ($cat_type == 'host') {
	get_template_part('woocommerce/_header','shop-host-cat'); // host specific header
} else if (is_shop()) {
	get_template_part('woocommerce/_header','shop-index'); // shop index page
}
include('_shop-before-wrap.php'); 
echo '<!-- TPL_PATH: '.__FILE__.' -->';
?>
	<?php
		/**
		 * woocommerce_before_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		//do_action('woocommerce_before_main_content');
	?>

		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

<script type="text/javascript">
 jQuery(window).load(function() {
       jQuery('.bxslider').show().bxSlider({
  		mode: 'horizontal',
  		captions: true,
  		controls: false,
   		auto: true,
  		preloadImages: 'all'
});
jQuery('.textwidget').replaceWith($('.textwidget').contents());
$('.removeh2').remove();
    });
    </script>
    
<div class="slider-bxwrapper">
  <?php if (	!dynamic_sidebar('Slider Parent') ) : ?>
  <?php endif; ?>
</div>

		<?php endif; ?>

		<?php //do_action( 'woocommerce_archive_description' ); ?>
		

		<?php if ( have_posts() ) : ?>
			
			<?php if ($cat['sub_product_category_headline'] !== '' || $cat['sub_product_category_sub_headline'] !== '') :?>
				<h2><span><?php print $cat['sub_product_category_headline']; ?></span> <?php print $cat['sub_product_category_sub_headline']; ?></h2>
			<?php endif; ?>
			
			<?php if (is_shop()) : // shop index ?>
				<?php if ( function_exists( 'get_option_tree') ) :
						$vn_home_page_product_category_headline = get_option_tree( 'vn_home_page_product_category_headline' );
				?>
				<h2 class="large"><?php print $vn_home_page_product_category_headline; ?></h2>
				<?php endif; ?>
			<?php endif; ?>
			
			<?php
				/**
				 * woocommerce_before_shop_loop hook
				 *
				 * @hooked woocommerce_result_count - 20
				 * @hooked woocommerce_catalog_ordering - 30
				 */
				do_action( 'woocommerce_before_shop_loop' );
				//do_action('woocommerce_sidebar');
				
			?>

			<?php woocommerce_product_loop_start(); ?>

				<?php woocommerce_product_subcategories(); ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php woocommerce_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; // end of the loop. ?>
				
				
			<?php woocommerce_product_loop_end(); ?>
			
			<?php if (is_active_sidebar('page') && $cat_type == 'host') : ?>
			<aside class="product-filters sidebar right">
				<?php get_sidebar('page'); ?>
				<div class="clearfix"></div>
			</aside>
			<?php endif; ?>
			
			<div class="clearfix"></div>
			
			<?php
				/**
				 * woocommerce_after_shop_loop hook
				 *
				 * @hooked woocommerce_pagination - 10
				 */
				do_action( 'woocommerce_after_shop_loop' );
			?>

		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

			<?php woocommerce_get_template( 'loop/no-products-found.php' ); ?>

		<?php endif; ?>

	<?php
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action('woocommerce_after_main_content');
	?>

	<?php
		/**
		 * woocommerce_sidebar hook
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		//do_action('woocommerce_sidebar');
	?>

<?php if (is_active_sidebar('page')) : ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		jQuery("ul.products").addClass('with-sidebar');
	});
</script>
<?php endif; ?>
<?php 
include('_shop-after-wrap.php'); 
get_footer('shop'); ?>