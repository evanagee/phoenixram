<?php
/**
 * Product attributes
 *
 * Used by list_attributes() in the products class
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.8
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woocommerce;

$alt = 1;
$attributes = $product->get_attributes();

if ( empty( $attributes ) && ( ! $product->enable_dimensions_display() || ( ! $product->has_dimensions() && ! $product->has_weight() ) ) ) return;
?>

<div class="product_attributes">
<div class="content_wrapper" style="height: auto;">
<ul>
	<?php if ( $product->enable_dimensions_display() ) : ?>

		<?php if ( $product->has_weight() ) : ?>

			<li class="<?php if ( ( $alt = $alt * -1 ) == 1 ) echo 'alt'; ?>">
				
				<strong><?php echo $product->get_weight() . ' ' . esc_attr( get_option('woocommerce_weight_unit') ); ?></strong>
				<span><?php _e( 'Weight', 'woocommerce' ) ?></span>
			</li>

		<?php endif; ?>

		<?php if ( $product->has_dimensions() ) : ?>

			<li class="<?php if ( ( $alt = $alt * -1 ) == 1 ) echo 'alt'; ?>">
				<strong class="product_dimensions"><?php echo $product->get_dimensions(); ?></strong>
				<span><?php _e( 'Dimensions', 'woocommerce' ) ?></span>
			</li>

		<?php endif; ?>

	<?php endif; ?>

	<?php foreach ( $attributes as $attribute ) :
		if ( empty( $attribute['is_visible'] ) || ( $attribute['is_taxonomy'] && ! taxonomy_exists( $attribute['name'] ) ) ) continue; ?>

		<li class="<?php if ( ( $alt = $alt * -1 ) == 1 ) echo 'alt'; ?> rounded darken1 attribute">
		<strong><?php
			if ( $attribute['is_taxonomy'] ) {

				$values = woocommerce_get_product_terms( $product->id, $attribute['name'], 'names' );
				echo apply_filters( 'woocommerce_attribute', implode( ', ', $values ), $attribute, $values );

			} else {

				// Convert pipes to commas and display values
				$values = array_map( 'trim', explode( '|', $attribute['value'] ) );
				echo apply_filters( 'woocommerce_attribute', wpautop( wptexturize( implode( ', ', $values ) ) ), $attribute, $values );

			}
		?></strong>
		<span><?php echo $woocommerce->attribute_label( $attribute['name'] ); ?></span>
		
	</li>

	<?php endforeach; ?>
	<div class="clearfix"></div>
</ul>
</div><!-- /.product_attributes -->
</div>