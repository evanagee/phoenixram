<?php
/**
 * Single Product title
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
$title = get_the_title();
if (strpos($title, ' PC')) {
	$title = '<span>'.str_replace(' PC', '</span> PC', $title);
}
?>
<h1 itemprop="name" class="product_title entry-title"><?php print $title; ?></h1>