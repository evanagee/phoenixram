<?php
	global $post;
?>
<!-- Start of header wrapper -->
<div id="header_wrapper">

<!-- Start of content wrapper -->
<div class="content_wrapper shop-index">

<div class="hero-img" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/modules-fire.png');"></div>
<div class="header_right_column">
	<!-- Start of logo -->
	<div id="logo">
		<a href="<?php echo site_url(); ?>"><?php 
		if ( function_exists( 'get_option_tree' ) ) {
		$logopath = get_option_tree( 'vn_toplogo' );
		} ?><img src="<?php echo $logopath; ?>" alt="logo" /></a>
	</div><!-- End of logo -->
	
	<?php if ( function_exists( 'get_option_tree' ) ) : ?>
	<h1> <?php print get_option_tree( 'vn_home_page_banner_title' ); ?></h1>
	
	<div class="the_content">
		<?php print get_option_tree( 'vn_home_page_banner_text' ); ?>
	</div>
	<?php endif; ?>
	<div class="darken rounded">
		<div class="pad">
		
		<?php if ( function_exists( 'get_option_tree' ) ) : ?>
		<strong><?php print strip_tags(get_option_tree( 'vn_home_page_dropdown_action_text_1' )); ?></strong>
		<?php endif; ?>
		
		<strong></strong>
		<?php 
		$args = array(
			'parent'		=> 0,
			'orderby'	=> 'name',
			'hide_empty'	=> 0,
			'hierarchical'	=> 0,
			'taxonomy'		=> 'product_cat',
			'pad_counts'	=> 1
		);
		$product_categories = get_categories( $args );
		//print_r($product_categories);
		?>
		
		<h3 class="dropdown-header rounded">Browse All Categories</h3>
		<ul class="dropdown-menu rounded">
			<?php 
				if (count($product_categories) > 0) :
				foreach ($product_categories as $c) : 
				$thumbnail_id = get_woocommerce_term_meta( $c->term_id, 'thumbnail_id', true ); 
			    // get the image URL
			    $c->image = wp_get_attachment_image_src( $thumbnail_id, 'medium' );
			    
			?>
				<li style="background-image: url(<?php echo $c->image[0]; ?>);"><a href="<?php print get_term_link( $c->slug, 'product_cat' ); ?>"><?php print $c->name; ?></a></li>
			<?php endforeach; endif; ?>
		</ul>
		<div class="clearfix"></div>
		
		<?php if ( function_exists( 'get_option_tree' ) ) : ?>
		<div><?php print get_option_tree( 'vn_home_page_dropdown_action_text_2' ); ?></div>
		<?php endif; ?>
		
		</div><!-- /.pad -->
	</div><!-- /.darken -->
	
	<?php if ( function_exists( 'get_option_tree' ) ) : ?>
	<div class="the_content" style="margin-top:15px;">
		<?php print get_option_tree( 'vn_home_page_banner_text_2' ); ?>
	</div>
	<?php endif; ?>

</div><!-- /.header_right_column -->
<!-- Clear Fix --><div class="clear"></div>
<!-- Clear Fix --><div class="clear"></div>

</div><!-- End of content wrapper -->
<!-- Clear Fix --><div class="clear"></div>
</div><!-- End of header wrapper -->