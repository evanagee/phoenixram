<?php  
/* 
Template Name: Homepage-DynamicWithSlider
*/  
?>

<?php get_header(); ?>
<?php //global $woocommerce; ?>


<section class="slider_wrapper">

<!-- Start of slider -->
<section class="slider">

	<ul class="slides">
	
		<?php
		$my_query = null;
		$my_query = new WP_Query('post_type=slider&showposts=10');
		$my_query->query('post_type=slider&showposts=10');
		?>
		
		<?php while ( $my_query->have_posts() ) : $my_query->the_post(); ?> 
		<li>
			<?php the_content(); ?>
		</li>
		
		<?php endwhile; ?>
	
	</ul>
	
	<?php wp_reset_query(); ?>
    
</section><!-- /.slider -->

<div class="clear"></div>

</section><!-- /.slider_wrapper -->

</div><!-- End of content wrapper -->

<div class="clear"></div>

</div><!-- End of header wrapper -->

<!-- Start of message wrapper -->
<div id="message_wrapper">

<!-- Start of content wrapper -->
<div class="content_wrapper">

<?php 
if ( function_exists( 'get_option_tree' ) ) {
$homepagemessageleft = get_option_tree( 'vn_homepagemessageleft' );
} ?>

<?php if ($homepagemessageleft != ('')){ ?>

<!-- Start of contentleft -->
<div class="contentleft">
<p><?php echo stripslashes($homepagemessageleft); ?></p>

</div><!-- End of contentleft -->

<?php } ?>

<?php 
if ( function_exists( 'get_option_tree' ) ) {
$homepagemessageright = get_option_tree( 'vn_homepagemessageright' );
} ?>

<?php if ($homepagemessageright != ('')){ ?>

	<!-- Start of contentright -->
	<div class="contentright">
	<?php echo $homepagemessageright; ?>
	
	</div><!-- End of contentright -->

<?php } ?>

</div><!-- End of content wrapper -->

<div class="clear"></div>

</div><!-- End of message wrapper -->


<div id="contentwrapper">
	<div class="content_wrapper">
		<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Header Widgets')) : ?>
		
		<?php endif; ?>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
		<?php the_content('        '); ?> 
		
		<?php endwhile; endif; ?>
		
		<?php wp_reset_query(); ?>
		
		<div class="clear"></div>
		
		<hr />
		
	</div><!-- /.content_wrapper -->

	<div class="clear"></div>
</div><!-- /#contentwrapper -->

<?php get_footer(); ?>