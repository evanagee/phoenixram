<?php
/**
 * Initialize the options before anything else.
 */
add_action( 'admin_init', 'custom_theme_options', 1 );

/**
 * Build the custom settings & update OptionTree.
 */
function custom_theme_options() {
  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( 'option_tree_settings', array() );
  
  /**
   * Custom settings array that will eventually be 
   * passes to the OptionTree Settings API Class.
   */
  $custom_settings = array( 
    'contextual_help' => array(
      'sidebar'       => '<p>Sidebar content goes here!</p>'
    ),
    'sections'        => array( 
      array(
        'id'          => 'setup',
        'title'       => __( 'Global Settings', 'essentials' )
      ),
      array(
        'id'          => 'home_page_settings',
        'title'       => __( 'Home Page Settings', 'essentials' )
      ),
      array(
        'id'          => 'footer_settings',
        'title'       => __( 'Footer Settings', 'essentials' )
      ),
      array(
        'id'          => 'advanced_settings',
        'title'       => __( 'Advanced Settings', 'essentials' )
      ),
      array(
        'id'          => 'woocommerce_settings',
        'title'       => __( 'Woocommerce Settings', 'essentials' )
      )
    ),
    'settings'        => array( 
      array(
        'id'          => 'vn_toplogo',
        'label'       => __( 'Top Logo', 'essentials' ),
        'desc'        => __( 'Upload your logo.', 'essentials' ),
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'setup',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'vn_favicon',
        'label'       => __( 'Favicon', 'essentials' ),
        'desc'        => __( 'Upload your favicon.  16px X 16px .png.', 'essentials' ),
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'advanced_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'vn_specstyle',
        'label'       => __( 'Upload Special Stylesheet', 'essentials' ),
        'desc'        => __( 'Upload the color selected stylesheet.', 'essentials' ),
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'advanced_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'vn_customcss',
        'label'       => __( 'Custom CSS', 'essentials' ),
        'desc'        => __( 'Use this area to over ride any CSS from the stylesheet with custom CSS.', 'essentials' ),
        'std'         => '',
        'type'        => 'css',
        'section'     => 'advanced_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'vn_tracking',
        'label'       => __( 'Tracing Code', 'essentials' ),
        'desc'        => __( 'Enter your tracking code script here that will be injected into every page for better analytics.', 'essentials' ),
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'setup',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'vn_call_to_action_title',
        'label'       => __( 'Call to Action Headline', 'essentials' ),
        'desc'        => __( 'Enter the title for the call to action here.', 'essentials' ),
        'std'         => __( 'Looking for a custom quote?', 'essentials' ),
        'type'        => 'text',
        'section'     => 'footer_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'vn_call_to_action_subtitle',
        'label'       => __( 'Call to Action Sub-Headline', 'essentials' ),
        'desc'        => __( 'Enter the sub-title for the call to action here.', 'essentials' ),
        'std'         => __( 'Get in touch with us today!', 'essentials' ),
        'type'        => 'text',
        'section'     => 'footer_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'vn_call_to_action_content',
        'label'       => __( 'Call to Action Body', 'essentials' ),
        'desc'        => __( 'Enter the body for the call to action here.', 'essentials' ),
        'std'         => '',
        'type'        => 'textarea',
        'section'     => 'footer_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'vn_call_to_action_button_label',
        'label'       => __( 'Call to Action Button Label', 'essentials' ),
        'desc'        => __( 'Enter the button text for the call to action here.', 'essentials' ),
        'std'         => '',
        'type'        => 'text',
        'section'     => 'footer_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'vn_call_to_action_button_label',
        'label'       => __( 'Call to Action Button Label', 'essentials' ),
        'desc'        => __( 'Enter the button text for the call to action here.', 'essentials' ),
        'std'         => '',
        'type'        => 'text',
        'section'     => 'footer_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'vn_call_to_action_button_target',
        'label'       => __( 'Call to Action Button Target', 'essentials' ),
        'desc'        => __( 'What page should this call to action link to?', 'essentials' ),
        'std'         => '',
        'type'        => 'page-select',
        'section'     => 'footer_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'vn_copyright',
        'label'       => __( 'Copyright', 'essentials' ),
        'desc'        => __( 'Enter your copyright information here.  HTML such as links etc is acceptable.', 'essentials' ),
        'std'         => __( '2013 Jonathan Atkinson - Handcrafted in the U.S.A.', 'essentials' ),
        'type'        => 'textarea-simple',
        'section'     => 'footer_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'vn_home_page_banner_title',
        'label'       => __( 'Homepage Banner Title', 'essentials' ),
        'desc'        => __( 'Enter your banner title for the home page.  HTML is acceptable here.  Leave this blank to disable.', 'essentials' ),
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'home_page_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'vn_home_page_banner_text',
        'label'       => __( 'Homepage Banner Text', 'essentials' ),
        'desc'        => __( 'Enter your banner text for the home page.  HTML is acceptable here.  Leave this blank to disable.', 'essentials' ),
        'std'         => '',
        'type'        => 'textarea',
        'section'     => 'home_page_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'vn_home_page_banner_text_2',
        'label'       => __( 'Homepage Banner Text 2', 'essentials' ),
        'desc'        => __( 'Enter your banner text for the home page.  HTML is acceptable here.  Leave this blank to disable.', 'essentials' ),
        'std'         => '',
        'type'        => 'textarea',
        'section'     => 'home_page_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'vn_home_page_featured_text',
        'label'       => __( 'Homepage Featured Text/Quote', 'essentials' ),
        'desc'        => __( 'Enter your featured text for the home page.  HTML is acceptable here.  Leave this blank to disable.', 'essentials' ),
        'std'         => '',
        'type'        => 'textarea',
        'section'     => 'home_page_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
       array(
        'id'          => 'vn_home_page_dropdown_action_text_1',
        'label'       => __( 'Dropdown Action Text', 'essentials' ),
        'desc'        => __( 'This text will be displayed above the dropdown list.', 'essentials' ),
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'home_page_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
       array(
        'id'          => 'vn_home_page_dropdown_action_text_2',
        'label'       => __( 'Dropdown Action Text 2', 'essentials' ),
        'desc'        => __( 'This text will be displayed below the dropdown list.', 'essentials' ),
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'home_page_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),

      array(
        'id'          => 'vn_homepagemessageleft',
        'label'       => __( 'Homepage Message Left Column', 'essentials' ),
        'desc'        => __( 'Enter your message area here that will appear under the slider.  HTML is acceptable here.  Leave this blank to disable.', 'essentials' ),
        'std'         => __( 'Sign-up today for a FREE instant account with no monthly commitment!', 'essentials' ),
        'type'        => 'textarea-simple',
        'section'     => 'home_page_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'vn_homepagemessageright',
        'label'       => __( 'Homepage Message Right Column', 'essentials' ),
        'desc'        => __( 'Enter your message area here that will appear under the slider.  HTML is acceptable here.  Leave this blank to disable.', 'essentials' ),
        'std'         => __( '<div class="button_green_image"> <a href="#">No Credit Card Required</a> </div>', 'essentials' ),
        'type'        => 'textarea-simple',
        'section'     => 'home_page_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'vn_telephonenumber',
        'label'       => __( 'Telephone Number', 'essentials' ),
        'desc'        => __( 'Enter your phone number here that will appear in the menu bar on every page.  Leave this blank to disable.', 'essentials' ),
        'std'         => '0800 123 456 7890',
        'type'        => 'text',
        'section'     => 'setup',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'vn_homepageleftcolumntitle',
        'label'       => __( 'Homepage Left Column Title', 'essentials' ),
        'desc'        => __( 'Enter the title for the left column title if you have chosen the Homepage-Dynamic page template for your homepage.', 'essentials' ),
        'std'         => __( 'Our clients', 'essentials' ),
        'type'        => 'text',
        'section'     => 'home_page_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'vn_homepageleftcolumnlinktext',
        'label'       => __( 'Homepage Left Column Link Text', 'essentials' ),
        'desc'        => __( 'Enter some text here that will link through to wherever you would like.  Leaving this field blank will disable this feature.', 'essentials' ),
        'std'         => __( 'View all', 'essentials' ),
        'type'        => 'text',
        'section'     => 'home_page_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'vn_homepageleftcolumnlink',
        'label'       => __( 'Homepage Left Column Link', 'essentials' ),
        'desc'        => __( 'Enter a url here for the above text to point to.  If you left the above field blank, the link feature will be disabled.', 'essentials' ),
        'std'         => '#',
        'type'        => 'text',
        'section'     => 'home_page_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'vn_homepagerightcolumntitle',
        'label'       => __( 'Homepage Right Column Title', 'essentials' ),
        'desc'        => __( 'Enter the title for the left column title if you have chosen the Homepage-Dynamic page template for your homepage.', 'essentials' ),
        'std'         => __( 'Latest blog', 'essentials' ),
        'type'        => 'text',
        'section'     => 'home_page_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'vn_homepagerightcolumnlinktext',
        'label'       => __( 'Homepage Right Column Link Text', 'essentials' ),
        'desc'        => __( 'Enter some text here that will link through to wherever you would like.  Leaving this field blank will disable this feature.', 'essentials' ),
        'std'         => __( 'View all', 'essentials' ),
        'type'        => 'text',
        'section'     => 'home_page_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      
      array(
        'id'          => 'vn_home_page_product_category_headline',
        'label'       => __( 'Home Page Product Category Headline', 'essentials' ),
        'desc'        => __( 'A headline that will be displayed above the list of parent categories on home page.', 'essentials' ),
        'std'         => '',
        'type'        => 'text',
        'section'     => 'home_page_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      
      array(
        'id'          => 'vn_homepagerightcolumnlink',
        'label'       => __( 'Homepage Right Column Link', 'essentials' ),
        'desc'        => __( 'Enter a url here for the above text to point to.  If you left the above field blank, the link feature will be disabled.', 'essentials' ),
        'std'         => '#',
        'type'        => 'text',
        'section'     => 'home_page_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'vn_header_sales_pitch',
        'label'       => __( 'Header Sales Pitch', 'essentials' ),
        'desc'        => __( 'A promotional statement about the store', 'essentials' ),
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'setup',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'vn_woocommerce_display_product_tabs',
        'label'       => 'Display Product Tabs',
        'type'        => 'checkbox',
        'desc'        => 'Display set of tabs on product pages?',
        'choices'     => array(
          array (
            'label'       => 'Yes',
            'value'       => 'Yes'
          )
        ),
        'std'         => '#',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'section'     => 'woocommerce_settings'
      ),
    )
  );
  
  /* allow settings to be filtered before saving */
  $custom_settings = apply_filters( 'option_tree_settings_args', $custom_settings );
  
  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( 'option_tree_settings', $custom_settings ); 
  }
  
}