<!-- tpl:page.php -->
<?php get_header('shop'); ?>
<div id="header_wrapper">

	<div class="content_wrapper">
	
	<div id="logo">
		<a href="<?php echo site_url(); ?>"><?php 
		if ( function_exists( 'get_option_tree' ) ) {
		$logopath = get_option_tree( 'vn_toplogo' );
		} ?><img src="<?php echo $logopath; ?>" alt="logo" /></a>
	</div><!-- End of logo -->

	</div> <!-- /.content_wrapper -->
</div><!-- /#header_wrapper -->

<!-- Start of content wrapper -->
<div id="contentwrapper">

<!-- Start of content wrapper -->
<div class="content_wrapper">

<!-- Start of left content -->
<div class="left_content">
<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

<?php 
if ( has_post_thumbnail() ) {  ?>

<?php the_post_thumbnail('slide'); ?>

<?php } ?>
 
<?php the_content('        '); ?> 

<?php endwhile; ?> 

<?php else: ?> 
<p><?php _e( 'There are no posts to display. Try using the search.', 'essentials' ); ?></p> 

<?php endif; ?>

</div><!-- End of left content -->

<?php if (is_active_sidebar('page') && $cat_type == 'host') : ?>
<aside class="sidebar right">
	<?php get_sidebar('page'); ?>
	<div class="clearfix"></div>
</aside>
<?php endif; ?>

</div><!-- End of content wrapper -->

<!-- Clear Fix --><div class="clear"></div>

</div><!-- End of content wrapper -->

<?php get_footer(); ?>