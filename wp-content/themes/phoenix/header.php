<?php
/*
 *
 * A custom header for all category pages within the shop.
 *
 */
 global $cat_type, $post, $woocommerce, $product, $cat;
 if (!empty($post->ID)) $product = get_product( $post->ID );
 $alt = 1;
 if (is_object($product)) {
 	$attributes = $product->get_attributes();
 	if ($attributes["pa_breadcrumb"]["name"]) {
 		$breadcrumb = product_custom_breadcrumb($product, $attributes["pa_breadcrumb"]["name"]);
 	}
 }
 //print_r($post);
?>
<!DOCTYPE HTML>

<html <?php language_attributes(); ?>>

<!--[if IE 7 ]>    <html class= "ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class= "ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class= "ie9"> <![endif]-->

<!--[if lt IE 9]>
   <script>
      document.createElement('header');
      document.createElement('nav');
      document.createElement('section');
      document.createElement('article');
      document.createElement('aside');
      document.createElement('footer');
   </script>
<![endif]-->


<title><?php if (empty($cat["custom_page_title"])) { wp_title(); } else { print $cat["custom_page_title"]; } ?></title>
<?php if (!empty($cat["custom_meta_description"])) : ?>
	<meta name="description" content="<?php print $cat["custom_meta_description"]; ?>" />
<?php endif; ?>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="format-detection" content="telephone=no">
<!--<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"> -->
<meta name="viewport" content="width=device-width,maximum-scale=1,initial-scale=1,user-scalable=no"> 
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon.png" />
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript" src="http://s.sharethis.com/loader.js"></script>
<?php 
if ( function_exists( 'get_option_tree') ) {
  $specstyle = get_option_tree( 'vn_specstyle' );
  }
?>
<?php if ($specstyle != ('')){ ?>

<link href="<?php echo ($specstyle); ?>" rel="stylesheet" type="text/css" media="screen" />

<?php } else { ?>

<link href="<?php bloginfo('stylesheet_url') ?>" rel="stylesheet" type="text/css" media="screen" />

<?php } ?>

<?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

 <!-- *************************************************************************
*****************                FAVICON               ********************
************************************************************************** -->

<?php 
if ( function_exists( 'get_option_tree') ) {
  $favicon = get_option_tree( 'vn_favicon' );
}
?>
<link rel="shortcut icon" href="<?php echo ($favicon); ?>" />

<!--[if lt IE 9]>
<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- *************************************************************************
*****************              CUSTOM CSS              ********************
************************************************************************** -->


<style type="text/css">
<?php 
if ( function_exists( 'get_option_tree') ) {
  $css = get_option_tree( 'vn_customcss' );
}
?>
<?php echo ($css); ?>
	
</style>

<?php wp_head(); ?> 

</head>

<?php $theme_options = get_option('option_tree'); ?>

<body <?php body_class(); ?>>

<!-- Start of top wrapper -->
<div id="top_wrapper">

<!-- Start of content wrapper -->
<div class="content_wrapper">

	<div class="topsubmenu">
		<?php wp_nav_menu(
		array(
		'theme_location'  => 'topsub',
		)
		);
		?>
		<?php if ( function_exists( 'get_option_tree' ) ) : ?>
		<div class="sales_pitch mobile-hide"><?php print get_option_tree( 'vn_header_sales_pitch' ); ?></div>
		<?php endif; ?>
	</div><!-- /.topsubmenu -->
	
	<a class="cart-contents st-button" data-effect="st-effect-4" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>">
			<?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?><?php //echo $woocommerce->cart->get_cart_total(); ?>
	</a>
	<div id="searchbox">
		<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Header Widgets')) : ?><?php endif; ?>
		<?php //get_search_form(); ?>
	</div><!-- /#searchbox -->
	<a href="#" class="mobile-nav-button st-button mobile-only" data-effect="st-effect-2"></a>
	

<?php //print_r($woocommerce->cart->cart_contents); ?>

</div><!-- End of content wrapper -->

<div class="clear"></div>

</div><!-- End of top wrapper -->

<div id="st-container" class="st-container">
<nav class="st-menu st-effect-4" id="menu-4">
	<?php get_template_part( 'woocommerce/_includes/shopping-cart', 'preview' ); ?>
</nav>
<nav class="st-menu st-effect-2 mobile-nav" id="menu-2">
	<?php wp_nav_menu(
		array(
		'theme_location'  => 'topsub',
		)
		);
	?>
	<?php get_template_part( 'woocommerce/_includes/shopping-cart', 'preview' ); ?>
</nav>



<div class="st-pusher">
<div class="st-content"><!-- this is the wrapper for the content -->
<div class="st-content-inner"><!-- extra div for emulating position:fixed of the menu -->

<?php if(!is_shop()): ?>
<!-- Start of breadcrumb wrapper -->
<div class="breadcrumb_wrapper">
	<div class="breadcrumbs">
	    <?php 
	    	//woocommerce_breadcrumb(array('separator' => ' / '));
			if (function_exists('bcn_display') && !isset($cat_type) && !is_shop() && empty($breadcrumb)) {
				bcn_display();
			} else if (function_exists('bcn_display') && (isset($cat_type) && $cat_type !== '' && $cat_type !== NULL) && !is_shop()) {
				bcn_display();
			} else if ($breadcrumb !== '' && !is_shop()) {
				echo $breadcrumb;
			} else {
				?>
					<script type="text/javascript">
						jQuery(document).ready(function() {
							jQuery(".breadcrumb_wrapper").hide();
						});
					</script>
				<?php
			}
	    ?>
	</div><!-- /.breadcrumbs -->
<!-- Clear Fix --><div class="clear"></div>
</div><!-- End of breadcrumb wrapper -->
<?php endif; ?>
