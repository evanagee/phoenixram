<section id="newsletter">
	<div class="content_wrapper">
		<?php if (!dynamic_sidebar('pre_footer')) : ?><?php endif; ?>
		<div class="clearfix"></div>
	</div>
</section>

<!-- Start of bottom wrapper -->
<div id="bottom_wrapper">

	<!-- Start of content wrapper -->
	<div class="content_wrapper">
		
		<!-- Start of one fourth first -->
		<div class="one_fourth_first">
		<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer_one') && 1==2) : ?>
		<?php endif; ?>
		</div><!-- End of one fourth first -->
		
		<!-- Start of one fourth -->
		<div class="one_fourth">
		<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer_two')) : ?>
		<?php endif; ?>
		</div><!-- End of one fourth -->
		
		<!-- Start of one fourth -->
		<div class="one_fourth">
		<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer_three')) : ?>
		<?php endif; ?>
		</div><!-- End of one fourth -->
		
		<!-- Start of one fourth -->
		<div class="one_fourth">
		<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer_four')) : ?>
		<?php endif; ?>
		</div><!-- End of one fourth -->
	
		<div class="shop_with_confidence">
			Shop with confidence! &nbsp; <img src="/wp-content/plugins/woocommerce-gateway-authorize-net-dpm/images/cards.png" width="131" height="24" border="0" /><img src="/wp-content/plugins/woocommerce/assets/images/icons/paypal.png" width="38" height="24" border="0" style="margin-left: 1px;" /><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/ssl_icon.gif" border="0" style="margin-left: 40px;" />
		</div>
		
			<!-- Start of copyright wrapper -->
		<div id="copyright_wrapper">
			<!-- Start of copyright message -->
			<div class="copyright_message">
			<?php 
			if ( function_exists( 'get_option_tree' ) ) {
			$copyright = get_option_tree( 'vn_copyright' );
			} ?>     
			
			<?php if ($copyright != ('')){ ?> 
			 
			&copy;<?php echo stripslashes($copyright); ?> &nbsp; <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/us-flag.png" width="24" height="24" border="0" />
			
			<?php } else { } ?>
			
			</div><!-- End of copyright message -->
			
			<!-- Start of social icons -->
			<div class="social_icons">
			
				<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('social')) : ?>
				<?php endif; ?>
			
			</div><!-- End of social icons -->

			
			<!-- Clear Fix --><div class="clear"></div>
		
		</div><!-- End of copyright wrapper -->	
	
		
	</div><!-- End of content wrapper -->
		
	<!-- Clear Fix --><div class="clear"></div>
	
</div><!-- End of bottom wrapper -->


<?php 
if ( function_exists( 'get_option_tree' ) ) {
$analytics = get_option_tree( 'vn_tracking' );
} ?>     

<?php echo stripslashes($analytics); ?>

<script type="text/javascript">
// DOM ready
jQuery(document).ready(function(){
	
jQuery('.slider').flexslider();
 
// Create the dropdown base
jQuery("<select />").appendTo(".topmenu");

// Create default option "Go to..."
jQuery("<option />", {
 "selected": "selected",
 "value"   : "",
 "text"    : "Menu Selection"
}).appendTo(".topmenu select");

// Populate dropdown with menu items
jQuery(".topmenu a").each(function() {
var el = jQuery(this);
jQuery("<option />", {
   "value"   : el.attr("href"),
   "text"    : el.text()
}).appendTo(".topmenu select");
});

// To make dropdown actually work
// To make more unobtrusive: http://css-tricks.com/4064-unobtrusive-page-changer/
jQuery(".topmenu select").change(function() {
window.location = jQuery(this).find("option:selected").val();
});

});

</script>
<!-- Start of Woopra Code -->
<script>
(function(){
var t,i,e,n=window,o=document,a=arguments,s="script",r=["config","track","identify","visit","push","call"],c=function(){var t,i=this;for(i._e=[],t=0;r.length>t;t++)(function(t){i[t]=function(){return i._e.push([t].concat(Array.prototype.slice.call(arguments,0))),i}})(r[t])};for(n._w=n._w||{},t=0;a.length>t;t++)n._w[a[t]]=n[a[t]]=n[a[t]]||new c;i=o.createElement(s),i.async=1,i.src="//static.woopra.com/js/w.js",e=o.getElementsByTagName(s)[0],e.parentNode.insertBefore(i,e)
})("woopra");

woopra.config({
    domain: 'phoenixram.net',
    idle_timeout: 1800000
});
woopra.track('pv', {
    url: window.location.pathname+window.location.search,
    title: document.title
});
</script>
<!-- End of Woopra Code -->
<?php wp_footer(); ?>

</div></div>

</div><!-- /.st-pusher -->
</div><!-- /#st-container -->

<script type="text/javascript">stLight.options({publisher: "230b6c07-2ba9-4260-934e-6b20e58b1ad8", doNotHash: true, doNotCopy: false, hashAddressBar: false});</script>
<script>
var options={ "publisher": "230b6c07-2ba9-4260-934e-6b20e58b1ad8", "position": "right", "ad": { "visible": false, "openDelay": 5, "closeDelay": 0}, "chicklets": { "items": ["twitter", "facebook", "googleplus", "linkedin", "pinterest"]}};
var st_hover_widget = new sharethis.widgets.hoverbuttons(options);
</script>

</body>
</html>