jQuery(document).ready(function($) {  
	//.parallax(xPosition, speedFactor, outerHeight) options:
	//xPosition - Horizontal position of the element
	//inertia - speed to move relative to vertical scroll. Example: 0.1 is one tenth the speed of scrolling, 2 is twice the speed of scrolling
	//outerHeight (true/false) - Whether or not jQuery should use it's outerHeight option to determine when a section is in the viewport
	
/*
	$("#st-container").css("display","none").fadeIn(1000);
	
	$("a:not(.st-button)").click(function(event){
		event.preventDefault();
		linkLocation = this.href;
		$("#st-container").fadeOut(1000, redirectPage);
	});
	
	function redirectPage() { window.location = linkLocation; }
*/
	
	if ($('.parallax').length > 0) {
		$('.parallax').parallax("50%", -0.4, true);
		$('#phone_feature_img').parallax("50%", 0.4);
	}
	
	$(".scroll_to_continue").click(function() {
		$("body, html").animate({ scrollTop: $('#contentwrapper').offset().top }, 2000);
	});
	
	//$("body.tax-product_cat").delay(4000).animate({ scrollTop: $('#contentwrapper').position().top }, 2000);
	//$("#menu-primary").append('<li id="menu-item-4012" class="menu-item"><a href="#" class="st-button" data-effect="st-effect-2">Mobile</a></li>');
	
	$("#menu-primary li:last-child a").attr('data-effect','st-effect-2');
/*
	$("#shopping-cart-preview").hide();
		$("a.cart-contents").click(function(event) {
		toggleCartPreview();
	});
*/
	
/*
	$("#header_wrapper").on('click', function(event) {
		$("#shopping-cart-preview").slideUp();
	});
	
	var toggleCartPreview = function() {
		event.preventDefault();
		$("#shopping-cart-preview").slideToggle();
	}
	
*/
	
	$(".dropdown-menu").on('mouseleave',function() {
		// For ul dropdowns scroll back to the top
		// on mouse leave
		$(this).scrollTop(0);
	});

	
    $('.custom_upload_image_button').click(function() {  
        formfield = jQuery(this).siblings('.custom_upload_image');  
        preview = jQuery(this).siblings('.custom_preview_image');  
        tb_show('', 'media-upload.php?type=image&TB_iframe=true');  
		window.send_to_editor = function(html) {
		img = jQuery(html);
		imgurl = img.attr('src');
		classes = img.attr('class');
		id = classes.replace(/(.*?)wp-image-/, '');
		formfield.val(id);
		preview.attr('src', imgurl);
		tb_remove();
	} 
        return false;  
    });  
      
    $('.custom_clear_image_button').click(function() {  
        var defaultImage = jQuery(this).parent().siblings('.custom_default_image').text();  
        jQuery(this).parent().siblings('.custom_upload_image').val('');  
        jQuery(this).parent().siblings('.custom_preview_image').attr('src', defaultImage);  
        return false;  
    });  
    
    
	$(".dropdown-header").click(function(event) {
	    if ($(this).hasClass('active')) {
	        $(this).removeClass('active');
	        $(this).siblings(".dropdown-menu").stop().slideUp(200);
	    } else {
	        $(this).addClass('active');
	         $(this).siblings(".dropdown-menu").stop().slideDown(200);
	    }
	    event.preventDefault();
	});
  
}); 