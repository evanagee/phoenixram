jQuery(document).ready(function($) {

	$('label.category-host-field').closest('tr').addClass("category-host-field-marker");
	$('label.category-parent-field').closest('tr').addClass("category-parent-field-marker");
	
	var $category_type = $("#category-type").data('category-type');
	
	if ($category_type == 'host') {
		$('.category-parent-field-marker').hide();
	} else {
		$('.category-host-field-marker').hide();
	}
	
});