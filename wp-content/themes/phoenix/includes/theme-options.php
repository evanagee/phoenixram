<?php
if ( function_exists('register_sidebars') ) {
register_sidebar(array(
	'name'=>'Header Widgets',
	'before_widget' => ' <div id="%1$s" class="headingWidget %2$s"> ',
	'after_widget' => '</div>',
	'before_title' => '',
	'after_title' => '',
));
register_sidebar(array(
	'name'=>'Sidebar Widgets',
	'before_widget' => ' div id="%1$s" class="widgetSidebar %2$s" ',
	'after_widget' => ' /div ',
	'before_title' => ' h4 ',
	'after_title' => ' /h4 ',
));
register_sidebar(array(
	'name' => __( 'Shop', 'essentials' ),
	'id' => 'shop',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h5>',
    'after_title' => '</h5>'
));
register_sidebar(array(
	'name' => __( 'Page-Right', 'essentials' ),
	'id' => 'page',
   'before_widget' => '<div id="%1$s" class="widgetSidebar %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h5>',
    'after_title' => '</h5>'
));
register_sidebar(array(
	'name' => __( 'Page-Left', 'essentials' ),
	'id' => 'page-left',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h5>',
    'after_title' => '</h5>'
));
register_sidebar(array(
	'name' => __( 'Blog-Right', 'essentials' ),
	'id' => 'blog',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h5>',
    'after_title' => '</h5>'
));
register_sidebar(array(
	'name' => __( 'Blog-Left', 'essentials' ),
	'id' => 'blog-left',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h5>',
    'after_title' => '</h5>'
));
register_sidebar(array(
	'name' => __( 'Careers Page', 'essentials' ),
	'id' => 'careers',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h5>',
    'after_title' => '</h5>'
));
register_sidebar(array(
	'name' => __( 'Pre Footer', 'essentials' ),
	'id' => 'pre_footer',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h4 class="large">',
    'after_title' => '</h4>'
));
register_sidebar(array(
	'name' => __( 'Footer One', 'essentials' ),
	'id' => 'footer_one',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h5>',
    'after_title' => '</h5>'
));
register_sidebar(array(
	'name' => __( 'Footer Two', 'essentials' ),
	'id' => 'footer_two',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h5>',
    'after_title' => '</h5>'
));
register_sidebar(array(
	'name' => __( 'Footer Three', 'essentials' ),
	'id' => 'footer_three',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h5>',
    'after_title' => '</h5>'
));
register_sidebar(array(
	'name' => __( 'Footer Four', 'essentials' ),
	'id' => 'footer_four',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h5>',
    'after_title' => '</h5>'
));
register_sidebar(array(
	'name' => __( 'Footer Social Icons', 'essentials' ),
	'id' => 'social',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h5>',
    'after_title' => '</h5>'
));
register_sidebar(array(
	'name'=>'Home Page Content',
	'before_widget' => ' <div id="%1$s" class="home_page_content %2$s"> ',
	'after_widget' => '</div>',
	'before_title' => '',
	'after_title' => '',
));
}
?>