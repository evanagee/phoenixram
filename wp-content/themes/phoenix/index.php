<?php get_header('shop'); ?>
<div id="header_wrapper">

	<div class="content_wrapper">
	
	<div id="logo">
		<a href="<?php echo site_url(); ?>"><?php 
		if ( function_exists( 'get_option_tree' ) ) {
		$logopath = get_option_tree( 'vn_toplogo' );
		} ?><img src="<?php echo $logopath; ?>" alt="logo" /></a>
	</div><!-- End of logo -->

	</div> <!-- /.content_wrapper -->
</div><!-- /#header_wrapper -->

<!-- Start of content wrapper -->
<div id="contentwrapper">

<!-- Start of content wrapper -->
<div class="content_wrapper">

<!-- Start of left content -->
<div class="<?php if(is_active_sidebar('blog')) : ?>left_content<?php endif; ?>">

	<?php while(have_posts()) : the_post(); ?>
	
	<?php get_template_part( 'content', get_post_format() ); ?>
	
	<?php endwhile; ?> 

</div><!-- End of left content -->
<?php if(is_active_sidebar('blog')) : ?>
<!-- Start of right content -->
<div class="right_content">
	<?php get_sidebar ('blog'); ?> 

</div><!-- End of right content -->
<?php endif; ?>
<!-- Start of pagination -->
<div class="pagination">
<?php if (function_exists("pagination")) {
    pagination($wp_query->max_num_pages);
} ?>

</div><!-- End of pagination -->

</div><!-- End of content wrapper -->

<!-- Clear Fix --><div class="clear"></div>

</div><!-- End of content wrapper -->
           
<?php get_footer(); ?>