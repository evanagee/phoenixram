<?php
/**
 * Add default product tabs to product pages.
 *
 * @access public
 * @param mixed $tabs
 * @return void
 */
 
function woocommerce_default_product_tabs( $tabs = array() ) {
	global $product, $post;

	// Description tab - shows product content
	if ( $post->post_content )
		$tabs['description'] = array(
			'title'    => __( 'Description', 'woocommerce' ),
			'priority' => 10,
			'callback' => 'woocommerce_product_description_tab'
		);

	
	
	/*
$tabs['related_products'] = array(
		'title'    => __( 'Related Products', 'woocommerce' ),
		'priority' => 7,
		'callback' => 'woocommerce_related_products'
	);
*/
	
/*

	// Additional information tab - shows attributes
	if ( $product->has_attributes() || ( get_option( 'woocommerce_enable_dimension_product_attributes' ) == 'yes' && ( $product->has_dimensions() || $product->has_weight() ) ) )
		$tabs['additional_information'] = array(
			'title'    => __( 'Additional Information', 'woocommerce' ),
			'priority' => 20,
			'callback' => 'woocommerce_product_additional_information_tab'
		);
*/

	// Reviews tab - shows comments
	if ( comments_open() )
		$tabs['reviews'] = array(
			'title'    => sprintf( __( 'Reviews (%d)', 'woocommerce' ), get_comments_number( $post->ID ) ),
			'priority' => 5,
			'callback' => 'comments_template'
		);

	return $tabs;
}
/* 
 * Helper function to return the theme option value. If no value has been saved, it returns $default.
 * Needed because options are saved as serialized strings.
 *
 */

function product_custom_breadcrumb($product, $name) {
	$breadcrumb = array();
	$product->breadcrumb = array_shift(woocommerce_get_product_terms( $product->id, $name, 'names' ));
	if ($product->breadcrumb !== '') {
	
		// Trim/Explode the breadcrumb by >
		$bc = array_map('trim',explode('&gt;',$product->breadcrumb));
		
		// Get the details for the parent cat
		$parent = get_term_by('name', $bc[0], 'product_cat');
		
		// Get a list of all children for the parent item
		$children = get_term_children( $parent->term_id, 'product_cat' );
		
		if ($parent->name) $breadcrumb_s = '<a href="/">Home</a> / <a href="' . get_term_link( $parent->name, 'product_cat' ) . '">' . $parent->name . '</a> / ';
		
		foreach ( $children as $child ) {
			$term = get_term_by( 'id', $child, 'product_cat' );
			if (in_array($term->name,$bc)) {
				$breadcrumb_s .= '<a href="' . get_term_link( $term->name, 'product_cat' ) . '">' . $term->name . '</a> / ';
			}
		}
		
		$breadcrumb_s .= '<a href="' . get_permalink( $product->id ) . '">' . $product->post->post_title . '</a>';
		
		return $breadcrumb_s;
	}
	return false;
}

//add_filter( 'bcn_after_fill', 'product_cat_breadcrumbs' );
function product_cat_breadcrumbs($post, $return = false) {
    $taxonomy_name = 'product_cat';
    $terms = get_the_terms($post->ID, $taxonomy_name);
    $term_count = 0;
    /*
echo 'FACE<pre>';
    print_r($terms);
    $terms = rsort($terms);
    print_r($terms);
    echo '</pre>';
*/
    
    $terms = rsort($terms);
    foreach ($terms as $term) {
        $term_count++;
        global $bcn_admin;
        if ($bcn_admin !== null) {
            // Load options
            $bcn_admin->breadcrumb_trail->opt = wp_parse_args(get_option('bcn_options'), $bcn_admin->breadcrumb_trail->opt);
            $bcn_admin->breadcrumb_trail->term_parents($term->term_id, $taxonomy_name);
            /*
echo '<pre>';
            print_r($bcn_admin->breadcrumb_trail);
            echo '</pre>';
            
*/			//echo "<br>";
            return $bcn_admin->breadcrumb_trail->display($return);
        }
        break;
    }
    // If we didn't find any terms, fallback to default behaviour.
    if ($term_count == 0)
        return bcn_display($return);
}



/*
if ( !function_exists( 'of_get_option' ) ) {
function of_get_option($name, $default = false) {
	
	$optionsframework_settings = get_option('optionsframework');
	
	// Gets the unique option id
	$option_name = $optionsframework_settings['id'];
	
	if ( get_option($option_name) ) {
		$options = get_option($option_name);
	}
		
	if ( isset($options[$name]) ) {
		return $options[$name];
	} else {
		return $default;
	}
}
}
*/

function get_me_list_of($atts, $content = null)
{   
    $args = array( 'post_type' => 'product', 'posts_per_page' => 10, 'product_cat' => $atts[0], 'orderby' => 'rand' );

    $loop = new WP_Query( $args );

    echo '<h1 class="upp">Style '.$atts[0].'</h1>';
    echo "<ul class='mylisting'>";
    while ( $loop->have_posts() ) : $loop->the_post(); 
    global $product; 

    echo '<li><a href="'.get_permalink().'">'.get_the_post_thumbnail($loop->post->ID, 'thumbnail').'</a></li>';

    endwhile; 

    echo "</ul>";

    wp_reset_query(); 

}

////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////     Extract audio/video shortcode     /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////



function extracted_shortcode()
{
    global $post;
	if($post) {
		$pattern = get_shortcode_regex();
		preg_match('/'.$pattern.'/s', $post->post_content, $matches);
		if( $matches ) :
			if (is_array($matches) && $matches[2] == 'audio') {
			   $shortcode = $matches[0];
			   echo do_shortcode($shortcode);
			}
		endif; //$matches
	} // endif $post
}// end func. audio_shortcode

add_action( 'init', 'extracted_shortcode' );



////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////     Featured Image Functionality     ////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////

add_theme_support( 'post-thumbnails' );
add_image_size( 'slide', 980, 999999, true );



////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////     WP Tag Cloud     //////////////////////////////////////////////// 
////////////////////////////////////////////////////////////////////////////////////////////


add_filter( 'wp_tag_cloud', 'remove_tag_cloud', 10, 2 );

function remove_tag_cloud ( $return, $args )
{
        return false;
}



function mytheme_tags() {
			
$tags = get_tags();
foreach ($tags as $tag) {
$tag_link = get_tag_link($tag->term_id);
$html = '<div class="button_tag">';
$html .= "<a href='{$tag_link}' title='{$tag->name} Tag' class='{$tag->slug}'>";
$html .= "{$tag->name}</a>";
$html .= '</div>';
echo $html;
}
}
	
add_filter('widget_tag_cloud_args', 'mytheme_tags');	





////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////     Prev & Next Buttons    //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


add_filter('next_posts_link_attributes', 'posts_link_attributes_1');
add_filter('previous_posts_link_attributes', 'posts_link_attributes_2');

function posts_link_attributes_1() {
    return 'class="button arrow_left"';
}
function posts_link_attributes_2() {
    return 'class="button arrow_right"';
}



////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////     Post Format     /////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


add_theme_support( 'post-formats', array( 'audio', 'link', 'gallery', 'video', 'quote' ) );



////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////     2 WP Nav Menus     //////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


register_nav_menus( array(  
  'primary' => __( 'Primary Navigation', 'essentials' ),
  'topsub' => __( 'Top Sub Menu Navigation', 'essentials' ), 
  'sidebarone' => __('Sidebar Menu', 'essentials')  

) );  	



////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////     Setting up Option Tree includes     /////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////

/* START OPTION TREE */ 
add_filter( 'ot_show_pages', '__return_false' );  
add_filter( 'ot_theme_mode', '__return_true' );
//add_filter( 'ot_show_pages', '__return_true' );  
//add_filter( 'ot_theme_mode', '__return_false' );
include_once( 'option-tree/ot-loader.php' );
include_once( 'functions/ot-theme-options.php' );




////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////     Google Fonts     ////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
function mytheme_fonts() {
	$protocol = is_ssl() ? 'https' : 'http';
	wp_enqueue_style( 'essentials-lato', "$protocol://fonts.googleapis.com/css?family=Lato:300,400,700,100,400italic' rel='stylesheet' type='text/css" );
	//wp_enqueue_style( 'essentials-pacifico', "$protocol://fonts.googleapis.com/css?family=Pacifico:400' rel='stylesheet' type='text/css" );
	wp_enqueue_style( 'essentials-permanent-marker', "$protocol://fonts.googleapis.com/css?family=Permanent+Marker:400' rel='stylesheet' type='text/css" );
}
add_action( 'wp_enqueue_scripts', 'mytheme_fonts' );
/*
add_action('admin_head', 'admin_css');

	function admin_css() {
		echo "<link type='text/css' rel='stylesheet' href='" . get_template_directory_uri().'/css/admin/styles.css' . "' />";
	}


function admin_custom_css() {
	wp_enqueue_style( 'admin-styles', "get_template_directory_uri().'/css/admin/styles.css' rel='stylesheet' type='text/css'" );
}
add_filter('admin_enqueue_scripts','admin_custom_css');
*/

function my_login_stylesheet() {
   echo '<link rel="stylesheet" id="custom_wp_admin_css"  href="'. get_bloginfo( 'stylesheet_directory' ) . '/css/admin/styles.css" type="text/css" media="all" />';
}
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );
add_action( 'login_enqueue_scripts', 'mytheme_fonts' );

////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////     Theme Options for widget     ////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


$include_path = get_template_directory() . '/includes/';
require_once ($include_path . 'theme-options.php'); 


////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////     Comments     ////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


function mytheme_comment($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
   <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
   
   <div class="comment-author-avatar">
   <?php echo get_avatar($comment, 64); ?>
         
   </div>
   
   <div class="comment-main">
   
     <div class="comment-meta">
     <?php printf(__('<span class="comment-author">Written by: %s</span>', 'essentials'), get_comment_author()) ?>
     <div class="button"><?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?></div>
     </div>   
     
     <div class="comment-content">      
     <?php if ($comment->comment_approved == '0') : ?>
     <p><em><?php _e('Your comment is awaiting moderation.', 'essentials') ?></em></p>
     <?php comment_text() ?>
 
     </div> 
     
     </div>
     
     
     <?php else : { ?>
 
     <?php comment_text() ?>  
     
     <?php } ?>  
     
	 <?php endif; ?>
	 
     
     
     <?php
       }
				
	
////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////     Content width set     ///////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


if ( ! isset( $content_width ) ) 
    $content_width = 980;
		

////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////     Text Domain     /////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


load_theme_textdomain ('essentials');



////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////     Multi Language Ready     ////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


load_theme_textdomain( 'essentials', get_template_directory().'/languages' );

$locale = get_locale();
$locale_file = get_template_directory()."/languages/$locale.php";
if ( is_readable($locale_file) )
	require_once($locale_file);
	

////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////     Contact Form 7     //////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


/**
 * Functions:	Optimize and style Contact Form 7 - WPCF7
 *
 */
// Remove the default Contact Form 7 Stylesheet
function remove_wpcf7_stylesheet() {
	remove_action( 'wp_head', 'wpcf7_wp_head' );
}

// Add the Contact Form 7 scripts on selected pages
function add_wpcf7_scripts() {
	if ( is_page('contact') )
		wpcf7_enqueue_scripts();
}

// Change the URL to the ajax-loader image
function change_wpcf7_ajax_loader($content) {
	if ( is_page('contact') ) {
		$string = $content;
		$pattern = '/(<img class="ajax-loader" style="visibility: hidden;" alt="ajax loader" src=")(.*)(" \/>)/i';
		$replacement = "$1".get_template_directory_uri()."/images/ajax-loader.gif$3";
		$content =  preg_replace($pattern, $replacement, $string);
	}
	return $content;
}

// If the Contact Form 7 Exists, do the tweaks
if ( function_exists('wpcf7_contact_form') ) {
	if ( ! is_admin() && WPCF7_LOAD_JS )
		remove_action( 'init', 'wpcf7_enqueue_scripts' );

	add_action( 'wp', 'add_wpcf7_scripts' );
	add_action( 'init' , 'remove_wpcf7_stylesheet' );
	add_filter( 'the_content', 'change_wpcf7_ajax_loader', 100 );
}





////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////     Include post and page in search     /////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


function filter_search($query) {
    if ($query->is_search) {
	$query->set('post_type', array('post', 'page', 'staff', 'casestudy', 'careers'));
    };
    return $query;
};
add_filter('pre_get_posts', 'filter_search');



////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////              RSS Feed Links             /////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


add_theme_support( 'automatic-feed-links' );


////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////     Remove the jump on read more     ////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


function remove_more_jump_link($link) { 
$offset = strpos($link, '#more-');
if ($offset) {
$end = strpos($link, '"',$offset);
}
if ($end) {
$link = substr_replace($link, '', $offset, $end-$offset);
}
return $link;
}
add_filter('the_content_more_link', 'remove_more_jump_link');



////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////     Include tiny mce for shortcode buttons     //////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


include('tinyMCE.php');


////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////     Load JS Scripts     /////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


function tws_js_loader() {
	wp_enqueue_script('custom', get_template_directory_uri().'/js/custom-js.js', array('jquery'),'1.0', true );
	wp_enqueue_script('retina', get_template_directory_uri().'/js/retina.js', array('jquery'),'1.0', true );
	wp_enqueue_script('slider', get_template_directory_uri().'/js/jquery.flexslider-min.js', array('jquery'),'1.0', true );
	wp_enqueue_script('easing', get_template_directory_uri().'/js/jquery.easing.1.3.js', array('jquery'),'1.0', true );
	wp_enqueue_script('hover', get_template_directory_uri().'/js/hoverIntent.js', array('jquery'),'1.0', true );
	wp_enqueue_script('sfmenu', get_template_directory_uri().'/js/jquery.sfmenu.js', array('jquery'),'1.0', true );
	wp_enqueue_script('commentvaljs', get_template_directory_uri().'/js/jquery.validate.pack.js', array('jquery'),'1.0', true );
	wp_enqueue_script('commentval', get_template_directory_uri().'/js/comment-form-validation.js', array('jquery'),'1.0', true );
	wp_enqueue_script('parallax', get_template_directory_uri().'/js/jquery.parallax-1.1.3.js', array('jquery'),'1.0', true );
	wp_enqueue_script('img-rotate', get_template_directory_uri().'/js/jQueryRotateCompressed.js', array('jquery'),'1.0', true );
	wp_enqueue_script('outside-events', get_template_directory_uri().'/js/jquery.ba-outside-events.min.js', array('jquery'),'1.0', true );
	wp_enqueue_script('classie', get_template_directory_uri().'/js/classie.js', array('jquery'),'1.0', true );
	wp_enqueue_script('modernizr', get_template_directory_uri().'/js/modernizr.custom.js', array('jquery'),'1.0', false );
	wp_enqueue_script('sidebarEffects', get_template_directory_uri().'/js/sidebarEffects.js', array('jquery'),'1.0', true );
	
}

add_action('wp_enqueue_scripts', 'tws_js_loader');


////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////     PAGINATION     //////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////

		
function pagination($pages = '', $range = 1)
{ 
     $showitems = ($range * 2)+1; 
 
     global $paged;
     if(empty($paged)) $paged = 1;
 
     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }  
 
     if(1 != $pages)
     {
         echo "<div class=\"pagination\"><span>Page ".$paged." of ".$pages."</span>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
 
         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
             }
         }
 
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
         echo "</div>\n";
     }
}



////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////     Slider post type     ////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


add_action('init', 'slider_register');
 
function slider_register() {
 
	$labels = array(
		'name' => __('Slider Images', 'post type general name', 'essentials'),
		'singular_name' => __('Slider Item', 'post type singular name', 'essentials'),
		'add_new' => __('Add New', 'slider item', 'essentials'),
		'add_new_item' => __('Add New Slider Item', 'essentials'),
		'edit_item' => __('Edit Slider Item', 'essentials'),
		'new_item' => __('New Slider Item', 'essentials'),
		'view_item' => __('View Slider Item', 'essentials'),
		'search_items' => __('Search Slider', 'essentials'),
		'not_found' =>  __('Nothing found', 'essentials'),
		'not_found_in_trash' => __('Nothing found in Trash', 'essentials'),
		'parent_item_colon' => ''
	);
 
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title','editor','thumbnail')
	  ); 
 
	register_post_type( 'slider' , $args );
}

////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////     Change excerpt length     ///////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


function string_limit_words($string, $word_limit)
{
  $words = explode(' ', $string, ($word_limit + 1));
  if(count($words) > $word_limit)
  array_pop($words);
  return implode(' ', $words);
}


////////////////////////////////////////////////////////////////////////////////////////////
/////////////////    Extract first occurance of text from a string     /////////////////////
////////////////////////////////////////////////////////////////////////////////////////////

// Extract first occurance of text from a string
function my_extract_from_string($start, $end, $tring) {
	$tring = stristr($tring, $start);
	$trimmed = stristr($tring, $end);
	return substr($tring, strlen($start), -strlen($trimmed));
}


function get_content_link( $content = false, $echo = false )
{
    // allows using this function also for excerpts
    if ( $content === false )
        $content = get_the_content(); // You could also use $GLOBALS['post']->post_content;

    $content = preg_match_all( '/href\s*=\s*[\"\']([^\"\']+)/', $content, $links );
    $content = $links[1][0];
    $content = make_clickable( $content );

    // if you set the 2nd arg to true, you'll echo the output, else just return for later usage
    if ( $echo === true )
        echo $content;

    return $content;
}

////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////     Shortcodes    ///////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


//	blockquote
add_shortcode('quote', 'tws_quote');

function tws_quote($atts, $content = null) {
	return '<div class="quote">' .do_shortcode($content).'</div>';
}

//	intro
add_shortcode('intro', 'tws_intro');

function tws_intro($atts, $content = null) {
	return '<div class="intro">' .do_shortcode($content).'</div>';
}

//	hr
add_shortcode('hr', 'tws_hr');

function tws_hr($atts, $content = null) {
	return '<div class="hrr">' .do_shortcode($content).'</div>';
}

//	pullquoteleft
add_shortcode('pullquoteleft', 'tws_pullquoteleft');

function tws_pullquoteleft($atts, $content = null) {
	return '<div class="pullquoteleft">' .do_shortcode($content).'</div>';
}

//	pullquoteright
add_shortcode('pullquoteright', 'tws_pullquoteright');

function tws_pullquoteright($atts, $content = null) {
	return '<div class="pullquoteright">' .do_shortcode($content).'</div>';
}

//	alert_yellow
add_shortcode('alert_yellow', 'tws_alert_yellow');

function tws_alert_yellow($atts, $content = null) {
	return '<div class="alert_yellow">' .do_shortcode($content).'</div>';
}

//	alert_blue
add_shortcode('alert_blue', 'tws_alert_blue');

function tws_alert_blue($atts, $content = null) {
	return '<div class="alert_blue">' .do_shortcode($content).'</div>';
}

//	alert_green
add_shortcode('alert_green', 'tws_alert_green');

function tws_alert_green($atts, $content = null) {
	return '<div class="alert_green">' .do_shortcode($content).'</div>';
}

//	alert_red
add_shortcode('alert_red', 'tws_alert_red');

function tws_alert_red($atts, $content = null) {
	return '<div class="alert_red">' .do_shortcode($content).'</div>';
}

//	one_half
add_shortcode('one_half', 'tws_one_half');

function tws_one_half($atts, $content = null) {
	return '<div class="one_half">' .do_shortcode($content).'</div>';
}

//	one_third
add_shortcode('one_third', 'tws_one_third');

function tws_one_third($atts, $content = null) {
	return '<div class="one_third">' .do_shortcode($content).'</div>';
}

//	two_third
add_shortcode('two_third', 'tws_two_third');

function tws_two_third($atts, $content = null) {
	return '<div class="two_third">' .do_shortcode($content).'</div>';
}

//	one_fourth
add_shortcode('one_fourth', 'tws_one_fourth');

function tws_one_fourth($atts, $content = null) {
	return '<div class="one_fourth">' .do_shortcode($content).'</div>';
}

//	three_fourth
add_shortcode('three_fourth', 'tws_three_fourth');

function tws_three_fourth($atts, $content = null) {
	return '<div class="three_fourth">' .do_shortcode($content).'</div>';
}

//	one_fifth
add_shortcode('one_fifth', 'tws_one_fifth');

function tws_one_fifth($atts, $content = null) {
	return '<div class="one_fifth">' .do_shortcode($content).'</div>';
}

//	two_fifth
add_shortcode('two_fifth', 'tws_two_fifth');

function tws_two_fifth($atts, $content = null) {
	return '<div class="two_fifth">' .do_shortcode($content).'</div>';
}

//	three_fifth
add_shortcode('three_fifth', 'tws_three_fifth');

function tws_three_fifth($atts, $content = null) {
	return '<div class="three_fifth">' .do_shortcode($content).'</div>';
}

//	four_fifth
add_shortcode('four_fifth', 'tws_four_fifth');

function tws_four_fifth($atts, $content = null) {
	return '<div class="four_fifth">' .do_shortcode($content).'</div>';
}

//	one_sixth
add_shortcode('one_sixth', 'tws_one_sixth');

function tws_one_sixth($atts, $content = null) {
	return '<div class="one_sixth">' .do_shortcode($content).'</div>';
}

//	five_sixth
add_shortcode('five_sixth', 'tws_five_sixth');

function tws_five_sixth($atts, $content = null) {
	return '<div class="five_sixth">' .do_shortcode($content).'</div>';
}

//	one_half_first
add_shortcode('one_half_first', 'tws_one_half_first');

function tws_one_half_first($atts, $content = null) {
	return '<div class="one_half_first">' .do_shortcode($content).'</div>';
}

//	one_third_first
add_shortcode('one_third_first', 'tws_one_third_first');

function tws_one_third_first($atts, $content = null) {
	return '<div class="one_third_first">' .do_shortcode($content).'</div>';
}

//	one_fourth_first
add_shortcode('one_fourth_first', 'tws_one_fourth_first');

function tws_one_fourth_first($atts, $content = null) {
	return '<div class="one_fourth_first">' .do_shortcode($content).'</div>';
}

//	one_fifth_first
add_shortcode('one_fifth_first', 'tws_one_fifth_first');

function tws_one_fifth_first($atts, $content = null) {
	return '<div class="one_fifth_first">' .do_shortcode($content).'</div>';
}

//	one_sixth_first
add_shortcode('one_sixth_first', 'tws_one_sixth_first');

function tws_one_sixth_first($atts, $content = null) {
	return '<div class="one_sixth_first">' .do_shortcode($content).'</div>';
}

//	two_third_first
add_shortcode('two_third_first', 'tws_two_third_first');

function tws_two_third_first($atts, $content = null) {
	return '<div class="two_third_first">' .do_shortcode($content).'</div>';
}

//	three_fourth_first
add_shortcode('three_fourth_first', 'tws_three_fourth_first');

function tws_three_fourth_first($atts, $content = null) {
	return '<div class="three_fourth_first">' .do_shortcode($content).'</div>';
}

//	two_fifth_first
add_shortcode('two_fifth_first', 'tws_two_fifth_first');

function tws_two_fifth_first($atts, $content = null) {
	return '<div class="two_fifth_first">' .do_shortcode($content).'</div>';
}

//	three_fifth_first
add_shortcode('three_fifth_first', 'tws_three_fifth_first');

function tws_three_fifth_first($atts, $content = null) {
	return '<div class="three_fifth_first">' .do_shortcode($content).'</div>';
}

//	four_fifth_first
add_shortcode('four_fifth_first', 'tws_four_fifth_first');

function tws_four_fifth_first($atts, $content = null) {
	return '<div class="four_fifth_first">' .do_shortcode($content).'</div>';
}

//	button_red
add_shortcode('button_red', 'tws_button_red');

function tws_button_red($atts, $content = null) {
	return '<div class="button_red">' .do_shortcode($content).'</div>';
}

//	button_green
add_shortcode('button_green', 'tws_button_green');

function tws_button_green($atts, $content = null) {
	return '<div class="button_green">' .do_shortcode($content).'</div>';
}

//	button_blue
add_shortcode('button_blue', 'tws_button_blue');

function tws_button_blue($atts, $content = null) {
	return '<div class="button_blue">' .do_shortcode($content).'</div>';
}

//	button_red_image
add_shortcode('button_red_image', 'tws_button_red_image');

function tws_button_red_image($atts, $content = null) {
	return '<div class="button_red_image">' .do_shortcode($content).'</div>';
}

//	button_green_image
add_shortcode('button_green_image', 'tws_button_green_image');

function tws_button_green_image($atts, $content = null) {
	return '<div class="button_green_image">' .do_shortcode($content).'</div>';
}

//	button_blue_image
add_shortcode('button_blue_image', 'tws_button_blue_image');

function tws_button_blue_image($atts, $content = null) {
	return '<div class="button_blue_image">' .do_shortcode($content).'</div>';
}



////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////     Remove shortcode from excerpt     ///////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'custom_trim_excerpt');

function custom_trim_excerpt($text = '')
{
	$raw_excerpt = $text;
	if ( '' == $text ) {
		$text = get_the_content('');
 
		//$text = strip_shortcodes( $text );
 
		$text = apply_filters('the_content', $text);
		$text = str_replace(']]&gt;', ']]&gt;', $text);
		$excerpt_length = apply_filters('excerpt_length', 25);
		$excerpt_more = apply_filters('excerpt_more', ' ' . '');
		$text = wp_trim_words( $text, $excerpt_length, $excerpt_more );
	}
	return apply_filters('wp_trim_excerpt', $text, $raw_excerpt);
}

add_filter('get_the_excerpt','do_shortcode');



////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////     Allow Shortcodes in Widgets     /////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


add_filter('widget_text', 'do_shortcode');


////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////     Remove height/width on images for responsive     ////////////////
////////////////////////////////////////////////////////////////////////////////////////////


add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10 );
add_filter( 'image_send_to_editor', 'remove_thumbnail_dimensions', 10 );

function remove_thumbnail_dimensions( $html ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}


////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////              Exclude thumbnail from gallery              ////////////
////////////////////////////////////////////////////////////////////////////////////////////


function exclude_thumbnail_from_gallery($null, $attr)
{
    if (!$thumbnail_ID = get_post_thumbnail_id())
        return $null; // no point carrying on if no thumbnail ID

    // temporarily remove the filter, otherwise endless loop!
    remove_filter('post_gallery', 'exclude_thumbnail_from_gallery');

    // pop in our excluded thumbnail
    if (!isset($attr['exclude']) || empty($attr['exclude']))
        $attr['exclude'] = array($thumbnail_ID);
    elseif (is_array($attr['exclude']))
        $attr['exclude'][] = $thumbnail_ID;

    // now manually invoke the shortcode handler
    $gallery = gallery_shortcode($attr);

    // add the filter back
    add_filter('post_gallery', 'exclude_thumbnail_from_gallery', 10, 2);

    // return output to the calling instance of gallery_shortcode()
    return $gallery;
}
add_filter('post_gallery', 'exclude_thumbnail_from_gallery', 10, 2);




////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////    Link Extraction for Post Format Link     /////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


// Extract first occurance of text from a string
if( !function_exists ('extract_from_string') ) :
function extract_from_string($start, $end, $tring) {
	$tring = stristr($tring, $start);
	$trimmed = stristr($tring, $end);
	return substr($tring, strlen($start), -strlen($trimmed));
}
endif;



function filter_ptags_on_images($content){
    return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}
add_filter('the_content', 'filter_ptags_on_images');

function cleanup_shortcode_fix($content) {   
          $array = array (
            '<p>[' => '[', 
            ']</p>' => ']', 
            ']<br />' => ']',
            ']<br>' => ']',
			'<br />' => '',
			'<br>' => ''
          );
          $content = strtr($content, $array);
            return $content;
        }
        add_filter('the_content', 'cleanup_shortcode_fix', 10);


////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////      WooCommerce Integration Settings       /////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////

//remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
//remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

add_theme_support( 'woocommerce' );

add_action( 'woocommerce_archive_description', 'woocommerce_category_image', 2 );
function woocommerce_category_image() {
    if ( is_product_category() ){
	    global $wp_query;
	    $cat = $wp_query->get_queried_object();
	    $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
	    $image = wp_get_attachment_url( $thumbnail_id );
	    if ( $image ) {
		    echo '<img src="' . $image . '" alt="" />';
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////              Admin JS Actions               /////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////

function add_jquery_function_edit_taxonomy() {
    global $parent_file;
	if ( isset( $_GET['action'] ) && $_GET['action'] == 'edit' && isset( $_GET['taxonomy'] ) && $_GET['taxonomy'] == 'product_cat' && $_GET['post_type'] == 'product') {
    	// Executed when we're editing product categories
    	$term = get_term($_GET["tag_ID"], 'product_cat');
    	/*
echo '<pre>';
    	print_r($term);
    	echo '</pre>';
*/
    	if ($term->parent == 0) {
    		// it has no parent, we're editing a Top Level Cat
    		echo '<div id="category-type" data-category-type="parent"></div>';
	    	wp_enqueue_script('custom', get_template_directory_uri().'/js/custom-admin.js?hide=host', array('jquery'),'1.0', true );
    	} else {
    		// We're editing a host
    		echo '<div id="category-type" data-category-type="host"></div>';
	    	wp_enqueue_script('custom', get_template_directory_uri().'/js/custom-admin.js?hide=parent', array('jquery'),'1.0', true );
    	}
    	//wp_enqueue_script('custom', get_template_directory_uri().'/js/custom-admin.js', array('jquery'),'1.0', true );
    }
}
add_filter('admin_footer', 'add_jquery_function_edit_taxonomy');

?>