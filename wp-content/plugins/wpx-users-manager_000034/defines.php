<?php
/**
 * Defines
 *
 * @author             wpXtreme, Inc.
 * @copyright          Copyright (C) 2012 wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-08 12:56:31
 * @version            0.1.0
 *
 */

/**
 * @var WPXUsersManager $this
 */

define( 'WPXUSERSMANAGER_VERSION', $this->version );
define( 'WPXUSERSMANAGER_TEXTDOMAIN', $this->textDomain );
define( 'WPXUSERSMANAGER_TEXTDOMAIN_PATH', $this->textDomainPath );

define( 'WPXUSERSMANAGER_URL_ASSETS', $this->assetsURL );
define( 'WPXUSERSMANAGER_URL_CSS', $this->cssURL );
define( 'WPXUSERSMANAGER_URL_JAVASCRIPT', $this->javascriptURL );
define( 'WPXUSERSMANAGER_URL_IMAGES', $this->imagesURL );



