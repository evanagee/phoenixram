<?php
/// @cond private
/**
 * Plugin Name:     Users Manager
 * Plugin URI:      https://wpxtre.me
 * Description:     Manage login (sign in), registration (sign up) and profile with extra fields
 * Version:         1.0.5
 * Author:          wpXtreme, Inc.
 * Author URI:      https://wpxtre.me
 * Text Domain:     wpx-users-manager
 * Domain Path:     localization
 *
 * WPX PHP Min: 5.2.4
 * WPX WP Min: 3.5
 * WPX MySQL Min: 5.0
 * WPX wpXtreme Min: 1.0.0
 *
 */
/// @endcond

/* Avoid directly access. */
if ( !defined( 'ABSPATH' ) ) {
  exit;
}

// wpXtreme kickstart logic
require_once( trailingslashit( dirname( __FILE__ ) ) . 'kickstart.php' );

wpxtreme_kickstart( __FILE__, 'WPXUsersManager', 'wpx-users-manager.php' );
