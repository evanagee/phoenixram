<?php
/**
 * WPXUsersManager is the main class of this plugin.
 * This class extends WPDKWordPressPlugin in order to make easy several WordPress functions.
 *
 * @class              WPXUsersManager
 * @author             wpXtreme, Inc.
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2012-11-28
 * @version            0.1.0
 *
 */
final class WPXUsersManager extends WPXPlugin {

  /**
   * Create and return a singleton instance of WPXUsersManager class
   *
   * @brief Init
   *
   * @param string $file The main file of this plugin. Usually __FILE__ (main.php)
   *
   * @return WPXUsersManager
   */
  public static function boot( $file ) {
    static $instance = null;
    if ( is_null( $instance ) ) {
      $instance = new WPXUsersManager( $file );
      do_action( __CLASS__ );
    }
    return $instance;
  }

  /**
   * Create an instance of WPXUsersManager class
   *
   * @brief Construct
   *
   * @param string $file The main file of this plugin. Usually __FILE__ (main.php)
   *
   * @return WPXUsersManager object instance
   */
  public function __construct( $file ) {

    parent::__construct( $file );

    $this->defines();
    $this->registerClasses();

    /* Add custom placeholder to WPDKMail. */
    add_filter( 'wpdk_mail_placeholders', array( $this, 'wpdk_mail_placeholders' ) );

    /* Added a screen help for placeholder replacement */
    add_action( 'load-post.php', array( 'WPXUsersManagerPlaceholderScreenHelp', 'init'), 99 );
    add_action( 'load-post-new.php', array( 'WPXUsersManagerPlaceholderScreenHelp', 'init'), 99 );
    //add_action( 'load-page-new.php', array( 'WPXUsersManagerPlaceholderScreenHelp', 'init'), 99 );

    /* Comment the code line below */
    //add_filter( 'wpdk_mail_replace_placeholders', array( $this, 'wpdk_mail_replace_placeholders' ), 10, 2 );

    /* Process Post data for signin, signup, profile, etc... */
    add_action( 'init', array( 'WPXUsersManagerPostData', 'init' ) );

    /* Register shortcode. */
    add_action( 'template_redirect', array( 'WPXUsersManagerShortcode', 'init' ) );
  }

  /**
   * Include the external defines file
   *
   * @brief Defines
   */
  private function defines() {
    include_once( 'defines.php' );
  }

  /**
   * Include all core file
   *
   * @brief Includes
   */
  private function registerClasses() {
    $includes = array(
    	$this->classesPath . 'admin/wpx-users-manager-about-vc.php' => 'WPXUsersManagerAboutViewController',

    	$this->classesPath . 'admin/wpx-users-manager-admin.php' => 'WPXUsersManagerAdmin',

    	$this->classesPath . 'configuration/wpx-users-manager-configuration-profile-view-controller.php' => array( 'WPXUsersManagerConfigurationProfileViewController',
    'WPXUsersManagerConfigurationProfileView',
    'WPXUsersManagerConfigurationExtraFieldsView' ),

    	$this->classesPath . 'configuration/wpx-users-manager-configuration-signin-view-controller.php' => array( 'WPXUsersManagerConfigurationSigninViewController',
    'WPXUsersManagerConfigurationSigninView',
    'WPXUsersManagerConfigurationResetPasswordView' ),

    	$this->classesPath . 'configuration/wpx-users-manager-configuration-signup-view-controller.php' => array( 'WPXUsersManagerConfigurationSignupViewController',
    'WPXUsersManagerConfigurationSignupView',
    'WPXUsersManagerConfigurationDoubleOptinView' ),

    	$this->classesPath . 'configuration/wpx-users-manager-configuration.php' => array( 'WPXUsersManagerConfiguration',
    'WPXUsersManagerConfigurationExtraFields',
    'WPXUsersManagerConfigurationSignin',
    'WPXUsersManagerConfigurationSignup',
    'WPXUsersManagerConfigurationResetPassword',
    'WPXUsersManagerConfigurationProfile' ),

    	$this->classesPath . 'core/extra-fields/wpx-users-manager-extra-fields.php' => 'WPXUsersManagerExtraFields',

    	$this->classesPath . 'core/profile/wpx-users-manager-profile.php' => array( 'WPXUsersManagerProfile',
    'WPXUsersManagerProfileView' ),

    	$this->classesPath . 'core/reset-password/wpx-users-manager-reset-password.php' => array( 'WPXUsersManagerResetPassword',
    'WPXUsersManagerResetPasswordView' ),

    	$this->classesPath . 'core/signin/wpx-users-manager-signin.php' => array( 'WPXUsersManagerSignin',
    'WPXUsersManagerSigninView' ),

    	$this->classesPath . 'core/signup/wpx-users-manager-signup.php' => array( 'WPXUsersManagerSignup',
    'WPXUsersManagerSignupView' ),

    	$this->classesPath . 'core/unlock-code/wpx-users-manager-unlock-code.php' => array( 'WPXUsersManagerUnlockCode',
    'WPXUsersManagerUnlockCodeView' ),

    	$this->classesPath . 'core/wpx-users-manager-placesholder.php' => array( 'WPXUsersManagerPlaceholder',
    'WPXUsersManagerPlaceholderScreenHelp' ),

    	$this->classesPath . 'core/wpx-users-manager-post-data.php' => array( 'WPXUsersManagerPostDataAction',
    'WPXUsersManagerPostData' ),

    	$this->classesPath . 'core/wpx-users-manager-shortcode.php' => 'WPXUsersManagerShortcode',

    	$this->classesPath . 'core/wpx-users-manager-usermeta.php' => array( 'WPXUsersManagerUserMeta',
    'WPXUsersManagerUserStatus' ),

    	$this->classesPath . 'core/wpx-users-manager-users.php' => 'WPXUsersManagerUsers' );

    $this->registerAutoloadClass( $includes );
  }

  /**
   * Catch for ajax
   *
   * @brief Ajax
   */
  public function ajax() {
    /**
     * To override.
     *
     * For example:
     *
     * @todo completare questo esempio con la nuova logica autoload
     *       WPXUsersManagerAjax::init();
     */
  }

  /**
   * Catch for admin
   *
   * @brief Admin backend
   */
  public function admin() {

    /* General admin */
    WPXUsersManagerAdmin::init( $this );

    /* @todo Improved with add_action( 'load-users.php', array( 'WPXUsersManagerUsers', 'init' ) ); */
    WPXUsersManagerUsers::init();
  }

  /**
   * Catch for frontend
   *
   * @brief Frontend
   */
  public function theme() {

  }

  /**
   * Ready to init plugin configuration
   *
   * @brief Init configuration
   */
  public function configuration() {
    WPXUsersManagerConfiguration::init();
  }

  /**
   * Catch for activation. This method is called one shot.
   *
   * @brief Activation
   */
  public function activation() {
    WPXUsersManagerConfiguration::init()->delta();
  }

  /**
   * Catch for deactivation. This method is called when the plugin is deactivate.
   *
   * @brief Deactivation
   */
  public function deactivation() {
    /** To override. */
  }

  /**
   * Do log in easy way
   *
   * @brief Helper for log
   *
   * @param mixed  $txt
   * @param string $title Optional. Any free string text to context the log
   *
   */
  public static function log( $txt, $title = '' ) {
    /**
     * @var WPXUsersManager $me
     */
    $me = $GLOBALS[__CLASS__];
    $me->log->log( $txt, $title );
  }

  /**
   * Add custom placeholder to WPDKMail placeholder engine
   *
   * @brief Placeholder
   *
   * @param array  $placesholder Default placeholder
   *
   * @return array
   */
  public function wpdk_mail_placeholders( $placesholder ) {
    $custom_placesholder = WPXUsersManagerPlaceholder::placeholders();
    return array_merge( $placesholder, $custom_placesholder );
  }

  /**
   * Add a replaced placeholder in array replacement.
   *
   * @brief Placeholder replace
   *
   * @param array  $replaces Key value pairs array
   * @param int    $id_user  User id
   *
   * @note Not used
   *
   * @return array
   * @sa WPDKMail::replacePlaceholder()
   */
  public function wpdk_mail_replace_placeholders( $replaces, $id_user ) {

    /* Bypass - questa parte qui sotto è sbagliata secondo me, i codici di sblocco vanno
    generati onfly quando servono, inoltre alcuni scadono, quindi questa parte di codice
    è sicuramente un refuso oltre a non essere completa; vedi variabile $permalink vuota.
    */
    return $replaces;

    if ( !empty( $id_user ) && is_numeric( $id_user ) ) {
      /* Get user object. */
      $user = new WPDKUser( $id_user );

      /* Get user email. */
      $email = $user->email;

      /* Unlock code and URL. */
      $permalink   = '';
      $unlock_code = $user->get( 'wpx_users_manager_unlock_code' );
      $unlock_url  = sprintf( '%s?uc=%s', $permalink, $unlock_code );

      /* Reset password. */
      $auto_signin_unique_code = WPDKCrypt::randomAlphaNumber( 16 );
      $auto_signin_unique_code = md5( $auto_signin_unique_code );
      $auto_signin_unique_code .= uniqid();
      $permalink = '';
      $reset_url = sprintf( '%s?rp=%s', $permalink, $auto_signin_unique_code );

      //      $timeout              = absint( WPXUsersManagerConfiguration::init()->signin->timeout );
      //      $reset_mail_slug      = WPXUsersManagerConfiguration::init()->signin->mail_slug;
      //      $reset_mail_post_type = WPXUsersManagerConfiguration::init()->signin->mail_slug;
      //      set_transient( $auto_signin_unique_code, $email, $timeout );

      /* Placeholder. */
      $replaces[WPXUsersManagerPlaceholder::DOUBLE_OPTIN_ACTIVATION_URL] = $unlock_url;
      $replaces[WPXUsersManagerPlaceholder::USER_PASSWORD_RESET_URL]     = $reset_url;
    }

    return $replaces;
  }

}

