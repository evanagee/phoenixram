<?php
/**
 * Enhancer users list view
 *
 * ## Overview
 *
 * Description
 *
 * @class           WPXUsersManagerUsers
 * @author          =undo= <info@wpxtre.me>
 * @copyright       Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date            2013-02-27
 * @version         1.0.0
 *
 */
class WPXUsersManagerUsers {

  /**
   * Return a singleton instance of WPXUsersManagerUsers class
   *
   * @brief Singleton
   *
   * @return WPXUsersManagerUsers
   */
  public static function init() {
    static $instance = null;
    if ( is_null( $instance ) ) {
      $instance = new WPXUsersManagerUsers();
    }
    return $instance;
  }

  /**
   * Create an instance of WPXUsersManagerUsers class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerUsers
   */
  private function __construct() {
    /* Extends Users List Table */
    add_filter( 'manage_users_columns', array( $this, 'manage_users_columns' ) );
    add_action( 'manage_users_custom_column', array( $this, 'manage_users_custom_column' ), 10, 3 );
    add_filter( 'manage_users_sortable_columns', array( $this, 'manage_users_sortable_columns' ) );
    add_action( 'pre_user_query', array( $this, 'pre_user_query' ) );

    /* Display the extra fields in the backend user profile; below wpdk */
    add_action( 'wpdk_users_show_user_profile', array( $this, 'wpdk_users_show_user_profile') );

    /* Update extra field in backend admin area */
    add_action( 'personal_options_update', array( $this, 'updateExtraFieldsProfile' ) );
    add_action( 'edit_user_profile_update', array( $this, 'updateExtraFieldsProfile' ) );
  }

  /**
   * Updated the extra fields from backend area profile view
   *
   * @brief Update profile
   *
   * @param int $user_id User id
   */
  public function updateExtraFieldsProfile( $user_id ) {
    /* Check extra fields. */
    $has_extra = WPXUsersManagerConfiguration::init()->extraFields->fields;
    if ( !empty( $has_extra ) ) {
      WPXUsersManagerExtraFields::init()->update( $user_id, $_POST );
    }
  }

  /**
   * Display the extra fields in the backend user profile
   *
   * @brief Extra fields
   *
   * @param WP_User $user WordPress user object
   */
  public function wpdk_users_show_user_profile( $user ) {
    /* Check extra fields. */
    $has_extra = WPXUsersManagerConfiguration::init()->extraFields->fields;
    if ( !empty( $has_extra ) ) {
      $url = WPDKMenu::url( 'WPXUsersManagerConfigurationProfileViewController' );
      $message              = sprintf( __( 'This view <strong>is enhanced</strong> by Users Manager plugin. You can manage additional information below by <a class="button button-primary" href="%s#extra_fields">click here</a>', WPXUSERSMANAGER_TEXTDOMAIN ), $url );
      $alert                = new WPDKTwitterBootstrapAlert( 'info', $message, WPDKTwitterBootstrapAlertType::SUCCESS );
      $alert->dismissButton = false;
      $alert->block         = true;
      $alert->display();

      $root = array(
        __( 'Extra Fields', WPXUSERSMANAGER_TEXTDOMAIN ) => array()
      );
      $extra_fields = WPXUsersManagerExtraFields::init()->fields( $root, $user->ID );

      $layout = new WPDKUIControlsLayout( $extra_fields );
      $layout->display();
    }
  }

  /**
   * Return the list table columns
   *
   * @brief Columns
   *
   * @param array $columns A key value pairs
   *
   * @return array
   */
  public function manage_users_columns( $columns ) {
    /* user_registered is a standard WordPress column field. */
    $columns['user_registered']                            = __( 'Signup', WPXUSERSMANAGER_TEXTDOMAIN );
    $columns[WPXUsersManagerUserMeta::TIMESTAMP_CONFIRMED] = __( 'Confirmed', WPXUSERSMANAGER_TEXTDOMAIN );
    return $columns;
  }

  /**
   * Render a content of column cell
   *
   * @brief Render cell
   *
   * @param mixed  $value       Raw data value
   * @param string $column_name Column name
   * @param int    $user_id     User ID
   *
   * @return int|mixed|string
   */
  public function manage_users_custom_column( $value, $column_name, $user_id ) {

    /* Get user for retrive user meta. */
    $user = new WPDKUser( $user_id );

    if ( 'user_registered' == $column_name ) {
      $user_registered = $user->get( $column_name );
      if ( !empty( $user_registered ) ) {
        $sanitize_date = WPDKDateTime::formatFromFormat( $user_registered, '', __( 'm/d/Y H:i:s', WPXUSERSMANAGER_TEXTDOMAIN ) );
        $value         = WPDKDateTime::timeNewLine( $sanitize_date );
      }
    }
    elseif ( WPXUsersManagerUserMeta::TIMESTAMP_CONFIRMED == $column_name ) {
      $date_signup = $user->get( $column_name );
      if ( !empty( $date_signup ) ) {
        $sanitize_date = date( __( 'm/d/Y H:i:s', WPXUSERSMANAGER_TEXTDOMAIN ), $date_signup );
        $value         = WPDKDateTime::timeNewLine( $sanitize_date );
      } else {
        $value = sprintf( '<strong>%s</strong>', __( 'Not confirmed', WPXUSERSMANAGER_TEXTDOMAIN) );
      }
    }

    return $value;
  }

  /**
   * Return the new sortable columns
   *
   * @brief Sortable columns
   *
   * @param array $columns Sortable columns array
   *
   * @return array
   */
  public function manage_users_sortable_columns( $columns ) {
    $columns[WPXUsersManagerUserMeta::TIMESTAMP_CONFIRMED] = WPXUsersManagerUserMeta::TIMESTAMP_CONFIRMED;
    return $columns;
  }

  /**
   * If no order is set by GET then se the default to user_register date in DESC.
   * Added order for other custom column and user meta.
   *
   * @brief Alter query
   *
   * @param object $query
   */
  public function pre_user_query( $query ) {
    global $wpdb, $current_screen;

    if ( !is_object( $current_screen ) ) {
      return;
    }

    if ( 'users' != $current_screen->id ) {
      return;
    }

    /* Do sorting... */
    if ( WPXUsersManagerUserMeta::TIMESTAMP_CONFIRMED == $query->query_vars['orderby'] ) {
      $query->query_from .= " LEFT JOIN {$wpdb->usermeta} usermeta ON {$wpdb->users}.ID = usermeta.user_id AND (usermeta.meta_key = '" . WPXUsersManagerUserMeta::TIMESTAMP_CONFIRMED . "')";
      $query->query_orderby = ' ORDER BY usermeta.meta_value ' . $query->query_vars['order'];
    }

    if ( !isset( $_REQUEST['orderby'] ) ) {
      /* By default the user list is sorted for user_registered field. */
      $query->query_orderby = 'ORDER BY user_registered DESC';
    }
  }
}

