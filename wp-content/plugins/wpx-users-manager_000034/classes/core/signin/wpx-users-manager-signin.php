<?php
/**
 * Signin model
 *
 * ## Overview
 *
 * Description
 *
 * @class           WPXUsersManagerSignin
 * @author          =undo= <info@wpxtre.me>
 * @copyright       Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date            2013-02-19
 * @version         1.0.0
 *
 */
class WPXUsersManagerSignin {

  /**
   * Create and return a singleton instance of WPXUsersManagerSignin class
   *
   * @brief Init
   *
   * @return WPXUsersManagerSignin
   */
  public static function init() {
    static $instance = null;
    if ( is_null( $instance ) ) {
      $instance = new WPXUsersManagerSignin();
    }
    return $instance;
  }

  /**
   * Create an instance of WPXUsersManagerSignin class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerSignin
   */
  public function __construct() {
  }

  /**
   * Do signin
   *
   * @brief Signin
   */
  public function signin() {
    $remember = isset( $_POST['remember'] );
    $email    = sanitize_email( $_POST['user_email'] );
    $password = $_POST['user_password'];
    $result   = WPDKUsers::init()->signIn( $email, $password, $remember );
    if ( false === $result ) {
      do_action( 'wpx_users_manager_signin_error' );
      add_filter( 'wpx_users_manager_before_signin_form', array( $this, 'wpx_users_manager_before_signin_form' ) );
    }
    else {
      do_action( 'wpx_users_manager_signin_successfully' );

      /* @todo Si potrebbe mettere come configurazione. */
      /* Per default ridirezione sulla home page del sito. */
      $blog_url = get_bloginfo( 'url' );
      wp_safe_redirect( $blog_url );
      exit();
    }
  }

  /**
   * Return an alert content to prepend to the form view
   *
   * @param string $content Content to filter
   *
   * @return string
   */
  public function wpx_users_manager_before_signin_form( $content ) {
    /* @todo Personalizzare messaggio di fallimento. */
    $message = __( '<h4>Warning!</h4><p>Signin failed. Please check your email and password</p>', WPXUSERSMANAGER_TEXTDOMAIN );
    $alert   = new WPDKTwitterBootstrapAlert( 'signin', $message, WPDKTwitterBootstrapAlertType::ALERT );
    $content = $alert->html();
    return $content;
  }
}


/**
 * Signin view
 *
 * @class              WPXUsersManagerSigninView
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-19
 * @version            0.9.2
 *
 */
class WPXUsersManagerSigninView extends WPDKView {

  /**
   * Create an instance of WPXUsersManagerSigninView class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerSigninView
   */
  public function __construct() {
    parent::__construct( 'wpx-users-manager-signin' );
  }

  /**
   * Return the layout fields array
   *
   * @brief Fields
   *
   * @return array
   */
  public function fields() {

    /* Fields. */
    $fields = array(
      __( 'Access data', WPXUSERSMANAGER_TEXTDOMAIN ) => array(

        array(
          'type'  => WPDKUIControlType::HIDDEN,
          'name'  => WPXUsersManagerPostDataAction::ID,
          'value' => WPXUsersManagerPostDataAction::SIGNIN
        ),

        array(
          array(
            'type'     => WPDKUIControlType::TEXT,
            'name'     => 'user_email',
            'label'    => __( 'Email', WPXUSERSMANAGER_TEXTDOMAIN ),
            'required' => 'required'
          ),
        ),

        array(
          array(
            'type'         => WPDKUIControlType::PASSWORD,
            'name'         => 'user_password',
            'autocomplete' => 'on',
            'label'        => __( 'Password', WPXUSERSMANAGER_TEXTDOMAIN ),
            'required'     => 'required'
          )
        ),

        wpdk_is_bool( WPXUsersManagerConfiguration::init()->signin->remember ) ? array(
          array(
            'type'  => WPDKUIControlType::CHECKBOX,
            'name'  => 'remember',
            'label' => __( 'Remember', WPXUSERSMANAGER_TEXTDOMAIN )
          )
        ) : '',

        array(
          array(
            'type'  => WPDKUIControlType::SUBMIT,
            'name'  => 'signin',
            'class' => 'btn btn-primary pull-right',
            'value' => __( 'Signin', WPXUSERSMANAGER_TEXTDOMAIN )
          ),
        ),

      ),
    );
    return $fields;
  }

  /**
   * Display the form
   *
   * @brief Display
   */
  public function draw() {

    /* Create a nonce key. */
    $nonce                     = md5( $this->id );
    $input_hidden_nonce        = new WPDKHTMLTagInput( '', $nonce, $nonce );
    $input_hidden_nonce->type  = WPDKHTMLTagInputType::HIDDEN;
    $input_hidden_nonce->value = wp_create_nonce( $this->id );

    /* Layout fields. */
    $layout = new WPDKUIControlsLayout( $this->fields() );

    /* Form. */
    $form         = new WPDKHTMLTagForm( $input_hidden_nonce->html() . $layout->html()  );
    $form->name   = 'wpx-users-manager-signin';
    $form->id     = $form->name;
    $form->method = 'post';
    $form->action = '';
    $form->display();
  }

}