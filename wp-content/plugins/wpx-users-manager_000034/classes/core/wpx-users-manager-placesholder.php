<?php
/**
 * This class contains the definition of user placeholder for the Users manager.
 *
 * @class              WPXUsersManagerPlaceholder
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-18
 * @version            0.12.1
 */
class WPXUsersManagerPlaceholder {

  const DOUBLE_OPTIN_ACTIVATION_URL = '${DOUBLE_OPTIN_ACTIVATION_URL}';
  const USER_PASSWORD_RESET_URL     = '${USER_PASSWORD_RESET_URL}';

  /**
   * Return a key values pais array with the list of placehodlers
   *
   * @brief Placeholders list
   *
   * @return array
   */
  public static function placeholders()
  {
    $placeholders = array(
      self::DOUBLE_OPTIN_ACTIVATION_URL => array(
        __( 'Double Opt-In activation URL', WPXUSERSMANAGER_TEXTDOMAIN ),
        'Users Manager'
      ),
      self::USER_PASSWORD_RESET_URL     => array(
        __( 'URL to reset user password', WPXUSERSMANAGER_TEXTDOMAIN ),
        'Users Manager'
      )
    );

    return apply_filters( 'wpx_users_manager_placeholders', $placeholders );
  }
}


/**
 * Description
 *
 * ## Overview
 *
 * Description
 *
 * @class           WPXUsersManagerPlaceholderScreenHelp
 * @author          =undo= <info@wpxtre.me>
 * @copyright       Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date            2013-03-22
 * @version         1.0.0
 *
 */
class WPXUsersManagerPlaceholderScreenHelp extends WPDKScreenHelp {

  /**
   * Create and return a singleton instance of WPXUsersManagerPlaceholderScreenHelp class when the global $typenow is
   * `page` or `post`
   *
   * @brief Init
   *
   * @return WPXUsersManagerPlaceholderScreenHelp
   */
  public static function init()
  {
    global $typenow;

    static $instance = null;
    if ( in_array( $typenow, array( 'post', 'page' ) ) ) {
      if ( is_null( $instance ) ) {
        $instance = new WPXUsersManagerPlaceholderScreenHelp();
      }
    }
    return $instance;
  }

  /**
   * Used this method to add tab and sidebar to the screen
   *
   * @brief Display
   * @note  To override
   */
  public function display()
  {
    $this->addTab( __( 'Users Manager', WPXUSERSMANAGER_TEXTDOMAIN ), array( $this, 'placeHolders' ) );
  }

  // TODO
  public function placeHolders()
  {
    ?>

    <style type="text/css">
      #wpx-users-manager-help-placeholder
      {
      }

      #wpx-users-manager-help-placeholder thead th
      {
        border-bottom  : 1px solid #aaa;
        text-align     : left;
        color          : #6C6C8D;
        padding-bottom : 8px;
        font-size      : 13px;
      }

      #wpx-users-manager-help-placeholder thead th:last-child
      {
        text-align : center;
      }

      #wpx-users-manager-help-placeholder tbody td
      {
        border-bottom : 1px solid #ddd;
      }

      #wpx-users-manager-help-placeholder tbody td:nth-child(2)
      {
        background-color : #FFFFEC;
        border-left      : 1px solid #ddd;
        padding-left     : 12px;
      }

      #wpx-users-manager-help-placeholder tbody td:last-child
      {
        text-align  : center;
        font-weight : bold;
        border-left : 1px solid #ddd;
      }
    </style>
    <h3><?php _e( 'Placeholder', WPXUSERSMANAGER_TEXTDOMAIN ) ?></h3>
    <p><?php _e( 'If you are writing this post in order to use it as a mail, you can use following placeholders which will be replaced by matching values.', WPXUSERSMANAGER_TEXTDOMAIN ) ?></p>

    <table id="wpx-users-manager-help-placeholder" width="100%" border="0" cellpadding="0" cellspacing="0">
      <thead>
      <tr>
        <th><?php _e( 'Placeholder', WPXUSERSMANAGER_TEXTDOMAIN ) ?></th>
        <th><?php _e( 'Description', WPXUSERSMANAGER_TEXTDOMAIN ) ?></th>
        <th><?php _e( 'Owner', WPXUSERSMANAGER_TEXTDOMAIN ) ?></th>
      </tr>
      </thead>
      <tbody>
      <?php foreach ( WPXUsersManagerPlaceholder::placeholders() as $key => $value ) : ?>
        <tr>
          <td><code><?php echo $key ?></code></td>
          <td><?php echo $value[0] ?></td>
          <td><?php echo $value[1] ?></td>
        </tr>
      <?php endforeach; ?>
      </tbody>
    </table>

  <?php
  }

}