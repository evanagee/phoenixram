<?php
/**
 * Extra fields model
 *
 * ## Overview
 *
 * Description
 *
 * @class           WPXUsersManagerExtraFields
 * @author          =undo= <info@wpxtre.me>
 * @copyright       Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date            2013-03-21
 * @version         1.0.0
 *
 */
class WPXUsersManagerExtraFields {

  /**
   * Used to prepend, replace or append to content
   *
   * @brief Alert
   *
   * @var WPDKTwitterBootstrapAlert $alert
   */
  public $alert;

  /**
   * Create and return a singleton instance of WPXUsersManagerExtraFields class
   *
   * @brief Init
   *
   * @return WPXUsersManagerExtraFields
   */
  public static function init() {
    static $instance = null;
    if ( is_null( $instance ) ) {
      $instance = new WPXUsersManagerExtraFields();
    }
    return $instance;
  }

  /**
   * Create an instance of WPXUsersManagerExtraFields class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerExtraFields
   */
  public function __construct() {
    /* Add extra fields in signup by filters. */
    add_filter( '_wpx_users_manager_signup_extra_fields', array( $this, 'fields' ), 10, 2 );

    /* Add extra fields in profile by filters. */
    add_filter( '_wpx_users_manager_profile_extra_fields', array( $this, 'fields' ), 10, 2 );

  }

  /**
   * Return a key value pairs array (in CLA format) with extra fields
   *
   * @brief Extra fields
   *
   * @param array $root    The CLA array root
   * @param int   $id_user Optional. User id
   *
   * @return array
   */
  public function fields( $root, $id_user = 0 ) {
    $extra_fields = WPXUsersManagerConfiguration::init()->extraFields->fields;
    $fields       = & $root[key( $root )];
    foreach ( $extra_fields as $field ) {

      /* Get the stored value. */
      if( !empty( $id_user ) ) {
        $value = get_user_meta( $id_user, $field['name'], true );
        $value = apply_filters( 'wpx_users_manager_get_extra_field_' . $field['name'], $value );
      }

      switch ( $field['type'] ) {
        case WPDKUIControlType::SECTION:
          $fields                  = & $root;
          $fields[$field['label']] = array();
          $fields                  = & $fields[$field['label']];
          break;

        case WPDKUIControlType::SELECT:
          /* Convert preset value in array for options. Value con be a string or a jSON string. */
          $string_options = stripslashes( $field['value'] );

          if ( null != json_decode( $string_options ) ) {
            $options = (array)json_decode( $string_options );
            if ( !is_array( $options ) ) {
              $options = array();
            }
          }
          else {
            $options = explode( ',', $string_options );
          }
          $field['options'] = apply_filters( 'wpx_users_manager_extra_field_options_' . $field['name'], $options, $field );
          $field['value']   = $value;
          $fields[]         = array( $field );
          break;

        case WPDKUIControlType::CHECKBOX:
          if( !empty( $value) ) {
            $field['checked'] = $value;
          }
          $fields[] = array( $field );
          break;

        default:
          if( !empty( $value) ) {
            $field['value'] = $value;
          }
          $fields[] = array( $field );
          break;
      }
    }
    return $root;
  }

  /**
   * Update the extra fields for user
   *
   * @brief Update
   *
   * @param int $user_id User id
   */
  public function update( $user_id, $post_data ) {
    $extra_fields = WPXUsersManagerConfiguration::init()->extraFields->fields;
    if ( !empty( $extra_fields ) ) {
      foreach ( $extra_fields as $field ) {
        if ( isset( $field['name'] ) ) {
          /* Check post here because for the checkbox, for example, we have to delete anyway */
          $name = $field['name'];
          /* Delete previous store version */
          delete_user_meta( $user_id, $name );

          /* Update data */
          if ( isset( $post_data[$name] ) && !empty( $post_data[$name] ) ) {
            $value = apply_filters( 'wpx_users_manager_update_extra_field_' . $name, $post_data[$name] );
            update_user_meta( $user_id, $name, $value );
          }
        }
      }
    }
  }

}