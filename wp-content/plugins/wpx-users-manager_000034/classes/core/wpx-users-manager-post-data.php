<?php
/**
 * Description
 *
 * ## Overview
 *
 * Description
 *
 * @class           WPXUsersManagerPostData
 * @author          =undo= <info@wpxtre.me>
 * @copyright       Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date            2013-02-19
 * @version         1.0.0
 *
 */
class WPXUsersManagerPostDataAction {
  /**
   * Name of input tyoe hidden field
   *
   * @brief Name
   */
  const ID = 'wpx_users_manager_action';

  const SIGNIN                 = 'signin';
  const SIGNUP                 = 'signup';
  const PROFILE                = 'profile';
  const RESET_PASSWORD         = 'reset_password';
  const RESEND_UNLOCK          = 'resend_unlock';
}


/**
 * Process Post data for signin, signup, profile, etc...
 *
 * ## Overview
 *
 * This class instance is init on WordPress `init` action (see construct in WPXUsersmanager)
 *
 * @class           WPXUsersManagerPostData
 * @author          =undo= <info@wpxtre.me>
 * @copyright       Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date            2013-02-19
 * @version         1.0.0
 *
 */
class WPXUsersManagerPostData {

  /**
   * Create and return a singleton instance of WPXUsersManagerPostData class
   *
   * @brief Init
   *
   * @return WPXUsersManagerPostData
   */
  public static function init() {
    static $instance = null;
    if ( is_null( $instance ) ) {
      $instance = new WPXUsersManagerPostData();
    }
    return $instance;
  }

  /**
   * Create an instance of WPXUsersManagerPostData class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerPostData
   */
  private function __construct() {
    $this->process();
  }

  /**
   * Process the post data
   *
   * @brief Process
   */
  public function process() {
    /* Check post data. */
    $action = esc_attr( isset( $_POST[WPXUsersManagerPostDataAction::ID] ) ? $_POST[WPXUsersManagerPostDataAction::ID] : '' );

    if ( !empty( $action ) ) {
      switch ( $action ) {
        case WPXUsersManagerPostDataAction::SIGNUP:
           WPXUsersManagerSignup::init()->signup();
          break;

        case WPXUsersManagerPostDataAction::SIGNIN:
           WPXUsersManagerSignin::init()->signin();
          break;

        case WPXUsersManagerPostDataAction::RESET_PASSWORD:
           WPXUsersManagerResetPassword::init()->send();
          break;

        case WPXUsersManagerPostDataAction::PROFILE:
           WPXUsersManagerProfile::init()->update();
          break;

        case WPXUsersManagerPostDataAction::RESEND_UNLOCK:
           WPXUsersManagerUnlockCode::init()->send();
          break;
      }
    }

    /* Check for Double Opt-In unlock code; GET */
    if ( isset( $_GET['uct'] ) && !empty( $_GET['uct'] ) ) {
      WPXUsersManagerSignup::init()->completeSignupWithUnlockCodeToken( $_GET['uct'] );
    }
  }
}