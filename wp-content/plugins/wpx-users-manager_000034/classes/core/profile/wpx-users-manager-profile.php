<?php
/**
 * Profile model
 *
 * ## Overview
 *
 * Description
 *
 * @class           WPXUsersManagerProfile
 * @author          =undo= <info@wpxtre.me>
 * @copyright       Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date            2013-02-19
 * @version         1.0.0
 *
 */
class WPXUsersManagerProfile {

  /**
   * Used to prepend, replace or append to content
   *
   * @brief Alert
   *
   * @var WPDKTwitterBootstrapAlert $alert
   */
  public $alert;

  /**
   * Create and return a singleton instance of WPXUsersManagerProfile class
   *
   * @brief Init
   *
   * @return WPXUsersManagerProfile
   */
  public static function init() {
    static $instance = null;
    if ( is_null( $instance ) ) {
      $instance = new WPXUsersManagerProfile();
    }
    return $instance;
  }

  /**
   * Create an instance of WPXUsersManagerProfile class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerProfile
   */
  public function __construct() {
    /* Feedback */
    add_filter( 'wpx_users_manager_before_profile_form', array( $this, 'feedback' ) );
  }

  /**
   * Update an user profile
   *
   * @brief Update profile
   */
  public function update() {

    /* Prepare update array. */
    $userdata = array();

    /* Case reset password. */
    if ( false === ( $reset_password = WPXUsersManagerResetPassword::init()->isPasswordReset() ) ) {
      $userdata = array(
        'first_name' => esc_attr( $_POST['first_name'] ),
        'last_name'  => esc_attr( $_POST['last_name'] ),
        'user_email' => sanitize_email( $_POST['email'] ),
      );
    }

    /* This key is mandatory. */
    $userdata['ID'] = $user_id = absint( esc_attr( $_POST['user_id'] ) );

    /* Updated password too? */
    if ( !empty( $_POST['password'] ) ) {
      $userdata['user_pass'] = esc_attr( $_POST['password'] );
    }
    /* If reset password mode is on the password must be valid! */
    elseif ( $reset_password ) {
      /* @todo Display alert. */
      $message = __( '<h4>Error!</h4><p>Password field cannot be empty!</p>', WPXUSERSMANAGER_TEXTDOMAIN );
      $this->alert   = new WPDKTwitterBootstrapAlert( 'profile', $message, WPDKTwitterBootstrapAlertType::ALERT );
    }

    $result = wp_update_user( $userdata );
    if ( $result != $user_id ) {
      $message = __( '<h4>Warning!</h4><p>Error while updating user profile.</p>', WPXUSERSMANAGER_TEXTDOMAIN );
      $this->alert   = new WPDKTwitterBootstrapAlert( 'profile', $message, WPDKTwitterBootstrapAlertType::ALERT );
    }
    else {
      /* Destroy transient if reset password is on */
      WPXUsersManagerResetPassword::init()->deleteToken();

      /* Update Extra fields */
      WPXUsersManagerExtraFields::init()->update( $user_id, $_POST );

      $message = __( '<h4>Successfully!</h4><p>Your profile has been successfully updated!</p>', WPXUSERSMANAGER_TEXTDOMAIN );
      $this->alert   = new WPDKTwitterBootstrapAlert( 'profile', $message, WPDKTwitterBootstrapAlertType::SUCCESS );
    }
  }

  /**
   * Display a feedback result as alert
   *
   * @brief Display alert
   */
  public function feedback( $content ) {
    if ( !empty( $this->alert ) ) {
      $content = $this->alert->html();
    }
    return $content;
  }

}


/**
 * Profile frontend form layout
 *
 * @class              WPXUsersManagerProfileView
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-19
 * @version            0.9.2
 *
 */
class WPXUsersManagerProfileView extends WPDKView {

  /**
   * Alert used to feedback before form
   *
   * @brief Alert
   *
   * @var WPDKTwitterBootstrapAlert $alert
   */
  private $alert = null;

  /**
   * Create an instance of WPXUsersManagerProfileView class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerProfileView
   */
  public function __construct() {
    parent::__construct( 'wpx-users-manager-profile' );

    /* Init the extra fields model */
    WPXUsersManagerExtraFields::init();

    /* Feedback */
    add_filter( 'wpx_users_manager_before_profile_form', array( $this, 'feedback' ) );

    $error = WPXUsersManagerResetPassword::init()->isPasswordReset();
    if ( is_wpdk_error( $error ) ) {
      $message     = __( '<h4>Warnig!</h4><p>You requested a password reset with an invalid token!</p>', WPXUSERSMANAGER_TEXTDOMAIN );
      $this->alert = new WPDKTwitterBootstrapAlert( 'reset-password', $message, WPDKTwitterBootstrapAlertType::ALERT );
    }
  }

  /**
   * Display feedback before form
   *
   * @brief Feedback
   *
   * @param string $content Original content
   *
   * @return string
   */
  public function feedback( $content ) {
    if ( $this->alert ) {
      $content = $this->alert->html();
    }
    return $content;
  }

  /**
   * Return the layout fields array
   *
   * @brief Fields
   *
   * @return array
   */
  private function fields() {

    /* Default is current user. */
    $id_user = 0;

    /* Case reset password. */
    $disabled = false;
    if ( true === ( $error = WPXUsersManagerResetPassword::init()->isPasswordReset() ) ) {
      $id_user  = WPXUsersManagerResetPassword::init()->userID;
      $disabled = true;
    }

    /* Get the current user. */
    $user = new WPDKUser( $id_user );

    $fields = array(
      __( 'Your profile', WPXUSERSMANAGER_TEXTDOMAIN ) => array(

        array(
          'type'  => WPDKUIControlType::HIDDEN,
          'name'  => WPXUsersManagerPostDataAction::ID,
          'value' => WPXUsersManagerPostDataAction::PROFILE
        ),

        array(
          'type'  => WPDKUIControlType::HIDDEN,
          'name'  => 'user_id',
          'value' => $user->ID
        ),

        /* Name, last name and email */
        array(
          array(
            'type'     => WPDKUIControlType::TEXT,
            'name'     => 'first_name',
            'value'    => $user->first_name,
            'label'    => __( 'First name', WPXUSERSMANAGER_TEXTDOMAIN ),
            'disabled' => $disabled,
          )
        ),
        array(
          array(
            'type'     => WPDKUIControlType::TEXT,
            'name'     => 'last_name',
            'value'    => $user->last_name,
            'label'    => __( 'Last name', WPXUSERSMANAGER_TEXTDOMAIN ),
            'disabled' => $disabled,
          )
        ),
        array(
          array(
            'type'     => WPDKUIControlType::TEXT,
            'name'     => 'email',
            'value'    => $user->email,
            'data'     => array( 'placement' => 'right' ),
            'title'    => '',
            'label'    => __( 'Email', WPXUSERSMANAGER_TEXTDOMAIN ),
            'disabled' => $disabled,
          )
        ),

        /* Password */
        array(
          array(
            'type'   => WPDKUIControlType::PASSWORD,
            'label'  => __( 'Password', WPXUSERSMANAGER_TEXTDOMAIN ),
            'attrs'  => array(
              'autocomplete' => 'off',
              'autofocus'    => $disabled
            ),
            'name'   => 'password',
            'append' => $disabled ? __( 'Reset your password', WPXUSERSMANAGER_TEXTDOMAIN ) : __( 'Leave blank to unchange your password', WPXUSERSMANAGER_TEXTDOMAIN )
          )
        ),
        array(
          array(
            'type'  => WPDKUIControlType::PASSWORD,
            'label' => __( 'Repeat Password', WPXUSERSMANAGER_TEXTDOMAIN ),
            'name'  => 'password_repeat'
          )
        ),
      ),
    );

    $fields = apply_filters( '_wpx_users_manager_profile_extra_fields', $fields, $user->ID );

    end( $fields );
    $fields[key( $fields )][] = array(
      array(
        'type'  => WPDKUIControlType::SUBMIT,
        'name'  => 'update',
        'class' => 'btn btn-primary pull-right',
        'value' => __( 'Update', WPXUSERSMANAGER_TEXTDOMAIN )
      ),
    );

    reset( $fields );

    return apply_filters( 'wpx_users_manager_profile_extra_fields', $fields, $user->ID );
  }

  /**
   * Display the form
   *
   * @brief Display
   */
  public function draw() {

    /* Create a nonce key. */
    $nonce                     = md5( $this->id );
    $input_hidden_nonce        = new WPDKHTMLTagInput( '', $nonce, $nonce );
    $input_hidden_nonce->type  = WPDKHTMLTagInputType::HIDDEN;
    $input_hidden_nonce->value = wp_create_nonce( $this->id );

    /* Layout fields. */
    $layout = new WPDKUIControlsLayout( $this->fields() );

    /* Form. */
    $form         = new WPDKHTMLTagForm( $input_hidden_nonce->html() . $layout->html()  );
    $form->name   = 'wpx-users-manager-profile';
    $form->id     = $form->name;
    $form->method = 'post';
    $form->action = '';
    $form->display();
  }

}