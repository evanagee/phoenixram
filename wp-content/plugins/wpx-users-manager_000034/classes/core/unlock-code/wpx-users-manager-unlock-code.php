<?php
/**
 * Manage unlock code model
 *
 * ## Overview
 *
 * The unlock code is a special token sent to the register (disable) user. This code is a token store in the user meta
 * database table. An user must be unlock this code to confirm registration. This class manage the unlock code when the
 * user register himself and when a resend unlock code is perform.
 *
 * When an user require a registration he goes into user database in status disable. Into the user meta a unlock code
 * token is create for that user id. After create the user the `send()` method is called with user email as param.
 *
 * @class           WPXUsersManagerUnlockCode
 * @author          =undo= <info@wpxtre.me>
 * @copyright       Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date            2013-02-20
 * @version         1.0.0
 *
 */
class WPXUsersManagerUnlockCode {

  /**
   * An useful instance of alert
   *
   * @brief Alert feedback
   *
   * @var WPDKTwitterBootstrapAlert $feedback
   */
  public $feedback;

  /**
   * Create an instance of WPXUsersManagerUnlockCode class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerUnlockCode
   */
  public function __construct() {
  }

  /**
   * Create and return a singleton instance of WPXUsersManagerUnlockCode class
   *
   * @brief Init
   *
   * @return WPXUsersManagerUnlockCode
   */
  public static function init() {
    static $instance = null;
    if ( is_null( $instance ) ) {
      $instance = new WPXUsersManagerUnlockCode();
    }
    return $instance;
  }

  /**
   * Return TRUE if the mail within unlock code url is sent successfully.
   *
   * @brief Send unlock code via mail
   *
   * @param string Optional. The user email where send the unlock code url
   *
   * @return bool
   */
  public function send( $email = null ) {

    /* Useful bool to switch fromd directly send and form post send. */
    $from_post = is_null( $email );

    if ( $from_post ) {
      /* Prepare feedback. */
      add_filter( 'wpx_users_manager_before_resend_unlock_url_form', array( $this, 'feedback' ) );
    }

    /* Work directly of via POST data. */
    $user_email = sanitize_email( $from_post ? $_POST['user_email'] : $email );

    if ( !empty( $user_email ) ) {

      /* User exists? */
      $id_user = email_exists( $user_email );

      if ( false !== $id_user ) {

        /* Get this user instance. */
        $user = new WPDKUser( $id_user );

        /* Check if exists a unlock code. */
        $unlock_code_token = get_user_meta( $id_user, WPXUsersManagerUserMeta::UNLOCK_CODE_TOKEN, true );

        if ( WPDKUserStatus::DISABLED == $user->status || !empty( $unlock_code_token ) ) {
          /* Send/Resend unlock code. */
          $mail_slug      = WPXUsersManagerConfiguration::init()->signup->mail_slug_confirm;
          $mail_post_type = WPXUsersManagerConfiguration::init()->signup->mail_slug_confirm_post_type;
          $mail           = new WPDKMail( $mail_slug, $mail_post_type );

          /* Build the unlock_code_url. */
          $unlock_code_url = $this->unlockCodeURL( $unlock_code_token );

          $mail->send( $id_user, false, false, array( WPXUsersManagerPlaceholder::DOUBLE_OPTIN_ACTIVATION_URL => $unlock_code_url ) );

          if ( $from_post ) {
            /* Aggiungo al posto del form di reset password un feedback visivo. */
            $message        = __( '<h4>Resend unlock code URL successfully!</h4><p>Please, check your mail and follow instructions to complete.</p>', WPXUSERSMANAGER_TEXTDOMAIN );
            $this->feedback = new WPDKTwitterBootstrapAlert( 'unlock-code', $message, WPDKTwitterBootstrapAlertType::SUCCESS );
          }
          return true;
        }
        /* No unlock found. */
        elseif ( $from_post ) {
          $message        = __( '<h4>Sorry!</h4><p>Seems that your email is wrong! Please, check your data.</p>' . WPXTREME_SUPPORT, WPXUSERSMANAGER_TEXTDOMAIN );
          $this->feedback = new WPDKTwitterBootstrapAlert( 'unlock-code', $message, WPDKTwitterBootstrapAlertType::ALERT );
        }
      }
      /* Wrong email. This email address doesn't exists in user database. */
      elseif ( $from_post ) {
        $message        = __( '<h4>Sorry!</h4><p>It is not possible resend the unlock code. Please, check your data.</p>' . WPXTREME_SUPPORT, WPXUSERSMANAGER_TEXTDOMAIN );
        $this->feedback = new WPDKTwitterBootstrapAlert( 'unlock-code', $message, WPDKTwitterBootstrapAlertType::ALERT );
      }
    }
    /* Email address empty. */
    elseif ( $from_post ) {
      $message        = __( '<h4>Ops!</h4><p>Email\'s field is empty</p>', WPXUSERSMANAGER_TEXTDOMAIN );
      $this->feedback = new WPDKTwitterBootstrapAlert( 'unlock-code', $message, WPDKTwitterBootstrapAlertType::ALERT );
    }
    return false;
  }

  /**
   * Return the unlock code url from unlock code token. FALSE otherwise.
   * The unlock code url is the registration page with `uct` GET parameter set.
   *
   *     For instance http://mydomain.com/singup/?uct=[unlock code token]
   *
   * @param string $uct The unlock code token
   *
   * @return bool|string
   */
  public function unlockCodeURL( $uct ) {
    if ( !empty( $uct ) ) {
      $profile_slug = WPXUsersManagerConfiguration::init()->signup->page_registration_slug;
      if ( !empty( $profile_slug ) ) {
        $profile_url = wpdk_permalink_page_with_slug( $profile_slug );
        if ( !empty( $profile_url ) ) {
          $unlock_url = sprintf( '%s?uct=%s', trailingslashit( $profile_url ), $uct );
          return $unlock_url;
        }
      }
    }
    return false;
  }

  /**
   * Display a feedback result as alert
   *
   * @brief Display alert
   */
  public function feedback( $content ) {
    if ( !empty( $this->feedback ) ) {
      $content = $this->feedback->html();
    }
    return $content;
  }

  /**
   * Create and return an unlock code token for an user. FALSE otherwise.
   * The unlock code token is a md5 of [email][random alphanumber 16char]
   *
   * @brief Create token
   *
   * @param string $email User email
   *
   * @return string|bool
   */
  public function createUnlockCodeToken( $email ) {
    if ( !empty( $email ) ) {
      $uct = sprintf( '%s', md5( $email . WPDKCrypt::randomAlphaNumber( 16 ) ) );
      return $uct;
    }
    return false;
  }

}

/**
 * Resend unlock code url for Double Opt-In frontend form layout
 *
 * @class              WPXUsersManagerUnlockCodeView
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-19
 * @version            0.9.2
 *
 */
class WPXUsersManagerUnlockCodeView extends WPDKView {

  /**
   * Create an instance of WPXUsersManagerUnlockCodeView class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerUnlockCodeView
   */
  public function __construct() {
    parent::__construct( 'wpx-users-manager-unlock-code' );
  }

  /**
   * Display the form
   *
   * @brief Display
   */
  public function draw() {

    /* Create a nonce key. */
    $nonce                     = md5( $this->id );
    $input_hidden_nonce        = new WPDKHTMLTagInput( '', $nonce, $nonce );
    $input_hidden_nonce->type  = WPDKHTMLTagInputType::HIDDEN;
    $input_hidden_nonce->value = wp_create_nonce( $this->id );

    /* Layout fields. */
    $layout = new WPDKUIControlsLayout( $this->fields() );

    /* Form. */
    $form         = new WPDKHTMLTagForm( $input_hidden_nonce->html() . $layout->html() );
    $form->name   = 'wpx-users-manager-unlock-code';
    $form->id     = $form->name;
    $form->method = 'post';
    $form->action = '';
    $form->display();
  }

  /**
   * Return the layout fields array
   *
   * @brief Fields
   *
   * @return array
   */
  private function fields() {
    $fields = array(
      __( 'Did not receive any unlock instruction?', WPXUSERSMANAGER_TEXTDOMAIN ) => array(

        array(
          'type'  => WPDKUIControlType::HIDDEN,
          'name'  => WPXUsersManagerPostDataAction::ID,
          'value' => WPXUsersManagerPostDataAction::RESEND_UNLOCK
        ),

        __( 'Forgot mail?! No worries, enter your email address into the field below in order to receive another unlock code mail.', WPXUSERSMANAGER_TEXTDOMAIN ),

        array(
          array(
            'type'  => WPDKUIControlType::TEXT,
            'name'  => 'user_email',
            'label' => __( 'Email', WPXUSERSMANAGER_TEXTDOMAIN )
          ),
          array(
            'type'  => WPDKUIControlType::SUBMIT,
            'name'  => 'button_resend_unlock_url',
            'class' => 'btn btn-success wpdk-disable-after-click',
            'value' => __( 'Resend Unlock Code', WPXUSERSMANAGER_TEXTDOMAIN )
          )
        ),
      ),
    );

    return $fields;
  }
}