<?php
/**
 * Register the Users Manager shortcode for WordPress
 *
 * @class              WPXUsersManagerShortcode
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-19
 * @version            0.9.2
 */
class WPXUsersManagerShortcode extends WPDKShortcode {

  /**
   * Create or return a singleton instance of WPXUsersManagerShortcode
   *
   * @brief Create or return a singleton instance of WPXUsersManagerShortcode
   *
   * @return WPXUsersManagerShortcode
   */
  public static function getInstance() {
    static $instance = null;
    if ( is_null( $instance ) ) {
      $instance = new WPXUsersManagerShortcode();
    }
    return $instance;
  }

  /**
   * Alias of getInstance();
   *
   * @brief Init the shortcode register
   *
   * @return WPXUsersManagerShortcode
   */
  public static function init() {
    return self::getInstance();
  }

  /**
   * Return a Key value pairs array with key as shortcode name and value TRUE/FALSE for turn on/off the shortcode.
   *
   * @brief List of allowed shorcode
   *
   * @return array Shortcode array
   */
  protected function shortcodes() {
    $shortcodes = array(
      'wpx_users_manager_signin'            => true,
      'wpx_users_manager_reset_password'    => true,
      'wpx_users_manager_resend_unlock_url' => true,
      'wpx_users_manager_signup'            => true,
      'wpx_users_manager_profile'           => true,
    );
    return $shortcodes;
  }


  // -----------------------------------------------------------------------------------------------------------------
  // Shortcode methods
  // -----------------------------------------------------------------------------------------------------------------

  /**
   * Display the user signin form into a post/page. If the user is logged nothing is displayed.
   * If you reload the page when submit the form, this does the log-in procedure.
   *
   * @brief Display user signin
   *
   * @param array       $attrs   Attribute into the shortcode
   * @param null|string $content Optional. HTML content
   *
   * @return bool|string
   */
  public function wpx_users_manager_signin( $attrs, $content = null ) {

    /**
     * Get the signin configuration
     *
     * @brief Signin
     *
     * @var WPXUsersManagerConfigurationSignin $signin
     */
    $signin = WPXUsersManagerConfiguration::init()->signin;

    /* Exit if logged in. */
    if ( is_user_logged_in() ) {
      $message = __( '<h4>Information!</h4><p>You are logged in! You have to signout in order to make a signin.</p>', WPXUSERSMANAGER_TEXTDOMAIN );
      $alert   = new WPDKTwitterBootstrapAlert( 'wpx-users-manager-alert', $message, WPDKTwitterBootstrapAlertType::INFORMATION );
      $alert->dismissButton = false;
      return $alert->html();
    }

    /* If the configuration is not properly set, then an alert is display. */
    if ( empty( $signin->page_slug ) ) {
      $message = __( '<h4>Warning!</h4><p>You have to set the signin page slug in Users Manager configuration backend area.</p>', WPXUSERSMANAGER_TEXTDOMAIN );
      $alert   = new WPDKTwitterBootstrapAlert( 'wpx-users-manager-alert', $message, WPDKTwitterBootstrapAlertType::ALERT );
      $alert->dismissButton = false;
      return $alert->html();
    }

    /* Grab the content. */
    $content = is_null( $content ) ? '' : $content;

    /* Create the view. */
    $form = new WPXUsersManagerSigninView();

    /* Top header alert message. */
    $before = apply_filters( 'wpx_users_manager_before_signin_form', '' );
    $after  = apply_filters( 'wpx_users_manager_after_signin_form', '' );

    /* @todo Da impostazioni decidere se visualizzare in automatico il resend dell'attivazione + reset della password. */

    return $content . $before . $form->html() . $after;
  }

  /**
   * Display the reset password form for who forgot the password. If the user is logged nothing is displayed.
   * If you reload the page when submit the form, this shortcode does the reset password procedure.
   *
   * @brief  Display the reset password form for who forgot the password
   *
   * @param array       $attrs   Attribute into the shortcode
   * @param null|string $content Optional. HTML content
   *
   * @return bool|string
   */
  public function wpx_users_manager_reset_password( $attrs, $content = null ) {

    /* Exit if logged in. */
    if ( is_user_logged_in() ) {
      return false;
    }

    /* Grab the content. */
    $content = is_null( $content ) ? '' : $content;

    /* Create the view. */
    $form = new WPXUsersManagerResetPasswordView();

    /* Top header alert message. */
    $before = apply_filters( 'wpx_users_manager_before_reset_password_form', '' );
    $after  = apply_filters( 'wpx_users_manager_after_reset_password_form', '' );
    $body   = apply_filters( 'wpx_users_manager_reset_password_form', $form->html() );

    return $content . $before . $body . $after;
  }

  /**
   * Display the re-send unlock code form for who forgot the mail. If the user is logged in nothing is displayed.
   * If you reload the page when submit the form, this shortcode does the re-send unlock.
   *
   * @brief  Display the e-send unlock code form
   * @since 1.0.0.b2
   *
   * @param array       $attrs   Attribute into the shortcode
   * @param null|string $content Optional. HTML content
   *
   * @return bool|string
   */
  public function wpx_users_manager_resend_unlock_url( $attrs, $content = null ) {

    /* Exit if logged in. */
    if ( is_user_logged_in() ) {
      return false;
    }

    /* Grab the content. */
    $content = is_null( $content ) ? '' : $content;

    /* Create the view. */
    $form = new WPXUsersManagerUnlockCodeView();

    /* Top header alert message. */
    $before = apply_filters( 'wpx_users_manager_before_resend_unlock_url_form', '' );
    $after  = apply_filters( 'wpx_users_manager_after_resend_unlock_url_form', '' );

    return $content . $before . $form->html() . $after;
  }



  /**
   * Display the signup/registration form if the user is logged nothing is displayed.
   * This shortcode manage also the Double Opt-In procedure.
   *
   * @brief Display user form signup
   *
   * @param array       $attrs   Attribute into the shortcode
   * @param null|string $content Optional. HTML content
   *
   * @return bool|string
   */
  public function wpx_users_manager_signup( $attrs, $content = null ) {

    /**
     * Get the signup configuration
     *
     * @brief Signup
     *
     * @var WPXUsersManagerConfigurationSignup $signup
     */
    $signup = WPXUsersManagerConfiguration::init()->signup;

    /* Exit if logged in. */
    if ( is_user_logged_in() ) {
      $message = __( '<h4>Information!</h4><p>You are logged in! You have to signout in order to make a registration.</p>', WPXUSERSMANAGER_TEXTDOMAIN );
      $alert   = new WPDKTwitterBootstrapAlert( 'wpx-users-manager-alert', $message, WPDKTwitterBootstrapAlertType::INFORMATION );
      $alert->dismissButton = false;
      return $alert->html();
    }

    /* If the configuration is not properly set, then an alert is display. */
    if ( empty( $signup->page_registration_slug ) ) {
      $message = __( '<h4>Warning!</h4><p>You have to set the signup (registration) page slug in Users Manager configuration backend area.</p>', WPXUSERSMANAGER_TEXTDOMAIN );
      $alert   = new WPDKTwitterBootstrapAlert( 'wpx-users-manager-alert', $message, WPDKTwitterBootstrapAlertType::ALERT );
      $alert->dismissButton = false;
      return $alert->html();
    }

    /* Check for Double Opt-In. */
    if ( wpdk_is_bool( $signup->double_optin ) ) {
      if ( empty( $signup->mail_slug_confirm ) ) {
        $message = __( '<h4>Warning!</h4><p>You have chosen the Double Opt-In method. In order to send a mail to end user, you have to set the mail confirm page slug in Users Manager configuration backend area.</p>', WPXUSERSMANAGER_TEXTDOMAIN );
        $alert   = new WPDKTwitterBootstrapAlert( 'wpx-users-manager-alert', $message, WPDKTwitterBootstrapAlertType::ALERT );
        $alert->dismissButton = false;
        return $alert->html();
      }

      if ( empty( $signup->page_slug_confirm ) ) {
        $message = __( '<h4>Warning!</h4><p>You have choose the Double Opt-In method. In order to display the confirm page to end user, you have to set the confirm page slug in Users Manager configuration backend area.</p>', WPXUSERSMANAGER_TEXTDOMAIN );
        $alert   = new WPDKTwitterBootstrapAlert( 'wpx-users-manager-alert', $message, WPDKTwitterBootstrapAlertType::ALERT );
        $alert->dismissButton = false;
        return $alert->html();
      }
    }

    /* Complete registration. */
    if ( empty( $signup->mail_slug_confirmed ) ) {
      $message = __( '<h4>Warning!</h4><p>In order to complete the registration process, you have to set the confirmed mail slug in Users Manager configuration backend area.</p>', WPXUSERSMANAGER_TEXTDOMAIN );
      $alert   = new WPDKTwitterBootstrapAlert( 'wpx-users-manager-alert', $message, WPDKTwitterBootstrapAlertType::ALERT );
      $alert->dismissButton = false;
      return $alert->html();
    }

    if ( empty( $signup->page_slug_confirmed ) ) {
      $message = __( '<h4>Warning!</h4><p>In order to complete the registration process, you have to set the confirmed page slug in Users Manager configuration backend area.</p>', WPXUSERSMANAGER_TEXTDOMAIN );
      $alert   = new WPDKTwitterBootstrapAlert( 'wpx-users-manager-alert', $message, WPDKTwitterBootstrapAlertType::ALERT );
      $alert->dismissButton = false;
      return $alert->html();
    }

    /* Grab the content. */
    $content = is_null( $content ) ? '' : $content;

    /* Top header alert message. */
    $before = apply_filters( 'wpx_users_manager_before_signup_form', '' );
    $after  = apply_filters( 'wpx_users_manager_after_signup_form', '' );

    /* Create the view. */
    $form = new WPXUsersManagerSignupView();

    return $content . $before . $form->html() . $after;
  }


  /**
   * Display profile form for logged users. This shortcode update user data too when is reload from submit page.
   *
   * @brief Display profile form
   *
   * @param array       $attrs   Attribute into the shortcode
   * @param null|string $content Optional. HTML content
   *
   * @return bool|string|WP_Error
   */
  public function wpx_users_manager_profile( $attrs, $content = null ) {

    /**
     * Get profile configuration
     *
     * @brief Profile
     *
     * @var WPXUsersManagerConfigurationProfile $profile
     */
    $profile = WPXUsersManagerConfiguration::init()->profile;

    /* Exit if not logged in. */
    if ( !is_user_logged_in() && false === WPXUsersManagerResetPassword::init()->isPasswordReset() ) {
      $message = __( '<h4>Information!</h4><p>You are <strong>not</strong> logged in! You have to signin in order display or edit your profile.</p>', WPXUSERSMANAGER_TEXTDOMAIN );
      $alert   = new WPDKTwitterBootstrapAlert( 'wpx-users-manager-alert', $message, WPDKTwitterBootstrapAlertType::INFORMATION );
      $alert->dismissButton = false;
      return $alert->html();
    }

    /* If the profile page slug is not properly set, then an alert is display. */
    if ( empty( $profile->page_slug ) ) {
      $message = __( '<h4>Warning!</h4><p>You have to set the profile page slug in order to display the profile page in Users Manager configuration backend area.</p>', WPXUSERSMANAGER_TEXTDOMAIN );
      $alert   = new WPDKTwitterBootstrapAlert( 'wpx-users-manager-alert', $message, WPDKTwitterBootstrapAlertType::ALERT );
      $alert->dismissButton = false;
      return $alert->html();
    }

    /* Grab the content. */
    $content = is_null( $content ) ? '' : $content;

    /* Create the view. */
    $form = new WPXUsersManagerProfileView();

    /* Top header alert message. */
    $before = apply_filters( 'wpx_users_manager_before_profile_form', '' );
    $after  = apply_filters( 'wpx_users_manager_after_profile_form', '' );

    return $content . $before . $form->html() . $after;

  }
} // class WPDKServiceShortcode