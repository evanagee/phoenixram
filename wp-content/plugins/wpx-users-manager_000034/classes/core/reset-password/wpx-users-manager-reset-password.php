<?php
/**
 * Reset Password model
 *
 * ## Overview
 *
 * Description
 *
 * @class           WPXUsersManagerResetPassword
 * @author          =undo= <info@wpxtre.me>
 * @copyright       Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date            2013-02-19
 * @version         1.0.0
 *
 */
class WPXUsersManagerResetPassword {

  /**
   * This is the token transient via GET
   *
   * @brief Token transient
   *
   * @var string $token
   */
  public $token;

  /**
   * The user id to reset password
   *
   * @brief User ID
   *
   * @var int $userID
   */
  public $userID;

  /**
   * The user email to reset password
   *
   * @brief User email
   *
   * @var string $userEmail
   */
  public $userEmail;

  /**
   * Alert used to display something before the form
   *
   * @brief Alert
   *
   * @var WPDKTwitterBootstrapAlert $alert
   */
  public $alert;

  /**
   * Create and return a singleton instance of WPXUsersManagerResetPassword class
   *
   * @brief Init
   *
   * @return WPXUsersManagerResetPassword
   */
  public static function init() {
    static $instance = null;
    if ( is_null( $instance ) ) {
      $instance = new WPXUsersManagerResetPassword();
    }
    return $instance;
  }

  /**
   * Create an instance of WPXUsersManagerResetPassword class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerResetPassword
   */
  public function __construct() {
    $this->token     = '';
    $this->userEmail = '';
    $this->userID    = '';
    $this->alert     = null;
    $this->alertForm = null;

    /* Set feedback */
    add_filter( 'wpx_users_manager_reset_password_form', array( $this, 'feedback' ) );
    add_filter( 'wpx_users_manager_before_reset_password_form', array( $this, 'feedback' ) );
  }

  /**
   * Return an alert content to prepend to the form view
   *
   * @param string $content Content to filter
   *
   * @return string
   */
  public function feedback( $content ) {
    if ( $this->alert ) {
      $content = $this->alert->html();
    }
    return $content;
  }

  /**
   * Send a mail to valid user within the instruction to reset the password.
   * Retrun true on successfully or false otherwise.
   *
   * @brief Reset password
   */
  public function send() {

    $email = sanitize_email( $_POST['user_email'] );

    if ( !empty( $email ) ) {
      $id_user = email_exists( $email );

      if ( false !== $id_user ) {

        /* Check if the users is enabled. */
        $user = new WPDKUser( $id_user );
        if ( WPDKUserStatus::DISABLED !== $user->status ) {

          /* Create an unique code to autologin user for change password. */
          $auto_signin_unique_code = WPDKCrypt::randomAlphaNumber( 16 );
          $auto_signin_unique_code = md5( $auto_signin_unique_code );
          $auto_signin_unique_code .= uniqid();

          /* This unique code is temporary. */
          $timeout = absint( WPXUsersManagerConfiguration::init()->resetPassword->timeout );
          set_transient( $auto_signin_unique_code, $email, $timeout );

          /* Costruisco l'url per auto accedere e sbloccare. */
          $profile_slug = WPXUsersManagerConfiguration::init()->profile->page_slug;
          $permalink    = wpdk_permalink_page_with_slug( $profile_slug );
          $reset_url    = sprintf( '%s?rp=%s', $permalink, $auto_signin_unique_code );

          /* Gli invio una mail per cambiare password */
          $mail_slug = WPXUsersManagerConfiguration::init()->resetPassword->mail_slug;
          $mail_post_type = WPXUsersManagerConfiguration::init()->resetPassword->mail_slug_post_type;
          $mail = new WPDKMail( $mail_slug, $mail_post_type );

          $mail->send( $id_user, false, false, array( WPXUsersManagerPlaceholder::USER_PASSWORD_RESET_URL => $reset_url ) );

          $message         = __( '<h4>Your password reset\'s request has been successful!</h4><p>Please, check your mail and follow instructions to complete.</p>', WPXUSERSMANAGER_TEXTDOMAIN );
          $this->alert = new WPDKTwitterBootstrapAlert( 'reset-password', $message, WPDKTwitterBootstrapAlertType::SUCCESS );
          return true;
        }
        else {
          $message = sprintf( __( '<h4>Error in user!</h4><p>Your account is disabled: %s.</p>', WPXUSERSMANAGER_TEXTDOMAIN ), $user->statusDescription );
          $this->alert   = new WPDKTwitterBootstrapAlert( 'reset-password', $message, WPDKTwitterBootstrapAlertType::ALERT );
          return false;
        }
      }
      else {
        $message     = __( '<h4>Sorry!</h4><p>It is not possibile to reset your password. Please, check your data.</p>', WPXUSERSMANAGER_TEXTDOMAIN );
        $this->alert = new WPDKTwitterBootstrapAlert( 'reset-password', $message, WPDKTwitterBootstrapAlertType::ALERT );
        return false;
      }
    }
    /* Email address empty. */
    else {
      $message     = __( '<h4>Ops!</h4><p>Email\'s field is empty</p>', WPXUSERSMANAGER_TEXTDOMAIN );
      $this->alert = new WPDKTwitterBootstrapAlert( 'reset-password', $message, WPDKTwitterBootstrapAlertType::ALERT );
    }

    return false;

  }


  /**
   * Return TRUE if the reset password mode is true. Check for $_GET[] and look for transient token. The transient
   * could be delete because limit in time.
   * This method set the `token`, `userEmail` and `userID` properties too.
   *
   * @brief Reset password
   *
   * @return bool
   */
  public function isPasswordReset() {
    if ( isset( $_GET['rp'] ) && !empty( $_GET['rp'] ) ) {
      $token = esc_attr( $_GET['rp'] );
      $email = get_transient( $token );

      if ( !empty( $email ) ) {
        /* Verifico nuovamente che questa email corrisponda ad un utente registrato. */
        $id_user = email_exists( $email );
        if ( false !== $id_user ) {
          $this->token     = $token;
          $this->userEmail = $email;
          $this->userID    = $id_user;
          return true;
        }
      }
      /* Wrong reset password token or transient expired. */
      else {
        return new WPDKError( 'invalid-token' );
      }
    }
    return false;
  }

  /**
   * Return TRUE if the transient token for reset password was successfully deleted, FALSE otherwise.
   *
   * @brief Delete transient token
   *
   * @return bool
   */
  public function deleteToken() {
    if( !empty( $this->token ) ) {
      return delete_transient( $this->token );
    }
    return false;
  }

}



/**
 * Reset Password frontend form layout
 *
 * @class              WPXUsersManagerResetPasswordView
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-19
 * @version            0.9.2
 *
 */
class WPXUsersManagerResetPasswordView extends WPDKView {

  /**
   * Create an instance of WPXUsersManagerResetPasswordView class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerResetPasswordView
   */
  public function __construct() {
    parent::__construct( 'wpx-users-manager-reset-password' );
  }

  /**
   * Return the layout fields array
   *
   * @brief Fields
   *
   * @return array
   */
  private function fields() {
    $fields = array(
      __( 'Forgot Password?', WPXUSERSMANAGER_TEXTDOMAIN ) => array(

        array(
          'type'  => WPDKUIControlType::HIDDEN,
          'name'  => WPXUsersManagerPostDataAction::ID,
          'value' => WPXUsersManagerPostDataAction::RESET_PASSWORD
        ),

        __( 'Forgot your Password?! No worries, enter your email address into the field below: : you will receive a mail with password reset\'s instructions.', WPXUSERSMANAGER_TEXTDOMAIN ),

        array(
          array(
            'type'  => WPDKUIControlType::TEXT,
            'name'  => 'user_email',
            'label' => __( 'Email', WPXUSERSMANAGER_TEXTDOMAIN )
          ),
          array(
            'type'  => WPDKUIControlType::SUBMIT,
            'name'  => 'reset',
            'class' => 'btn btn-success wpdk-disable-after-click',
            'value' => __( 'Reset Password', WPXUSERSMANAGER_TEXTDOMAIN )
          )
        ),
      ),
    );

    return $fields;
  }

  /**
   * Display the form
   *
   * @brief Display
   */
  public function draw() {

    /* Create a nonce key. */
    $nonce                     = md5( $this->id );
    $input_hidden_nonce        = new WPDKHTMLTagInput( '', $nonce, $nonce );
    $input_hidden_nonce->type  = WPDKHTMLTagInputType::HIDDEN;
    $input_hidden_nonce->value = wp_create_nonce( $this->id );

    /* Layout fields. */
    $layout = new WPDKUIControlsLayout( $this->fields() );

    /* Form. */
    $form         = new WPDKHTMLTagForm( $input_hidden_nonce->html() . $layout->html()  );
    $form->name   = 'wpx-users-manager-reset-password';
    $form->id     = $form->name;
    $form->method = 'post';
    $form->action = '';
    $form->display();
  }


}