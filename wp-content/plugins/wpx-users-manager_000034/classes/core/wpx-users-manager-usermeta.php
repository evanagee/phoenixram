<?php
/**
 * Users Manager user meta consstant define
 *
 * ## Overview
 *
 * This class containt the define constant for the user meta key used to store some extra information.
 *
 * @class           WPXUsersManagerUserMeta
 * @author          =undo= <info@wpxtre.me>
 * @copyright       Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date            2013-02-20
 * @version         1.0.0
 *
 */
class WPXUsersManagerUserMeta {

  /**
   * Timestamp user confirmed
   *
   * @brief Signup date
   */
  const TIMESTAMP_CONFIRMED = 'wpx_users_manager_column_signup_confirmed';

  /**
   * User meta used to store the user signup status
   *
   * @brief Status
   */
  const STATUS = 'wpx_users_manager_status';

  /**
   * User meta used to store the unlock token
   *
   * @brief Unlock token
   */
  const UNLOCK_CODE_TOKEN = 'wpx_users_manager_unlock_code_token';

}

/**
 * Users Manager user status
 *
 * ## Overview
 *
 * Special status used to manage a user
 *
 * @class           WPXUsersManagerUserStatus
 * @author          =undo= <info@wpxtre.me>
 * @copyright       Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date            2013-02-27
 * @version         1.0.0
 *
 */
class WPXUsersManagerUserStatus {

  /**
   * User status before confirmed - used in Double Opt-In
   *
   * @brief Denied
   */
  const DENIED = 'denied';
  /**
   * User status after signup confirmed
   *
   * @brief Granted
   */
  const GRANTED = 'granted';

}