<?php
/**
 * Signup model
 *
 * ## Overview
 *
 * Description
 *
 * @class           WPXUsersManagerSignup
 * @author          =undo= <info@wpxtre.me>
 * @copyright       Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date            2013-02-19
 * @version         1.0.0
 *
 */
class WPXUsersManagerSignup {

  /**
   * Used to prepend, replace or append to content
   *
   * @brief Alert
   *
   * @var WPDKTwitterBootstrapAlert $alert
   */
  public $alert;

  /**
   * Create and return a singleton instance of WPXUsersManagerSignup class
   *
   * @brief Init
   *
   * @return WPXUsersManagerSignup
   */
  public static function init() {
    static $instance = null;
    if ( is_null( $instance ) ) {
      $instance = new WPXUsersManagerSignup();
    }
    return $instance;
  }

  /**
   * Create an instance of WPXUsersManagerProfile class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerProfile
   */
  public function __construct() {
    /* Prepare feedback */
    add_filter( 'wpx_users_manager_before_signup_form', array( $this, 'feedback' ) );
  }

  /**
   * Do signup. Perform the signup process
   *
   * @brief Signup
   */
  public function signup() {
    /* Get post parameters, check and sanitize */
    $first_name = esc_attr( $_POST['first_name'] );
    $last_name  = esc_attr( $_POST['last_name'] );
    $email      = sanitize_email( $_POST['email'] );
    $password   = $_POST['password'];

    /* Checking some data before try to create the user. */
    if ( email_exists( $email ) ) {
      $message     = __( '<h4>Warning!</h4><p>Signup failed. The email address is not valid.</p>', WPXUSERSMANAGER_TEXTDOMAIN );
      $this->alert = new WPDKTwitterBootstrapAlert( 'signup', $message, WPDKTwitterBootstrapAlertType::ALERT );
      return false;
    }

    if ( empty( $password ) ) {
      $message = __( '<h4>Warning!</h4><p>Signup failed. Your password\'s field is empty!</p>', WPXUSERSMANAGER_TEXTDOMAIN );
      $this->alert   = new WPDKTwitterBootstrapAlert( 'signup', $message, WPDKTwitterBootstrapAlertType::ALERT );
      return false;
    }

    /* Get default role */
    $role = WPXUsersManagerConfiguration::init()->signup->default_user_role;

    /* Try to Create the user */
    $result = WPDKUsers::init()->create( $first_name, $last_name, $email, $password, false, $role );

    if ( is_wp_error( $result ) ) {
      do_action( 'wpx_users_manager_signup_error', $result, $_POST );
      $message     = __( '<h4>Warning!</h4><p>Signup failed. Please check your data and retry.</p>', WPXUSERSMANAGER_TEXTDOMAIN );
      $this->alert = new WPDKTwitterBootstrapAlert( 'signup', $message, WPDKTwitterBootstrapAlertType::ALERT );
    }
    /* User create successfully; in $result the user id */
    else {
      /* Update/create the extra fields for this user */
      WPXUsersManagerExtraFields::init()->update( $result, $_POST );

      if ( wpdk_is_bool( WPXUsersManagerConfiguration::init()->signup->double_optin ) ) {
        $this->signupWithDoubleOptin( $result );
      }
      else {
        $this->completeSignup( $result );
      }
    }
  }

  /**
   * Signup process with Double Opt-In.
   *
   * 1. Create an unlock code token
   * 2. Store in the user meta
   * 3. Send mail to the user (with the unlock code url)
   * 4. Display the confirm page
   *
   * @brief Double Opt-In signup
   *
   * @param int $user_id User id
   */
  private function signupWithDoubleOptin( $user_id ) {
    if ( !empty( $user_id ) ) {

      /* Get user */
      $user = new WPDKUser( $user_id );

      /* Create an unlock code token */
      $uct = WPXUsersManagerUnlockCode::init()->createUnlockCodeToken( $user->email );

      /* Store in user meta */
      update_user_meta( $user_id, WPXUsersManagerUserMeta::UNLOCK_CODE_TOKEN, $uct );

      /* Send unlock code token */
      $result = WPXUsersManagerUnlockCode::init()->send( $user->email );

      if ( true === $result ) {

        do_action( 'wpx_users_manager_signup_to_confirm', $user_id );

        /* Redirect on "to confirm" page */
        $page_slug = WPXUsersManagerConfiguration::init()->signup->page_slug_confirm;
        $url       = wpdk_permalink_page_with_slug( $page_slug );
        wp_safe_redirect( $url );
        exit();
      }
      else {
        $message     = __( '<h4>Warning!</h4><p>Signup process failed. An error occurred while sending the confirmation mail.</p>' . WPXTREME_SUPPORT, WPXUSERSMANAGER_TEXTDOMAIN );
        $this->alert = new WPDKTwitterBootstrapAlert( 'signup', $message, WPDKTwitterBootstrapAlertType::ALERT );
      }
    }
  }

  /**
   * Complete the signup process.
   *
   * 1. Enable the user
   * 2. Update the TIMESTAMP_CONFIRMED user meta
   * 3. Send a confirmation mail to the user ($user_id).
   * 4. Display the confirmed page
   *
   * @brief Complete signup
   *
   * @param int $user_id User id
   */
  private function completeSignup( $user_id ) {
    if ( !empty( $user_id ) ) {
      /* Enable user; remove all previous status */
      delete_user_meta( $user_id, WPDKUserMeta::STATUS );
      update_user_meta( $user_id, WPXUsersManagerUserMeta::TIMESTAMP_CONFIRMED, time() );

      /* Get confirmation mail and type */
      $mail_slug      = WPXUsersManagerConfiguration::init()->signup->mail_slug_confirmed;
      $mail_post_type = WPXUsersManagerConfiguration::init()->signup->mail_slug_confirmed_post_type;

      /* Create a new Mail object and send */
      $mail = new WPDKMail( $mail_slug, $mail_post_type );
      $result = $mail->send( $user_id );

      do_action( 'wpx_users_manager_complete_signup', $user_id );

      /* Redirect on confirmation page */
      $page_slug = WPXUsersManagerConfiguration::init()->signup->page_slug_confirmed;
      $url       = wpdk_permalink_page_with_slug( $page_slug );
      wp_safe_redirect( $url );
      exit();
    }
  }

  /**
   * Complete signup from Double Opt-In
   *
   * @brief complete signup
   *
   * @param string $uct Unlock code token
   *
   * @return bool
   */
  public function completeSignupWithUnlockCodeToken( $uct ) {
    if ( !empty( $uct ) ) {
      $user_id = WPDKUsers::userWithMetaAndValue( WPXUsersManagerUserMeta::UNLOCK_CODE_TOKEN, $uct );
      if ( !empty( $user_id ) ) {
        delete_user_meta( $user_id, WPXUsersManagerUserMeta::UNLOCK_CODE_TOKEN );
        $this->completeSignup( $user_id );
        return true;
      }
      $message     = __( '<h4>Warning!</h4><p>Something has gone wrong! Your unlock code token is invalid! Please, try to signup again.</p>' . WPXTREME_SUPPORT, WPXUSERSMANAGER_TEXTDOMAIN );
      $this->alert = new WPDKTwitterBootstrapAlert( 'signup', $message, WPDKTwitterBootstrapAlertType::ALERT );
    }
    return false;
  }


  /**
   * Display a feedback result as alert
   *
   * @brief Display alert
   */
  public function feedback( $content ) {
    if ( !empty( $this->alert ) ) {
      $content = $this->alert->html();
    }
    return $content;
  }
}



/**
 * Signup frontend form layout
 *
 * @class              WPXUsersManagerSignupView
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-19
 * @version            0.9.2
 *
 */
class WPXUsersManagerSignupView extends WPDKView {

  /**
   * Create an instance of WPXUsersManagerSignupView class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerSignupView
   */
  public function __construct() {
    parent::__construct( 'wpx-users-manager-signup' );

    /* Init the signup model */
    WPXUsersManagerSignup::init();

    /* Init the extra fields model */
    WPXUsersManagerExtraFields::init();
  }

  /**
   * Return the layout fields array
   *
   * @brief Fields
   *
   * @return array
   */
  private function fields() {

    $configuration = WPXUsersManagerConfiguration::init()->signup;

    if ( wpdk_is_bool( $configuration->double_optin ) ) {
      $title_email  = __( 'This email address will be used in order to send you a confirmation mail. Remember to confirm the URL address in the confirmation mail.', WPXUSERSMANAGER_TEXTDOMAIN );
    }
    else {
      $title_email  = __( 'This email address will be your login username.', WPXUSERSMANAGER_TEXTDOMAIN );
    }

    $fields = array(
      __( 'Account', WPXUSERSMANAGER_TEXTDOMAIN ) => array(

        array(
          'type'  => WPDKUIControlType::HIDDEN,
          'name'  => WPXUsersManagerPostDataAction::ID,
          'value' => WPXUsersManagerPostDataAction::SIGNUP
        ),

        __( 'Fill out all <strong>required</strong> fields', WPXUSERSMANAGER_TEXTDOMAIN ),

        array(
          array(
            'type'   => WPDKUIControlType::TEXT,
            'name'   => 'first_name',
            'label'  => __( 'First name', WPXUSERSMANAGER_TEXTDOMAIN ),
            'append' => __( '<strong>(required)</strong>', WPXUSERSMANAGER_TEXTDOMAIN ),
            'required' => 'required'
          )
        ),

        array(
          array(
            'type'   => WPDKUIControlType::TEXT,
            'name'   => 'last_name',
            'label'  => __( 'Last name', WPXUSERSMANAGER_TEXTDOMAIN ),
            'append' => __( '<strong>(required)</strong>', WPXUSERSMANAGER_TEXTDOMAIN ),
            'required' => 'required'
          )
        ),

        array(
          array(
            'type'   => WPDKUIControlType::TEXT,
            'name'   => 'email',
            'data'   => array( 'placement' => 'right' ),
            'title'  => $title_email,
            'label'  => __( 'Email', WPXUSERSMANAGER_TEXTDOMAIN ),
            'append' => __( '<strong>(required)</strong>', WPXUSERSMANAGER_TEXTDOMAIN ) . ( wpdk_is_bool( $configuration->double_optin ) ? ' ' . __( 'a confirmation mail will be sent to this address', WPXUSERSMANAGER_TEXTDOMAIN ) : '' ),
            'required' => 'required'
          )
        ),

        array(
          array(
            'type'   => WPDKUIControlType::PASSWORD,
            'name'   => 'password',
            'label'  => __( 'Password', WPXUSERSMANAGER_TEXTDOMAIN ),
            'append' => __( '<strong>(required)</strong>', WPXUSERSMANAGER_TEXTDOMAIN ),
            'required' => 'required'
          )
        ),

        array(
          array(
            'type'  => WPDKUIControlType::PASSWORD,
            'name'  => 'password_repeat',
            'label' => __( 'Repeat Password', WPXUSERSMANAGER_TEXTDOMAIN ),
            'required' => 'required'
          )
        ),

      )
    );

    $fields = apply_filters( '_wpx_users_manager_signup_extra_fields', $fields );
    end( $fields );
    $fields[key( $fields )][] = array(
      array(
        'type'  => WPDKUIControlType::SUBMIT,
        'name'  => 'signup',
        'class' => 'btn btn-primary pull-right',
        'value' => __( 'Signup', WPXUSERSMANAGER_TEXTDOMAIN )
      ),
    );

    reset( $fields );

    return apply_filters( 'wpx_users_manager_signup_extra_fields', $fields );
  }

  /**
   * Display the form
   *
   * @brief Display
   */
  public function draw() {

    /* Create a nonce key. */
    $nonce                     = md5( $this->id );
    $input_hidden_nonce        = new WPDKHTMLTagInput( '', $nonce, $nonce );
    $input_hidden_nonce->type  = WPDKHTMLTagInputType::HIDDEN;
    $input_hidden_nonce->value = wp_create_nonce( $this->id );

    /* Layout fields. */
    $layout = new WPDKUIControlsLayout( $this->fields() );

    /* Form. */
    $form         = new WPDKHTMLTagForm( $input_hidden_nonce->html() . $layout->html()  );
    $form->name   = 'wpx-users-manager-signup';
    $form->id     = $form->name;
    $form->method = 'post';
    $form->action = '';
    $form->display();
  }


}