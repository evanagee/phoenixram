<?php
/**
 * Users Manager configuration (jQuery tabs) view controller
 *
 * @class              WPXUsersManagerConfigurationSignupViewController
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-18
 * @version            0.8.3
 *
 */
class WPXUsersManagerConfigurationSignupViewController extends WPDKjQueryTabsViewController {

  /**
   * Create an instance of WPXUsersManagerConfigurationSignupViewController class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerConfigurationSignupViewController
   */
  public function __construct() {

    /* Single instances of tab content. */
    $signup_view       = new WPXUsersManagerConfigurationSignupView();
    $double_optin_view = new WPXUsersManagerConfigurationDoubleOptinView();

    /* Create each single tab. */
    $tabs = array(
      new WPDKjQueryTab( 'signup', __( 'Signup (Registration)', WPXUSERSMANAGER_TEXTDOMAIN ), $signup_view->html() ),
      new WPDKjQueryTab( 'double-opt-in', __( 'Double Opt-In', WPXUSERSMANAGER_TEXTDOMAIN ), $double_optin_view->html() ),
    );

    /* Create the view. */
    $view = new WPDKjQueryTabsView( 'wpx-users-manager-configuration', $tabs );

    parent::__construct( 'wpx-users-manager-configuration', __( 'Signup settings', WPXUSERSMANAGER_TEXTDOMAIN ), $view );

  }

  /**
   * Call when this view controller is loaded
   *
   * @brief Head loaded
   */
  public static function didHeadLoad() {
    /* Scripts */
    wp_enqueue_script( 'wpx-users-manager-admin', WPXUSERSMANAGER_URL_JAVASCRIPT . 'wpx-users-manager-admin.js', array( 'jquery' ), WPXUSERSMANAGER_VERSION, true );
    wp_enqueue_style( 'wpx-users-manager-admin', WPXUSERSMANAGER_URL_CSS . 'wpx-users-manager-admin.css', array(), WPXUSERSMANAGER_VERSION );

  }
}



/**
 * Signup configuration view
 *
 * @class              WPXUsersManagerConfigurationSignupView
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2012-11-28
 * @version            0.8.1
 *
 */
class WPXUsersManagerConfigurationSignupView extends WPDKConfigurationView {

  /**
   * Create an instance of WPXUsersManagerConfigurationSignupView class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerConfigurationSignupView
   */
  public function __construct() {
    /* Get configuration instance. */
    $configuration = WPXUsersManagerConfiguration::init();

    /* This specific sub configuration branch. */
    $sub_configuration = $configuration->signup;

    parent::__construct( 'signup', __( 'Signup (Registration)', WPXUSERSMANAGER_TEXTDOMAIN ), $configuration, $sub_configuration );
  }

  /**
   * Return an array in sdf format for form fileds
   *
   * @brief Form fields
   *
   * @return array
   */
  public function fields() {

    /* Get configuration instance. */
    $configuration = WPXUsersManagerConfiguration::init();

    /* This specific sub configuration branch. */
    $sub_configuration = $configuration->signup;

    /* URL to create a new page. */
    $page_new_url = admin_url( 'post-new.php?post_type=page' );
    $new_url      = admin_url( 'post-new.php?post_type=' );

    /* Post types. */
    $post_types         = get_post_types( array( 'public' => true ) );
    $post_types         = apply_filters( 'wpx_users_manager_post_types', $post_types );
    $post_types_options = array();

    foreach ( $post_types as $key ) {
      $post_type                = get_post_type_object( $key );
      $post_types_options[$key] = $post_type->labels->singular_name;
    }

    $post_types_options = apply_filters( 'wpx_users_manager_post_types_options', $post_types_options );

    $fields = array(
      __( 'WordPress Integration', WPXUSERSMANAGER_TEXTDOMAIN ) => array(
        __( 'Choose a theme page to display an User Signup (registration). Use the <code>wpx_users_manager_signup</code> shortcode.', WPXUSERSMANAGER_TEXTDOMAIN ),
        array(
          array(
            'type'   => WPDKUIControlType::TEXT,
            'name'   => 'page_registration_slug',
            'data'   => array(
              'autocomplete' => 'posts',
              'post_type'    => 'page'
            ),
            'label'  => __( 'Registration Page\'s slug', WPXUSERSMANAGER_TEXTDOMAIN ),
            'size'   => '32',
            'value'  => $sub_configuration->page_registration_slug,
            'title'  => __( 'Press down key to retrive Page list or press any key to search for slug or title.', WPXUSERSMANAGER_TEXTDOMAIN ),
            'append' => sprintf( '<a class="button button-small" href="%s">%s</a>', $page_new_url, __( 'Create new', WPXUSERSMANAGER_TEXTDOMAIN ) ),
          )
        ),

        empty( $sub_configuration->page_registration_slug ) ? array(
          array(
            'type'           => WPDKUIControlType::ALERT,
            'alert_type'     => WPDKTwitterBootstrapAlertType::ALERT,
            'dismiss_button' => false,
            'classes'        => 'alert-arrow-top',
            'value'          => __( '<h4>You have to set the above page slug in order to use the shortcode.</h4>', WPXUSERSMANAGER_TEXTDOMAIN ),
          )
        ) : '',

      ),

      __( 'Default user settings', WPXUSERSMANAGER_TEXTDOMAIN ) => array(
        __( 'Set default Role and Status corresponding to the Confirmed User.', WPXUSERSMANAGER_TEXTDOMAIN ),
        array(
          array(
            'type'    => WPDKUIControlType::SELECT,
            'name'    => 'default_user_role',
            'label'   => __( 'Default user Role', WPXUSERSMANAGER_TEXTDOMAIN ),
            'options' => WPDKRoles::getInstance()->role_names,
            'value'   => $sub_configuration->default_user_role,
            'title'   => __( 'Default Role applied confirming a New User', WPXUSERSMANAGER_TEXTDOMAIN ),
          ),
          array(
            'type'    => WPDKUIControlType::SELECT,
            'name'    => 'default_user_status',
            'label'   => __( 'Default user status', WPXUSERSMANAGER_TEXTDOMAIN ),
            'options' => array(
              'disabled' => __( 'Disabled', WPXUSERSMANAGER_TEXTDOMAIN ),
              'enabled'  => __( 'Enabled', WPXUSERSMANAGER_TEXTDOMAIN ),
            ),
            'value'   => $sub_configuration->default_user_status,
            'title'   => __( 'When an user is created, this is the dafault status applied. Usually it is set on disabled.', WPXUSERSMANAGER_TEXTDOMAIN ),
          )
        ),
        array(
          array(
            'type'  => WPDKUIControlType::TEXT,
            'name'  => 'mail_slug_confirmed',
            'data'  => array(
              'autocomplete' => 'posts',
              'post_type'    => '#mail_slug_confirmed_post_type'
            ),
            'label' => __( 'Confirmed mail slug', WPXUSERSMANAGER_TEXTDOMAIN ),
            'size'  => '32',
            'value' => $sub_configuration->mail_slug_confirmed,
            'title' => __( 'Press down key to retrive Post Type list or press any key to search for slug or title.', WPXUSERSMANAGER_TEXTDOMAIN ),
          ),
          array(
            'type'    => WPDKUIControlType::SELECT,
            'name'    => 'mail_slug_confirmed_post_type',
            'options' => $post_types_options,
            'value'   => $sub_configuration->mail_slug_confirmed_post_type,
            'append'  => sprintf( '<a data-post_type="#mail_slug_confirmed_post_type" data-url="%s" class="wpx-users-manager-create-post button button-small" href="#">%s</a>', $new_url, __( 'Create new', WPXUSERSMANAGER_TEXTDOMAIN ) ),
          ),
        ),

        ( empty( $sub_configuration->mail_slug_confirmed ) ) ? array(
          array(
            'type'           => WPDKUIControlType::ALERT,
            'alert_type'     => WPDKTwitterBootstrapAlertType::ALERT,
            'dismiss_button' => false,
            'classes'        => 'alert-arrow-top',
            'value'          => __( '<h4>You have to set the above mail slug in order to complete the signup process.</h4>', WPXUSERSMANAGER_TEXTDOMAIN ),
          )
        ) : '',

        array(
          array(
            'type'   => WPDKUIControlType::TEXT,
            'name'   => 'page_slug_confirmed',
            'data'   => array(
              'autocomplete' => 'posts',
              'post_type'    => 'page'
            ),
            'label'  => __( 'Confirmed page slug', WPXUSERSMANAGER_TEXTDOMAIN ),
            'size'   => '32',
            'value'  => $sub_configuration->page_slug_confirmed,
            'title'  => __( 'Press down key to retrive Page list or press any key to search for slug or title.', WPXUSERSMANAGER_TEXTDOMAIN ),
            'append' => sprintf( '<a class="button button-small" href="%s">%s</a>', $page_new_url, __( 'Create new', WPXUSERSMANAGER_TEXTDOMAIN ) ),
          )
        ),

        ( empty( $sub_configuration->page_slug_confirmed ) ) ? array(
          array(
            'type'           => WPDKUIControlType::ALERT,
            'alert_type'     => WPDKTwitterBootstrapAlertType::ALERT,
            'dismiss_button' => false,
            'classes'        => 'alert-arrow-top',
            'value'          => __( '<h4>You have to set above page slug in order to complete the signup process.</h4>', WPXUSERSMANAGER_TEXTDOMAIN ),
          )
        ) : '',

      ),

    );

    return $fields;
  }

  // -----------------------------------------------------------------------------------------------------------------
  // Configuration actions
  // -----------------------------------------------------------------------------------------------------------------

  /**
   * Process and set you own post data
   *
   * @brief Update configuration
   *
   * @return bool TRUE to update the configuration and display the standard sucessfully message, or FALSE to avoid
   * the update configuration and do a custom display.
   */
  public function updatePostData() {

    /* Get configuration instance. */
    $configuration = WPXUsersManagerConfiguration::init();

    /* This specific sub configuration branch. */
    $sub_configuration = $configuration->signup;

    /* @todo 'subscriber' deve diventare una costante. Vedi WPDKRole e ... */

    $sub_configuration->page_registration_slug      = esc_attr( $_POST['page_registration_slug'] );

    $sub_configuration->default_user_role   = isset( $_POST['default_user_role'] ) ? $_POST['default_user_role'] : 'subscriber';
    $sub_configuration->default_user_status = isset( $_POST['default_user_status'] ) ? $_POST['default_user_status'] : 'disabled';

    $sub_configuration->page_slug_confirmed           = esc_attr( $_POST['page_slug_confirmed'] );
    $sub_configuration->mail_slug_confirmed           = esc_attr( $_POST['mail_slug_confirmed'] );
    $sub_configuration->mail_slug_confirmed_post_type = esc_attr( $_POST['mail_slug_confirmed_post_type'] );

    return true;
  }
}


/**
 * Description
 *
 * ## Overview
 *
 * Description
 *
 * @class           WPXUsersManagerConfigurationDoubleOptinView
 * @author          =undo= <info@wpxtre.me>
 * @copyright       Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date            25/03/13
 * @version         1.0.0
 *
 */
class WPXUsersManagerConfigurationDoubleOptinView extends WPDKConfigurationView {

  /**
   * Create an instance of WPXUsersManagerConfigurationDoubleOptinView class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerConfigurationDoubleOptinView
   */
  public function __construct() {
    /* Get configuration instance. */
    $configuration = WPXUsersManagerConfiguration::init();

    /* This specific sub configuration branch. */
    $sub_configuration = $configuration->signup;

    parent::__construct( 'double-opt-in', __( 'Double Opt-In', WPXUSERSMANAGER_TEXTDOMAIN ), $configuration, $sub_configuration );

  }

  /**
   * Return an array in sdf format for form fileds
   *
   * @brief Form fields
   *
   * @return array
   */
  public function fields() {
    /* Get configuration instance. */
    $configuration = WPXUsersManagerConfiguration::init();

    /* This specific sub configuration branch. */
    $sub_configuration = $configuration->signup;

    /* URL to create a new page. */
    $page_new_url = admin_url( 'post-new.php?post_type=page' );
    $new_url      = admin_url( 'post-new.php?post_type=' );

    /* Post types. */
    $post_types         = get_post_types( array( 'public' => true ) );
    $post_types         = apply_filters( 'wpx_users_manager_post_types', $post_types );
    $post_types_options = array();

    foreach ( $post_types as $key ) {
      $post_type                = get_post_type_object( $key );
      $post_types_options[$key] = $post_type->labels->singular_name;
    }

    $post_types_options = apply_filters( 'wpx_users_manager_post_types_options', $post_types_options );

    $fields = array(

      __( 'Double Opt-In', WPXUSERSMANAGER_TEXTDOMAIN ) => array(
        __( 'The Double Opt-In option displays and sends an information to end users before confirmation.', WPXUSERSMANAGER_TEXTDOMAIN ),
        array(
          array(
            'type'    => WPDKUIControlType::SWIPE,
            'name'    => 'double_optin',
            'label'   => __( 'Enable Double Opt-In', WPXUSERSMANAGER_TEXTDOMAIN ),
            'value'   => $sub_configuration->double_optin,
            'title'   => __( 'This setting allows you to send a confirmation mail to an end user. This mail contains an URL request for user validation.', WPXUSERSMANAGER_TEXTDOMAIN ),
          )
        ),
        array(
          array(
            'type'  => WPDKUIControlType::TEXT,
            'name'  => 'mail_slug_confirm',
            'data'  => array(
              'autocomplete' => 'posts',
              'post_type'    => '#mail_slug_confirm_post_type'
            ),
            'label' => __( 'Confirmation mail slug', WPXUSERSMANAGER_TEXTDOMAIN ),
            'size'  => '32',
            'value' => $sub_configuration->mail_slug_confirm,
            'title' => __( 'Press down key to retrive Post Type list or press any key to search for slug or title.', WPXUSERSMANAGER_TEXTDOMAIN ),
          ),
          array(
            'type'    => WPDKUIControlType::SELECT,
            'name'    => 'mail_slug_confirm_post_type',
            'options' => $post_types_options,
            'value'   => $sub_configuration->mail_slug_confirm_post_type,
            'append'  => sprintf( '<a data-post_type="#mail_slug_confirm_post_type" data-url="%s" class="wpx-users-manager-create-post button button-small" href="#">%s</a>', $new_url, __( 'Create new', WPXUSERSMANAGER_TEXTDOMAIN ) ),
          ),
        ),

        ( wpdk_is_bool( $sub_configuration->double_optin ) && empty( $sub_configuration->mail_slug_confirm ) ) ? array(
          array(
            'type'           => WPDKUIControlType::ALERT,
            'alert_type'     => WPDKTwitterBootstrapAlertType::ALERT,
            'dismiss_button' => false,
            'classes'        => 'alert-arrow-top',
            'value'          => __( '<h4>You have to set the above mail slug in order to use the Double Opt-In.</h4>', WPXUSERSMANAGER_TEXTDOMAIN ),
          )
        ) : '',

        array(
          array(
            'type'   => WPDKUIControlType::TEXT,
            'name'   => 'page_slug_confirm',
            'data'   => array(
              'autocomplete' => 'posts',
              'post_type'    => 'page'
            ),
            'label'  => __( 'Confirmation page slug', WPXUSERSMANAGER_TEXTDOMAIN ),
            'size'   => '32',
            'value'  => $sub_configuration->page_slug_confirm,
            'title'  => __( 'Press down key to retrive Page list or press any key to search for slug or title.', WPXUSERSMANAGER_TEXTDOMAIN ),
            'append' => sprintf( '<a class="button button-small" href="%s">%s</a>', $page_new_url, __( 'Create new', WPXUSERSMANAGER_TEXTDOMAIN ) ),
          )
        ),

        ( wpdk_is_bool( $sub_configuration->double_optin ) && empty( $sub_configuration->page_slug_confirm ) ) ? array(
          array(
            'type'           => WPDKUIControlType::ALERT,
            'alert_type'     => WPDKTwitterBootstrapAlertType::ALERT,
            'dismiss_button' => false,
            'classes'        => 'alert-arrow-top',
            'value'          => __( '<h4>You have to set the above page slug in order to use the Double Opt-In.</h4>', WPXUSERSMANAGER_TEXTDOMAIN ),
          )
        ) : '',

      ),
    );

    return $fields;
  }

  // -----------------------------------------------------------------------------------------------------------------
  // Configuration actions
  // -----------------------------------------------------------------------------------------------------------------

  /**
   * Process and set you own post data
   *
   * @brief Update configuration
   *
   * @return bool TRUE to update the configuration and display the standard sucessfully message, or FALSE to avoid
   * the update configuration and do a custom display.
   */
  public function updatePostData() {

    /* Get configuration instance. */
    $configuration = WPXUsersManagerConfiguration::init();

    /* This specific sub configuration branch. */
    $sub_configuration = $configuration->signup;

    /* @todo 'subscriber' deve diventare una costante. Vedi WPDKRole e ... */

    $sub_configuration->double_optin                = isset( $_POST['double_optin'] ) ? $_POST['double_optin'] : 'n';
    $sub_configuration->page_slug_confirm           = esc_attr( $_POST['page_slug_confirm'] );
    $sub_configuration->mail_slug_confirm           = esc_attr( $_POST['mail_slug_confirm'] );
    $sub_configuration->mail_slug_confirm_post_type = esc_attr( $_POST['mail_slug_confirm_post_type'] );

    return true;
  }

}
