<?php
/**
 * Extra fields configuration (jQuery tabs) view controller
 *
 * @class              WPXUsersManagerConfigurationProfileViewController
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-18
 * @version            0.8.3
 *
 */
class WPXUsersManagerConfigurationProfileViewController extends WPDKjQueryTabsViewController {

  /**
   * Create an instance of WPXUsersManagerConfigurationProfileViewController class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerConfigurationProfileViewController
   */
  public function __construct() {

    /* Single instances of tab content. */
    $profile_view      = new WPXUsersManagerConfigurationProfileView();
    $extra_fields_view = new WPXUsersManagerConfigurationExtraFieldsView();

    /* Create each single tab. */
    $tabs = array(
      new WPDKjQueryTab( 'profile', __( 'Display Profile', WPXUSERSMANAGER_TEXTDOMAIN ), $profile_view->html() ),
      new WPDKjQueryTab( 'extra_fields', __( 'Extra Fields', WPXUSERSMANAGER_TEXTDOMAIN ), $extra_fields_view->html() ),
    );

    /* Create the view. */
    $view = new WPDKjQueryTabsView( 'wpx-users-manager-configuration-profile', $tabs );

    parent::__construct( 'wpx-users-manager-configuration-profile', __( 'Users Profile', WPXUSERSMANAGER_TEXTDOMAIN ), $view );

  }

  /**
   * Call when this view controller is loaded
   *
   * @brief Head loaded
   */
  public static function didHeadLoad() {
    /* Scripts */
    wp_enqueue_script( 'wpx-users-manager-admin', WPXUSERSMANAGER_URL_JAVASCRIPT . 'wpx-users-manager-admin.js', array( 'jquery' ), WPXUSERSMANAGER_VERSION, true );
    wp_enqueue_style( 'wpx-users-manager-admin', WPXUSERSMANAGER_URL_CSS . 'wpx-users-manager-admin.css', array(), WPXUSERSMANAGER_VERSION );
  }
}

/**
 * Profile configuration view
 *
 * @class              WPXUsersManagerConfigurationProfileView
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-18
 * @version            0.11.1
 */
class WPXUsersManagerConfigurationProfileView extends WPDKConfigurationView {
  /**
   * Create an instance of WPXUsersManagerConfigurationProfileView class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerConfigurationProfileView
   */
  public function __construct() {
    /* Get configuration instance. */
    $configuration = WPXUsersManagerConfiguration::init();

    /* This specific sub configuration branch. */
    $sub_configuration = $configuration->profile;

    parent::__construct( 'profile', __( 'Profile', WPXUSERSMANAGER_TEXTDOMAIN ), $configuration, $sub_configuration );
  }

  /**
   * Return an array in sdf format for form fileds
   *
   * @brief Form fields
   *
   * @return array
   */
  public function fields() {

    /* Get configuration instance. */
    $configuration = WPXUsersManagerConfiguration::init();

    /* This specific sub configuration branch. */
    $sub_configuration = $configuration->profile;

    /* URL to create a new page. */
    $page_new_url = admin_url( 'post-new.php?post_type=page' );

    $fields = array(
      __( 'WordPress Integration', WPXUSERSMANAGER_TEXTDOMAIN ) => array(
        __( 'Choose a theme page to display an User Profile. Use the <code>wpx_users_manager_profile</code> shortcode.', WPXUSERSMANAGER_TEXTDOMAIN ),
        array(
          array(
            'type'   => WPDKUIControlType::TEXT,
            'name'   => 'page_slug',
            'data'   => array(
              'autocomplete' => 'posts',
              'post_type'    => 'page'
            ),
            'label'  => __( 'Page profile slug', WPXUSERSMANAGER_TEXTDOMAIN ),
            'size'   => '32',
            'value'  => $sub_configuration->page_slug,
            'title'  => __( 'Press down key to retrive Page list or press any key to search for slug or title.', WPXUSERSMANAGER_TEXTDOMAIN ),
            'append' => sprintf( '<a class="button button-small" href="%s">%s</a>', $page_new_url, __( 'Create new', WPXUSERSMANAGER_TEXTDOMAIN ) ),
            //'guide'  => 'how-to-update-your-bootstrap-php'
          )
        ),

        empty( $sub_configuration->page_slug ) ? array(
          array(
            'type'           => WPDKUIControlType::ALERT,
            'alert_type'     => WPDKTwitterBootstrapAlertType::ALERT,
            'dismiss_button' => false,
            'classes'        => 'alert-arrow-top',
            'value'          => __( '<h4>You have to set the page profile slug in order to use the shortcode.</h4>', WPXUSERSMANAGER_TEXTDOMAIN ),
          )
        ) : '',

      ),
    );

    return $fields;
  }

  // -----------------------------------------------------------------------------------------------------------------
  // Configuration actions
  // -----------------------------------------------------------------------------------------------------------------

  /**
   * Process and set you own post data
   *
   * @brief Update configuration
   *
   * @return bool TRUE to update the configuration and display the standard sucessfully message, or FALSE to avoid
   * the update configuration and do a custom display.
   */
  public function updatePostData() {

    /* Get configuration instance. */
    $configuration = WPXUsersManagerConfiguration::init();

    /* This specific sub configuration branch. */
    $sub_configuration = $configuration->profile;

    $sub_configuration->page_slug = esc_attr( $_POST['page_slug'] );

    return true;
  }
}

/**
 * Extra fields configuration view
 *
 * @class              WPXUsersManagerConfigurationExtraFieldsView
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-18
 * @version            0.12.1
 *
 */

class WPXUsersManagerConfigurationExtraFieldsView extends WPDKConfigurationView {

  /**
   * Create an instance of WPXUsersManagerConfigurationExtraFieldsView class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerConfigurationExtraFieldsView
   */
  public function __construct() {

    /* Get configuration instance. */
    $configuration = WPXUsersManagerConfiguration::init();

    /* This specific sub configuration branch. */
    $sub_configuration = $configuration->extraFields;

    parent::__construct( 'extra_fields', __( 'Extra Fields', WPXUSERSMANAGER_TEXTDOMAIN ), $configuration, $sub_configuration );
  }

  // -----------------------------------------------------------------------------------------------------------------
  // Custom display
  // -----------------------------------------------------------------------------------------------------------------

  /**
   * Use `content` instead `fields for custom content
   *
   * @brief Custom content
   */
  public function draw() {

    $fields_type = array(
      ''                          => __( 'Select a field type', WPXUSERSMANAGER_TEXTDOMAIN ),
      WPDKUIControlType::CHECKBOX => __( 'Checkbox', WPXUSERSMANAGER_TEXTDOMAIN ),
      WPDKUIControlType::SELECT   => __( 'Combo Select', WPXUSERSMANAGER_TEXTDOMAIN ),
      WPDKUIControlType::DATE     => __( 'Date', WPXUSERSMANAGER_TEXTDOMAIN ),
      WPDKUIControlType::DATETIME => __( 'Date Time', WPXUSERSMANAGER_TEXTDOMAIN ),
      WPDKUIControlType::EMAIL    => __( 'Email', WPXUSERSMANAGER_TEXTDOMAIN ),
      //WPDKUIControlType::FILE     => __( 'File', WPXUSERSMANAGER_TEXTDOMAIN ),
      WPDKUIControlType::NUMBER   => __( 'Number', WPXUSERSMANAGER_TEXTDOMAIN ),
      WPDKUIControlType::TEXT     => __( 'Text', WPXUSERSMANAGER_TEXTDOMAIN ),
      WPDKUIControlType::TEXTAREA => __( 'Text Area', WPXUSERSMANAGER_TEXTDOMAIN ),
      WPDKUIControlType::PASSWORD => __( 'Password', WPXUSERSMANAGER_TEXTDOMAIN ),
      WPDKUIControlType::SECTION  => __( 'Section', WPXUSERSMANAGER_TEXTDOMAIN ),
    );

    $columns = array(
      'type'        => array(
        'table_title' => __( 'Type', WPXUSERSMANAGER_TEXTDOMAIN ),
        'type'        => WPDKUIControlType::SELECT,
        'name'        => 'type[]',
        'class'       => 'wpxm_users_extra_field_type',
        'title'       => __( 'Select a field type', WPXUSERSMANAGER_TEXTDOMAIN ),
        'data'        => array( 'placement' => 'left' ),
        'options'     => $fields_type,
        'value'       => '',
      ),
      'name'        => array(
        'table_title' => __( 'Name', WPXUSERSMANAGER_TEXTDOMAIN ),
        'type'        => WPDKUIControlType::TEXT,
        'name'        => 'name[]',
        'class'       => 'wpxm_users_extra_field_name',
        //'attrs'       => array( 'required' => 'required' ),
        'data'        => array( 'placement' => 'left' ),
        'size'        => 12,
        'value'       => '',
      ),
      'label'       => array(
        'table_title' => __( 'Label', WPXUSERSMANAGER_TEXTDOMAIN ),
        'type'        => WPDKUIControlType::TEXT,
        'name'        => 'label[]',
        'class'       => 'wpxm_users_extra_field_label',
        //'attrs'       => array( 'required' => 'required' ),
        'data'        => array( 'placement' => 'left' ),
        'size'        => 16,
        'value'       => '',
      ),
      'placeholder' => array(
        'table_title' => __( 'Placeholder', WPXUSERSMANAGER_TEXTDOMAIN ),
        'type'        => WPDKUIControlType::TEXT,
        'name'        => 'placeholder[]',
        'class'       => 'wpxm_users_extra_field_placeholder',
        'data'        => array( 'placement' => 'left' ),
        'size'        => 16,
        'value'       => '',
      ),
      'value'       => array(
        'table_title' => __( 'Default value', WPXUSERSMANAGER_TEXTDOMAIN ),
        'type'        => WPDKUIControlType::TEXT,
        'name'        => 'value[]',
        'class'       => 'wpxm_users_extra_field_value',
        'data'        => array( 'placement' => 'left' ),
        'size'        => 10,
        'value'       => '',
      ),
    );

    /* Get configuration instance. */
    $configuration = WPXUsersManagerConfiguration::init();

    /* This specific sub configuration branch. */
    $sub_configuration = $configuration->extraFields;

    $items           = $sub_configuration->fields;
    $table           = new WPDKDynamicTable( 'wpxm-dynamic-table-extra-fields', $columns, $items );
    $table->sortable = true;

    /* Helps. */
    $icon_add              = '<span style="float:none;display:inline-block;margin:0 8px 0 4px" class="wpdk-dt-add-row"></span>';
    $icon_del              = '<span style="float:none;display:inline-block;margin:0 8px 0 4px" class="wpdk-dt-delete-row"></span>';
    $message               = sprintf( __( 'Click on %s to add a row. Click on %s to delete a row.', WPXUSERSMANAGER_TEXTDOMAIN ), $icon_add, $icon_del );
    $alert                 = new WPDKTwitterBootstrapAlert( 'wpxm-extra-fields', $message, WPDKTwitterBootstrapAlertType::INFORMATION );
    $alert->dismissButton  = false;
    $alert->block          = true;
    $alert->display();

    ?>
  <form class="wpdk-settings-view-<?php echo $this->id ?> wpdk-form" action="" method="post">
    <?php
    echo WPDKUI::inputNonce( $this->id );

    $table->display();
    echo WPDKUI::buttonsUpdateReset();
    ?></form><?php
  }

  // -----------------------------------------------------------------------------------------------------------------
  // Configuration actions
  // -----------------------------------------------------------------------------------------------------------------

  /**
   * Process and set you own post data. Return  TRUE to update the configuration and display the standard sucessfully
   * message, or FALSE to avoid the update configuration and do a custom display.
   *
   * @brief Update configuration
   *
   * @return bool
   */
  public function updatePostData() {

    /* Get configuration instance. */
    $configuration = WPXUsersManagerConfiguration::init();

    /* This specific sub configuration branch. */
    $sub_configuration = $configuration->extraFields;

    $sub_configuration->fields = array();
    for ( $i = 0; $i < count( $_POST['type'] ); $i++ ) {
      $name = esc_attr( sanitize_key( $_POST['name'][$i] ) );
      if ( !empty( $_POST['type'][$i] ) && !empty( $name ) ) {
        $sub_configuration->fields[] = array(
          'type'        => $_POST['type'][$i],
          'name'        => $name,
          'label'       => $_POST['label'][$i],
          'placeholder' => $_POST['placeholder'][$i],
          'value'       => $_POST['value'][$i],
        );
      }
    }

    return true;
  }
}