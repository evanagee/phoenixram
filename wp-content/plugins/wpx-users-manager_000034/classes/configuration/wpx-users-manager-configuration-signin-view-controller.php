<?php
/**
 * Users Manager configuration (jQuery tabs) view controller
 *
 * @class              WPXUsersManagerConfigurationSigninViewController
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-18
 * @version            0.8.3
 *
 */
class WPXUsersManagerConfigurationSigninViewController extends WPDKjQueryTabsViewController {

  /**
   * Create an instance of WPXUsersManagerConfigurationSigninViewController class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerConfigurationSigninViewController
   */
  public function __construct() {

    /* Single instances of tab content. */
    $singin_view            = new WPXUsersManagerConfigurationSigninView();
    $reset_password         = new WPXUsersManagerConfigurationResetPasswordView();
    $resend_unlock_url_view = new WPXUsersManagerConfigurationResendUnlockURLView();

    /* Create each single tab. */
    $tabs = array(
      new WPDKjQueryTab( 'signin', __( 'Signin (Login)', WPXUSERSMANAGER_TEXTDOMAIN ), $singin_view->html() ),
      new WPDKjQueryTab( 'reset-password', __( 'Reset Password', WPXUSERSMANAGER_TEXTDOMAIN ), $reset_password->html() ),
      new WPDKjQueryTab( 'resend-unlock-url', __( 'Resend unlock URL', WPXUSERSMANAGER_TEXTDOMAIN ), $resend_unlock_url_view->html() ),
    );

    /* Create the view. */
    $view = new WPDKjQueryTabsView( 'wpx-users-manager-configuration', $tabs );

    parent::__construct( 'wpx-users-manager-configuration', __( 'Signin settings', WPXUSERSMANAGER_TEXTDOMAIN ), $view );

  }

  /**
   * Call when this view controller is loaded
   *
   * @brief Head loaded
   */
  public static function didHeadLoad() {
    /* Scripts */
    wp_enqueue_script( 'wpx-users-manager-admin',
      WPXUSERSMANAGER_URL_JAVASCRIPT . 'wpx-users-manager-admin.js', array( 'jquery' ), WPXUSERSMANAGER_VERSION, true );
    wp_enqueue_style( 'wpx-users-manager-admin',
      WPXUSERSMANAGER_URL_CSS . 'wpx-users-manager-admin.css', array(), WPXUSERSMANAGER_VERSION );

  }
}


/**
 * Signin configuration view
 *
 * @class              WPXUsersManagerConfigurationSigninView
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-18
 * @version            0.11.1
 */

class WPXUsersManagerConfigurationSigninView extends WPDKConfigurationView {
  /**
   * Create an instance of WPXUsersManagerConfigurationSigninView class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerConfigurationSigninView
   */
  public function __construct() {
    /* Get configuration instance. */
    $configuration = WPXUsersManagerConfiguration::init();

    /* This specific sub configuration branch. */
    $sub_configuration = $configuration->signin;

    parent::__construct( 'signin', __( 'Signin', WPXUSERSMANAGER_TEXTDOMAIN ), $configuration, $sub_configuration );
  }

  /**
   * Return an array in sdf format for form fileds
   *
   * @brief Form fields
   *
   * @return array
   */
  public function fields() {

    /* Get configuration instance. */
    $configuration = WPXUsersManagerConfiguration::init();

    /* This specific sub configuration branch. */
    $sub_configuration = $configuration->signin;

    /* URL to create a new page. */
    $page_new_url = admin_url( 'post-new.php?post_type=page' );

    $fields = array(
      __( 'WordPress integration', WPXUSERSMANAGER_TEXTDOMAIN ) => array(

        __( 'Choose a theme page to display an User Signin (login). Use the <code>wpx_users_manager_signin</code> shortcode.', WPXUSERSMANAGER_TEXTDOMAIN ),
        array(
          array(
            'type'   => WPDKUIControlType::TEXT,
            'name'   => 'page_slug',
            'data'   => array(
              'autocomplete' => 'posts',
              'post_type'    => 'page'
            ),
            'label'  => __( 'Page signin slug', WPXUSERSMANAGER_TEXTDOMAIN ),
            'size'   => '32',
            'value'  => $sub_configuration->page_slug,
            'title'  => __( 'Press down key to retrive Page list or press any key to search for slug or title.', WPXUSERSMANAGER_TEXTDOMAIN ),
            'append' => sprintf( '<a class="button button-small" href="%s">%s</a>', $page_new_url, __( 'Create new', WPXUSERSMANAGER_TEXTDOMAIN ) ),
          )
        ),

        empty( $sub_configuration->page_slug ) ? array(
          array(
            'type'           => WPDKUIControlType::ALERT,
            'alert_type'     => WPDKTwitterBootstrapAlertType::ALERT,
            'dismiss_button' => false,
            'classes'        => 'alert-arrow-top',
            'value'          => __( '<h4>You have to set the signin page slug in order to use the shortcode.</h4>', WPXUSERSMANAGER_TEXTDOMAIN ),
          )
        ) : '',

        array(
          array(
            'type'  => WPDKUIControlType::SWIPE,
            'name'  => 'remember',
            'label' => __( 'Display the remember option checkbox', WPXUSERSMANAGER_TEXTDOMAIN ),
            'value' => $sub_configuration->remember,
          ),
        ),
      ),

    );
    return $fields;
  }

  // -----------------------------------------------------------------------------------------------------------------
  // Configuration actions
  // -----------------------------------------------------------------------------------------------------------------

  /**
   * Process and set you own post data
   *
   * @brief Update configuration
   *
   * @return bool TRUE to update the configuration and display the standard sucessfully message, or FALSE to avoid
   * the update configuration and do a custom display.
   */
  public function updatePostData() {

    /* Get configuration instance. */
    $configuration = WPXUsersManagerConfiguration::init();

    /* This specific sub configuration branch. */
    $sub_configuration = $configuration->signin;

    $sub_configuration->page_slug = esc_attr( $_POST['page_slug'] );
    $sub_configuration->remember  = isset( $_POST['remember'] ) ? $_POST['remember'] : 'n';

    return true;
  }
}


/**
 * Reset Password configuration view
 *
 * @class              WPXUsersManagerConfigurationResetPasswordView
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-18
 * @version            0.11.1
 */
class WPXUsersManagerConfigurationResetPasswordView extends WPDKConfigurationView {
  /**
   * Create an instance of WPXUsersManagerConfigurationResetPasswordView class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerConfigurationResetPasswordView
   */
  public function __construct() {
    /* Get configuration instance. */
    $configuration = WPXUsersManagerConfiguration::init();

    /* This specific sub configuration branch. */
    $sub_configuration = $configuration->resetPassword;

    parent::__construct( 'reset-password', __( 'Reset Password', WPXUSERSMANAGER_TEXTDOMAIN ), $configuration, $sub_configuration );
  }

  /**
   * Return an array in sdf format for form fileds
   *
   * @brief Form fields
   *
   * @return array
   */
  public function fields() {

    /* Get configuration instance. */
    $configuration = WPXUsersManagerConfiguration::init();

    /* This specific sub configuration branch. */
    $sub_configuration = $configuration->resetPassword;

    $post_new_url = admin_url( 'post-new.php?post_type=' );

    /* Post types. */
    $post_types         = get_post_types( array( 'public' => true ) );
    $post_types         = apply_filters( 'wpx_users_manager_post_types', $post_types );
    $post_types_options = array();


    foreach ( $post_types as $key ) {
      $post_type                = get_post_type_object( $key );
      $post_types_options[$key] = $post_type->labels->singular_name;
    }

    $post_types_options = apply_filters( 'wpx_users_manager_post_types_options', $post_types_options );

    $fields = array(
      __( 'Password Reset', WPXUSERSMANAGER_TEXTDOMAIN ) => array(
        __( 'Use the <code>wpx_users_manager_reset_password</code> shortcode.', WPXUSERSMANAGER_TEXTDOMAIN ),
        array(
          array(
            'type'  => WPDKUIControlType::TEXT,
            'name'  => 'mail_slug',
            'data'  => array(
              'autocomplete' => 'posts',
              'post_type'    => '#post_type_reset'
            ),
            'label' => __( 'Mail slug for password reset', WPXUSERSMANAGER_TEXTDOMAIN ),
            'value' => $sub_configuration->mail_slug,
            'title' => __( 'Start typing the title (or the slug) of the Mail you\'ve previously created for the password reset message.', WPXUSERSMANAGER_TEXTDOMAIN ),
          ),
          array(
            'type'    => WPDKUIControlType::SELECT,
            'name'    => 'post_type_reset',
            'options' => $post_types_options,
            'value'   => $sub_configuration->mail_slug_post_type,
            'append'  => sprintf( '<a data-post_type="#post_type_reset" data-url="%s" class="wpx-users-manager-create-post button button-small" href="#">%s</a>', $post_new_url, __( 'Create new', WPXUSERSMANAGER_TEXTDOMAIN ) ),
          ),
        ),

        empty( $sub_configuration->mail_slug ) ? array(
          array(
            'type'           => WPDKUIControlType::ALERT,
            'alert_type'     => WPDKTwitterBootstrapAlertType::ALERT,
            'dismiss_button' => false,
            'classes'        => 'alert-arrow-top',
            'value'          => __( '<h4>You have to set the mail slug in order to use the shortcode.</h4>', WPXUSERSMANAGER_TEXTDOMAIN ),
          )
        ) : '',

        array(
          array(
            'type'    => WPDKUIControlType::SELECT,
            'name'    => 'timeout',
            'label'   => __( 'Reset Password timeout', WPXUSERSMANAGER_TEXTDOMAIN ),
            'value'   => $sub_configuration->timeout,
            'options' => array(
              ( 60 * 60 * 1 )      => __( 'One hour', WPXUSERSMANAGER_TEXTDOMAIN ),
              ( 60 * 60 * 6 )      => __( 'Six hours', WPXUSERSMANAGER_TEXTDOMAIN ),
              ( 60 * 60 * 12 )     => __( 'Twelve hours', WPXUSERSMANAGER_TEXTDOMAIN ),
              ( 60 * 60 * 24 )     => __( 'One day', WPXUSERSMANAGER_TEXTDOMAIN ),
              ( 60 * 60 * 24 * 2 ) => __( 'Two days', WPXUSERSMANAGER_TEXTDOMAIN ),
            )
          )
        ),
      ),

    );

    return $fields;
  }

  // -----------------------------------------------------------------------------------------------------------------
  // Configuration actions
  // -----------------------------------------------------------------------------------------------------------------

  /**
   * Process and set you own post data
   *
   * @brief Update configuration
   *
   * @return bool TRUE to update the configuration and display the standard sucessfully message, or FALSE to avoid
   * the update configuration and do a custom display.
   */
  public function updatePostData() {

    /* Get configuration instance. */
    $configuration = WPXUsersManagerConfiguration::init();

    /* This specific sub configuration branch. */
    $sub_configuration = $configuration->resetPassword;

    $sub_configuration->mail_slug           = esc_attr( $_POST['mail_slug'] );
    $sub_configuration->mail_slug_post_type = esc_attr( $_POST['post_type_reset'] );
    $sub_configuration->timeout             = esc_attr( $_POST['timeout'] );

    return true;
  }
}

/**
 * Resend unlock url configuration view
 *
 * @class              WPXUsersManagerConfigurationResendUnlockURLView
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-03-25
 * @version            0.1.0
 */
class WPXUsersManagerConfigurationResendUnlockURLView extends WPDKConfigurationView {
  /**
   * Create an instance of WPXUsersManagerConfigurationResendUnlockURLView class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerConfigurationResendUnlockURLView
   */
  public function __construct() {
    /* Get configuration instance. */
    $configuration = WPXUsersManagerConfiguration::init();

    /* This specific sub configuration branch. */
    $sub_configuration = $configuration->resetPassword;

    parent::__construct( 'resend-unlock-url', __( 'Resend unlock URL', WPXUSERSMANAGER_TEXTDOMAIN ), $configuration, $sub_configuration );
  }

  /**
   * Return an array in sdf format for form fileds
   *
   * @brief Form fields
   *
   * @return array
   */
  public function fields() {

    /* Get configuration instance. */
    $configuration = WPXUsersManagerConfiguration::init();

    /* This specific sub configuration branch. */
    $sub_configuration = $configuration->resetPassword;

    $post_new_url = admin_url( 'post-new.php?post_type=' );

    /* Post types. */
    $post_types         = get_post_types( array( 'public' => true ) );
    $post_types         = apply_filters( 'wpx_users_manager_post_types', $post_types );
    $post_types_options = array();

    foreach ( $post_types as $key ) {
      $post_type                = get_post_type_object( $key );
      $post_types_options[$key] = $post_type->labels->singular_name;
    }

    $post_types_options = apply_filters( 'wpx_users_manager_post_types_options', $post_types_options );

    $fields = array(
      __( 'Resend unlock URL', WPXUSERSMANAGER_TEXTDOMAIN ) => array(
        __( 'Use the <code>wpx_users_manager_resend_unlock_url</code> shortcode.', WPXUSERSMANAGER_TEXTDOMAIN ),
      ),

    );

    return $fields;
  }

  /**
   * Override this method because now this view has not any configuration.
   *
   * @brief Buttons
   *
   * @return string
   */
  public function buttonsUpdateReset() {
    return '';
  }

  // -----------------------------------------------------------------------------------------------------------------
  // Configuration actions
  // -----------------------------------------------------------------------------------------------------------------

  /**
   * Process and set you own post data
   *
   * @brief Update configuration
   *
   * @return bool TRUE to update the configuration and display the standard sucessfully message, or FALSE to avoid
   * the update configuration and do a custom display.
   */
  public function updatePostData() {
    return false;
  }
}