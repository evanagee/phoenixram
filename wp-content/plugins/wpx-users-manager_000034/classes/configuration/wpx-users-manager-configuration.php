<?php
/**
 * Sample configuration class. In this class you define your tree configuration.
 *
 * @class              WPXUsersManagerConfiguration
 * @author             wpXtreme, Inc.
 * @copyright          Copyright (C) 2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-18
 * @version            0.2.0
 */
class WPXUsersManagerConfiguration extends WPDKConfiguration {

  /**
   * The config name used on database
   *
   * @brief Config name
   *
   * @var string
   */
  const CONFIGURATION_NAME = 'wpx-users-manager-configuration';

  /**
   * Your own configuration property
   *
   * @brief Configuration version
   *
   * @var string $version
   */
  public $version = WPXUSERSMANAGER_VERSION;

  /**
   * Instance of WPXUsersManagerConfigurationExtraFields class model
   *
   * @brief Extra fields
   *
   * @var WPXUsersManagerConfigurationExtraFields $extraFields
   */
  public $extraFields;

  /**
   * Instance of WPXUsersManagerConfigurationSignin class model
   *
   * @brief Signin
   *
   * @var WPXUsersManagerConfigurationSignin $signin
   */
  public $signin;

  /**
   * Instance of WPXUsersManagerConfigurationSignup class model
   *
   * @brief Sign up
   *
   * @var WPXUsersManagerConfigurationSignup $signup
   */
  public $signup;

  /**
   * Instance of WPXUsersManagerConfigurationResetPassword class model
   *
   * @brief Reset Password
   *
   * @var WPXUsersManagerConfigurationResetPassword $resetPassword
   */
  public $resetPassword;

  /**
   * Instance of WPXUsersManagerConfigurationProfile class model
   *
   * @brief Profile
   *
   * @var WPXUsersManagerConfigurationProfile $profile
   */
  public $profile;

  /**
   * Return an instance of WPXUsersManagerConfiguration class from the database or onfly.
   *
   * @brief Get the configuration
   *
   * @return WPXUsersManagerConfiguration
   */
  public static function init() {
    $instance = parent::init( self::CONFIGURATION_NAME, __CLASS__ );

    /* Or if the obfly version is different from stored version. */
    if ( version_compare( $instance->version, WPXUSERSMANAGER_VERSION ) < 0 ) {
      /* For i.e. you would like update the version property. */
      $instance->version = WPXUSERSMANAGER_VERSION;
      $instance->update();
    }

    return $instance;
  }

  /**
   * Create an instance of WPXUsersManagerConfiguration class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerConfiguration
   */
  public function __construct() {
    parent::__construct( self::CONFIGURATION_NAME );

    /* Init my tree settings. */
    $this->extraFields   = new WPXUsersManagerConfigurationExtraFields();
    $this->signin        = new WPXUsersManagerConfigurationSignin();
    $this->signup        = new WPXUsersManagerConfigurationSignup();
    $this->resetPassword = new WPXUsersManagerConfigurationResetPassword();
    $this->profile       = new WPXUsersManagerConfigurationProfile();
  }

}


/**
 * Description
 *
 * ## Overview
 *
 * Description
 *
 * @class           WPXUsersManagerConfigurationExtraFields
 * @author          =undo= <info@wpxtre.me>
 * @copyright       Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date            2013-02-18
 * @version         1.0.0
 *
 */
class WPXUsersManagerConfigurationExtraFields {

  /**
   * The extra custom fields for an user
   *
   * @brief List of extra custom fields
   *
   * @var array $fields
   */
  public $fields;

  /**
   * Create an instance of WPXUsersManagerConfigurationExtraFields class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerConfigurationExtraFields
   */
  public function __construct() {
    $this->defaults();
  }

  /**
   * Reset to default values
   *
   * @brief Dafaults
   */
  public function defaults() {
    $this->fields = array(
      array(
        'type'  => WPDKUIControlType::TEXT,
        'label' => __( 'Address', WPXTREME_TEXTDOMAIN ),
        'size'  => 32,
        'name'  => 'bill_address',
        'value' => ''
      ),
      array(
        'type'  => WPDKUIControlType::NUMBER,
        'label' => __( 'ZIP code', WPXTREME_TEXTDOMAIN ),
        'size'  => 6,
        'name'  => 'bill_zipcode',
        'value' => ''
      ),
      array(
        'type'  => WPDKUIControlType::TEXT,
        'label' => __( 'Town', WPXTREME_TEXTDOMAIN ),
        'size'  => 11,
        'name'  => 'bill_town',
        'value' => ''
      ),
    );
  }

}


/**
 * Users Manager Signin configuration model
 *
 * @class              WPXUsersManagerConfigurationSignin
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-26
 * @version            1.0.7
 *
 */
class WPXUsersManagerConfigurationSignin {

  /**
   * Display the remember
   *
   * @brieif Remember
   *
   * @var string $remember
   */
  public $remember;

  /**
   * The signin page slug
   *
   * @brief Signin page
   *
   * @var string $page_slug
   */
  public $page_slug;

  /**
   * Create an instance of WPXUsersManagerConfigurationSignin class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerConfigurationSignin
   */
  public function __construct() {
    $this->defaults();
  }

  /**
   * Reset to default values
   *
   * @brief Dafaults
   */
  public function defaults() {
    $this->page_slug = '';
    $this->remember  = 'y';
  }

}


/**
 * Users Manager Signup configuration model
 *
 * @class              WPXUsersManagerConfigurationSignup
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2012-11-28
 * @version            0.16.1
 *
 */
class WPXUsersManagerConfigurationSignup {

  public $page_registration_slug;
  public $default_user_role;
  public $default_user_status;
  public $page_slug_confirm;
  public $page_slug_confirmed;

  public $double_optin;
  public $mail_slug_confirm;
  public $mail_slug_confirm_post_type;
  public $mail_slug_confirmed;
  public $mail_slug_confirmed_post_type;

  /**
   * Create an instance of WPXUsersManagerConfigurationSignup class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerConfigurationSignup
   */
  public function __construct() {
    $this->defaults();
  }

  /**
   * Reset to default values
   *
   * @brief Dafaults
   */
  public function defaults() {
    $this->page_registration_slug = '';
    $this->default_user_role      = 'subscriber';
    $this->default_user_status    = 'disabled';
    $this->page_slug_confirmed    = '';
    $this->page_slug_confirm      = '';

    $this->double_optin                  = 'y';
    $this->mail_slug_confirm             = '';
    $this->mail_slug_confirm_post_type   = WPDKPostType::PAGE;
    $this->mail_slug_confirmed           = '';
    $this->mail_slug_confirmed_post_type = WPDKPostType::PAGE;
  }
}


/**
 * Users Manager Reset Password configuration model
 *
 * @class              WPXUsersManagerConfigurationResetPassword
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-18
 * @version            0.13.12
 *
 */
class WPXUsersManagerConfigurationResetPassword {

  /**
   * Slug of CPT Mail. This mail is send when a password reset is request
   *
   * @brief Slug mail
   *
   * @var string $mail_slug
   */
  public $mail_slug;
  public $mail_slug_post_type;

  /**
   * Seconds for transient storage of automatic login for reset password
   *
   * @brief Timeout for reset password
   *
   * @var int $timeout
   */
  public $timeout;


  /**
   * Create an instance of WPXUsersManagerConfigurationResetPassword class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerConfigurationResetPassword
   */
  public function __construct() {
    $this->defaults();
  }

  /**
   * Reset to default values
   *
   * @brief Dafaults
   */
  public function defaults() {
    $this->mail_slug           = '';
    $this->mail_slug_post_type = WPDKPostType::PAGE;
    $this->timeout             = 60 * 60 * 24;
  }

}


/**
 * Users Manager Profile configuration model
 *
 * @class              WPXUsersManagerConfigurationProfile
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-18
 * @version            0.13.12
 *
 */
class WPXUsersManagerConfigurationProfile {

  /**
   * Page profile slug
   *
   * @brief Page profile slug
   *
   * @var string $page_slug
   */
  public $page_slug;

  /**
   * Create an instance of WPXUsersManagerConfigurationProfile class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerConfigurationProfile
   */
  public function __construct() {
    $this->defaults();
  }

  /**
   * Reset to default values
   *
   * @brief Dafaults
   */
  public function defaults() {
    $this->page_slug = '';
  }

}