<?php
/**
 * Brief Description here
 *
 * ## Overview
 * Markdown here
 *
 * @class              WPXUsersManagerAdmin
 * @author             wpXtreme, Inc.
 * @copyright          Copyright (C) 2012 wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-08 12:56:31
 * @version            0.1.0
 *
 */

class WPXUsersManagerAdmin extends WPDKWordPressAdmin {

  /**
   * This is the minumun capability required to display admin menu item
   *
   * @brief Menu capability
   */
  const MENU_CAPABILITY = 'manage_options';


  /**
   * Create and return a singleton instance of WPXUsersManagerAdmin class
   *
   * @brief Init
   *
   * @param WPXUsersManager $plugin The main class of this plugin.
   *
   * @return WPXUsersManagerAdmin
   */
  public static function init( WPXUsersManager $plugin ) {
    static $instance = null;
    if ( is_null( $instance ) ) {
      $instance = new WPXUsersManagerAdmin( $plugin );
    }
    return $instance;
  }



  /**
   * Create an instance of WPXUsersManagerAdmin class
   *
   * @brief Construct
   *
   * @param WPXUsersManager $plugin Main class of your plugin
   *
   * @return WPXUsersManagerAdmin
   */
  public function __construct( WPXUsersManager $plugin ) {
    parent::__construct( $plugin );

    /* Loading Script & style for backend */
    add_action( 'admin_head', array( $this, 'admin_head' ) );

    /* Enable link on Control Panel. */
    add_filter( 'wpxm_control_panel_widget_url-' . $this->plugin->name, array( $this, 'controlPanel' ) );
  }

  /**
   * Return the complete URL for the control panel
   *
   * @brief Control Panel
   *
   * @return string
   */
  public function controlPanel() {
    return WPDKMenu::url( 'WPXUsersManagerConfigurationProfileViewController' );
  }

  /**
   * Called by WPDKWordPressAdmin parent when the admin head is loaded
   *
   * @brief Admin head
   */
  public function admin_head() {
    wp_enqueue_style( 'wpx-users-manager-plugins-list', $this->plugin->cssURL . 'wpx-users-manager-plugins-list.css' );
  }

  /**
   * Called when WordPress is ready to build the admin menu.
   * Sample hot to build a simple menu.
   *
   * @brief Admin menu
   */
  public function admin_menu() {

    /* Hack for wpXtreme icon. */
    $icon_menu = $this->plugin->imagesURL . 'logo-16x16.png';

    $menus = array(
      'wpx_users_manager' => array(
        'menuTitle'  => __( 'Users Manager', WPXUSERSMANAGER_TEXTDOMAIN ),
        'capability' => self::MENU_CAPABILITY,
        'icon'       => $icon_menu,
        'subMenus'   => array(
          array(
            'menuTitle'      => __( 'Profile', WPXUSERSMANAGER_TEXTDOMAIN ),
            'capability'     => self::MENU_CAPABILITY,
            'viewController' => 'WPXUsersManagerConfigurationProfileViewController',
          ),

          array(
            'menuTitle'      => __( 'Signup', WPXUSERSMANAGER_TEXTDOMAIN ),
            'capability'     => self::MENU_CAPABILITY,
            'viewController' => 'WPXUsersManagerConfigurationSignupViewController',
          ),

          array(
            'menuTitle'      => __( 'Signin', WPXUSERSMANAGER_TEXTDOMAIN ),
            'capability'     => self::MENU_CAPABILITY,
            'viewController' => 'WPXUsersManagerConfigurationSigninViewController',
          ),

          WPDKSubMenuDivider::DIVIDER,

          array(
            'menuTitle'      => __( 'About', WPXUSERSMANAGER_TEXTDOMAIN ),
            'viewController' => 'WPXUsersManagerAboutViewController',
          ),
        )
      )
    );

    WPDKMenu::renderByArray( $menus );

  }
}
