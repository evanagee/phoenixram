<?php
/**
 * Standard about view controller
 *
 * @class           WPXUsersManagerAboutViewController
 * @author          =undo= <info@wpxtre.me>
 * @copyright       Copyright (C) 2013 wpXtreme Inc. All Rights Reserved.
 * @date            2013-02-21
 * @version         1.0.0
 *
 */
class WPXUsersManagerAboutViewController extends WPXAboutViewController {

  /**
   * Create an instance of WPXUsersManagerAboutViewController class
   *
   * @brief Construct
   *
   * @return WPXUsersManagerAboutViewController
   */
  public function __construct() {
    parent::__construct( $GLOBALS['WPXUsersManager'] );
  }

  /**
   * Call when this view controller is loaded
   *
   * @brief Head loaded
   */
  public static function didHeadLoad() {
    wp_enqueue_style( 'wpx-users-manager-admin',
      WPXUSERSMANAGER_URL_CSS . 'wpx-users-manager-admin.css', array(), WPXUSERSMANAGER_VERSION );

  }

}
