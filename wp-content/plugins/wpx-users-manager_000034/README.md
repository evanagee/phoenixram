# Users Manager

## Overview

Manage login (sign in), registration (sign up) and profile with extra fields

## System Requirements

* wpXtreme framework 1.0.0.b4 or higher (last version is suggested)
* Wordpress 3.5 or higher (last version is suggest)
* PHP version 5.2.4 or greater
* MySQL version 5.0 or greater

## Optional

* Mail manager plugin in order to send mail to the user and Double Opt-In signup
* Roles & Capabilities...

## Browser compatibility

* We suggest to update your browser always at its last version.

## Versioning

For transparency and insight into our release cycle, and for striving to maintain backward compatibility, this code will be maintained under the Semantic Versioning guidelines as much as possible.

Releases will be numbered with the following format:

`<major>.<minor>.<patch>`

And constructed with the following guidelines:

* Breaking backward compatibility bumps the major (and resets the minor and patch)
* New additions without breaking backward compatibility bumps the minor (and resets the patch)
* Bug fixes and misc changes bumps the patch

For more information on SemVer, please visit http://semver.org/.

## Getting started

The Users Manager Plugin allow:

* Display a customizable Sign in (Login) form from shortcode [wpx_users_manager_signin] or widget
* Display a customizable Sign up (Registration) form from shortcode [wpx_users_manager_signup] or widget
* Display a customizable fontend user profile to show or edit
* Manage a customizable extra user fields
* Display a customizable fontend user reset password (* only with Mail Manager)
* Display a customizable fontend reset unlock code for Double Opt-In featured