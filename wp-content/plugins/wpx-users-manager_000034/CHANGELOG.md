# CHANGELOG

## Version 1.0.5
### 2013-06-11

#### Bugs

* Fixed name of filters

#### Enhancements

* Minor cleanup
* wpXtreme framework 1.0.0 compatibility


## Version 1.0.2
### 2013-05-09

#### Bugs

* Fixed wrong shortcode name in documentation



## Version 1.0.1
### 2013-05-03

#### Enhancements

* Updated kickstart.php

#### Bugs

* Fixed wrong url in create new button in configuration


## Version 1.0.0
### 2013-02-28

* First release