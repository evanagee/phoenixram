<?php
/**
 * Add a several enhancer to users
 *
 * @class              WPXtremeEnhancerUser
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-26
 * @version            1.0.3
 *
 */

final class WPXtremeEnhancerUser {

  /**
   * The original role filter
   *
   * @brief Roles
   *
   * @var array $_views
   */
  private $_views;

  /**
   * Create an instance of WPXtremeEnhancerUser class
   *
   * @brief Construct
   *
   * @return WPXtremeEnhancerUser
   */
  private function __construct() {

    /* Remove standard WordPress filter for role. */
    add_action( 'views_users', array( $this, 'views_users') );

    /* Not used yet */
    //add_action( 'restrict_manage_users', array( $this, 'restrict_manage_users') );

    /* Extends Users List Table */
    add_filter( 'manage_users_columns', array( $this, 'manage_users_columns' ) );
    add_action( 'manage_users_custom_column', array( $this, 'manage_users_custom_column' ), 10, 3 );

    /* @todo Improve in future version. */
    add_filter( 'manage_users_sortable_columns', array( $this, 'manage_users_sortable_columns' ) );
    add_action( 'pre_user_query', array( $this, 'pre_user_query' ) );

  }

  /**
   * Return a singleton instance of WPXtremeEnhancerUser class
   *
   * @brief Init
   *
   * @return WPXtremeEnhancerUser
   */
  public static function init() {
    static $instance = null;
    if ( is_null( $instance ) ) {
      $instance = new WPXtremeEnhancerUser();
    }
    return $instance;
  }

  /**
   * Return the list table columns
   *
   * @brief Columns
   *
   * @param array $columns A key value pairs
   *
   * @return array
   */
  public function manage_users_columns( $columns ) {
    $columns[WPDKUserMeta::LAST_TIME_SUCCESS_LOGIN] = __( 'Last login', WPXTREME_TEXTDOMAIN );
    $columns[WPDKUserMeta::LAST_TIME_LOGOUT]        = __( 'Last logout', WPXTREME_TEXTDOMAIN );
    $columns[WPDKUserMeta::COUNT_SUCCESS_LOGIN]     = __( '# Login', WPXTREME_TEXTDOMAIN );
    $columns[WPDKUserMeta::COUNT_WRONG_LOGIN]       = __( '# Wrong', WPXTREME_TEXTDOMAIN );
    $columns[WPDKUserMeta::STATUS]                  = __( 'Enabled', WPXTREME_TEXTDOMAIN );
    return $columns;
  }

  /**
   * Render a content of column cell
   *
   * @brief Render cell
   *
   * @param mixed  $value       Raw data value
   * @param string $column_name Column name
   * @param int    $user_id     User ID
   *
   * @return int|mixed|string
   */
  public function manage_users_custom_column( $value, $column_name, $user_id ) {

    /* Get user for retrive user meta. */
    $user = new WPDKUser( $user_id );

    /* Dates... */
    if ( in_array( $column_name, array( WPDKUserMeta::LAST_TIME_SUCCESS_LOGIN, WPDKUserMeta::LAST_TIME_LOGOUT ) ) ) {
      $date = $user->get( $column_name );
      if ( !empty( $date ) ) {
        $value = WPDKDateTime::timeNewLine( date( __( 'm/d/Y H:i:s', WPXTREME_TEXTDOMAIN ), $date ) );
      }
    }
    elseif ( WPDKUserMeta::STATUS == $column_name ) {
      $status             = $user->get( $column_name );
      $status_description = $user->get( WPDKUserMeta::STATUS_DESCRIPTION );
      $item    = array(
        'type'       => WPDKUIControlType::SWIPE,
        'name'       => 'wpxm-user-enabled',
        'id'         => 'swipe-' . $user_id,
        'userdata'   => $user_id,
        'afterlabel' => '',
        'title'      => empty( $status_description ) ? ucfirst( $status ) : $status_description,
        'value'      => ( empty( $status ) || $status !== WPDKUserStatus::DISABLED ) ? 'on' : 'off'
      );
      $control = new WPDKUIControlSwipe( $item );
      $value   = $control->html();
    }
    elseif ( WPDKUserMeta::COUNT_SUCCESS_LOGIN == $column_name ) {
      $success_login = $user->get( $column_name );
      $value         = empty( $success_login ) ? '0' : $success_login;
    }
    elseif ( WPDKUserMeta::COUNT_WRONG_LOGIN == $column_name ) {
      $wrong_login = $user->get( $column_name );
      $value       = empty( $wrong_login ) ? '0' : $wrong_login;
    }

    /* For lucky ignore swipe button. */
    if ( isset( $_REQUEST['orderby'] ) && $column_name == $_REQUEST['orderby'] ) {
      $value = sprintf( '<strong>%s</strong>', $value );
    }

    return $value;
  }

  /**
   * Return the new sortable columns
   *
   * @brief Sortable columns
   *
   * @param array $columns Sortable columns array
   *
   * @return array
   */
  public function manage_users_sortable_columns( $columns ) {
    $columns[WPDKUserMeta::STATUS]                  = WPDKUserMeta::STATUS;
    $columns[WPDKUserMeta::LAST_TIME_SUCCESS_LOGIN] = WPDKUserMeta::LAST_TIME_SUCCESS_LOGIN;
    return $columns;
  }

  /**
   * If no order is set by GET then se the default to user_register date in DESC.
   * Added order for other custom column and user meta.
   *
   * @brief Alter query
   * @todo This action can be set by preference in backend
   *
   * @param object $query
   */
  public function pre_user_query( $query ) {
    global $wpdb, $current_screen;

    if ( !is_object( $current_screen ) ) {
      return;
    }

    if ( 'users' != $current_screen->id ) {
      return;
    }

    if( !empty( $_REQUEST['cap'] ) ) {
      $users = WPDKUsers::usersWithCaps( $_REQUEST['cap'] );
      //$query->query_from .= " JOIN {$wpdb->usermeta} cap_usermeta ON {$wpdb->users}.ID = cap_usermeta.user_id AND (cap_usermeta.meta_key = 'wp_capabilities' AND cap_usermeta.meta_value RLIKE '[[:<:]]" . $_REQUEST['cap'] . "[[:>:]]' )";
      $query->query_where .= ' AND ID IN( ' . join(',', $users ) . ')';
    }

    if ( WPDKUserMeta::STATUS == $query->query_vars['orderby'] ) {
      $query->query_from .= " LEFT JOIN {$wpdb->usermeta} usermeta ON {$wpdb->users}.ID = usermeta.user_id AND (usermeta.meta_key = '" . WPDKUserMeta::STATUS . "')";
      $query->query_orderby = ' ORDER BY usermeta.meta_value ' . $query->query_vars['order'];
    }
    elseif ( WPDKUserMeta::LAST_TIME_SUCCESS_LOGIN == $query->query_vars['orderby'] ) {
      $query->query_from .= " LEFT JOIN {$wpdb->usermeta} usermeta ON {$wpdb->users}.ID = usermeta.user_id AND (usermeta.meta_key = '" . WPDKUserMeta::LAST_TIME_SUCCESS_LOGIN . "')";
      $query->query_orderby = ' ORDER BY usermeta.meta_value ' . $query->query_vars['order'];
    }
  }

  /**
   * Return an empty array to remove the standard default WordPress horizontal filter for role.
   *
   * @param array $views The role filter
   *
   * @return array
   */
  public function views_users( $views ) {
    /* Save original role filter list for catch link later. See restrict_manage_users() method. */
    $this->_views = $views;

    $role = isset( $_REQUEST['role'] ) ? $_REQUEST['role'] : '';
    ob_start();
    ?>
    <select class="wpdk-form-select" onchange="document.location=jQuery(this).val()" name='wpxm-enhancer-users-roles-filter'>
      <option style="color:#888" class="wpdk-form-option"><?php _e( 'Filter by Role', WPXTREME_TEXTDOMAIN) ?></option>
    <?php
    foreach( $this->_views as $key => $value ) :
      $url = preg_match( "/(?<=href=(\"|'))[^\"']+(?=(\"|'))/", $value, $match ); ?>
      <option class="wpdk-form-option" <?php selected( $key, $role) ?> value="<?php echo $url ? $match[0] : '' ?>"><?php echo strip_tags( $value ) ?></option>
    <?php endforeach; ?>
    </select>
    <?php
    /*
       <input class="button "name="wpxm-filter" type="button" value="<?php _e( 'Filter', WPXTREME_TEXTDOMAIN ) ?>" />
    */
    ?>
  <?php
    $wpxtreme_role_filter = ob_get_contents();
    ob_end_clean();

    /* Capabilities filter. */
    $cap = isset( $_REQUEST['cap'] ) ? $_REQUEST['cap'] : '';
    ob_start();
    ?>
    <select class="wpdk-form-select" onchange="document.location=jQuery(this).val()" name='wpxm-enhancer-users-capabilities-filter'>
      <option style="color:#888" class="wpdk-form-option"><?php _e( 'Filter by Capability', WPXTREME_TEXTDOMAIN) ?></option>
    <?php
    foreach( WPDKCapabilities::getInstance()->userCapabilities as $value ) : ?>
      <option class="wpdk-form-option" <?php selected( $value, $cap) ?> value="?cap=<?php echo $value ?>"><?php echo $value ?></option>
    <?php endforeach; ?>
    </select>
    <?php
    /*
       <input class="button "name="wpxm-filter" type="button" value="<?php _e( 'Filter', WPXTREME_TEXTDOMAIN ) ?>" />
    */
    ?>
  <?php
    $wpxtreme_capability_filter = ob_get_contents();
    ob_end_clean();

    $views = array( 'wpxtreme_role_filter' => $wpxtreme_role_filter, 'wpxtreme_capability_filter' => $wpxtreme_capability_filter );

    return apply_filters( 'wpxm_users_views', $views );
  }

}