<?php
/**
 * Appearance View controller
 *
 * @class           WPXtremeConfigurationEnhancerAppearanceViewController
 * @author          =undo= <info@wpxtre.me>
 * @copyright       Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date            2013-01-30
 * @version         0.8.2
 */

class WPXtremeConfigurationEnhancerAppearanceViewController extends WPDKjQueryTabsViewController {

  /**
   * Create an instance of WPXtremeConfigurationEnhancerAppearanceViewController class
   *
   * @brief Construct
   *
   * @return WPXtremeConfigurationEnhancerAppearanceViewController
   */
  public function __construct() {

    /* Single instances of tab content. */
    $general_config_view = new WPXtremeConfigurationEnhancerAppearanceGeneralView();
    $posts_config_view   = new WPXtremeConfigurationEnhancerAppearancePostView();

    /* Create each single tab. */
    $tabs = array(
      new WPDKjQueryTab( 'enhancer-appearance', __( 'Appearance', WPXTREME_TEXTDOMAIN ), $general_config_view->html() ),
      new WPDKjQueryTab( 'enhancer-list-table', __( 'List Tables', WPXTREME_TEXTDOMAIN ), $posts_config_view->html() ),
    );

    /* Create the view. */
    $view = new WPDKjQueryTabsView( 'wpxtreme-enhancer-appearance', $tabs );

    parent::__construct( 'wpxtreme-enhancer-appearance', __( 'Preferences', WPXTREME_TEXTDOMAIN ), $view );

    /* WordPress Pointer */
    if ( !isset( $_GET['subtour'] ) ) {
      $content           = __( 'Welcome to the new <strong>Preference</strong> view!', WPXTREME_TEXTDOMAIN );
      $pointer           = new WPDKPointer( 'wpxm-preferences', '#wpxtreme-enhancer-appearance-header-view', __( 'Welcome to wpXtreme Preferences', WPXTREME_TEXTDOMAIN ), $content, 'top', 'left' );
      $pointer->nextPage = 'wpxm_menu_wpxtreme-submenu-5&subtour=appearance';
    }
    elseif ( 'appearance' == $_GET['subtour'] ) {
      $content           = __( 'From this tab <strong>Appearance</strong> you can set in <strong>real time</strong> a lot of UI enhancers. Try and enjoy!', WPXTREME_TEXTDOMAIN );
      $pointer           = new WPDKPointer( 'wpxm-preferences', 'li.enhancer-appearance', __( 'Welcome to wpXtreme Preferences', WPXTREME_TEXTDOMAIN ), $content, 'top', 'left' );
      $pointer->nextPage = 'wpxm_menu_wpxtreme-submenu-5&subtour=list-tables#enhancer-list-table';
    }
    elseif ( 'list-tables' == $_GET['subtour'] ) {
      $content           = __( 'The <strong>List Table</strong> tab manages the standard WordPress table list for Post, Page and Media. You can enhance these views as you like.', WPXTREME_TEXTDOMAIN );
      $pointer           = new WPDKPointer( 'wpxm-preferences', 'li.enhancer-list-table', __( 'Welcome to wpXtreme Preferences', WPXTREME_TEXTDOMAIN ), $content, 'top', 'left' );
      $pointer->nextPage = 'wpxm_menu_wpxtreme-submenu-2';
    }

  }
}


/**
 * Appearance General configuration View
 *
 * @class               WPXtremeConfigurationEnhancerAppearanceGeneralView
 * @author              =undo= <info@wpxtre.me>
 * @copyright           Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date                2013-01-30
 * @version             0.8.2
 *
 */
class WPXtremeConfigurationEnhancerAppearanceGeneralView extends WPDKConfigurationView {

  /**
   * Create an instance of WPXtremeConfigurationEnhancerAppearanceGeneralView class
   *
   * @brief Construct
   *
   * @return WPXtremeConfigurationEnhancerAppearanceGeneralView
   */
  public function __construct() {
    /* Get configuration instance for user. */
    $configuration = WPXtremeConfigurationEnhancerAppearanceGeneral::getInstance();

    parent::__construct( 'enhancer-appearance', __( 'General', WPXTREME_TEXTDOMAIN ), null, $configuration );
  }

  /**
   * Return the HTML markup for reset to default button
   *
   * @brief Reset default button
   * @since 1.0.0.b3
   *
   * @return string
   */
  public function buttonsUpdateReset() {
    $button_reset = WPDKUI::button( __( 'Reset to default', WPDK_TEXTDOMAIN ), array( 'name'  => 'resetToDefault', 'classes' => 'button-secondary' ) );
    return sprintf( '<p>%s</p>', $button_reset );
  }

  /**
   * Return the sdf array for the form fields
   *
   * @brief Fields array
   *
   * @return array
   */
  public function fields() {
    /* Get configuration instance for user. */
    $appearance = WPXtremeConfigurationEnhancerAppearanceGeneral::getInstance();

    $fields = array(
      __( 'Improvements', WPXTREME_TEXTDOMAIN ) => array(

        __( 'These settings are applied to your user only', WPXTREME_TEXTDOMAIN ),

        array(
          'class'     => 'column-2',
          'container' => array(
            array(
              array(
                'type'            => WPDKUIControlType::SWIPE,
                'name'            => 'shadow_header',
                'label'           => __( 'Shadow Header', WPXTREME_TEXTDOMAIN ),
                'label_placement' => 'right',
                'data'            => array( 'placement' => 'right' ),
                'title'           => __( 'Display a shadow on top of all section headers', WPXTREME_TEXTDOMAIN ),
                'value'           => $appearance->shadow_header,
              )
            ),
            array(
              array(
                'type'            => WPDKUIControlType::SWIPE,
                'name'            => 'gradient_header',
                'label'           => __( 'Gradient Header', WPXTREME_TEXTDOMAIN ),
                'label_placement' => 'right',
                'data'            => array( 'placement' => 'right' ),
                'title'           => __( 'Add a gradient color on top of all section headers', WPXTREME_TEXTDOMAIN ),
                'value'           => $appearance->gradient_header
              ),
            ),
            array(
              array(
                'type'            => WPDKUIControlType::SWIPE,
                'name'            => 'fullscreen',
                'label'           => __( 'Fullscreen', WPXTREME_TEXTDOMAIN ),
                'label_placement' => 'right',
                'data'            => array( 'placement' => 'right' ),
                'title'           => __( 'Display a special button on top right of the header in order to switch to a fullscreen mode', WPXTREME_TEXTDOMAIN ),
                'value'           => $appearance->fullscreen
              )
            ),
            array(
              array(
                'type'            => WPDKUIControlType::SWIPE,
                'name'            => 'badge',
                'label'           => __( 'Improves Badges', WPXTREME_TEXTDOMAIN ),
                'label_placement' => 'right',
                'data'            => array( 'placement' => 'right' ),
                'title'           => __( 'Improves look of badge notifications', WPXTREME_TEXTDOMAIN ),
                'value'           => $appearance->badge
              )
            ),
            array(
              array(
                'type'            => WPDKUIControlType::SWIPE,
                'name'            => 'messages',
                'label'           => __( 'Improves messages', WPXTREME_TEXTDOMAIN ),
                'label_placement' => 'right',
                'data'            => array( 'placement' => 'right' ),
                'title'           => __( 'Improves look for the standard WordPress messages notification', WPXTREME_TEXTDOMAIN ),
                'value'           => $appearance->messages
              ),
            ),
            array(
              array(
                'type'            => WPDKUIControlType::SWIPE,
                'name'            => 'shadow_postbox',
                'label'           => __( 'Shadow on Box', WPXTREME_TEXTDOMAIN ),
                'label_placement' => 'right',
                'data'            => array( 'placement' => 'right' ),
                'title'           => __( 'Add a MAC OS style shadow on the box', WPXTREME_TEXTDOMAIN ),
                'value'           => $appearance->shadow_postbox
              ),
            ),
            array(
              array(
                'type'            => WPDKUIControlType::SWIPE,
                'name'            => 'toolbars',
                'label'           => __( 'Toolbars', WPXTREME_TEXTDOMAIN ),
                'label_placement' => 'right',
                'data'            => array( 'placement' => 'right' ),
                'title'           => __( 'Add a border to list table toolbar', WPXTREME_TEXTDOMAIN ),
                'value'           => $appearance->toolbars
              ),
            ),
            array(
              array(
                'type'            => WPDKUIControlType::SWIPE,
                'name'            => 'editor_title',
                'label'           => __( 'Improves title in editor', WPXTREME_TEXTDOMAIN ),
                'label_placement' => 'right',
                'data'            => array( 'placement' => 'right' ),
                'title'           => __( 'Make the title more readable in the post editor', WPXTREME_TEXTDOMAIN ),
                'value'           => $appearance->editor_title
              ),
            ),
            array(
              array(
                'type'            => WPDKUIControlType::SWIPE,
                'name'            => 'inputs',
                'label'           => __( 'Improves input fields', WPXTREME_TEXTDOMAIN ),
                'label_placement' => 'right',
                'data'            => array( 'placement' => 'right' ),
                'title'           => __( 'Make input styles more readable for text, text area and more', WPXTREME_TEXTDOMAIN ),
                'value'           => $appearance->inputs
              ),
            ),
            array(
              array(
                'type'            => WPDKUIControlType::SWIPE,
                'name'            => 'tables',
                'label'           => __( 'Improves Tables', WPXTREME_TEXTDOMAIN ),
                'label_placement' => 'right',
                'data'            => array( 'placement' => 'right' ),
                'title'           => __( 'Improved look & feel of all tables, list tables and form tables', WPXTREME_TEXTDOMAIN ),
                'value'           => $appearance->tables
              ),
            ),
            array(
              array(
                'type'            => WPDKUIControlType::SWIPE,
                'name'            => 'inline_edit',
                'label'           => __( 'Improves Inline Edit Layout', WPXTREME_TEXTDOMAIN ),
                'label_placement' => 'right',
                'data'            => array( 'placement' => 'right' ),
                'title'           => __( 'Improves inline edit layout with more readable field blocks', WPXTREME_TEXTDOMAIN ),
                'value'           => $appearance->inline_edit
              ),
            ),
            array(
              array(
                'type'            => WPDKUIControlType::SWIPE,
                'name'            => 'table_actions',
                'label'           => __( 'Improves table action links', WPXTREME_TEXTDOMAIN ),
                'label_placement' => 'right',
                'data'            => array( 'placement' => 'right' ),
                'title'           => __( 'Improves table view action links', WPXTREME_TEXTDOMAIN ),
                'value'           => $appearance->table_actions
              ),
            ),

          )
        ),
      ),
    );

    return $fields;
  }
}


/**
 * Appearance Post View
 *
 * @class               WPXtremeConfigurationEnhancerAppearancePostView
 * @author              =undo= <info@wpxtre.me>
 * @copyright           Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date                2013-01-30
 * @version             0.8.5
 *
 */

class WPXtremeConfigurationEnhancerAppearancePostView extends WPDKConfigurationView {

  /**
   * Create an instance of WPXtremeConfigurationEnhancerAppearancePostView class
   *
   * @brief Construct
   *
   * @return WPXtremeConfigurationEnhancerAppearancePostView
   */
  public function __construct() {
    /* Get configuration instance for user. */
    $configuration = WPXtremeConfigurationEnhancerAppearancePosts::getInstance();

    parent::__construct( 'enhancer-list-table', __( 'Posts', WPXTREME_TEXTDOMAIN ), null, $configuration );
  }

  /**
   * Return the HTML markup for reset to default button
   *
   * @brief Reset default button
   * @since 1.0.0.b4
   *
   * @return string
   */
  public function buttonsUpdateReset() {
    $button_reset = WPDKUI::button( __( 'Reset to default', WPDK_TEXTDOMAIN ), array( 'name'  => 'resetToDefault', 'classes' => 'button-secondary' ) );
    return sprintf( '<p>%s</p>', $button_reset );
  }

  /**
   * Return the sdf array for the form fields
   *
   * @brief Return CLA array
   *
   * @return array
   */
  public function fields() {

    /* Get configuration instance for user. */
    $configuration = WPXtremeConfigurationEnhancerAppearancePosts::getInstance();

    $fields = array(

      __( 'Media', WPXTREME_TEXTDOMAIN ) => array(
        __( 'These settings are applied to your user only', WPXTREME_TEXTDOMAIN ),
        array(
          array(
            'type'            => WPDKUIControlType::SWIPE,
            'name'            => 'media_thickbox_icon',
            'label'           => __( 'Enable preview on the Media thumbnails', WPXTREME_TEXTDOMAIN ),
            'label_placement' => 'right',
            'data'            => array( 'placement' => 'right' ),
            'title'           => __( 'This option enables a light box effect on Media thumbnails', WPXTREME_TEXTDOMAIN ),
            'value'           => $configuration->media_thickbox_icon
          )
        ),
        array(
          array(
            'type'            => WPDKUIControlType::SWIPE,
            'name'            => 'media_thumbnail_author',
            'label'           => __( 'Enable Author gravatar thumbnails', WPXTREME_TEXTDOMAIN ),
            'label_placement' => 'right',
            'data'            => array( 'placement' => 'right' ),
            'value'           => $configuration->media_thumbnail_author
          )
        ),
      ),

      __( 'Pages', WPXTREME_TEXTDOMAIN ) => array(
        __( 'These settings are applied to your user only', WPXTREME_TEXTDOMAIN ),
        array(
          array(
            'type'            => WPDKUIControlType::SWIPE,
            'name'            => 'pages_thumbnail_author',
            'label'           => __( 'Enable Author gravatar thumbnails', WPXTREME_TEXTDOMAIN ),
            'label_placement' => 'right',
            'data'            => array( 'placement' => 'right' ),
            'value'           => $configuration->pages_thumbnail_author
          )
        ),
        array(
          array(
            'type'            => WPDKUIControlType::SWIPE,
            'name'            => 'pages_swipe_publish',
            'label'           => __( 'Enable Publish/Draft Swipe button', WPXTREME_TEXTDOMAIN ),
            'label_placement' => 'right',
            'data'            => array( 'placement' => 'right' ),
            'value'           => $configuration->pages_swipe_publish
          )
        ),
      ),

      __( 'Posts', WPXTREME_TEXTDOMAIN ) => array(
        __( 'These settings are applied to your user only', WPXTREME_TEXTDOMAIN ),
        array(
          array(
            'type'            => WPDKUIControlType::SWIPE,
            'name'            => 'posts_thumbnail_author',
            'label'           => __( 'Enable Author gravatar thumbnails', WPXTREME_TEXTDOMAIN ),
            'label_placement' => 'right',
            'data'            => array( 'placement' => 'right' ),
            'value'           => $configuration->posts_thumbnail_author
          )
        ),
        array(
          array(
            'type'            => WPDKUIControlType::SWIPE,
            'name'            => 'posts_swipe_publish',
            'label'           => __( 'Enable Publish/Draft Swipe button', WPXTREME_TEXTDOMAIN ),
            'label_placement' => 'right',
            'data'            => array( 'placement' => 'right' ),
            'value'           => $configuration->posts_swipe_publish
          )
        ),
      ),

    );

    return $fields;
  }
}
