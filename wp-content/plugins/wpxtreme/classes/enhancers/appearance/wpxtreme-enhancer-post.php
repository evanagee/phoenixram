<?php
/**
 * Manage enhancer Post
 *
 * ## Overview
 * Add several post enhancer.
 *
 * @class              WPXtremeEnhancerPost
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-01-02
 * @version            0.8.2
 *
 */

class WPXtremeEnhancerPost {

  /**
   * Return a singleton instance of WPXtremeEnhancerPost class
   *
   * @return WPXtremeEnhancerPost
   */
  public static function init() {
    static $instance = null;
    if ( is_null( $instance ) ) {
      $instance = new WPXtremeEnhancerPost();
    }
    return $instance;
  }

  /**
   * Create an instance of WPXtremeEnhancerPost class
   *
   * @return WPXtremeEnhancerPost
   */
  private function __construct() {

    /* Extends Posts List Table */
    add_filter( 'manage_posts_columns', array( $this, 'manage_posts_columns' ) );
    add_filter( 'manage_edit-post_sortable_columns', array( $this, 'manage_edit_sortable_columns' ) );
    add_action( 'manage_posts_custom_column', array( $this, 'manage_posts_custom_column' ), 10, 2 );

    /* Extends Pages List Table */
    add_filter( 'manage_pages_columns', array( $this, 'manage_posts_columns' ) );
    add_filter( 'manage_edit-page_sortable_columns', array( $this, 'manage_edit_sortable_columns' ) );
    add_action( 'manage_pages_custom_column', array( $this, 'manage_posts_custom_column' ), 10, 2 );

    /* Use to extend page with drag & drop sorter. */
    add_action( 'restrict_manage_posts', array( $this, 'restrict_manage_posts' ) );

    /* Extends Media (upload) List Table */
    add_filter( 'manage_media_columns', array( $this, 'manage_media_columns' ) );
    add_filter( 'manage_upload_sortable_columns', array( $this, 'manage_upload_sortable_columns' ) );
    add_action( 'manage_media_custom_column', array( $this, 'manage_posts_custom_column' ), 10, 2 );

    /* Enhancer other custom posts of type page. */
    $post_types = get_post_types( array( '_builtin' => false ) );
    foreach ( $post_types as $id ) {
      add_filter( 'manage_edit-' . $id . '_sortable_columns', array( $this, 'manage_edit_sortable_columns' ) );
    }

  }

  // -----------------------------------------------------------------------------------------------------------------
  // List of enhancer post type
  // -----------------------------------------------------------------------------------------------------------------

  /**
   * @brief Check if a post type is to enhance
   *
   * Return TRUE if a post type have to enhance
   *
   * @return bool
   */
  private function isEnhance() {
    global $typenow;

    /* @todo Aggiungere impostazioni nel backend con la lista dei post type a cui applicare il dope. */
    $dopable = array(
      'post',
      'page',
      ''
    );

    return true;

    return in_array( $typenow, $dopable );
  }

  // -----------------------------------------------------------------------------------------------------------------
  // Enhancer Post
  // -----------------------------------------------------------------------------------------------------------------

  /**
   * Altera le colonne della List table degli utenti di WordPress
   *
   * @static
   *
   * @param array $columns Elenco Key value pairs delle colonne
   *
   * @return array
   */
  public function manage_posts_columns( $columns ) {
    global $typenow;

    if ( !$this->isEnhance() ) {
      return $columns;
    }

    /* Get the post type. */
    $cpt = get_post_type_object( $typenow );

    $post_swipe = ( WPDKPostType::POST == $typenow ) &&
      wpdk_is_bool( WPXtremeConfigurationEnhancerAppearancePosts::getInstance()->posts_swipe_publish );
    $page_swipe = ( WPDKPostType::PAGE == $typenow || WPDKPostType::PAGE == $cpt->capability_type ) &&
      wpdk_is_bool( WPXtremeConfigurationEnhancerAppearancePosts::getInstance()->pages_swipe_publish );

    /* Added quick publish/draft column with swipe control. */
    if ( $page_swipe || $post_swipe ) {
      $columns['wpdk_post_internal-publish'] = __( 'Published', WPXTREME_TEXTDOMAIN );
    }

    $post_thumbnail_author = ( WPDKPostType::POST == $typenow ) &&
      wpdk_is_bool( WPXtremeConfigurationEnhancerAppearancePosts::getInstance()->posts_thumbnail_author );
    $page_thumbnail_author = ( WPDKPostType::PAGE == $typenow ) &&
      wpdk_is_bool( WPXtremeConfigurationEnhancerAppearancePosts::getInstance()->pages_thumbnail_author );

    /* Replace author column with author thumbnail image. */
    if ( $page_thumbnail_author || $post_thumbnail_author ) {
      unset( $columns['author'] );
      $columns = WPDKArray::insertKeyValuePairs( $columns, 'wpdk_post_internal-author', __( 'Author', WPXTREME_TEXTDOMAIN ), 2 );
    }

    if ( WPDKPostType::PAGE == $cpt->capability_type ) {
      $columns = array_merge( array( 'menu_order' => __( 'Order', WPXTREME_TEXTDOMAIN ) ), $columns );
    }

    return $columns;
  }

  /**
   * List of sortable columns
   *
   * @brief Sortable columns
   *
   * @param array $columns Array Default sortable columns
   *
   * @return array
   */
  public function manage_edit_sortable_columns( $columns ) {
    global $typenow;

    $post_thumbnail_author = ( WPDKPostType::POST == $typenow ) &&
      wpdk_is_bool( WPXtremeConfigurationEnhancerAppearancePosts::getInstance()->posts_thumbnail_author );
    $page_thumbnail_author = ( WPDKPostType::PAGE == $typenow ) &&
      wpdk_is_bool( WPXtremeConfigurationEnhancerAppearancePosts::getInstance()->pages_thumbnail_author );

    if ( $page_thumbnail_author || $post_thumbnail_author ) {

      if ( !$this->isEnhance() ) {
        return $columns;
      }

      $columns = array(
        'title'                     => 'title',
        'wpdk_post_internal-author' => 'author',
        'parent'                    => 'parent',
        'comments'                  => 'comment_count',
        'date'                      => array( 'date', true )
      );
    }

    /* Get the post type. */
    $cpt = get_post_type_object( $typenow );

    if ( WPDKPostType::PAGE == $cpt->capability_type ) {
      $columns['date']       = 'date';
      $columns['menu_order'] = array( 'menu_order', true );
    }

    return $columns;
  }

  /**
   * Contenuto (render) di una colonna
   *
   * @static
   *
   * @param string $column_name Nome della colonna
   * @param int    $post_id     Post ID
   *
   * @return string
   */
  public function manage_posts_custom_column( $column_name, $post_id ) {
    global $post;

    if ( !$this->isEnhance() ) {
      return $column_name;
    }

    if ( 'wpdk_post_internal-publish' == $column_name ) {

      /* Get the post status. */
      $post_status = get_post_status( $post_id );

      /* Get all readable statuses. */
      $statuses    = WPDKPostStatus::statuses();

      if ( WPDKPostStatus::PUBLISH == $post_status || WPDKPostStatus::DRAFT == $post_status ) {
        $item    = array(
          'type'       => WPDKUIControlType::SWIPE,
          'name'       => 'wpdk-post-publish',
          'id'         => 'swipe-' . $post_id,
          'userdata'   => $post_id,
          'afterlabel' => '',
          'title'      => $statuses[$post_status],
          'value'      => ( WPDKPostStatus::PUBLISH == $post_status ) ? 'on' : 'off'
        );
        $control = new WPDKUIControlSwipe( $item );
        $control->display();
      }
      else {
        $icons = array(
          WPDKPostStatus::FUTURE   => 'wpdk-icon-clock',
          WPDKPostStatus::PRIVATE_ => 'wpdk-icon-lock_fill',
          WPDKPostStatus::TRASH    => 'wpdk-icon-trash_fill',
          WPDKPostStatus::PENDING  => 'wpdk-icon-pin',
        );
        printf( '<span title="%s" class="wpdk-tooltip wpdk-icon post-status-%s %s"><span>%s</span></span>', $statuses[$post_status], $post_status, $icons[$post_status], ucfirst( $post_status ) );
      }
    }
    elseif ( 'wpdk_post_internal-author' == $column_name ) {
      echo WPDKUsers::init()->gravatar( get_the_author_meta( 'ID' ), 48 );
      printf( '<br/><a href="%s">%s</a>', esc_url( add_query_arg( array( 'post_type' => $post->post_type, 'author' => get_the_author_meta( 'ID' ) ), 'edit.php' ) ), get_the_author() );
    }
    elseif ( 'wpdk_post_internal-icon' == $column_name ) {
      if ( ( $thumb = wp_get_attachment_image( $post->ID, array( 80, 60 ), true ) ) ) {
        $url = wp_get_attachment_image_src( $post->ID, 'full' );
        printf( '<a class="thickbox" title="%s" href="%s">%s</a>', _draft_or_post_title( $post->ID ), $url[0], $thumb );
      }
    }
    elseif ( 'menu_order' == $column_name ) {
      printf( '<i data-order="%s" class="wpdk-icon-sorting"></i>', $post->menu_order );
    }
  }

  // -----------------------------------------------------------------------------------------------------------------
  // Enhancer Page
  // -----------------------------------------------------------------------------------------------------------------

  /* See above. Post and page are very similar. */

  public function restrict_manage_posts() {
    global $typenow, $per_page;

    /* Get the post type. */
    $cpt = get_post_type_object( $typenow );

    /* Enabled drag & drop menu order only for post type page. */
    if ( !empty( $cpt ) && is_object( $cpt ) && WPDKPostType::PAGE == $cpt->capability_type ) :

      /* Build info on pagination. Useful for sorter. */
      $paged = isset( $_REQUEST['paged'] ) ? $_REQUEST['paged'] : '1';
      ?>
    <input rel="<?php echo $typenow ?>"
           type="hidden"
           name="wpx-per-page"
           id="wpx-per-page"
           value="<?php echo $per_page ?>"/>
    <input type="hidden" name="wpx-paged" id="wpx-paged" value="<?php echo $paged ?>"/>
    <?php

    endif;

    /*
     * If you only want this to work for your specific post type, check for that $type here and then return.
     * This function, if unmodified, will add the dropdown for each post type / taxonomy combination.
     *
     * // Return the registered custom post types; exclude the builtin
     * $post_types = get_post_types( array( '_builtin' => false ) );
     *
     */

  }

  // -----------------------------------------------------------------------------------------------------------------
  // Enhancer media
  // -----------------------------------------------------------------------------------------------------------------

  /**
   * @brief Media
   */
  public function manage_media_columns( $columns ) {
    if ( !$this->isEnhance() ) {
      return $columns;
    }

    if ( wpdk_is_bool( WPXtremeConfigurationEnhancerAppearancePosts::getInstance()->media_thumbnail_author ) ) {
      unset( $columns['author'] );
      $columns = WPDKArray::insertKeyValuePairs( $columns, 'wpdk_post_internal-author', __( 'Author', WPXTREME_TEXTDOMAIN ), 3 );
    }

    if ( wpdk_is_bool( WPXtremeConfigurationEnhancerAppearancePosts::getInstance()->media_thickbox_icon ) ) {
      unset( $columns['icon'] );
      $columns = WPDKArray::insertKeyValuePairs( $columns, 'wpdk_post_internal-icon', __( 'Icon', WPXTREME_TEXTDOMAIN ), 1 );
    }
    return $columns;
  }

  /**
   * @brief Sorter media columns
   */
  public function manage_upload_sortable_columns( $columns ) {
    if ( wpdk_is_bool( WPXtremeConfigurationEnhancerAppearancePosts::getInstance()->media_thumbnail_author ) ) {
      if ( !$this->isEnhance() ) {
        return $columns;
      }

      $columns = array(
        'title'                     => 'title',
        'wpdk_post_internal-author' => 'author',
        'parent'                    => 'parent',
        'comments'                  => 'comment_count',
        'date'                      => array( 'date', true )
      );
    }
    return $columns;
  }
}