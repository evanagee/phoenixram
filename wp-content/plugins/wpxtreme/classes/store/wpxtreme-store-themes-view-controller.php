<?php
/**
 * Store themes view controller (coming)
 *
 * @class           WPXtremeStoreThemesViewController
 * @author          =undo= <info@wpxtre.me>
 * @copyright       Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date            2013-04-04
 * @version         1.0.0
 *
 */
class WPXtremeStoreThemesViewController extends WPDKViewController {

  /**
   * Create an instance of WPXtremeStoreThemesViewController class
   *
   * @brief Construct
   *
   * @return WPXtremeStoreThemesViewController
   */
  public function __construct() {
    parent::__construct( 'wpxtreme-store-themes', __( 'Themes', WPXTREME_TEXTDOMAIN ) );
    $view = new WPXtremeStoreThemesView();
    $this->view->addSubview( $view );

    /* WordPress Pointer */
    $content = __( 'Awesome! Our team is working hard to implement theme\'s store, focused on developing new classes and specific API for themes. If you want to keep informed, please visit <a href="https://wpxtre.me">wpXtreme</a> and subscribe our newsletter.<br/><br/>Thank you for visiting us.', WPXTREME_TEXTDOMAIN );
    $pointer = new WPDKPointer( 'wpxm-store-themes', '#wpxtreme-store-themes-header-view h2', __( 'Welcome to WPX Store Themes', WPXTREME_TEXTDOMAIN ), $content, 'top', 'left' );

  }

}

/**
 * Store themes view
 *
 * @class           WPXtremeStoreThemesView
 * @author          =undo= <info@wpxtre.me>
 * @copyright       Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date            04/04/13
 * @version         1.0.0
 *
 */
class WPXtremeStoreThemesView extends WPDKView {

  /**
   * Create an instance of WPXtremeStoreThemesView class
   *
   * @brief Construct
   *
   * @return WPXtremeStoreThemesView
   */
  public function __construct() {
    parent::__construct( 'wpxtreme-store-themes', 'wpdk-border-container' );
  }

  /**
   * Display
   *
   * @brief Display
   */
  public function draw() {
    ?>
    <div style="width:525px;margin:16px auto;text-align:center">
      <img alt="WPX Store Theme is Coming" src="<?php echo WPXTREME_URL_IMAGES ?>coming.png" />
      <h3>Themes coming...</h3>
    </div>
  <?php
	}

}