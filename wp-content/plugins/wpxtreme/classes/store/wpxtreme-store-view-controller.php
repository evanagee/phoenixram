<?php
/**
 * WPX Store View Controller. In questa release lo store coincide con il Plugin Store. Più avanti avremmo varie videata
 * che permettono di separare vari tipi di store, come quello dedicato ai temi. La suddivisione è fatta seguendo il case
 * study di Apple che separa le applicazioni dai libri.
 *
 * @class              WPXtremeStoreViewController
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-06-11
 * @version            1.0.13
 */

class WPXtremeStoreViewController extends WPDKViewController {

  /**
   * Instance of WPXtremeAPI class
   *
   * @brief API
   *
   * @var WPXtremeAPI $api
   */
  private $api;

  /**
   * Create an instance of WPXtremeStoreViewController class
   *
   * @brief Construct
   *
   * @return WPXtremeStoreViewController
   */
  public function __construct()
  {
    parent::__construct( 'wpxtreme-store-plugins', __( 'Plugins', WPXTREME_TEXTDOMAIN ) );

    /* Create the view. */
    $view = new WPXtremeStoreView();
    $this->view->addSubview( $view );

    /* wpXtreme API. */
    $this->api = new WPXtremeAPI();

    /* WordPress Pointer */
    if ( !isset( $_GET['subtour'] ) ) {
      $content           = __( 'And finally welcome to our amazing <strong>WPX Store</strong>', WPXTREME_TEXTDOMAIN );
      $pointer           = new WPDKPointer( 'wpxm-store', '#wpxtreme-store-plugins-header-view h2', __( 'Welcome to WPX Store', WPXTREME_TEXTDOMAIN ), $content, 'top', 'left' );
      $pointer->nextPage = 'wpxm_menu_wpxtreme-submenu-2&subtour=control-panel';
    }
    elseif ( 'control-panel' == $_GET['subtour'] ) {
      $content           = __( 'By clicking here you can back to the <strong>Control Panel</strong>', WPXTREME_TEXTDOMAIN );
      $pointer           = new WPDKPointer( 'wpxm-store', '#wpxm-toolbar-store p:first-child a', __( 'Welcome to WPX Store', WPXTREME_TEXTDOMAIN ), $content, 'top', 'left' );
      $pointer->nextPage = 'wpxm_menu_wpxtreme-submenu-2&subtour=login';
    }
    elseif ( 'login' == $_GET['subtour'] ) {
      $content                         = __( 'Remeber to <strong>login/register</strong> in order to <strong>purchase, download and install</strong> any plugin!', WPXTREME_TEXTDOMAIN );
      $pointer                         = new WPDKPointer( 'wpxm-store', '#wpxm-toolbar-store-login', __( 'Welcome to WPX Store', WPXTREME_TEXTDOMAIN ), $content, 'top', 'left' );
      $pointer->buttonPrimary['label'] = __( 'One more thing...', WPXTREME_TEXTDOMAIN );
      $pointer->nextPage               = 'wpxm_menu_wpxtreme-submenu-3';
    }
  }

  /**
   * This static method is called when the head of this view controller is loaded by WordPress.
   *
   * @brief Head
   */
  public static function didHeadLoad() { }
}


/**
 * The store view
 *
 * @class              WPXtremeStoreView
 * @author             =undo= <g.fazioli@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2012-11-28
 * @version            0.8.1
 */

class WPXtremeStoreView extends WPDKView {

  /**
   * An instance of WPXtremeAPI class
   *
   * @brief API
   *
   * @var WPXtremeAPI $api
   */
  private $api;

  /**
   * Create an instance of WPXtremeStoreView class
   *
   * @brief Construct
   *
   * @return WPXtremeStoreView
   */
  public function __construct()
  {
    parent::__construct( 'wpxs-plugin-store' );

    /* API */
    $this->api = new WPXtremeAPI();
  }

  /**
   * Draw the view content
   *
   * @brief Draw
   */
  public function draw()
  {
    /* Process the POST actions. */
    if ( wpdk_is_request_post() ) {
      $action = isset( $_POST['action'] ) ? $_POST['action'] : '';
      if ( !empty( $action ) ) {

        switch ( $action ) {
          case 'signout':
            $this->api->deleteToken();
            $this->api->deleteWPXStoreUserEmail();
            echo $this->api->wpxstore_plugins();
            break;

          case 'plugin_card':
            echo $this->api->wpxstore_plugin_card();
            break;
        }
      }
    }
    /* Process the GET actions. */
    elseif ( wpdk_is_request_get() ) {
      $action = isset( $_GET['action'] ) ? $_GET['action'] : '';

      /* Decoding. */
      $action = json_decode( base64_decode( $action ) );

      if ( !empty( $action ) ) {

        /* The action/token must be as:
         *
         *     object(stdClass)#321 (5) {
         *      ["action"]        => string(7) "install"
         *      ["plugin_id"]     => string(3) "996"
         *      ["plugin"]        => string(29) "wpx-bannerize_000036/main.php"
         *      ["download_link"] => string(44) "http://beta.wpxtre.me/api/v1/wpxstore/plugin_download"
         *      ["token"]         => string(32) "5c960ad0fb1c2d71bbc102d7c02ff4eb"
         *    }
         *
        */

        $action->token = $this->api->getToken();
        $action->download_link .= sprintf( '?token=%s&id=%s', $action->token, $action->plugin_id );

        switch ( $action->action ) {
          case 'install':
            $this->install_plugin( $action );
            break;

          case 'update':
            $this->update_plugin( $action );
            break;
        }
      }
      else {
        /* Display the plugins store showcase. */
        echo $this->api->wpxstore_plugins();
      }
    }
  }

  /**
   * Use WordPress UpgraderSkin to update a plugin
   *
   * @brief Update a plugin
   *
   * @param object $action A stdClass object with plugin information
   *
   * The action/token must be as:
   *
   *     object(stdClass)#321 (5) {
   *      ["action"]        => string(7) "install"
   *      ["plugin_id"]     => string(3) "996"
   *      ["plugin"]        => string(29) "wpx-bannerize_000036/main.php"
   *      ["download_link"] => string(44) "http://beta.wpxtre.me/api/v1/plugin_download"
   *      ["token"]         => string(32) "5c960ad0fb1c2d71bbc102d7c02ff4eb"
   *    }
   *
   */
  private function update_plugin( $action )
  {

    /* Already included via wpdk-update.php */
    require_once( ABSPATH . 'wp-admin/includes/plugin-install.php' );

    $plugin = $action->plugin;

    $args = array(
      'title'  => __( 'Update this plugin through the WPX Store', WPXTREME_TEXTDOMAIN ),
      'nonce'  => '',
      'url'    => WPDKWordPressPlugin::currentURL(),
      'type'   => 'web',
      'api'    => array(),
      'plugin' => $plugin
    );

    $updater_skin = new WPXPluginUpgraderSkin( $args );
    $upgrader     = new WPXPluginUpgrader( $updater_skin );

    $upgrader->upgrade( $action );
  }

  /**
   * Use WordPress UpgraderSkin to install a new plugin
   *
   * @brief Install a plugin
   *
   * @param object $action A stdClass object with plugin information
   *
   * The action/token must be as:
   *
   *     object(stdClass)#321 (5) {
   *      ["action"]        => string(7) "install"
   *      ["plugin_id"]     => string(3) "996"
   *      ["plugin"]        => string(29) "wpx-bannerize_000036/main.php"
   *      ["download_link"] => string(44) "http://beta.wpxtre.me/api/v1/plugin_download"
   *      ["token"]         => string(32) "5c960ad0fb1c2d71bbc102d7c02ff4eb"
   *    }
   *
   */
  private function install_plugin( $action )
  {

    /* Already included via wpdk-update.php */
    //require_once( ABSPATH . 'wp-admin/includes/class-wp-upgrader.php' );
    require_once( ABSPATH . 'wp-admin/includes/plugin-install.php' );

    $plugin        = $action->plugin;
    $download_link = $action->download_link;

    $args = array(
      'title'  => __( 'Install the plugin through the WPX Store', WPXTREME_TEXTDOMAIN ),
      'nonce'  => '',
      'url'    => WPDKWordPressPlugin::currentURL(),
      'type'   => 'web',
      'api'    => array(),
      'plugin' => $plugin
    );

    $install_skin = new WPXPluginUpgraderSkin( $args );
    $installer    = new WPXPluginUpgrader( $install_skin );

    $installer->install( $download_link );

    /* Flush plugins cache so we can make sure that the installed plugins list is always up to date */
    wp_cache_flush();
  }

  /**
   * Activate a plugin
   *
   * @brief Activate a plugin
   * @since 1.0.0.b2
   *
   */
  private function activatePlugin()
  {
    if ( !empty( $_GET['plugin'] ) ) {

      $plugin = $_GET['plugin'];

      if ( !current_user_can( 'activate_plugins' ) ) {
        wp_die( __( 'You do not have sufficient permissions to activate plugins for this site.' ) );
      }

      check_admin_referer( 'activate-plugin_' . $plugin );

      $result = activate_plugin( $plugin, add_query_arg( 'action', 'error-on-plugin-activate' ), is_network_admin() );

      if ( !is_wp_error( $result ) ) {
        $args                   = array(
          'error',
          'deleted',
          'activate',
          'activate-multi',
          'deactivate',
          'deactivate-multi',
          '_error_nonce'
        );
        $_SERVER['REQUEST_URI'] = remove_query_arg( $args, $_SERVER['REQUEST_URI'] );
        wp_redirect( add_query_arg( 'action', 'activate-plugin-sucessfully' ) );
        exit;
      }
    }
  }

  // -----------------------------------------------------------------------------------------------------------------
  // Error
  // -----------------------------------------------------------------------------------------------------------------

  /**
   * Display an alert message
   *
   * @brief Display an error
   */
  private function error()
  {
    $message = __( 'Error: #983522-1: An error detect', WPXTREME_TEXTDOMAIN );
    $alert   = new WPDKTwitterBootstrapAlert( 'wpxtreme-store-alert', $message, WPDKTwitterBootstrapAlertType::ALERT );
    $alert->display();
  }

  /**
   * Display the unreachable message
   *
   * @brief Display the unreachable server
   */
  private function serverUnreachable()
  {
    $message = __( '<strong>Warnig!</strong> Sorry, but wpXtreme server is not responding at the moment. Please, try later.' . WPXTREME_SUPPORT, WPXTREME_TEXTDOMAIN );
    $alert   = new WPDKTwitterBootstrapAlert( 'wpxtreme-store-alert', $message, WPDKTwitterBootstrapAlertType::INFORMATION );
    $alert->display();
  }

}