<?php
/**
 * Admin Class for backend managment
 *
 * @class              WPXtremeAdmin
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-22
 * @version            0.8.3
 *
 */

class WPXtremeAdmin extends WPDKWordPressAdmin {

  /**
   * This is the minimun capability required to display admin menu item
   *
   * @note This constant will be removed when the new roles and caps engine is ready
   *
   * @brief Menu capability
   */
  const MENU_CAPABILITY = 'manage_options';

  /**
   * Create an instance of WPXtremeAdmin class
   *
   * @brief Construct
   *
   * @param WPXtreme $plugin A WPXtreme object pointer
   *
   * @return WPXtremeAdmin
   */
  public function __construct( WPXtreme $plugin ) {
    parent::__construct( $plugin );

    /* Plugin List */
    add_action( 'plugin_action_links_' . $this->plugin->pluginBasename, array( $this, 'plugin_action_links' ), 10, 4 );

    /* Loading Script & style for backend */
    add_action( 'admin_head', array( $this, 'admin_head' ) );

    /* Dashboard. */
    add_action( 'wp_dashboard_setup', array( $this, 'wp_dashboard_setup' ) );

    /* Admin footer. */
    add_action( 'in_admin_footer', array( $this, 'in_admin_footer' ) );
    add_action( 'admin_footer', array( $this, 'admin_footer' ) );

    /* Enable link on Control Panel. */
    add_filter( 'wpxm_control_panel_widget_url-' . $this->plugin->name, array( $this, 'controlPanel' ) );

    /* Control panel badge */
    add_filter( 'wpxm_widget_badge_' . sanitize_key( $plugin->folderName ), array( $this, 'wpxm_widget_badge' ) );
    add_filter( 'wpxm_widget_badge_wpxm_plugin_store', array( $this, 'wpxm_widget_badge' ) );

    /* Add classes for enhancer UI layout. */
    $appearance = WPXtremeConfigurationEnhancerAppearanceGeneral::getInstance();

    $this->bodyClasses['wpxm-body-shadow_header']   = wpdk_is_bool( $appearance->shadow_header );
    $this->bodyClasses['wpxm-body-gradient_header'] = wpdk_is_bool( $appearance->gradient_header );
    $this->bodyClasses['wpxm-body-fullscreen']      = wpdk_is_bool( $appearance->fullscreen );
    $this->bodyClasses['wpxm-body-badge']           = wpdk_is_bool( $appearance->badge );
    $this->bodyClasses['wpxm-body-shoadow_postbox'] = wpdk_is_bool( $appearance->shadow_postbox );
    $this->bodyClasses['wpxm-body-toolbars']        = wpdk_is_bool( $appearance->toolbars );
    $this->bodyClasses['wpxm-body-messages']        = wpdk_is_bool( $appearance->messages );
    $this->bodyClasses['wpxm-body-editor_title']    = wpdk_is_bool( $appearance->editor_title );
    $this->bodyClasses['wpxm-body-inputs']          = wpdk_is_bool( $appearance->inputs );
    $this->bodyClasses['wpxm-body-tables']          = wpdk_is_bool( $appearance->tables );
    $this->bodyClasses['wpxm-body-table_actions']   = wpdk_is_bool( $appearance->table_actions );
    $this->bodyClasses['wpxm-body-inline_edit']     = wpdk_is_bool( $appearance->inline_edit );


    $state = get_option( 'wpxtreme_action_control_panel_plugin_state' );
    $state = empty( $state ) ? 'active' : $state;

    $this->bodyClasses['wpxm-hide-plugin-no-active'] = ( 'active' == $state );
    $this->bodyClasses['wpxm-hide-widget-nolink']    = ( 'linked' == $state );

    /* Added wpXtreme menu to admin bar. */
    //add_action( 'wp_before_admin_bar_render', array( $this, 'wp_before_admin_bar_render' ) );
    //add_action( 'in_admin_header', array( $this, 'in_admin_header' ), -1 );

    /* Display the WP Pointer for welcome and start a tour. */
    $this->_pointerWelcomeTour();

  }

  /**
   * Return the complete URL for the control panel
   *
   * @brief Control Panel
   *
   * @return string
   */
  public function controlPanel() {
    return WPDKMenu::url( 'WPXtremeControlPanelViewController' );
  }

  /**
   * Start the Tour when we are in plugins page, after install.
   *
   * @brief WP Pointer tour
   */
  private function _pointerWelcomeTour() {
    global $pagenow;

    if ( 'plugins.php' == $pagenow ) {
      /* WordPress Pointer */
      $content                = sprintf( __( 'Welcome to wpXtreme release %s.<br/>If you like discover all news from this release, please click on <strong>Start a tour</strong> button below.<br/><br/>Thank you!', WPXTREME_TEXTDOMAIN ), WPXTREME_VERSION );
      $pointer                = new WPDKPointer( 'wpxm-welcome', '#toplevel_page_wpxm_menu_wpxtreme', __( 'Congratulations!', WPXTREME_TEXTDOMAIN ), $content, 'top' );
      $pointer->buttonPrimary = array(
        'id'    => 'wpxm-button-start-tour',
        'label' => __( 'Start a tour', WPXTREME_TEXTDOMAIN ),
        'page'  => 'wpxm_menu_wpxtreme'
      );
    }
  }

  /* Uncomment to display something before admin bar
  public function in_admin_header() {
      if ( !class_exists( 'WPXtremeControlPanelViewController' ) ) {
          require_once( $this->plugin->classesPath . 'admin/wpxtreme-pad-vc.php' );
      }
      $view_controller = new WPXtremeControlPanelViewController();
      $view_controller->display();
  }
  */

  /* Uncomment to add menu in admin bar
  public function wp_before_admin_bar_render() {
      /**
       * @var WP_Admin_Bar $wp_admin_bar
       * /
      global $wp_admin_bar;

      $node = array(
          'id'    => 'wpxm-logo',
          'title' => sprintf( '<img src="%s" />', $this->plugin->url_images . 'logo-16x16.png' ),
          'href'  => '#wpxm-control-panel',
          'meta'  => array(
              'title' => __( 'wpXteme Control Panel' ),
          ),
      );

      $wp_admin_bar->add_menu( $node );

  }
  */

  // -----------------------------------------------------------------------------------------------------------------
  // WordPress Hooks
  // -----------------------------------------------------------------------------------------------------------------

  /**
   * @brief Init the wpXtreme Dashboard
   * @since 1.0.0.b3
   */
  public function wp_dashboard_setup() {
    WPXtremeDashboard::init();
  }

  /**
   * Override and called when admin backend loaded
   *
   * @brief Admin head
   */
  public function admin_head() {

    /* Styles */
    wp_deregister_style( 'jquery-ui' );

    wp_enqueue_style( 'wpxm-admin', $this->plugin->cssURL . 'wpxm-admin.css', array(), $this->plugin->version );

    /* Since 1.0.0.b3 this style is loading because the styles are set onfly. */
    wp_enqueue_style( 'wpxm-admin-enhanced',
      $this->plugin->cssURL . 'wpxm-admin-enhanced.css', array(), $this->plugin->version );

    /* Scripts */
    wp_enqueue_script( 'wpxm-admin',
      $this->plugin->javascriptURL . 'wpxm-admin.js', array( 'jquery' ), $this->plugin->version, true );
  }

  /**
   * Override and called when WordPress is ready to draw the menu in admin area
   *
   * @brief Admin menu area
   */
  public function admin_menu() {
    /* Hack for wpXtreme icon. */
    $icon_menu = $this->plugin->imagesURL . 'logo-16x16.png';

    /* Hack for submenu. Not use yet. */
    // $icon_submenu = '<i class="wpxm-menu-item-icon"></i>';

    $menus = array(
      'wpxm_menu_wpxtreme' => array(
        'menuTitle'  => 'wpXtreme',
        'capability' => self::MENU_CAPABILITY,
        'icon'       => $icon_menu,
        'position'   => 0,
        'subMenus'   => array(

          array(
            'menuTitle' =>  __( 'Control Panel', WPXTREME_TEXTDOMAIN ),
            'capability' => self::MENU_CAPABILITY,
            'viewController' => 'WPXtremeControlPanelViewController',
          ),

          array( WPDKSubMenuDivider::DIVIDER => __( 'WPX Store', WPXTREME_TEXTDOMAIN ) ),

          array(
            'menuTitle'       => __( 'Plugins', WPXTREME_TEXTDOMAIN ),
            'capability'      => self::MENU_CAPABILITY,
            'viewController'  => 'WPXtremeStoreViewController',
          ),
          array(
            'menuTitle'       => __( 'Themes', WPXTREME_TEXTDOMAIN ),
            'capability'      => self::MENU_CAPABILITY,
            'viewController'  => 'WPXtremeStoreThemesViewController',
          ),

          array( WPDKSubMenuDivider::DIVIDER => __( 'More', WPXTREME_TEXTDOMAIN ) ),

          array(
            'menuTitle' =>  __( 'Preferences', WPXTREME_TEXTDOMAIN ),
            'capability'     => self::MENU_CAPABILITY,
            'viewController' => 'WPXtremeConfigurationEnhancerAppearanceViewController',
          ),
          array(
            'menuTitle' =>  __( 'About', WPXTREME_TEXTDOMAIN ),
            'viewController' => 'WPXtremeAboutViewController',
            'capability'     => self::MENU_CAPABILITY,
          ),
        )
      )
    );

    /* Filters for badge in menu and on widget icon. */

    /* Subitem menu plugin. */
    add_filter( 'wpdk_submenu_title', array( $this, 'wpdk_submenu_title' ), 10, 3 );

    WPDKMenu::renderByArray( $menus );

    // -------------------------------------------------------------------------------------------------------------
    // Widgets
    // -------------------------------------------------------------------------------------------------------------

    /**
     * @var WPXtremeControlPanelWidgets $widgets
     */
    $widgets = WPXtremeControlPanelWidgets::getInstance();

    /* Plugin store */
    $widgets->add( 'wpxm_plugin_store', __( 'Plugins', WPXTREME_TEXTDOMAIN ), __( 'Plugins', WPXTREME_TEXTDOMAIN ), self::MENU_CAPABILITY, sprintf( '%s%s', $this->plugin->imagesURL, 'icon-store-plugins-64x64.png' ), 'WPXtremeStoreViewController',  __( 'Download and Install free and pro plugins in two clicks', WPXTREME_TEXTDOMAIN ) );

    /* Themes store */
    $widgets->add( 'wpxm_themes_store', __( 'Themes', WPXTREME_TEXTDOMAIN ), __( 'Themes', WPXTREME_TEXTDOMAIN ), self::MENU_CAPABILITY, sprintf( '%s%s', $this->plugin->imagesURL, 'icon-store-themes-64x64.png' ), 'WPXtremeStoreThemesViewController',  __( 'Coming...', WPXTREME_TEXTDOMAIN ) );

    /* Settings */
    $widgets->add( 'wpxm_preferences', __( 'Preferences', WPXTREME_TEXTDOMAIN ), __( 'Preferences', WPXTREME_TEXTDOMAIN ), self::MENU_CAPABILITY, sprintf( '%s%s', $this->plugin->imagesURL, 'icon-appearance-64x64.png' ), 'WPXtremeConfigurationEnhancerAppearanceViewController',  __( 'Enhance and manage some WordPress appearances', WPXTREME_TEXTDOMAIN ) );

    /* Issue report. */
    $widgets->add( 'wpxm_issue_tracker', __( 'Issue Report', WPXTREME_TEXTDOMAIN ), __( 'Issue Report', WPXTREME_TEXTDOMAIN ), self::MENU_CAPABILITY, sprintf( '%s%s', $this->plugin->imagesURL, 'icon-issue-report-64x64.png' ), '#issue-report', __( 'Any bug? Please click here to send us a complete and automatic report', WPXTREME_TEXTDOMAIN ) );

    /* Internal debug; default off */
    if ( defined( 'WPXTREME_DEBUG' ) && true === WPXTREME_DEBUG ) {
      $widgets->add( 'wpxm_debug', 'Debug', 'Debug', self::MENU_CAPABILITY, sprintf( '%s%s', $this->plugin->imagesURL, 'icon-debug-64x64.png' ), array( $this, 'menuDebug' ) );
    }

  }

  /**
   * Display something before (into) the standard WordPress admin footer.
   *
   * @brief Before (into) admin footer
   * @since 1.0.0.b3
   */
  public function in_admin_footer() {
    $issue_reposrt_view = new WPXtremeIssueReportView;
    ?>
  <div id="wpx-admin-footer">
    <p class="alignleft wpx-logo">
      <a href="https://wpxtre.me/">wpXtreme</a> ver.<?php echo WPXTREME_VERSION ?> / <a href="http://www.wpdk.io/">WPDK</a> ver.<?php echo WPDK_VERSION ?>
      <?php echo apply_filters( 'wpxm_admin_footer_left', '' ) ?>
    </p>

    <p class="alignright">
      <?php echo apply_filters( 'wpxm_admin_footer_right', '' ) ?>
      <a href="https://wpxtre.me/support">Support</a> <?php if( current_user_can( self::MENU_CAPABILITY ) ) : ?>| <a href="#issue-report">Issue Report</a>
      <?php echo $issue_reposrt_view->footer() ?>
      <?php endif; ?>
    </p>

    <div class="clear"></div>
  </div>
  <?php
  }

  /**
   * Display something after the standard WordPress admin footer.
   *
   * @brief After admin footer
   * @since 1.0.0.b3
   */
  public function admin_footer() {
    $issue_reposrt_view = new WPXtremeIssueReportView;
    echo $issue_reposrt_view->modal();
  }

  /**
   * Badge filter for WPX Store plugin update
   *
   * @param string $title  Menu title
   * @param string $id     Menu id
   * @param string $parent Parent menu id
   *
   * @return string
   */
  public function wpdk_submenu_title( $title, $id, $parent ) {

    $menu_info = WPDKMenu::menu( 'WPXtremeStoreViewController' );

    if ( $menu_info['page'] == $id ) {
      $count = WPXUpdate::countUpdatingPlugins();
      /* Count wpXtreme plugin to update. */
      $badge = WPDKUI::badge( $count, 'wpxm-badge-update-plugins' );
      $title = $title . $badge;
    }

    return $title;
  }

  /**
   * Display the badge in Control Panel
   *
   * @brief Badge
   *
   * @param string $badge Defaul is empty
   *
   * @return string
   */
  public function wpxm_widget_badge( $badge ) {
    $count = WPXUpdate::countUpdatingPlugins();
    $badge = WPDKUI::badge( $count, 'wpxm-badge-update-plugins' );
    return $badge;
  }

  // -----------------------------------------------------------------------------------------------------------------
  // Plugin page Table List integration
  // -----------------------------------------------------------------------------------------------------------------

  /**
   * Return an HTML markup link to add in Plugin column in Plugin List.
   *
   * @brief WordPress links hook
   *
   * @param array $links
   *
   * @return array
   */
  public function plugin_action_links( $links ) {

    $l10n = array(
      'warnig_confirm_disable_plugin' => __( 'WARNING! If you disable WPXtreme plugin, you\'ll no longer be able to use any of the connected plugins!\n\nDo you really want to continue?', WPXTREME_TEXTDOMAIN )
    );

    wp_enqueue_script( 'wpxm-plugin-list', $this->plugin->javascriptURL . 'wpxm-plugin-list.js', $this->plugin->version, true );
    wp_localize_script( 'wpxm-plugin-list', 'WPXMPluginListL10n', $l10n );

    $url    = WPDKMenu::url( 'WPXtremeControlPanelViewController' );
    $result = '<a href="' . $url . '">' . __( 'Control Panel', WPXTREME_TEXTDOMAIN ) . '</a>';
    array_unshift( $links, $result );
    return $links;
  }

  /**
   * Return an HTML markup link to add in Plugin column in Plugin List.
   *
   * @brief WordPress row meta hook
   *
   * @param array  $links Links array
   * @param string $file  Plugin path/main_file.php
   *
   * @return array
   */
  public function plugin_row_meta( $links, $file ) {
    /* am I? */
    if ( $file == $this->plugin->pluginBasename ) {
      $links[] = '<span class="wpxm-row-meta">' . __( 'For more info and plugins visit', WPXTREME_TEXTDOMAIN ) .
        ' <a href="http://www.wpxtre.me">wpXtre.me</a></span>';
    }
    return $links;
  }

  // -----------------------------------------------------------------------------------------------------------------
  // Menu Debug
  // -----------------------------------------------------------------------------------------------------------------

  /**
   * This is enabled if WPXTREME_DEBUG is true
   *
   * @brief Manual
   */
  public function menuDebug() {
    include_once( trailingslashit( dirname( __FILE__)) . 'wpxtreme-debug.php' );
  }

}