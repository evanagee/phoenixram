<?php
/**
 * The wpx Control Panel view controller
 *
 * @class              WPXtremeControlPanelViewController
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2012-11-28
 * @version            0.8.1
 *
 */
final class WPXtremeControlPanelViewController extends WPDKViewController {

  /**
   * Return an instance of WPXtremeControlPanelViewController class
   *
   * @brief Construct
   *
   * @return WPXtremeControlPanelViewController
   */
  public function __construct() {
    parent::__construct( 'wpxm-control-panel', __( 'Control Panel', WPXTREME_TEXTDOMAIN ) );

    $view = new WPXtremeControlPanelView();
    $this->view->addSubview( $view );

    /* The control panel has not the top header view. */
    $this->viewHead->removeFromSuperview();

    /* WordPress Pointer */
    $content           = __( 'The wpXtreme <strong>Control Panel</strong> allows to display all system widgets (on top) and all wpXtreme plugins.<br/> Starting from this release, you will be able to switch the plugin display mode by <strong>select All, Active and Linked</strong> buttons on the right.', WPXTREME_TEXTDOMAIN );
    $pointer           = new WPDKPointer( 'wpxm-control-panel', '#wpxm-widgets', __( 'Welcome to wpXtreme Control Panel', WPXTREME_TEXTDOMAIN ), $content, 'top', 'left' );
    $pointer->nextPage = 'wpxm_menu_wpxtreme-submenu-5';
  }
}


/**
 * The wpx Control Panel View
 *
 * @class              WPXtremeControlPanelView
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2012-11-28
 * @version            0.8.1
 *
 */
class WPXtremeControlPanelView extends WPDKView {

  /**
   * Return an instance of WPXtremeControlPanelView class
   *
   * @brief Construct
   *
   * @return WPXtremeControlPanelView
   */
  public function __construct() {
    parent::__construct( 'wpxm-control-panel', 'wpdk-border-container' );
  }


  /**
   * Display the Control Panel
   *
   * @brief Draw
   */
  public function draw() {
    /* Display internal hidden widgets. */

    if ( !empty( WPXtremeControlPanelWidgets::getInstance()->widgets ) ) {
      ?>
    <div id="wpxm-widgets" class="clearfix">
      <h3><?php _e( 'Quick Widgets', WPXTREME_TEXTDOMAIN ) ?></h3><?php
      /**
       * @var WPXtremeControlPanelWidget $value
       */
      $value = null;
      foreach ( WPXtremeControlPanelWidgets::getInstance()->widgets as $key => $value ) {
        if ( wpdk_is_url( $value->hook ) ) {
          $href = $value->hook;
        }
        else {
          $href = add_query_arg( array( 'page' => $key ), 'admin.php' );
        }
        $view = new WPXtremeControlPanelWidgetView( $key, $href, $value->icon, $value->label, $value->tooltip );
        $view->display();
      }
      ?></div>
    <?php
    }

    /* Display the plugin as icon. */

    $plugins = WPDKPlugins::getInstance();

    /* Get only wpXtreme plugins. */
    if ( !empty( $plugins->wpxPlugins ) ) {
      $state = get_option( 'wpxtreme_action_control_panel_plugin_state' );
      $state = empty( $state ) ? 'active' : $state;
      ?>
      <div id="wpxm-installed-plugins" class="clearfix wpdk-jquery-ui">
      <h3><?php _e( 'Installed Plugins', WPXTREME_TEXTDOMAIN ) ?>
        <span style="float:right" class="btn-group" data-toggle="buttons-radio">
        <button type="button" data-value="all" class="<?php echo ( 'all' == $state ) ? 'active' : '' ?> button button-primary btn btn-primary"><?php _e( 'All', WPXTREME_TEXTDOMAIN) ?></button>
        <button title="<?php _e( 'Display only active wpXtreme plugins', WPXTREME_TEXTDOMAIN ) ?>" type="button" data-value="active" class="<?php echo ( 'active' == $state ) ? 'active' : '' ?> button button-primary btn btn-primary"><?php _e( 'Active', WPXTREME_TEXTDOMAIN) ?></button>
        <button title="<?php _e( 'Display only the wpXtreme plugins that have a link to a view', WPXTREME_TEXTDOMAIN ) ?>" type="button" data-value="linked" class="<?php echo ( 'linked' == $state ) ? 'active' : '' ?> button button-primary btn btn-primary"><?php _e( 'Linked', WPXTREME_TEXTDOMAIN) ?></button>
      </span>
      </h3>
      <?php

      /**
       * @var WPDKPlugin $value
       */
      $value = null;
      foreach ( $plugins->wpxPlugins as $key => $value ) {

        /* Ask if have to display this plugin in control panel. */
        $continue = apply_filters( 'wpxm_control_panel_should_' . $value->name . '_display', true );

        if ( true == $continue ) {
          /* Check if active. */
          $href            = '';
          $no_active_class = $value->active ? '' : 'wpxm-plugin-no-active';
          $value->description .= $value->active ? '' : ' (OFF)';
          if ( $value->active ) {
            $href = apply_filters( 'wpxm_control_panel_widget_url-' . $value->name, $href );
          }

          /* In key these is the path/main_file.php. Then get only the folder. */
          $folder = dirname( $key );
          $view   = new WPXtremeControlPanelWidgetView( sanitize_key( $folder ), $href, $value->icon, $value->name, strip_tags( $value->description ),
            'wpxm-plugin ' . $no_active_class );
          $view->display();
        }
      }
      ?></div><?php
    }
  }
}


/**
 * The control panel widgets model.
 *
 * @class              WPXtremeControlPanelWidgets
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-28
 * @version            1.0.0
 *
 */
class WPXtremeControlPanelWidgets {

  /**
   * List of system widgets
   *
   * @brief List of xtreme widgets
   *
   * @var array $widgets
   */
  public $widgets;

  /**
   * Return a static instance of WPXtremeControlPanelWidgets class
   *
   * @brief Init
   *
   * @return WPXtremeControlPanelWidgets
   */
  public static function getInstance() {
    static $instance = null;
    if ( is_null( $instance ) ) {
      $instance = new WPXtremeControlPanelWidgets();
    }
    return $instance;
  }

  /**
   * Create an instance of WPXtremeControlPanelWidgets class
   *
   * @brief Construct
   */
  private function __construct() {
    $this->widgets = array();

    /* This is a patch for WordPress. */
    add_filter( 'admin_title', array( $this, 'admin_title' ), 10, 2 );
  }

  /**
   * Return a non empty title for admin backend area. This can happen when you add an hidden custom page.
   *
   * @brief WordPress filter
   *
   * @param string $admin_title Formatted title
   * @param string $title       Single title
   *
   * @return string
   */
  public function admin_title( $admin_title, $title ) {
    global $plugin_page, $admin_page_hooks;

    if ( empty( $title ) ) {
      if ( !empty( $admin_page_hooks[$plugin_page] ) ) {
        return $admin_page_hooks[$plugin_page];
      }
    }

    return $admin_title;

  }

  /**
   * Add an instance of WPXtremeControlPanelWidget class to the global list
   *
   * @brief Add a single wpx Pad to the global list
   *
   * @param string   $id                   Unique id widegt
   * @param string   $name                 Label display under the widget
   * @param string   $title                Page title
   * @param string   $icon                 URL of icon (the icon is a 64x64)
   * @param callback $view_controller      Function to display widget page or view controller name
   * @param string   $tooltip              Optional. The tooltip markup
   *
   * @return bool|WPXtremeControlPanelWidget FALSE if current user has not capability
   */
  public function add( $id, $name, $title, $capability, $icon, $view_controller, $tooltip = '' ) {
    if ( current_user_can( $capability ) ) {
      $id                 = sanitize_key( $id );
      $widget             = new WPXtremeControlPanelWidget( $id, $name, $title, $capability, $icon, $view_controller );
      $widget->tooltip    = $tooltip;
      $this->widgets[$id] = $widget;

      return $widget;
    }
    return false;
  }
}



/**
 * A single control panel widget model
 *
 * @class              WPXtremeControlPanelWidget
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-28
 * @version            1.0.0
 *
 */
class WPXtremeControlPanelWidget {

  /**
   * Widget ID
   *
   * @brief ID
   *
   * @var string $id
   */
  public $id;

  /**
   * The label name under the icon
   *
   * @brief Label
   *
   * @var string $label
   */
  public $label;

  /**
   * The page title when you display the widget page
   *
   * @brief Page title
   *
   * @var string $pageTitle
   */
  public $pageTitle;

  /**
   * The URL of widget icon. Usually is an icon 64x64 (transparent)
   *
   * @brief Widget icon
   *
   * @var string $icon
   */
  public $icon;

  /**
   * Only the user with this capability can be view this widget
   *
   * @brief Capability
   *
   * @var string $capability
   */
  public $capability;

  /**
   * View controller to display this widget
   *
   * @brief View controller
   *
   * @var callback viewController
   */
  public $viewController;

  /**
   * A Widget tooltip
   *
   * @brief Widget tooltip
   *
   * @var string $tooltip
   */
  public $tooltip;

  /**
   * WordPress page hook suffix
   *
   * @brief Hook suffix
   *
   * @var string $hook
   */
  public $hook;


  /**
   * Return an instance of WPXtremeControlPanelWidget class
   *
   * @brief Construct
   *
   * @param string   $id              The widget ID
   * @param string   $label           The widget label displayed under the widget icon
   * @param string   $page_title      The page
   * @param string   $capability      Minimun capability
   * @param string   $icon            Icon widget
   * @param callback $view_controller A view controller class or callback function used to display the page of widget
   *
   * @return WPXtremeControlPanelWidget
   */
  public function __construct( $id, $label, $page_title, $capability, $icon, $view_controller ) {
    $this->id             = sanitize_key( $id );
    $this->label          = $label;
    $this->pageTitle      = $page_title;
    $this->capability     = $capability;
    $this->icon           = $icon;
    $this->viewController = $view_controller;
    $this->tooltip        = '';
    $this->hook           = $view_controller;

    /* Store the hook. First check if is not an url. */
    if ( !wpdk_is_url( $view_controller ) ) {
      if ( is_string( $view_controller ) && !function_exists( $view_controller ) ) {
        $hook       = create_function( '', sprintf( '$view_controller = new %s; $view_controller->display();', $view_controller ) );
        $hook_head  = create_function( '', sprintf( '%s::didHeadLoad();', $view_controller ) );
        $hook_load  = create_function( '', sprintf( '%s::willLoad();', $view_controller ) );
        $this->hook = wpdk_add_page( $id, $page_title, $capability, $hook, $hook_head, $hook_load );
      }
      elseif ( is_array( $view_controller ) ) {
        $hook_head  = method_exists( $view_controller[0], 'didHeadLoad' ) ? array( $view_controller[0], 'didHeadLoad' ) : '';
        $hook_load  = method_exists( $view_controller[0], 'willLoad' ) ? array( $view_controller[0], 'willLoad' ) : '';
        $this->hook = wpdk_add_page( $id, $page_title, $capability, $view_controller, $hook_head, $hook_load );
      }
      elseif ( function_exists( $view_controller ) ) {
        $hook_head  = function_exists( $view_controller . '_head' ) ? $view_controller . '_head' : '';
        $hook_load  = function_exists( $view_controller . '_load' ) ? $view_controller . '_load' : '';
        $this->hook = wpdk_add_page( $id, $page_title, $capability, $view_controller, $hook_head, $hook_load );
      }
    }
  }
}

/**
 * A single control panel widget view (icon)
 *
 * @class              WPXtremeControlPanelWidgetView
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2012-11-28
 * @version            0.8.1
 */
class WPXtremeControlPanelWidgetView extends WPDKView {

  private $_id;
  private $_icon;
  private $_name;
  private $_href;
  private $_class;
  private $_tooltip;

  /**
   * Create an instance of WPXtremeControlPanelWidgetView class
   *
   * @brief Construct
   *
   * @param string        $id      View id
   * @param array|string  $href    (Optional) URL. If empty the widget icon is displayed grey
   * @param string        $icon    Path for icon
   * @param string        $name    Name of widget
   * @param string        $tooltip Optional. The tooltip
   * @param string|array  $class   Optional. Additiona class for view
   *
   * @return WPXtremeControlPanelWidgetView
   */
  public function __construct( $id, $href, $icon, $name, $tooltip = '', $class = '' ) {
    $this->_id      = $id;
    $this->_icon    = $icon;
    $this->_name    = $name;
    $this->_href    = $href;
    $this->_tooltip = $tooltip;
    $this->_class   = array( 'wpxm-widget' );

    if ( empty( $href ) ) {
      $this->_class[] = 'wpxm-widget-nolink';
    }

    if ( !empty( $class ) ) {
      if ( is_array( $class ) ) {
        $this->_class = array_merge( $this->_class, $class );
      }
      elseif ( is_string( $class ) ) {
        $this->_class = array_merge( $this->_class, explode( ' ', $class ) );
      }
    }

    parent::__construct( 'wpxm-widget-' . sanitize_key( $id ), $this->_class );
  }

  /**
   * Display a widget
   *
   * @brief Draw
   */
  public function draw() {

    /* Filter for badge. */
    /* In $this->_id you found wpxm_plugin_store, wpxm_preferences or wpx-cleanfix, wpx-deflector etc... */
    echo apply_filters( 'wpxm_widget_badge_' . $this->_id, '' );

    /* Add link only if href is not empty. */
    if ( !empty( $this->_href ) ) : ?>
            <a href="<?php echo $this->_href ?>">
        <?php endif; ?>
    <img title="<?php echo $this->_tooltip ?>" class="wpdk-tooltip" src="<?php echo $this->_icon ?>"/>
    <?php if ( !empty( $this->_href ) ) : ?>
            </a>
        <?php endif; ?>
  <p class=""><?php echo $this->_name ?></p>
  <?php
  }
}
