<?php
/**
 * The wpXtreme Dashboard
 *
 * ## Overview
 * Manage the wpXtreme Dashboard in WordPress dashboard screen
 *
 * @class           WPXtremeDashboard
 * @author          =undo= <info@wpxtre.me>
 * @copyright       Copyright (C) 2013 wpXtreme Inc. All Rights Reserved.
 * @date            2013-01-18
 * @version         0.1.0
 * @since           1.0.0.b3
 *
 */

class WPXtremeDashboard {

  /**
   * Return a singleton instance of WPXtremeDashboard class
   *
   * @brief Return singleton
   *
   * @return WPXtremeDashboard
   */
  public static function init() {
    static $instance = null;
    if ( is_null( $instance ) ) {
      $instance = new WPXtremeDashboard();
    }
    return;
  }


  /**
   * Create an instance of WPXtremeDashboard class
   *
   * @brief Construct
   *
   * @return WPXtremeDashboard
   */
  private function __construct() {
    $image_url = sprintf( '%s%s', WPXTREME_URL_IMAGES, 'logo-16x16.png' );
    $image     = sprintf( '<img style="vertical-align: top" src="%s" alt="%s" /> ', $image_url, 'wpXtreme' );
    wp_add_dashboard_widget( 'wpxtreme', $image . __( 'wpXtreme', WPXTREME_TEXTDOMAIN ), array( $this, 'display' ), array( $this, 'options' ) );
  }

  /**
   * Display the content of wpXtreme dashboard view
   *
   * @brief Display
   */
  public function display() {
    $view = new WPXtremeDashboardView;
    $view->display();
  }

  /**
   * Display the dashboard widget options
   *
   * @brief Dashboard widget options
   */
  public function options() {
    $appearance = WPXtremeConfigurationEnhancerAppearanceGeneral::getInstance();

    /* Check for updated. */
    if ( 'POST' == $_SERVER['REQUEST_METHOD'] && isset( $_POST['dashboard_number_feeds'] ) ) {
      $value                                      = esc_attr( $_POST['dashboard_number_feeds'] );
      $value                                      = max( min( $value, 21 ), 3 );
      $appearance->dashboard_number_feeds = $value;
      update_user_meta( get_current_user_id(), WPXtremeConfigurationEnhancerAppearanceGeneral::KEY, $appearance );
    }
    else {


      $fields = array(
        __( 'Breaking News', WPXTREME_TEXTDOMAIN ) => array(
          array(
            'type'    => WPDKUIControlType::SELECT,
            'name'    => 'dashboard_number_feeds',
            'label'   => __( 'Number of feed', WPXTREME_TEXTDOMAIN ),
            'value'   => $appearance->dashboard_number_feeds,
            'options' => array(
              '3'  => '3',
              '5'  => '5',
              '10' => '10',
              '15' => '15',
              '20' => '20'
            )
          )
        )
      );
      $layout = new WPDKUIControlsLayout( $fields );
      $layout->display();
    }
  }
}

/**
 * The wpXtreme Dashboard View
 *
 * ## Overview
 * Display the content of the wpXtreme Dashboard
 *
 * @class           WPXtremeDashboardView
 * @author          =undo= <info@wpxtre.me>
 * @copyright       Copyright (C) 2013 wpXtreme Inc. All Rights Reserved.
 * @date            2013-01-18
 * @version         0.1.0
 * @since           1.0.0.b3
 *
 */
class WPXtremeDashboardView extends WPDKView {

  /**
   * Create an instance of WPXtremeDashboardView class
   *
   * @brief Construct
   *
   * @return WPXtremeDashboardView
   */
  public function __construct() {
    parent::__construct( 'wpxtreme-dashboard' );
  }

  /**
   * @brief Drawing
   */
  public function draw() {
    /* @todo Before latest news we can attach a special channel with our customize message. Remember that the user
     *       could be logged in. We know him system configuration, etc...
     */

    /* Latest feeds. */
    $this->latestFeeds();

    /* @todo Check if wpXteme plugin has been updagrade! */
    /* Read the option plugin list? */
  }

  /**
   * Display the latest 5 feed from main web site
   *
   * @brief Latest feed
   */
  private function latestFeeds() {
    $appearance = WPXtremeConfigurationEnhancerAppearanceGeneral::getInstance();

    include_once( ABSPATH . WPINC . '/class-simplepie.php' );

    $feed = 'https://wpxtre.me/category/blog/feed/';
    $rss  = fetch_feed( $feed );
    if ( !is_wp_error( $rss ) ) {
      $maxitems  = $rss->get_item_quantity( $appearance->dashboard_number_feeds );
      $rss_items = $rss->get_items( 0, $maxitems );
      if ( $rss_items ) {
        printf( '<h4>%s</h4><ul>', __( 'Breaking News', WPXTREME_TEXTDOMAIN ) );
        foreach ( $rss_items as $item ) {
          printf( '<li>%s <a href="%s">%s</a></li>', $item->get_date( 'Y, j F' ), $item->get_permalink(), $item->get_title() );
        }
        echo '</ul>';
      }
    } else {
      /**
       * @var WP_Error $error
       */
      $error = $rss;
      echo $error->get_error_message();
    }
  }

  private function updates() {

  }

  private function pluginsToUpdates() {
    ?>
  <?php
  }

}
