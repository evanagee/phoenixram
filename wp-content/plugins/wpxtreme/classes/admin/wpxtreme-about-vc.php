<?php
/**
 * Display the plugin card from the WPX Store
 *
 * @class              WPXtremeAboutViewController
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-03-20
 * @version            0.9.3
 */

class WPXtremeAboutViewController extends WPXAboutViewController {

  /**
   * Create an instance of WPXtremeAboutViewController class
   *
   * @brief Construct
   *
   * @return WPXtremeAboutViewController
   */
  public function __construct() {
    parent::__construct( $GLOBALS['WPXtreme'] );
  }

}