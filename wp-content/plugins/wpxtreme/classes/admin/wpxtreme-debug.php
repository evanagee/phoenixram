<?php
/**
 * Debug panel
 *
 * ## Overview
 *
 * Use this file to debug
 *
 * @author          =undo= <info@wpxtre.me>
 * @copyright       Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date            2013-03-28
 * @version         1.0.0
 *
 */
?>

<pre class="wpdk-monitor">
<?php
global $wpdb;

function o( $v )
{
  ?>
  <pre><?php var_dump( $v ) ?></pre></hr><?php
}

$wpdb->show_errors();
error_reporting( E_ALL );
ini_set( 'display_errors', true );
$r = '';

// NON RIMUOVERE - PATCH PER FORZARE UPDATE DI CONFIG QUANDO SI EFFETTUA UN CAMBIAMENTO IN STRUTTURA!
//    $config = WPXtremeConfiguration::init();
//    $config->enhancers->security->webserver = new WPXtremeConfigurationEnhancerSecurityWebServer();
//    $config->enhancers->security->wordpresstheme = new WPXtremeConfigurationEnhancerSecurityWordpressTheme();
//    WPXtremeConfiguration::init()->update();
//    $config = get_option( WPXtremeConfiguration::$kConfigName );
//    output( $config );
// FINE PATCH
?>

  <?php
  // YOUR TEST HERE

  $a = 'test <g.fazioli@undolog.com>';
  $c = sanitize_email( $a );
  o( $c );
  //$t = filter_var( $c, FILTER_VALIDATE_EMAIL);
  $t = is_email( $c );
  o( $t );

  //o( $a );



//  $source = json_encode( $a );
//
//  o( strlen( $source ) );
//
//  $result = gzcompress( $source );
//
//  o( $result );
//
//  $result = gzuncompress( $result );
//
//  o( $result );
//
//
//  $result = gzdeflate( $source );
//
//  o( $result );
//
//  $result = gzinflate( $result );
//
//  o( $result );
//
//
//  $result = gzencode( $source );
//
//  o( $result );
//
//  $result = gzdecode( $result );
//
//  o( $result );
//
//
//  $result = gz( $source );
//
//  o( $result );
//
//  $result = gzdecode( $result );
//
//  o( $result );

//  $args = array(
//    'numbers' => '56'
//  );
//
//  $params = array(
//    'method'      => 'GET',
//    'timeout'     => '60',
//    'redirection' => 5,
//    'httpversion' => '1.0',
//    'user-agent'  => 'wpXtreme/1.0',
//    'blocking'    => true,
//    'headers'     => array(),
//    'cookies'     => array(),
//    'body'        => json_encode( $args ),
//    'compress'    => false,
//    'decompress'  => true,
//    'sslverify'   => true,
//  );
//  $request = wp_remote_request( 'http://beta.wpxtre.me/api/v1/wpxstore/plugins', $params );
//
//  output( $request );

  ?>

</pre>