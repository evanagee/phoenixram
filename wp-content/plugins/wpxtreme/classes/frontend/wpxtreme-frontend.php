<?php
/**
 * Frontend manage. Not user yet
 *
 * @class              WPXtremeFrontend
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-01-22
 * @version            0.9.5
 *
 */

class WPXtremeFrontend extends WPDKWordPressTheme {

  /**
   * Create an instance of WPXtremeFrontend class
   *
   * @brief Construct
   *
   * @param WPXtreme $plugin
   *
   * @return WPXtremeFrontend
   */
  public function __construct( WPXtreme $plugin ) {

    parent::__construct( $plugin );

    /**
     * @var WPXtremeConfiguration $config
     */
    $config = WPXtremeConfiguration::init();

  }
}