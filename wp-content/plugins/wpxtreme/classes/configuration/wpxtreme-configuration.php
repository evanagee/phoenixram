<?php
/**
 * wpXtreme configuration model
 *
 * @class              WPXtremeConfiguration
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-03-08
 * @version            0.8.8
 *
 */
class WPXtremeConfiguration extends WPDKConfiguration {

  /**
   * Unique configuration name id
   *
   * @brief Configuration name
   */
  const CONFIGURATION_NAME = 'wpxtreme-config';

  /**
   * Version number
   *
   * @brief Version
   *
   * @var string $version
   */
  public $version = WPXTREME_VERSION;

  /**
   * WPX Issue Report configuration
   *
   * @brief Configuration about the Issue Report Console
   *
   * @var WPXtremeConfigurationIssueReport $issueReport
   */
  public $issueReport;

  /**
   * Return an instance of WPXtremeConfiguration class from the database or onfly.
   *
   * @brief Get the configuration
   *
   * @return WPXtremeConfiguration
   */
  public static function init() {
    /**
     * @var WPXtremeConfiguration $instance
     */
    $instance = parent::init( self::CONFIGURATION_NAME, __CLASS__ );

    /* Or if the onfly version is different from stored version. */
    if ( version_compare( $instance->version, WPXTREME_VERSION ) < 0 ) {
      /* For i.e. you would like update the version property. */
      $instance          = $instance->delta();
      $instance->version = WPXTREME_VERSION;
      $instance->update();
    }

    return $instance;
  }

  /**
   * Create an instance of WPXtremeConfiguration class
   *
   * @brief Construct
   *
   * @return WPXtremeConfiguration
   */
  public function __construct() {
    parent::__construct( self::CONFIGURATION_NAME );

    /* Init my tree settings. */
    $this->resetToDefault();
  }

  /**
   * Optional. You can implement this method to reset to default value this sub configuration branch
   *
   * @breif Reset to default values
   */
  public function resetToDefault() {
    $this->issueReport = new WPXtremeConfigurationIssueReport();
  }

  /**
   * Override parent class method to set uniq name id setting and custom class name
   *
   * @deprecated Use init() instead
   *
   * @return WPXtremeConfiguration Singleton instance of this class
   */
  public static function config() {
    _deprecated_function( __METHOD__, '0.6.2', 'init()' );
    return self::init();
  }

}

/**
 * WPX Issue Report sub configuration model
 *
 * @class              WPXtremeConfigurationIssueReport
 * @author             yuma <info@wpxtre.me>
 * @copyright          Copyright (C) 2013- wpXtreme Inc. All Rights Reserved.
 * @date               2013-01-17
 * @version            1.0.0
 *
 */
class WPXtremeConfigurationIssueReport {

  /**
   * int - Issue Report Mode is enabled.
   *
   * @brief Issue Report Mode is enabled.
   *
   * @since 1.0.0
   */
  const ENABLED = 1;

  /**
   * int - Issue Report Mode is just disabled. It's time to send issue report to wpXtreme
   *
   * @brief Ready to send report to wpXtreme
   *
   * @since 1.0.0
   */
  const READY_TO_SEND = 2;

  /**
   * int - Issue Report Mode is disabled: normal WordPress behaviour.
   *
   * @brief Issue Report Mode is disabled.
   *
   * @since 1.0.0
   */
  const DISABLED = 3;

  /**
   * Issue Report flag: TRUE means issue report mode enabled, FALSE means default WordPress behaviour.
   *
   * @brief Issue Report flag.
   * @todo Rename in $mode
   *
   * @var int $issueReportMode
   */
  public $issueReportMode;

  /**
   * Create an instance of WPXtremeConfigurationIssueReport class
   *
   * @brief Construct
   *
   * @return WPXtremeConfigurationIssueReport
   */
  public function __construct() {
    $this->defaults();
  }

  /**
   * Set defaults
   */
  public function defaults() {

    $this->issueReportMode = self::DISABLED;

  }

}


// ---------------------------------------------------------------------------------------------------------------------
// Folowing classes are for user; every users has own configurations
// ---------------------------------------------------------------------------------------------------------------------


/**
 * Enhancer Appearance General configuration model
 *
 * @class              WPXtremeConfigurationEnhancerAppearanceGeneral
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-01-30
 * @version            1.0.0
 *
 */
class WPXtremeConfigurationEnhancerAppearanceGeneral {

  /**
   * Used to store to single user
   *
   * @brief Key for user meta
   * @since
   */
  const KEY = 'wpxtreme-enhancer-appearance';

  /**
   * Number of max feeds to display in dashboard widget
   *
   * @brief Max feeds number
   * @var int $dashboard_number_feeds
   */
  public $dashboard_number_feeds;

  /**
   * All appearance styles
   */
  public $shadow_header;
  public $gradient_header;
  public $fullscreen;
  public $badge;
  public $shadow_postbox;
  public $toolbars;
  public $messages;
  public $editor_title;
  public $inputs;
  public $tables;
  public $inline_edit;

  /**
   * Return a singleton instance of WPXtremeConfigurationEnhancerAppearanceGeneral class for the current user.
   * First of all this method try to get the instance from user meta. If no previous class instance found then create
   * it onfly.
   *
   * @brief Get instance
   *
   * @return WPXtremeConfigurationEnhancerAppearanceGeneral
   */
  public static function getInstance() {
    static $instance = null;
    if ( is_null( $instance ) ) {
      $user_id  = get_current_user_id();
      $instance = get_user_meta( $user_id, self::KEY, true );
      if ( empty( $instance ) ) {
        $instance = new WPXtremeConfigurationEnhancerAppearanceGeneral;
        update_user_meta( $user_id, self::KEY, $instance );
      }
    }
    return $instance;
  }

  /**
   * Create an instance of WPXtremeConfigurationEnhancerAppearanceGeneral class
   *
   * @brief Construct
   *
   * @return WPXtremeConfigurationEnhancerAppearanceGeneral
   */
  public function __construct() {
    $this->dashboard_number_feeds = 5;

    $this->shadow_header   = 'on';
    $this->gradient_header = 'on';
    $this->fullscreen      = 'on';
    $this->badge           = 'on';
    $this->shadow_postbox  = 'on';
    $this->toolbars        = 'on';
    $this->messages        = 'on';
    $this->editor_title    = 'on';
    $this->inputs          = 'on';
    $this->tables          = 'on';
    $this->table_actions   = 'on';
    $this->inline_edit     = 'on';
  }

}

/**
 * Enhancer Appearance Posts sub configuration model
 *
 * @class              WPXtremeConfigurationEnhancerAppearancePosts
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-01-23
 * @version            0.9.5
 *
 */
class WPXtremeConfigurationEnhancerAppearancePosts {

  /**
   * Used to store to single user
   *
   * @brief Key for user meta
   * @since
   */
  const KEY = 'wpxtreme-enhancer-list-table';

  public $posts_thumbnail_author;
  public $posts_swipe_publish;

  public $media_thickbox_icon;
  public $media_thumbnail_author;

  public $pages_thumbnail_author;
  public $pages_swipe_publish;

  /**
   * Return a singleton instance of WPXtremeConfigurationEnhancerAppearancePosts class for the current user.
   * First of all this method try to get the instance from user meta. If no previous class instance found then create
   * it onfly.
   *
   * @brief Get instance
   *
   * @return WPXtremeConfigurationEnhancerAppearancePosts
   */
  public static function getInstance() {
    static $instance = null;
    if ( is_null( $instance ) ) {
      $user_id  = get_current_user_id();
      $instance = get_user_meta( $user_id, self::KEY, true );
      if ( empty( $instance ) ) {
        $instance = new WPXtremeConfigurationEnhancerAppearancePosts;
        update_user_meta( $user_id, self::KEY, $instance );
      }
    }
    return $instance;
  }

  /**
   * Create an instance of WPXtremeConfigurationEnhancerAppearancePosts class
   *
   * @brief Construct
   *
   * @return WPXtremeConfigurationEnhancerAppearancePosts
   */
  public function __construct() {
    $this->posts_thumbnail_author = 'on';
    $this->posts_swipe_publish    = 'on';
    $this->media_thumbnail_author = 'on';
    $this->media_thickbox_icon    = 'on';
    $this->pages_thumbnail_author = 'on';
    $this->pages_swipe_publish    = 'on';
  }
}