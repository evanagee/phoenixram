<?php
/// @cond private

/*
 * [DRAFT]
 *
 * THIS DOCUMENTATION IS A DRAFT. YOU CAN READ IT AND MAKE SOME EXPERIMENT BUT DO NOT USE ANY CLASS BELOW IN YOUR
 * PROJECT. ALL CLASSES AND RELATIVE METHODS BELOW ARE SUBJECT TO CHANGE.
 *
 */

/**
 * The wpXtreme API interface
 *
 * @class              WPXAPI
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-05-22
 * @version            1.0.2
 *
 */

class WPXAPI {

  /**
   * Transient ID for token
   *
   * @brief Transient ID
   */
  const TOKEN_KEY = 'token';

  /**
   * Number of seconds to keep token stored in transient
   *
   * @brief Token timeout
   */
  const TOKEN_EXPIRED = 3600;

  /**
   * The user meta key used to store a permanent user email. This is as ID Apple or ID for the WPX Store.
   *
   * @brief ID wpXtreme
   */
  const WPXSTORE_USER_EMAIL = '_wpx_store_user_email';

  /**
   * The user meta key used to store a permanent client id.
   *
   * @brief Client ID
   */
  const WPXSTORE_CLIENT_ID = 'wpx_store_cid';

  /**
   * Timeout connection request
   *
   * @brief Timeout connection
   */
  const CONNECTION_TIMEOUT = 45;

  /**
   * The User Agent request
   *
   * @brief User agent
   */
  const USER_AGENT = 'wpXtreme/1.0';

  /**
   * The wpXtreme Server
   *
   * @brief Server
   */
  const SERVER = 'https://wpxtre.me/';

  /**
   * The wpXtreme API endpoint
   *
   * @brief API endpoint
   */
  const API_ENDPOINT = 'https://wpxtre.me/api/v1';

  /**
   * Who send API.
   *
   * @brief Sender
   *
   * @note  Not used yet
   *
   * @var string $sender
   */
  public $sender;

  /**
   * Create an instance of WPXAPI class
   *
   * @brief Construct
   *
   * @param string $sender Who that send
   *
   * @return WPXAPI
   */
  public function __construct( $sender = '' )
  {
    $this->sender = $sender;
  }

  /**
   * Store to the current user the WPX Store ID
   *
   * @brief Store WPX Store ID
   *
   * @param string $email wpXtreme ID used for login into the WPX Store
   */
  public function setWPXStoreUserEmail( $email )
  {
    $user_id = get_current_user_id();
    update_user_meta( $user_id, self::WPXSTORE_USER_EMAIL, $email );
  }

  /**
   * Get the WPX Store ID for the current user
   *
   * @brief Get the I WPX Store
   *
   * @return string
   */
  public function getWPXStoreUserEmail()
  {
    $user_id = get_current_user_id();
    $email   = get_user_meta( $user_id, self::WPXSTORE_USER_EMAIL, true );
    return $email;
  }

  /**
   * Delete the WPX Store ID for the current user
   *
   * @brief Delete a WPX Store ID
   */
  public function deleteWPXStoreUserEmail()
  {
    $user_id = get_current_user_id();
    delete_user_meta( $user_id, self::WPXSTORE_USER_EMAIL );
  }

  /**
   * Store the user token in transient
   *
   * @param string $token The returned token
   */
  public function setToken( $token )
  {
    wpdk_set_user_transient( self::TOKEN_KEY, $token, self::TOKEN_EXPIRED );
  }

  /**
   * Return the stored user token, FALSE otherwise.
   *
   * @brief Get the user token
   *
   * @return string|bool
   */
  public function getToken()
  {
    $token = wpdk_get_user_transient( self::TOKEN_KEY );
    if ( empty( $token ) ) {
      return false;
    }
    return $token;
  }

  /**
   * Delete user token
   *
   * @brief Delete user token
   */
  public function deleteToken()
  {
    wpdk_delete_user_transient( self::TOKEN_KEY );
  }

  // TODO
  public function getClientID()
  {
    $user_id   = get_current_user_id();
    $client_id = wpdk_get_user_transient( self::WPXSTORE_CLIENT_ID );
    if ( empty( $client_id ) ) {
      $client_id = md5( sprintf( '%s%s%s', get_bloginfo( 'url' ), $user_id, uniqid() ) );
      wpdk_set_user_transient( self::WPXSTORE_CLIENT_ID, $client_id, MINUTE_IN_SECONDS * 30 );
    }
    return $client_id;
  }

  /**
   * Do a request to the wpXtreme Server.
   *
   * @brief Request
   *
   * @param string $route    Route. Example `wpxstore/plugins/showcase`
   * @param array  $raw_body Optional. Params will be convert in jSON
   * @param string $verb     Optional. Verb of request. Default is WPXAPIMethod::GET
   *
   * @return WPXAPIResponse|WP_Error|bool
   */
  protected function request( $route, $raw_body = array(), $verb = WPXAPIMethod::GET )
  {
    global $wp_version;

    if ( !empty( $raw_body ) ) {
      $raw_body = (array)json_decode( $raw_body );
    }

    /* Get transient secure key. */
    $token = $raw_body['token'] = $this->getToken();

    /* Set referrer */
    $raw_body['referrer'] = WPXPlugin::currentURL();

    /* Useful backward urls. */
    $raw_body['urls'] = array(
      'control_panel'   => WPDKMenu::url( 'WPXtremeControlPanelViewController' ),
      'wpxstore_plugin' => WPDKMenu::url( 'WPXtremeStoreViewController' )
    );

    /* @deprecated Sending the control panel url. */
    $raw_body['control_panel_url'] = WPDKMenu::url( 'WPXtremeControlPanelViewController' );

    /* Send version of WordPress, PHP and wpXtreme to server. */
    $raw_body['wp_version']   = $wp_version;
    $raw_body['php_version']  = PHP_VERSION;
    $raw_body['wpx_version']  = WPXTREME_VERSION;
    $raw_body['wpdk_version'] = WPDK_VERSION;

    /* Sending client wpXtreme installed plugins list. */
    $raw_body['plugins'] = $this->wpxPluginsList();

    /* Client ID. */
    $raw_body['client_id'] = $this->getClientID();

    /* Who I am */
    $raw_body['wpxstore_user_email'] = $this->getWPXStoreUserEmail();

    /* Prepare array for request. */
    $args = array(
      'method'      => $verb,
      'timeout'     => self::CONNECTION_TIMEOUT,
      'redirection' => 5,
      'httpversion' => '1.0',
      'user-agent'  => self::USER_AGENT,
      'blocking'    => true,
      'headers'     => array(),
      'cookies'     => array(),
      'body'        => json_encode( $raw_body ),
      'compress'    => false,
      'decompress'  => true,
      'sslverify'   => true,
    );

    if ( !empty( $route ) ) {
      $endpoint = sprintf( '%s%s', trailingslashit( self::API_ENDPOINT ), $route );
      $request  = wp_remote_request( $endpoint, $args );

      if ( 200 != wp_remote_retrieve_response_code( $request ) ) {
        return false;
      }

      $response = new WPXAPIResponse( $request );
      if ( empty( $token ) && isset( $response->token ) ) {
        $this->setToken( $response->token );
      }
      return $response;
    }
    return false;
  }

  /**
   * Retrive a special status from server after request. This method is called when the secure iframe is loaded.
   * Return a json encode response from the server or false if error.
   *
   * @brief Get status from server
   *
   * @param string $client_id An unique ID for client request
   *
   * @return bool|string
   */
  public function status( $client_id )
  {
    /* Buildin client id. */
    $request = wp_remote_get( self::SERVER . 'cg?status=' . $client_id );
    if ( 200 != wp_remote_retrieve_response_code( $request ) ) {
      return false;
    }
    $body = wp_remote_retrieve_body( $request );

    /* Check for token after signin user. */
    $response = json_decode( $body );
    if ( isset( $response->token ) ) {
      /* Update the token. */
      $this->setToken( $response->token );
      /* Remove the token from response. */
      //unset( $response->token );
      $body = json_encode( $response );
    }

    /* Store the user email too. */
    if ( isset( $response->user_email ) ) {
      // TODO Remove email from response?
      $this->setWPXStoreUserEmail( $response->user_email );
    }

    return $body;
  }

  /**
   * Return the wpx Plugins list installed on the client. This array list is sent to the wpx Server.
   *
   * @brief Plugin list
   *
   * @return array
   */
  public function wpxPluginsList()
  {
    // TODO uncomment to debug
    //delete_option( '_site_transient_update_plugins' );

    /* Get active plugins. */
    $active_plugins = (array)get_option( 'active_plugins', array() );

    /* Get all installed plugins. */
    $all_plugins = get_plugins();
    $wpx_pluings = array();

    /* Get only wpXtreme plugin. */
    foreach ( $all_plugins as $key => $plugin ) {

      /* Use the URI to get wpXtreme plugins */
      if ( false !== strpos( $plugin['PluginURI'], '/wpxtre.me' ) ) {
        $wpx_pluings[$key] = array(
          'version' => $plugin['Version'],
          'active'  => in_array( $key, $active_plugins ),
          'open'    => apply_filters( 'wpxm_control_panel_widget_url-' . $plugin['Name'], '' )
        );
      }
    }

    return $wpx_pluings;
  }

  /**
   * Alter the standard WordPress transient with the plugin list to update. Every single plugin have to check it self
   * for update.
   *
   * @param array $args WPX Plugin info
   *
   *     $args = array(
   *         'action'      => 'update-check',
   *         'plugin_name' => $this->_plugin_slug,
   *         'version'     => $transient->checked[$this->_plugin_slug]
   *     );
   *
   * @see WPXUpdate::pre_set_site_transient_update_plugins()
   *
   * @return bool|mixed
   */
  public function updatePlugins( $args )
  {
    $_GET['slug'] = $args['plugin_name'];
    $query        = json_encode( $_GET );
    $response     = $this->request( 'wpxstore/plugin', $query );

    if ( isset( $response->response->data ) ) {
      $product = $response->response->data;

      if ( $product && version_compare( $args['version'], $product->version ) < 0 ) {
        $result              = new stdClass();
        $result->slug        = $args['plugin_name'];
        $result->new_version = $product->version;
        $result->url         = WPXtremeAPI::SERVER;
        $result->package     = '';
        return $result;
      }
    }
    return false;
  }

}


/**
 * The wpXtreme API response request interface. This class describe the jSON response request interface.
 *
 * @class              WPXAPI
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2012-11-28
 * @version            0.9.0
 *
 */
final class WPXAPIResponse {

  /**
   * HTML markup alread convert from base64
   *
   * @brief HTML markup
   *
   * @var string $html
   */
  public $html;

  /**
   * An object used as map for json decode response
   *
   * @brief Response
   *
   * @var object $response
   */
  public $response;

  /**
   * Original body
   *
   * @brief Body of request response
   *
   * @var string $json
   */
  public $body;

  /**
   * The content response
   *
   * @brief Content
   * @deprecated
   *
   * @var string $content
   */
  public $content;

  /**
   * An error
   *
   * @brief Error
   *
   * @deprecated
   *
   * @var string $error
   */
  public $error;


  /**
   * Create an instance of WPXAPIResponse class
   *
   * @brief Construct
   * @todo  Complete the response
   *
   * @param $response (Optional) default null. If null you can create a response custom object
   *
   * @return WPXAPIResponse
   */
  public function __construct( $response = null )
  {
    $this->response = new stdClass();
    $this->token    = false;

    if ( !is_null( $response ) ) {
      $body = wp_remote_retrieve_body( $response );
      if ( !empty( $body ) ) {
        $this->body = $body;
        /**
         * @var WPXAPIResponse $json_decode
         */
        $json_decode = json_decode( $body );
        if ( !empty( $json_decode ) ) {
          foreach ( $json_decode as $property => $value ) {
            if ( 'html' == $property ) {
              $this->html = base64_decode( $json_decode->html );
            }
            elseif ( 'token' == $property ) {
              // TODO
              $this->response->$property = $value;
            }
            else {
              $this->response->$property = $value;
            }
          }
        }
      }
    }
  }
}


/**
 * The wpXtreme API REST methods
 *
 * @class              WPXAPIMethod
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2012-11-28
 * @version            0.8.1
 *
 */
class WPXAPIMethod {

  const POST   = 'POST';
  const GET    = 'GET';
  const PUT    = 'PUT';
  const DELETE = 'DELETE';
}


/**
 * The wpXtreme API REST error code
 *
 * @class              WPXAPIErrorCode
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2012-11-28
 * @version            0.8.1
 *
 */
class WPXAPIErrorCode {

  /**
   * @brief Unrecognized resource
   */
  const UNRECOGNIZE_RESOURCE = '#80001000';
}
/// @endcond