<?php
/**
 * @mainpage   Introducing wpXtreme Framework
 *
 * @section    introduction Introduction
 *
 * Welcome to wpXtreme Framework, the first sub-sdk written with WPDK.
 * The wpXtreme Framework should be use to wirite a wpXtreme Plugin.
 *
 * Disclaimer: IMPORTANT: This wpXtreme software is supplied to you by wpXtreme
 * Inc. ("wpXtreme") in consideration of your agreement to the following
 * terms, and your use, installation, modification or redistribution of
 * this wpXtreme software constitutes acceptance of these terms.  If you do
 * not agree with these terms, please do not use, install, modify or
 * redistribute this wpXtreme software.
 *
 * In consideration of your agreement to abide by the following terms, and
 * subject to these terms, wpXtreme grants you a personal, non-exclusive
 * license, under wpXtreme's copyrights in this original wpXtreme software (the
 * "wpXtreme Software"), to use, reproduce, modify and redistribute the wpXtreme
 * Software, with or without modifications, in source and/or binary forms;
 * provided that if you redistribute the wpXtreme Software in its entirety and
 * without modifications, you must retain this notice and the following
 * text and disclaimers in all such redistributions of the wpXtreme Software.
 * Neither the name, trademarks, service marks or logos of wpXtreme Inc. may
 * be used to endorse or promote products derived from the wpXtreme Software
 * without specific prior written permission from wpXtreme.  Except as
 * expressly stated in this notice, no other rights or licenses, express or
 * implied, are granted by wpXtreme herein, including but not limited to any
 * patent rights that may be infringed by your derivative works or by other
 * works in which the wpXtreme Software may be incorporated.
 *
 * The wpXtreme Software is provided by wpXtreme on an "AS IS" basis. WPXTREME
 * MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 * THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE, REGARDING THE WPXTREME SOFTWARE OR ITS USE AND
 * OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 *
 * IN NO EVENT SHALL WPXTREME BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 * MODIFICATION AND/OR DISTRIBUTION OF THE WPXTREME SOFTWARE, HOWEVER CAUSED
 * AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 * STRICT LIABILITY OR OTHERWISE, EVEN IF WPXTREME HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 *
 *
 * @page            page_1 Getting Started
 *
 * @section         section_1_1 The wpXtreme Plugin basic structure
 *
 * These are the main rules to follow in order to write a WordPress plugin compatible to the wpXtreme environment.
 *
 * ### System Requirements
 *
 * * Wordpress 3.4 or higher (last version is suggest)
 * * PHP version 5.2.4 or greater
 * * MySQL version 5.0 or greater
 *
 * ### Browser compatibility
 *
 * * We suggest to update your browser always at its last version.
 *
 * ### Overview
 *
 * Some (few) rules are mandatory, because they allow your plugin to be properly identified as wpXtreme plugin.
 * Other rules are strongly recommended, but not mandatory; the attribute recommended/mandatory will be highlighted
 * wherever it needs.
 *
 * You can develop a wpXtreme WordPress plugin from scratch; however, it is strongly recommended to let's generate the
 * basic framework of your wpXtreme plugin directly through the Product Generator of wpXtreme Developer Center, and then
 * use this framework as foundation.
 *
 * ### Organization of the file system
 *
 * The file system structure is not binding. However, you should follow this standard nomenclature and organization to
 * make it readable and compliant with other plugins created. However, some files and some content, such as comments
 * ( as happens now for any other plugin), are required to build in the right way the foundation of a wpXtreme plugin.
 * Here it is: * However, some files and some contents, such as comments ( as happens now for any other plugin),
 * are **mandatory** to properly build the foundation of a wpXtreme plugin.
 *
 * Here it is:
 *
 * **assets/**
 * **classes/**
 * kickstart.php
 * defines.php
 * index.php
 * main.php
 * *(plugin name)*.php
 * README.md
 * CHANGELOG.md
 * LICENSE.md
 * readme.txt (optional)
 *
 *
 * For compatibility reasons and for avoiding case sensitive errors, all files and folders should be lowercase.
 * Anyway, this is not a mandatory guideline.
 *
 * ### Folders
 *
 * #### assets
 *
 * This folder contains two subfolders and/or any other files/folders needed to run the plugin:
 *
 * * js/
 * * css/
 *
 * In `css` folder, create an `images` folder and put your plugin logo, at the right dimension suggested by the name:
 *
 * * css/
 *  * images/
 *   * logo-16x16.png      // Used in admin backend menu
 *   * logo-64x64.png      // Icon on title of view controller in admin backend
 *   * logo-80x80.png      // Used in the WPX Store showcase
 *   * logo-160x160.png    // Used in the WPX Store plugin card
 *   * logo-512x512.png    // Master
 *
 * The only mandatory element here is path and filename of images about your plugin logo. If you omit them, your plugin
 * won't have any associated logo images.
 *
 * #### kickstart.php
 *
 * `kickstart.php` is a mandatory file, containing the interface needed by your plugin in order to boot in wpXtreme
 * Framework environment.
 *
 * You didn't have to modify it in any way. Just leave this file in the root directory of your plugin. File `main.php`
 * will use it to physically boot the plugin in WordPress loading. See below for details.
 *
 * This file also contains the versions control interface of your plugin in WordPress environment.
 * Anytime your plugin boots, this interface checks versions of WordPress, PHP, MySQL and wpXtreme Framework installed.
 * If any of these versions is lower than requested for your plugin, or if wpXtreme Framework is not installed or active
 * in your system, the plugin won't boot, and a warning message will be displayed in your admin area, alerting about
 * condition occurred.
 *
 * #### index.php
 *
 * _Silent is golden._
 *
 * This file is here only for security reason.
 *
 * #### main.php
 *
 * This is the main file of the plugin, the one that contains the comment header used by WordPress to recognize and
 * extract information about the plugin.
 * This file is also the file that boots your plugin in wpXtreme Framework environment.
 * All WordPress plugins, by definition, must have a header - in the form of comments - which allow them to be
 * recognized by WordPress core. In wpXtreme Framework environment, this header must be complete, **and integrated with
 * some specific additional fields**, because some key information is retrieved from this sequence of comments.
 *
 * A standard header detectable in an open plugin is so shaped:
 *               
 * ```php  
 * /**
 *  * Plugin Name: wpx CleanFix
 *  * Plugin URI: https://wpxtre.me
 *  * Description: Clean and fix tools
 *  * Version: 1.0.0
 *  * Author: wpXtreme, Inc.
 *  * Author URI: https://wpxtre.me
 *      * /
 * ````
 *
 * If you get the file containing this main header through Product Generator of wpXtreme Developer Center, you'll have 
 * all header fields correctly filled with informations you choose: plugin name, plugin description, plugin author,
 * plugin author URI.
 *
 * ```php  
 * /**
 *  * Plugin Name: Your Plugin Name
 *  * Plugin URI: https://wpxtre.me
 *  * Description: Your Plugin Description
 *  * Version: 1.0.0
 *  * Author: You
 *  * Author URI: Your URI
 *  * ..........
 *  * /
 * ````
 * 
 * Additional fields handled by wpXtreme Framework must be added in this header, and typically have this form:   
 *                                                                                                               
 * ```php                                                                                                        
 *  *                                                                                                            
 *  * WPX PHP Min: 5.2.4                                                                                         
 *  * WPX WP Min: 3.4                                                                                            
 *  * WPX MySQL Min: 5.0                                                                                         
 *  * WPX wpXtreme Min: 1.0.0.b4                                                                                 
 *  *                                                                                                            
 * ```                                                                                                           
 *    
 * These are the minimum required software versions of this plugin in order to properly boot in a wpXtreme Framework 
 * environment. If you need, you can manually set software versions at whatever version your plugin **REALLY** needs in 
 * order to work.
 *      
 * If you get the file containing this main header through the wpXtreme Developer Center, you'll have all header fields 
 * correctly filled with informations you choose: plugin name, plugin description, plugin author, plugin author URI. 
 * Moreover, software versions are properly initialized with generic default values valid in any WordPress environment. 
 * All token starting with `__WPXGENESI` will be replaced in the right way for you from the wpXtreme Developer Center:
 * 
 * ````php
 * /**
 * * Plugin Name: __WPXGENESI_PLUGIN_NAME__
 * * Plugin URI: https://wpxtre.me
 * * Description: __WPXGENESI_PLUGIN_DESCRIPTION__
 * * Version: 1.0.0
 * * Author: __WPXGENESI_PLUGIN_AUTHOR__
 * * Author URI: __WPXGENESI_PLUGIN_AUTHOR_URI__
 * * Text Domain: wpx-__WPXGENESI_PLUGIN_NAME_SLUG__
 * * Domain Path: localization
 * *
 * * WPX PHP Min: 5.2.4
 * * WPX WP Min: 3.4
 * * WPX MySQL Min: 5.0 
 * * WPX wpXtreme Min: 1.0.0.b4 
 * * 
 * *\/ 
 * ```` 
 * Moreover, you will automatically find all details about this header in some of the properties of the main class that 
 * inherits the `WPXPlugin` base class. In fact, you can define your own constant shorthands for access to the 
 * basic properties of the plugin, such as:
 *
 *     define( 'WPXSAMPLE_VERSION', $this->version );
 *     define( 'WPXSAMPLE_TEXTDOMAIN', $this->textDomain );
 *     define( 'WPXSAMPLE_TEXTDOMAIN_PATH', $this->textDomainPath );
 *
 * This topic is deepened in [this article](https://developer.wpxtre.me/guides/articles/the-main-class-of-your-wpx-plugin).
 * 
 * 
 * #### (plugin name).php
 * 
 * This file could be named as you wish, and this is the reason why you see the parenthesis around its name. If you get 
 * this file through the wpXtreme Developer Center, you'll have this file automatically renamed with a part of the 
 * plugin name you choose, in lowercase.
 * 
 * This file contains the main class of your plugin. Through it, you can control every aspect of your plugin and manage
 * what to do exactly where you want ( in WordPress front end, in WordPress admin area, in both worlds, and so on ). 
 * Using this class as a manager, you can write all code you need to fulfil the scope of your plugin.
 * 
 * If you get this file through the wpXtreme Developer Center, you'll have a base generic skeleton of this main class, 
 * that you can then customize as your plugin needs.
 * 
 * @section   section_2_1 How to use Product Generator to get the basic framework of your plugin
 *
 * [draft to do]
 *
 *
 * @section   section_3_1 The main class of your wpXtreme Plugin
 *
 * ## Overview
 *
 * The main class of your wpXtreme plugin for WordPress, your "plugin" from now on, is declared and engaged in the
 * plugin main file, the one that contains the comment header used by WordPress to recognize and extract informations
 * about any plugin. If you get this file through the Product Generator of wpXtreme Developer Center, you will find
 * this main class properly already declared and ready to be used and customized according to your needs.
 *
 * Let's assume that you are creating a new wpXtreme plugin for WordPress, named `Test Plugin`, through the Product
 * Generator of wpXtreme Developer Center.
 *
 * In the root directory of your plugin, you will find a file named `wpx-testplugin.php`. Inside this file, there are
 * some configurations that you don't need to edit in any way and then, the declaration of a class named
 * `WPXTestPlugin`, that extends `WPXPlugin` class. In the following way:
 *
 *     if ( !class_exists( 'WPXTestPlugin' ) ) {
 *       class WPXTestPlugin extends WPXPlugin {
 *       ....
 *       }
 *     }
 *
 * This is the main class of your plugin. Through it, you can control every aspect of your plugin and manage what to do
 * exactly where you want ( in WordPress front end, in WordPress admin area, in both worlds, and so on ). Using this
 * class as a manager, you can write all code you need to fulfil the scope of your plugin.
 *
 * When WordPress loads your plugin through this file, this class is also engaged, through the line:
 *
 *     $GLOBALS['WPXTestPlugin'] = new WPXTestPlugin();
 *
 * at the end of the class definition. In this way, your plugin becomes up and running, obviously if it was previously
 * activated.
 *
 * For a better comprehension, we have to deepen the behaviour of the `WPXPlugin` class, because your main
 * class inherits all things it needs directly from it.
 *
 * ## WPXPlugin class
 *
 * The `WPXPlugin` class is the most important class of the whole WordPress Development Kit (WPDK). It
 * performs all init procedures for a plugin in order to make it compatible with the wpXtreme standard.
 *
 * This class provides the fundamental startup of a wpXtreme plugin. You rarely (never) instantiate WPXPlugin
 * object directly. Instead, you instantiate subclasses of the WPXPlugin class.
 *
 * So, this class **must** be used to extend the main class of your wpXtreme plugin: its function is to initialize the
 * environment in which the plugin itself operates, and record the plugin for updates incoming from wpXtreme Store.
 *
 * In addition to initializing and recording, `WPXPlugin` class performs automatically for you a large series
 * of standard procedures needed in normal coding of a WordPress standard Plugin, and gives access to a lot of
 * properties and methods really useful:
 *
 *  * Gets directly from WordPress comments information about your plugin : plugin name, version, the text and the text
 * domain path.
 *
 *  * Prepares a set of standard properties with paths and urls most commonly used.
 *  * Provides a lot of hooks to wrap (filters and actions) among the most used in WordPress environment.
 *  * Prepare an instance of `WPDKWatchDog` object for your own log.
 *
 * All properties and methods of this class has been documented in `PHPDoc` format compatible with `Doxygen` tool, so
 * you can extract all detailed info and help through your PHP IDE. Describing in details the WPDK framework is outside
 * the scope of this document.
 *
 *
 * @section section_4_1 The basic execution flow of a wpXtreme plugin
 *
 * Now that you have properly obtained and configured your basic wpXtreme plugin framework, it's time to write your code
 * in order to put the right things in the right place.
 *
 * Let's always assume that you are created a new wpXtreme plugin for WordPress, named `Test Plugin`, through the
 * Product Generator of wpXtreme Developer Center. In the root directory of your plugin, you have a file named
 * `wpx-testplugin.php`. Inside this file, you have the declaration of a class named `WPXTestPlugin`, that extends
 * `WPXPlugin` class. In this way:
 *
 *     if ( !class_exists( 'WPXTestPlugin' ) ) {
 *       class WPXTestPlugin extends WPXPlugin {
 *       ....
 *       }
 *     }
 *
 *
 * ## Plugin activation
 *
 * The method `activation` of your `WPXTestPlugin` class is invoked every time your plugin is activated. Activation is
 * not loading: the activation of a WordPress plugin happens just once, normally through `plugin` page of WordPress
 * admin area, when a user choose to activate a plugin. From that moment on, the plugin becomes *active*, and this
 * method is not invoked anymore.
 *
 * The basic code of this method prepared for you through the Product Generator of wpXtreme Developer Center is this:
 *
 *     /// Hook when the plugin is activate - only first time
 *     function activation() {
 *
 *     }
 *
 * Here you can insert the code your plugin eventually needs to execute in plugin activation phase.
 *
 * ## Plugin deactivation
 *
 * The method `deactivation` of your `WPXTestPlugin` class is invoked every time your plugin is deactivated. The
 * deactivation of a WordPress plugin happens just once, normally through `plugin` page of WordPress admin area, when a
 * user choose to deactivate a plugin. From that moment on, the plugin becomes *inactive*, and this method is not
 * invoked anymore.
 *
 * The basic code of this method prepared for you through the Product Generator of wpXtreme Developer Center is this:
 *
 *     /// Hook when the plugin is deactivated
 *     function deactivation() {
 *         // To override.
 *     }
 *
 * You can insert here the code your plugin eventually needs to execute in plugin deactivation phase.
 *
 * ## Plugin loaded
 *
 * The method `loaded` of your `WPXTestPlugin` class is invoked every time your plugin is loaded. Loading is not
 * activation: every single time this plugin is loaded from WordPress environment, this method will be invoked.
 *
 * The basic code of this method is not directly included in your main class. Nevertheless, it is in
 * `WPXPlugin` class, so you can override it. If you need to execute some tasks every time your plugin is
 * loaded, create this method in your main class:
 *
 *     function loaded() {
 *     }
 *
 * and then put your own specific code in it.
 *
 * ## Plugin configuration
 *
 * The method `configuration` of your `WPXTestPlugin` class is invoked every time your plugin is loaded. Loading is not
 * activation: every single time this plugin is loaded from WordPress environment, this method will be invoked.
 *
 * Here you can put all stuffs about the configuration of your plugin; it is a commodity: you can perform the same task
 * in another way. Nevertheless, it can be really useful for you to use this hook, because this method is executed
 **AFTER** the plugin has been fully loaded from WordPress environment.
 *
 * The basic code of this method prepared for you through the Product Generator of wpXtreme Developer Center is this:
 *
 *     function configuration() {
 *         $this->config = WPXTestPluginConfig::config();
 *     }
 *
 * The instance of `WPXTestPluginConfig` is used to load and store the plugin settings on WordPress DB, according to
 * wpXtreme framework specs. But you can safely use your own code. In any case, you can insert here the code your plugin
 * eventually needs to execute in configuration phase.
 *
 * ## Commodity
 *
 * Your main class has also some commodity methods, useful to group together some similar tasks.
 *
 * In the method `defines`, you can insert the definition of all *PHP* `define` used by your class.
 *
 * The basic code of this method prepared for you through the Product Generator of wpXtreme Developer Center is this:
 *
 *     public function defines() {
 *         include_once( 'defines.php' );
 *     }
 *
 * You can write your own *PHP* `define` directly in this method, or you can also put your `define` in file
 * `defines.php`, stored in your plugin root directory, and included by this method.
 *
 * In the method `includes`, you can include all *PHP* files used by your class, through *PHP* directives `include`,
 * `require`, `require_once`, ecc.
 *
 * The basic code of this method prepared for you through the Product Generator of wpXtreme Developer Center is this:
 *
 *     public static function includes() {
 *         // Includes all your class file here.
 *
 *         // Core
 *         require_once( 'classes/core/wpxtestplugin-config.php' );
 *     }
 *
 * The line:
 *
 *     require_once( 'classes/core/wpxtestplugin-config.php' );
 *
 * is necessary for your plugin configuration core.
 *
 * Both `includes()` and `defines()` methods are invoked in the `WPXTestPlugin` constructor.
 *
 *
 *
 * @section     section_5_1 Writing code in your plugin specifically related to WordPress frontend
 *
 * Let's always assume that you created a new wpXtreme plugin for WordPress, named `Test Plugin`, through the Product
 * Generator of wpXtreme Developer Center. In the root directory of your plugin, you have a file named `wpx-testplugin.php`.
 * Inside this file, you have the declaration of a class named `WPXTestPlugin`, that extends `WPXPlugin` class.
 *
 * The method `theme` of your `WPXTestPlugin` class is called every time your plugin is loaded, after the invocation of
 * methods `loaded` and `configuration`; but this calling happens *if, and only if, the web request is related to the
 * front-end side of WordPress*: that is to say, not related in any way to the admin side of WordPress. Loading is not
 * activation: every single time this plugin is loaded from WordPress environment, this method will be invoked.
 *
 * The basic code of this method prepared for you through the Product Generator of wpXtreme Developer Center is this:
 *
 *     function theme() {
 *          /** To override. *\/
 *     }
 *
 * In this method, you can insert all code your plugin needs to execute in the front-end area of your WordPress
 * environment. For example, you can insert here the declaration of a specific class that handles all stuffs about
 * front-end area. Or you can directly add here some specific hooks related to front-end WordPress filters, like
 * `the_title` or `the_content`.
 *
 * For example, the WPXtreme WordPress plugin overwrites this method in this way:
 *
 *     function theme() {
 *        require_once( 'classes/frontend/wpxtreme-frontend.php' );
 *        $frontend = new WPXtremeFrontend( $this );
 *     }
 *
 * All code of this plugin related to the WordPress frontend area is hence encapsulated inside the `WPXtremeFrontend`
 * object, thus giving a plugin more readability and flow comprehension.
 *
 *
 * @section     section_6_1 Writing code in your plugin specifically related to WordPress admin area
 *
 * Let's always assume that you created a new wpXtreme plugin for WordPress, named `Test Plugin`, through the Product
 * Generator of wpXtreme Developer Center. In the root directory of your plugin, you have a file named
 * `wpx-testplugin.php`. Inside this file, you have the declaration of a class named `WPXTestPlugin`, that extends
 * `WPXPlugin` class.
 *
 * The method `admin` of your `WPXTestPlugin` class is called every time your plugin is loaded, after the invocation of
 * methods `loaded` and `configuration`; but this calling happens *if, and only if, the web request is related to the
 * admin side of WordPress*. Loading is not activation: every single time this plugin is loaded from WordPress
 * environment, this method will be invoked.
 *
 * The basic code of this method prepared for you through the Product Generator of wpXtreme Developer Center is this:
 *
 *     function admin() {
 *         require_once( 'classes/admin/wpxtestplugin-admin.php' );
 *         $admin = new WPXTestPluginAdmin( $this );
 *         /**
 *          * Add your code from now on
 *          * /
 *     }
 *
 *
 * In this method, you can insert all code your plugin needs to execute in the admin area of your WordPress environment.
 * For example, you can insert here the declaration of a specific class that handles all stuffs about admin area.
 * Or you can directly add here some specific hooks related to administration WordPress filters, like `menu_order` or
 * `admin_head`.
 *
 * The Product Generator of wpXtreme Developer Center adds automatically for you the declaration and the definition of
 * the `WPXTestPluginAdmin` object. This object handles for you some common tasks related to the admin area of
 * WordPress; for example, it creates automatically for you a sample menu item in the dashboard menu with the name of
 * your plugin, your plugin logo, and two related menu items, already hooked to specific methods of the object instance.
 *
 * You can choose to write directly in this method your code specifically related to the admin side of WordPress; or
 * you can choose to extend the functionality of `WPXTestPluginAdmin` object. In both cases, your plugin will gain more
 * readability and flow comprehension.
 *
 *
 *
 * @section     section_7_1 Introducing WPDK: a PHP framework for developing WordPress plugins
 *
 * Let's always assume that you created a new wpXtreme plugin for WordPress, named `Test Plugin`, through the Product
 * Generator of wpXtreme Developer Center. In the root directory of your plugin, you have a file named
 * `wpx-testplugin.php`. Inside this file, you have the declaration of a class named `WPXTestPlugin`, that extends
 * `WPXPlugin` class.
 *
 * The method `admin` of your `WPXTestPlugin` class is called every time your plugin is loaded, after the invocation
 * of methods `loaded` and `configuration`; but this calling happens *if, and only if, the web request is related to the
 * admin side of WordPress*. Loading is not activation: every single time this plugin is loaded from WordPress
 * environment, this method will be invoked.
 *
 * The basic code of this method prepared for you through the Product Generator of wpXtreme Developer Center is this:
 *
 *     function admin() {
 *         require_once( 'classes/admin/wpxtestplugin-admin.php' );
 *         $admin = new WPXTestPluginAdmin( $this );
 *         /**
 *          * Add your code from now on
 *          * /
 *     }
 *
 * In this method, you can insert all code your plugin needs to execute in the admin area of your WordPress environment.
 * For example, you can insert here the declaration of a specific class that handles all stuffs about admin area.
 * Or you can directly add here some specific hooks related to administration WordPress filters, like `menu_order` or
 * `admin_head`.
 *
 * The Product Generator of wpXtreme Developer Center adds automatically for you the declaration and the definition
 * of the `WPXTestPluginAdmin` object. This object handles for you some common tasks related to the admin area of
 * WordPress; for example, it creates automatically for you a sample menu item in the dashboard menu with the name of
 * your plugin, your plugin logo, and two related menu items, already hooked to specific methods of the object instance.
 *
 * You can choose to write directly in this method your code specifically related to the admin side of WordPress; or
 * you can choose to extend the functionality of `WPXTestPluginAdmin` object. In both cases, your plugin will gain more
 * readability and flow comprehension.
 *
 */