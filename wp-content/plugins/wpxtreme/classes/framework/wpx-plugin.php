<?php
/**
 * This is the main wpXtreme plugin class to extends
 *
 * @class           WPXPlugin
 * @author          =undo= <info@wpxtre.me>
 * @copyright       Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date            2013-06-05
 * @version         1.0.1
 *
 */
class WPXPlugin extends WPDKWordPressPlugin {

  /**
   * A WPXUpdate pointer for WPX Store (alternative API).
   *
   * @brief Alternative API URL
   *
   * @var WPXUpdate $update
   */
  private $update;

  /**
   * Create an instance of WPXPlugin class
   *
   * @brief Construct
   *
   * @param string $file The main file of this plugin. Usually __FILE__ (main.php)
   *
   * @return WPXPlugin
   */
  public function __construct( $file )
  {
    parent::__construct( $file );
  }

  /**
   * Method to override called by `plugins_loaded()`
   *
   * @brief Alias of plugins_loaded()
   */
  public function loaded()
  {
    /* My own alternative API check uri. */
    $this->update = new WPXUpdate( $this->file );
  }

  /* To override */
  public static function boot()
  {
    die( __METHOD__ . ' must be override in your subclass' );
  }

}