<?php
/// @cond private

/*
 * [DRAFT]
 *
 * THIS DOCUMENTATION IS A DRAFT. YOU CAN READ IT AND MAKE SOME EXPERIMENT BUT DO NOT USE ANY CLASS BELOW IN YOUR
 * PROJECT. ALL CLASSES AND RELATIVE METHODS BELOW ARE SUBJECT TO CHANGE.
 *
 */

/**
 * Manage update from wpXtreme plugin repository: WPX Store
 *
 * ## Overview
 * This class is instance for each wpx plugin. Each plugin ask to wpx store server if should update. More each plugin
 * draw thw own row in plugin wordpress list table view.
 *
 * @class              WPXUpdate
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-06-12
 * @version            1.0.0
 *
 */

class WPXUpdate {

  /**
   * This is the site transient used to store the wpx plugin to update
   */
  const UPDATE_PLUGINS = 'wpx_update_plugins';

  /**
   * The plugin basename from main plugin filename, ie. wpx-cleanfix/wpx-cleanfix.php
   *
   * @brief The plugin basename
   *
   * @var string $_plugin_slug
   */
  private $_plugin_slug;

  /**
   * Instance of base class WPXAPI
   *
   * @brief API
   *
   * @var WPXAPI $api
   */
  private $api;

  /**
   * Create an instance of WPXUpdate class
   *
   * @brief Construct
   *
   * @param string $file Plugin filename
   *
   * @return WPXUpdate
   */
  public function __construct( $file )
  {

    $this->_plugin_slug = plugin_basename( $file );

    /* Instance of own API. */
    $this->api = new WPXtremeAPI();

    /* Alternate checking repository */
    add_action( 'site_transient_update_plugins', array( $this, 'site_transient_update_plugins' ) );
    add_filter( 'pre_set_site_transient_update_plugins', array( $this, 'pre_set_site_transient_update_plugins' ) );

  }

  /**
   * Return the number of wpx plugin to update.
   *
   * @return int
   */
  public static function countUpdatingPlugins()
  {
    $count   = 0;
    $updates = get_site_transient( self::UPDATE_PLUGINS );

    if ( !empty( $updates ) ) {
      $count = count( $updates );
    }
    return $count;
  }

  // -----------------------------------------------------------------------------------------------------------------
  // Own checking repository
  // -----------------------------------------------------------------------------------------------------------------

  /**
   * Questo hook filter non fa parte propriamente dell'engine di aggiornamento dei plugin. I filtri di questo tipo
   * fanno parte delle transient. Questo in particolare è costruito dal filtro 'pre_set_transient_' . $transient e
   * chiamato dalla set_transient(). In definitiva serve per alterare la lista dei plugin da aggiornare che WordPress
   * memorizza nelle option ( tramite appunto transient ) una volta al giorno.
   *
   * @note  Questa viene utilizzata singolarmente da ogni plugin installato. Vedi infatti il parametro 'plugin_name'
   *        negli $args che viene valorizzato con $this->_plugin_slug. Ricordo che la classe WPXUpdate viene
   *        utilizzata come istanza in ogni bootstrap dei nostri plugin, che altrimenti non verrebbero mai aggiornati
   *        dallo store.
   *
   * @brief WordPress filter when fetch the plugin list
   *
   * @uses  WPXtremeAPI::updatePlugins()
   *
   * @param object $transient Elenco dei plugin da aggiornare:
   *
   *     object(stdClass)#272 (3) {
   *       ["last_checked"]=>     int(1342125406)
   *       ["checked"]=>          array(7) {
   *         ["akismet/akismet.php"]=>        string(5) "2.5.6"
   *         ["members/members.php"]=>        string(3) "0.2"
   *         ["wpx-cleanfix/main.php"]=>      string(3) "1.0"
   *         ["wpx-sample/main.php"]=>        string(3) "1.0"
   *         ["wpx-smartshop/main.php"]=>     string(3) "1.0"
   *         ["wpxtreme/main.php"]=>          string(3) "1.0"
   *         ["wpxtreme-server/main.php"]=>   string(3) "1.0"
   *       }
   *       ["response"]=> array(0) { }
   *     }
   *
   *
   * @return object
   *
   */
  public function pre_set_site_transient_update_plugins( $transient )
  {
    /* Only backend administration */
    if ( !is_admin() ) {
      return $transient;
    }

    /* Check if the transient contains the 'checked' information If no, just return its value without hacking it */
    if ( empty( $transient->checked ) ) {
      return $transient;
    }

    $updates = get_site_transient( self::UPDATE_PLUGINS );
    if ( empty( $updates ) ) {
      $updates = array();
    }

    /* The transient contains the 'checked' information Now append to it information form your own API */
    $args = array(
      'action'      => 'update-check',
      'plugin_name' => $this->_plugin_slug,
      'version'     => $transient->checked[$this->_plugin_slug]
    );

    /* Ask to the wpXtreme server if this plugin will be update. */
    $response = $this->api->updatePlugins( $args );

    /* If response is false, don't alter the transient */
    if ( false !== $response ) {
      $updates[$this->_plugin_slug] = $response;
    }
    elseif( isset( $updates[$this->_plugin_slug] ) ) {
      unset( $updates[$this->_plugin_slug] );
    }
    set_site_transient( self::UPDATE_PLUGINS, $updates );

    return $transient;
  }

  // TODO
  public function site_transient_update_plugins( $value )
  {
    global $plugins;
    $GLOBALS[self::UPDATE_PLUGINS] = $updates = get_site_transient( self::UPDATE_PLUGINS );

    if ( isset( $plugins['all'][$this->_plugin_slug] ) && isset( $updates[$this->_plugin_slug] ) ) {
      $plugins['all'][$this->_plugin_slug]['update'] = true;
      $plugins['upgrade'][$this->_plugin_slug]       = $plugins['all'][$this->_plugin_slug];
      add_action( 'after_plugin_row_' . $this->_plugin_slug, array( $this, 'wp_plugin_update_row' ), 10, 2 );
    }

    return $value;
  }

  // TODO
  public function wp_plugin_update_row( $file, $plugin_data )
  {
    $plugins_allowedtags = array(
      'a'       => array(
        'href'  => array(),
        'title' => array()
      ),
      'abbr'    => array( 'title' => array() ),
      'acronym' => array( 'title' => array() ),
      'code'    => array(),
      'em'      => array(),
      'strong'  => array()
    );

    $plugin_name = wp_kses( $plugin_data['Name'], $plugins_allowedtags );

    if ( isset( $GLOBALS[self::UPDATE_PLUGINS] ) ) {
      if ( !empty( $GLOBALS[self::UPDATE_PLUGINS] ) ) {
        $new_version = $GLOBALS[self::UPDATE_PLUGINS][$file]->new_version;
      }
    }

    $wp_list_table = _get_list_table( 'WP_Plugins_List_Table' );

    if ( is_network_admin() || !is_multisite() ) : ?>
      <tr class="plugin-update-tr">
        <td style="padding:8px 8px 0"
            colspan="<?php echo $wp_list_table->get_column_count() ?>"
            class="plugin-update colspanchange">
          <?php
          $url = WPDKMenu::url( 'WPXtremeStoreViewController' );
          $button = sprintf( '<a href="%s" class="button button-primary button-large alignright">%s <strong>%s</strong></a>', $url, __( 'Click HERE in order to update', WPXTREME_TEXTDOMAIN ), $plugin_name );
          $message = sprintf( __( '<p style="color:#3A87AD;margin:6px 0" class="alignleft">Wow! A new version <strong>%s</strong> of <strong>%s</strong> is available.</p>%s', WPXTREME_TEXTDOMAIN ), $new_version, $plugin_name, $button );
          $alert = new WPDKTwitterBootstrapAlert( 'update-plugin', $message, WPDKTwitterBootstrapAlertType::INFORMATION );
          $alert->classes = 'clearfix';
          $alert->block = true;
          $alert->dismissButton = false;
          $alert->display();
          ?>
        </td>
      </tr>
    <?php endif;
  }

}

/* You have to include. */
if ( !class_exists( 'Plugin_Upgrader' ) ) {
  if ( isset( $_GET['action'] ) && 'do-core-upgrade' == $_GET['action'] ) {
    return;
  }
  require_once( ABSPATH . 'wp-admin/includes/class-wp-upgrader.php' );
}

/**
 * Subclassing WordPress Plugin_Upgrader class
 *
 * @class              WPXPluginUpgrader
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2012-11-28
 * @version            0.8.1
 *
 */
class WPXPluginUpgrader extends Plugin_Upgrader {

  /**
   * @brief Generic strings
   *
   * Override for replace generic strings
   *
   */
  public function generic_strings()
  {
    $this->strings['bad_request']       = __( '<i class="wpdk-icon-warning"></i>Invalid Data provided.' );
    $this->strings['fs_unavailable']    = __( '<i class="wpdk-icon-warning"></i>Could not access filesystem.' );
    $this->strings['fs_error']          = __( '<i class="wpdk-icon-warning"></i>Filesystem error.' );
    $this->strings['fs_no_root_dir']    = __( '<i class="wpdk-icon-warning"></i>Unable to locate WordPress Root directory.' );
    $this->strings['fs_no_content_dir'] = __( '<i class="wpdk-icon-warning"></i>Unable to locate WordPress Content directory (wp-content).' );
    $this->strings['fs_no_plugins_dir'] = __( '<i class="wpdk-icon-warning"></i>Unable to locate WordPress Plugin directory.' );
    $this->strings['fs_no_themes_dir']  = __( '<i class="wpdk-icon-warning"></i>Unable to locate WordPress Theme directory.' );
    /* translators: %s: directory name */
    $this->strings['fs_no_folder']         = __( '<i class="wpdk-icon-warning"></i>Unable to locate needed folder (%s).' );
    $this->strings['download_failed']      = __( '<i class="wpdk-icon-warning"></i>Download failed.' );
    $this->strings['installing_package']   = __( '<i class="wpdk-icon-success"></i>Installing the latest version&#8230;' );
    $this->strings['folder_exists']        = __( '<i class="wpdk-icon-warning"></i>Destination folder already exists.' );
    $this->strings['mkdir_failed']         = __( '<i class="wpdk-icon-warning"></i>Could not create directory.' );
    $this->strings['incompatible_archive'] = __( '<i class="wpdk-icon-warning"></i>The package could not be installed.' );
    $this->strings['maintenance_start']    = __( '<i class="wpdk-icon-success"></i>Enabling Maintenance mode&#8230;' );
    $this->strings['maintenance_end']      = __( '<i class="wpdk-icon-success"></i>Disabling Maintenance mode&#8230;' );
  }

  /**
   * Override for replace install strings
   *
   * @brief Install strings
   */
  public function install_strings()
  {
    $this->strings['no_package']          = __( '<i class="wpdk-icon-warning"></i>Install package not available.', WPXTREME_TEXTDOMAIN );
    $this->strings['downloading_package'] = __( '<i class="wpdk-icon-success"></i>Downloading install package&#8230;', WPXTREME_TEXTDOMAIN );
    $this->strings['unpack_package']      = __( '<i class="wpdk-icon-success"></i>Unpacking the package&#8230;', WPXTREME_TEXTDOMAIN );
    $this->strings['installing_package']  = __( '<i class="wpdk-icon-success"></i>Installing the wpx plugin&#8230;', WPXTREME_TEXTDOMAIN );
    $this->strings['process_failed']      = __( '<i class="wpdk-icon-warning"></i>wpx Plugin install failed.', WPXTREME_TEXTDOMAIN );
    $this->strings['process_success']     = __( '<i class="wpdk-icon-success"></i>wpx Plugin installed successfully.', WPXTREME_TEXTDOMAIN );
  }

  /**
   * A ben guardare questa potrebbe essere inutile in quanto riproduce quello che già fa il metodo upgrade()
   * della classe ereditata, unica differenza è il parametro di ingresso che dev'essere un oggetto e non l'indirizzo
   * del package di scaricamento
   *
   * The action/token must be as:
   *
   *     object(stdClass)#321 (5) {
   *      ["action"]        => string(7) "install"
   *      ["plugin_id"]     => string(3) "996"
   *      ["plugin"]        => string(29) "wpx-bannerize_000036/main.php"
   *      ["download_link"] => string(44) "http://beta.wpxtre.me/api/v1/plugin_download"
   *      ["token"]         => string(32) "5c960ad0fb1c2d71bbc102d7c02ff4eb"
   *    }
   *
   * @brief Extract and update
   *
   * @todo For multiple upgrade see `bulk_upgrade` method
   *
   * @param object $action An action/token with plugin information
   *
   * @return bool
   */
  public function upgrade( $action )
  {
    wp_cache_flush();

    $this->init();
    $this->upgrade_strings();

    add_filter( 'upgrader_pre_install', array( $this, 'deactivate_plugin_before_upgrade' ), 10, 2 );
    add_filter( 'upgrader_clear_destination', array( $this, 'delete_old_plugin' ), 10, 4 );

    $args = array(
      'package'           => $action->download_link,
      'destination'       => WP_PLUGIN_DIR,
      'clear_destination' => true,
      'clear_working'     => true,
      'hook_extra'        => array( 'plugin' => $action->plugin )
    );

    $this->run( $args );

    // Cleanup our hooks, incase something else does a upgrade on this connection.
    remove_filter( 'upgrader_pre_install', array( $this, 'deactivate_plugin_before_upgrade' ) );
    remove_filter( 'upgrader_clear_destination', array( $this, 'delete_old_plugin' ) );

    if ( !$this->result || is_wp_error( $this->result ) ) {
      return $this->result;
    }

    delete_site_transient( WPXUpdate::UPDATE_PLUGINS );
    delete_site_transient( 'update_plugins' );
    wp_cache_delete( 'plugins', 'plugins' );

    /* Flush plugins cache so we can make sure that the installed plugins list is always up to date */
    wp_cache_flush();

    return false;
  }

  /**
   * Override for replace updagrade string
   *
   * @brief Updagrade string
   */
  public function upgrade_strings()
  {
    $this->strings['up_to_date']          = __( '<i class="wpdk-icon-warning"></i>The plugin is at the latest version.', WPXTREME_TEXTDOMAIN );
    $this->strings['no_package']          = __( '<i class="wpdk-icon-warning"></i>Update package not available.', WPXTREME_TEXTDOMAIN );
    $this->strings['downloading_package'] = __( '<i class="wpdk-icon-success"></i>Downloading update from WPX Store&#8230;', WPXTREME_TEXTDOMAIN );
    $this->strings['installing_package']  = __( '<i class="wpdk-icon-success"></i>Installing the latest version&#8230;', WPXTREME_TEXTDOMAIN );
    $this->strings['unpack_package']      = __( '<i class="wpdk-icon-success"></i>Unpacking the update&#8230;', WPXTREME_TEXTDOMAIN );
    $this->strings['deactivate_plugin']   = __( '<i class="wpdk-icon-success"></i>Deactivating the plugin&#8230;', WPXTREME_TEXTDOMAIN );
    $this->strings['remove_old']          = __( '<i class="wpdk-icon-success"></i>Removing the old version of the wpx plugin&#8230;', WPXTREME_TEXTDOMAIN );
    $this->strings['remove_old_failed']   = __( '<i class="wpdk-icon-warning"></i>Could not remove the old wpx plugin.', WPXTREME_TEXTDOMAIN );
    $this->strings['process_failed']      = __( '<i class="wpdk-icon-warning"></i>wpx Plugin update failed.', WPXTREME_TEXTDOMAIN );
    $this->strings['process_success']     = __( '<i class="wpdk-icon-success"></i>wpx Plugin updated successfully.', WPXTREME_TEXTDOMAIN );
  }
}

/**
 * Subclassing WordPress Plugin_Installer_Skin class
 *
 * ### Debug
 * http://www.undolog.com/wp-admin/admin.php?page=wpxm_menu_wpxtreme-submenu-2&t=wpxm-51a8ace201553c81e728d9d4c2f636f067f89cc14862c&action=install-plugin&plugin=wpx-followgram_000031/main.php
 *
 *     $this->upgrader->skin->options
 *
 *     array(7) {
 *       ["url"]     => string(154) "http://dev.wpxtre.me/wp-admin/admin.php?page=wpxm_menu_wpx_store&action=upgrade-plugin&plugin=wpx-sample/wpx-__WPXGENESI_SHORT_PLUGIN_NAME_LOWERCASE__.php"
 *       ["nonce"]   => string(0) ""
 *       ["title"]   => string(27) "Update Plugin from wpxStore"
 *       ["context"] => bool(false)
 *       ["type"]    => string(3) "web"
 *       ["plugin"]  => string(60) "wpx-sample/wpx-__WPXGENESI_SHORT_PLUGIN_NAME_LOWERCASE__.php"
 *       ["api"]     => array(0) {
 *       }
 *     }
 *
 * @class              WPXPluginUpgraderSkin
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2012-11-28
 * @version            0.8.1
 */
class WPXPluginUpgraderSkin extends Plugin_Installer_Skin {

  /**
   * @brief Update/Install after footer
   */
  public function after()
  {
    wp_ob_end_flush_all();
    flush();
  }

  /**
   * @brief Close the footer
   */
  public function footer()
  {
    ?>
    </fieldset>
    <?php

    /* Get Plugin info. */
    $plugin_file = $this->upgrader->plugin_info();

    /* Array delle azioni alla fine della schermata */
    $install_actions = array();

    /* Back to WPX Store button. */
    $url = remove_query_arg( array( 'action', 'plugin' ) );
    $title = __( 'Back to WPX store', WPXTREME_TEXTDOMAIN );
    $label = __( 'Back to WPX Store', WPXTREME_TEXTDOMAIN );
    $install_actions['wpx_store'] = sprintf( '<a class="button button-secondary" href="%s" title="%s">%s</a>', $url, $title, $label );

    $from = isset( $_GET['from'] ) ? stripslashes( $_GET['from'] ) : 'plugins';

    /* @todo Da testare e capire se nel contesto wpx Store abbia senso */
    if ( 'import' == $from ) {
      $url   = wp_nonce_url( 'plugins.php?action=activate&amp;from=import&amp;plugin=' . $plugin_file,
        'activate-plugin_' . $plugin_file );
      $title = __( 'Activate this plugin', WPXTREME_TEXTDOMAIN );
      $label = __( 'Activate Plugin &amp; Run Importer', WPXTREME_TEXTDOMAIN );

      $install_actions['activate_plugin'] = sprintf( '<a class="button button-primary alignright" href="%s" title="%s">%s</a>', $url, $title, $label );
    }
    /* This is the default. */
    else {

      // TODO to complete
      //$url = add_query_arg( array( 'action' => 'activate', '_wpnonce' => wp_create_nonce( 'activate-plugin_' . $plugin_file ) ) );

      $url   = wp_nonce_url( 'plugins.php?action=activate&amp;plugin=' . $plugin_file,
        'activate-plugin_' . $plugin_file );
      $title = __( 'Activate this plugin', WPXTREME_TEXTDOMAIN );
      $label = __( 'Activate Plugin', WPXTREME_TEXTDOMAIN );

      $install_actions['activate_plugin'] = sprintf( '<a class="button button-primary alignright" href="%s" title="%s">%s</a>', $url, $title, $label );
    }

    /* Network activate. */
    if ( is_multisite() && current_user_can( 'manage_network_plugins' ) ) {
      $url                                 = wp_nonce_url( 'plugins.php?action=activate&amp;networkwide=1&amp;plugin=' .
      $plugin_file, 'activate-plugin_' . $plugin_file );
      $title                               = __( 'Activate this plugin for all sites in this network', WPXTREME_TEXTDOMAIN );
      $label                               = __( 'Network Activate', WPXTREME_TEXTDOMAIN );
      $install_actions['network_activate'] = sprintf( '<a class="button button-primary alignright" href="%s" title="%s">%s</a>', $url, $title, $label );

      unset( $install_actions['activate_plugin'] );
    }

    /* If any error occour not active. */
    if ( !$this->result || is_wp_error( $this->result ) ) {
      unset( $install_actions['activate_plugin'] );
      unset( $install_actions['network_activate'] );
    }

    /* Check if the plugin is already active. */
    if ( is_plugin_active( $plugin_file ) ) {
      unset( $install_actions['activate_plugin'] );
    }

    $install_actions = apply_filters( 'install_plugin_complete_actions', $install_actions, $this->api, $plugin_file );
    if ( !empty( $install_actions ) ) {
      $this->feedback( sprintf( '<span class="clearfix">%s</span>', implode( ' ', $install_actions ) ) );
    }
    ?>

    </div><!-- wpdk-install-report-feedback -->
    <?php

    wp_ob_end_flush_all();
    flush();

  }

  /**
   * @brief Open the header
   */
  public function header()
  {
    if ( $this->done_header ) {
      return;
    }
    $this->done_header = true;

    $options = $this->upgrader->skin->options;

    ?>
    <div class="wpdk-install-report-feedback">
    <fieldset class="wpdk-form-fieldset">
    <legend><?php echo $options['title'] ?></legend>
    <?php

    wp_ob_end_flush_all();
    flush();

  }
}

/// @endcond