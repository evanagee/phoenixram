<?php
/**
 * This view controller is used to display the plugin card from standard `About` menu item
 *
 * ## Overview
 *
 * Extends your abut view controller to display your plugin card from WPX Store
 *
 * @class           WPXAboutViewController
 * @author          =undo= <info@wpxtre.me>
 * @copyright       Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date            2013-06-08
 * @version         1.1.0
 *
 */
class WPXAboutViewController extends WPDKViewController {

  /**
   * Your plugin instance
   *
   * @brief Your plugin instance
   *
   * @var WPXPlugin $plugin
   */
  private $plugin;

  /**
   * Create an instance of WPXAboutViewController class
   *
   * @brief Construct
   *
   * @param WPXPlugin $your_plugin Your plugin instance
   *
   * @return WPXAboutViewController
   */
  public function __construct( WPXPlugin $your_plugin )
  {

    $this->plugin = $your_plugin;

    $id    = sanitize_key( $this->plugin->name );
    $title = sprintf( '%s ver. %s ', $this->plugin->name, $this->plugin->version );
    parent::__construct( $id, $title );

    $api          = new WPXtremeAPI();
    $card_content = $api->wpxstore_plugin_card( $this->plugin->pluginBasename );

    /* Useful filter. */
    $card_content = apply_filters( 'wpx_about_view_controller_plugin_card', $card_content );

    if ( empty( $card_content ) ) {
      $message              = sprintf( '<h3>%s</h3><p>%s</p>', __( 'Your plugin is not into the WPX Store!', WPXTREME_TEXTDOMAIN ), __( 'If you are a developer, your plugin Base name is not yet registered into the WPX Store. Please rename your folder or contact the support.', WPXTREME_TEXTDOMAIN ) );
      $alert                = new WPDKTwitterBootstrapAlert( 'not-into-store', $message, WPDKTwitterBootstrapAlertType::ALERT );
      $alert->dismissButton = false;
      $card_content         = $alert->html();

      $card_view = WPDKView::initWithContent( 'wpxs-plugin-store', 'wpdk-border-container', $card_content );
    }
    else {
      $card_view = WPDKView::initWithContent( 'wpxs-plugin-store', '', $card_content );
    }
    $this->view->addSubview( $card_view );

  }

  /**
   * This static method is called when the head of this view controller is loaded by WordPress.
   *
   * @brief Head
   */
  public static function didHeadLoad() { }

}