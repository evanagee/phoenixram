<?php
/// @cond private

/*
 * [DRAFT]
 *
 * THIS DOCUMENTATION IS A DRAFT. YOU CAN READ IT AND MAKE SOME EXPERIMENT BUT DO NOT USE ANY CLASS BELOW IN YOUR
 * PROJECT. ALL CLASSES AND RELATIVE METHODS BELOW ARE SUBJECT TO CHANGE.
 *
 */

/**
 * Register the wpXtreme shortcodes for WordPress
 *
 * ## Overview
 *
 * This class is not use yet.
 *
 * @class              WPXtremeShortcode
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-16
 * @version            1.0.0
 */
class WPXtremeShortcode extends WPDKShortcode {

  /**
   * Create or return a singleton instance of WPXtremeShortcode
   *
   * @brief Get singleton instance
   *
   * @return WPXtremeShortcode
   */
  public static function getInstance() {
    static $instance = null;
    if ( is_null( $instance ) ) {
      $instance = new WPXtremeShortcode();
    }
    return $instance;
  }

  /**
   * Alias of getInstance();
   *
   * @brief Init the shortcode register
   *
   * @return WPXtremeShortcode
   */
  public static function init() {
    return self::getInstance();
  }

  /**
   * Return a Key value pairs array with key as shortcode name and value TRUE/FALSE for turn on/off the shortcode.
   *
   * @brief List of allowed shorcode
   *
   * @return array Shortcode array
   */
  protected function shortcodes() {
    $shortcodes = array( );
    return $shortcodes;
  }

  // -----------------------------------------------------------------------------------------------------------------
  // Shortcode methods
  // -----------------------------------------------------------------------------------------------------------------

} // class WPXtremeShortcode

/// @endcond