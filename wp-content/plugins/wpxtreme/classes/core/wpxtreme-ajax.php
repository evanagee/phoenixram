<?php

if ( wpdk_is_ajax() ) {


  /**
   * Ajax gateway engine
   *
   * @class              WPXtremeAjax
   * @author             =undo= <info@wpxtre.me>
   * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
   * @date               2013-02-01
   * @version            0.9.6
   *
   */
  class WPXtremeAjax extends WPDKAjax {

    /**
     * Instance of WPXtremeAPI class
     *
     * @brief API
     *
     * @var WPXtremeAPI $api
     */
    private $api;

    /**
     * Create or return a singleton instance of WPXtremeAjax
     *
     * @brief Create or return a singleton instance of WPXtremeAjax
     *
     * @return WPXtremeAjax
     */
    public static function getInstance()
    {
      static $instance = null;
      if ( is_null( $instance ) ) {
        $instance      = new WPXtremeAjax();
        $instance->api = new WPXtremeAPI();
      }
      return $instance;
    }

    /**
     * Alias of getInstance();
     *
     * @brief Init the ajax register
     *
     * @return WPXtremeAjax
     */
    public static function init()
    {
      return self::getInstance();
    }

    /**
     * Return the array with the list of ajax allowed methods
     *
     * @breief Allow ajax action
     *
     * @return array
     */
    protected function actions()
    {
      $actionsMethods = array(
        'wpxtreme_action_user_set_status'            => false,
        'wpxtreme_action_post_set_publish'           => false,

        'wpxtreme_action_slug_post_email'            => true,
        'wpxtreme_action_slug_post_page'             => true,
        'wpxtreme_action_sorting_post_page'          => true,

        'wpxtreme_action_get_remote_status'          => false,

        'wpxtreme_action_set_issue_report'           => false,
        'wpxtreme_action_send_issue_report'          => false,

        'wpxtreme_action_update_appearance_general'  => false,
        'wpxtreme_action_update_appearance_posts'    => false,

        'wpxtreme_action_control_panel_plugin_state' => false,

      );
      return $actionsMethods;
    }


    // -------------------------------------------------------------------------------------------------------------
    // Actions methods
    // -------------------------------------------------------------------------------------------------------------

    public function wpxtreme_action_control_panel_plugin_state()
    {
      if ( isset( $_POST['state'] ) && !empty( $_POST['state'] ) ) {
        update_option( 'wpxtreme_action_control_panel_plugin_state', $_POST['state'] );
      }
      die();
    }

    /**
     * Used to set the status of an user when the swipe is slided
     *
     * @brief Set user status
     *
     * @return string jSON
     */
    public function wpxtreme_action_user_set_status()
    {
      $id_user = absint( $_POST['id_user'] );
      $status  = esc_attr( $_POST['status'] );
      update_user_meta( $id_user, WPDKUserMeta::STATUS, $status );
      $result = array();
      echo json_encode( $result );
      die();
    }

    /**
     * Used to set the status of a post when the swipe is slided
     *
     * @brief Set post status
     *
     * @return string jSON
     */
    public function wpxtreme_action_post_set_publish()
    {
      $id_post     = absint( $_POST['id_post'] );
      $post_status = esc_attr( $_POST['status'] );

      $post = array(
        'ID'          => $id_post,
        'post_status' => $post_status
      );

      wp_update_post( $post );

      $result = array();
      echo json_encode( $result );

      die();
    }

    // -------------------------------------------------------------------------------------------------------------
    // Plugin Store
    // -------------------------------------------------------------------------------------------------------------

    // -------------------------------------------------------------------------------------------------------------
    // Admin backend
    // -------------------------------------------------------------------------------------------------------------

    /**
     * Get post type Mail by a term
     *
     * @brief Get post type Mail by a term
     *
     * @return string jSON
     */
    public function wpxtreme_action_slug_post_email()
    {
      global $wpdb;

      $term        = $_POST['term'];
      $table_posts = $wpdb->posts;
      $post_type   = WPXtremeMailCustomPostType::ID;

      $where_post_name = '';
      if ( !empty( $term ) ) {
        $where_post_name = sprintf( ' AND ( post_name LIKE "%%%s%%" OR post_title LIKE "%%%s%%" )', $term, $term );
      }

      $sql    = <<< SQL
SELECT post_name, post_title
FROM {$table_posts}
WHERE 1
{$where_post_name}
AND post_type = '{$post_type}'
AND post_status = 'publish'
ORDER BY post_title
SQL;
      $result = $wpdb->get_results( $sql );
      if ( !is_wp_error( $result ) ) {
        foreach ( $result as $post ) {
          $response[] = array(
            'value' => $post->post_name,
            'label' => apply_filters( 'the_title', $post->post_title ),
          );
        }
      }

      echo json_encode( $response );
      die();
    }

    /**
     * Get post type Page by a term
     *
     * @brief Get post type Page by a term
     *
     * @return string jSON
     */
    public function wpxtreme_action_slug_post_page()
    {
      global $wpdb;

      $term        = $_POST['term'];
      $table_posts = $wpdb->posts;
      $post_type   = 'page';

      $where_post_name = '';
      if ( !empty( $term ) ) {
        $where_post_name = sprintf( ' AND ( post_name LIKE "%%%s%%" OR post_title LIKE "%%%s%%" )', $term, $term );
      }

      $sql      = <<< SQL
SELECT post_name, post_title
FROM {$table_posts}
WHERE 1
{$where_post_name}
AND post_type = '{$post_type}'
AND post_status = 'publish'
ORDER BY post_title
SQL;
      $result   = $wpdb->get_results( $sql );
      $response = array();
      if ( !is_wp_error( $result ) ) {
        foreach ( $result as $post ) {
          $response[] = array(
            'value' => $post->post_name,
            'label' => apply_filters( 'the_title', $post->post_title ),
          );
        }
      }

      echo json_encode( $response );
      die();
    }

    /**
     * Updated `menu_order` field in post table.
     *
     * @brief    Sorting
     *
     * @internal param $_POST['sorted'] List sequence of sorted items
     * @internal param $_POST['paged'] Pagination value
     * @internal param $_POST['per_page'] Number of items per page
     *
     * @return string
     *
     */
    public function wpxtreme_action_sorting_post_page()
    {
      global $wpdb;

      $result = array();

      $sorted   = wp_parse_args( $_POST['sorted'] );
      $paged    = absint( esc_attr( $_POST['paged'] ) );
      $per_page = absint( esc_attr( $_POST['per_page'] ) );

      if ( is_array( $sorted['post'] ) ) {
        $offset = ( $paged - 1 ) * $per_page;
        foreach ( $sorted['post'] as $key => $value ) {
          $menu_order = $key + $offset;
          $sql        = sprintf( 'UPDATE %s SET menu_order = %s WHERE ID = %s', $wpdb->posts, $menu_order, absint( $value ) );
          $wpdb->query( $sql );
        }
      }

      echo json_encode( $result );
      die();
    }

    /**
     * Update the appearance general configuration for a single user
     *
     * @brief Update appearance general configuration
     * @since 1.0.0.b3
     *
     */
    public function wpxtreme_action_update_appearance_general()
    {

      $response = array();

      $data = $_POST['data'];
      if ( is_array( $data ) && !empty( $data ) ) {
        $user_id    = get_current_user_id();
        $appearance = get_user_meta( $user_id, WPXtremeConfigurationEnhancerAppearanceGeneral::KEY, true );
        foreach ( $data as $property => $value ) {
          $appearance->$property = $value;
        }
        update_user_meta( $user_id, WPXtremeConfigurationEnhancerAppearanceGeneral::KEY, $appearance );
      }
      else {
        $response['message'] = __( 'Software Failure! Guru Meditation: 0x8000C000', WPXTREME_TEXTDOMAIN );

      }

      echo json_encode( $response );
      die();
    }

    /**
     * Update the appearance posts configuration for a single user
     *
     * @brief Update appearance general configuration
     * @since 1.0.0.b4
     *
     */
    public function wpxtreme_action_update_appearance_posts()
    {

      $response = array();

      $data = $_POST['data'];
      if ( is_array( $data ) && !empty( $data ) ) {
        $user_id          = get_current_user_id();
        $appearance_posts = get_user_meta( $user_id, WPXtremeConfigurationEnhancerAppearancePosts::KEY, true );
        foreach ( $data as $property => $value ) {
          $appearance_posts->$property = $value;
        }
        update_user_meta( $user_id, WPXtremeConfigurationEnhancerAppearancePosts::KEY, $appearance_posts );
      }
      else {
        $response['message'] = __( 'Software Failure! Guru Meditation: 0x8000C001', WPXTREME_TEXTDOMAIN );

      }

      echo json_encode( $response );
      die();
    }


    /**
     * Set a new state mode for issue report
     *
     * @brief Issue Report state
     * @since 1.0.0.b3
     */
    public function wpxtreme_action_set_issue_report()
    {

      /* Prepare the response. */
      $response = array();

      if ( isset( $_POST['mode'] ) ) {
        $mode = absint( $_POST['mode'] );
        if ( WPXtremeConfigurationIssueReport::DISABLED == $mode ||
          WPXtremeConfigurationIssueReport::ENABLED == $mode || WPXtremeConfigurationIssueReport::READY_TO_SEND == $mode
        ) {
          WPXtremeConfiguration::init()->issueReport->issueReportMode = $mode;
          WPXtremeConfiguration::init()->update();

          //-----------------------------------------------------------------------
          // If I'm disabling issue report mode, delete previous PHP log file
          //-----------------------------------------------------------------------

          if ( WPXtremeConfigurationIssueReport::DISABLED == $mode ) {
            $cIssueReport = WPXtremeIssueReport::init();
            $cIssueReport->deleteLogFiles();
          }

          /* HTML markup for update modal and footer without reload entire document page. */
          $issue_reposrt_view  = new WPXtremeIssueReportView;
          $response['content'] = $issue_reposrt_view->modal();
          $response['footer']  = $issue_reposrt_view->footer();

        }
        else {
          $response['message'] = __( 'Error: wrong parameter', WPXTREME_TEXTDOMAIN );
        }
      }
      else {
        $response['message'] = __( 'Error: no parameter', WPXTREME_TEXTDOMAIN );
      }

      echo json_encode( $response );
      die();
    }

    /**
     * Send the issue report to Developer Center Endpoint
     *
     * @bried Send issue report
     * @since 1.0.0.b3
     */
    public function wpxtreme_action_send_issue_report()
    {
      /* Prepare the response. */
      $response = array();

      /* Some sanitize and check on POST params. */
      $name = stripslashes( $_POST['name'] );
      if ( !empty( $name ) ) {
        $email = sanitize_email( $_POST['email'] );
        if ( !empty( $email ) ) {
          $title = stripslashes( $_POST['title'] );
          if ( !empty( $title ) ) {

            /* Description is optional. */
            $description = stripslashes( substr( $_POST['description'], 0, WPXtremeIssueReport::MAX_LENGTH_DESCRIPTION ) );

            $report = stripslashes( $_POST['report'] );
            if ( !empty( $report ) ) {

              /* Do a get remote to Developer Center. */

              $args = array(
                'issue_report[name]'        => $name,
                'issue_report[email]'       => $email,
                'issue_report[title]'       => $title,
                'issue_report[description]' => $description,
                'issue_report[report]'      => $report
              );

              $params = array(
                'method'      => WPXAPIMethod::POST,
                'timeout'     => WPXtremeIssueReport::RESPONSE_TIMEOUT,
                'redirection' => 5,
                'httpversion' => '1.0',
                'user-agent'  => WPXtremeIssueReport::USER_AGENT,
                'blocking'    => true,
                'headers'     => array(),
                'cookies'     => array(),
                'body'        => $args,
                'compress'    => false,
                'decompress'  => true,
                'sslverify'   => true,
              );

              //-----------------------------------------------------------------------
              // Physically send report to Developer Center
              //-----------------------------------------------------------------------

              $cIssueReport = WPXtremeIssueReport::init();

              $request = wp_remote_request( $cIssueReport->restServerURL, $params );

              //-----------------------------------------------------------------------
              // Handle return status
              //-----------------------------------------------------------------------

              if ( is_array( $request ) ) {

                $iResponseCode = wp_remote_retrieve_response_code( $request );
                switch ( $iResponseCode ) {

                  case WPXtremeIssueReport::RESPONSE_SUCCESS:
                    $response['send_result'] =
                      __( 'Thank you! Your Issue Report has been successfully sent to wpXtreme Team!', WPXTREME_TEXTDOMAIN ) .
                      PHP_EOL . PHP_EOL . __( 'These are the main data sent:', WPXTREME_TEXTDOMAIN ) . PHP_EOL .
                      PHP_EOL . sprintf( "%s: %s", __( 'Name', WPXTREME_TEXTDOMAIN ), $name ) . PHP_EOL .
                      sprintf( "%s: %s", __( 'Email address', WPXTREME_TEXTDOMAIN ), $email ) . PHP_EOL .
                      sprintf( "%s: %s", __( 'Issue Title', WPXTREME_TEXTDOMAIN ), $title ) .
                      PHP_EOL . sprintf( "%s: %s", __( 'Issue Description', WPXTREME_TEXTDOMAIN ), $description );

                    //-----------------------------------------------------------------------
                    // If the report has been correctly sent, reset issue environment status
                    //-----------------------------------------------------------------------

                    // Switch status to 'disabled'
                    $iNewMode                                                   = WPXtremeConfigurationIssueReport::DISABLED;
                    WPXtremeConfiguration::init()->issueReport->issueReportMode = $iNewMode;
                    WPXtremeConfiguration::init()->update();

                    // Clear all log files
                    $cIssueReport->deleteLogFiles();

                    /* HTML markup for update modal and footer without reload entire document page. */
                    $issue_reposrt_view  = new WPXtremeIssueReportView;
                    $response['content'] = $issue_reposrt_view->modal();
                    $response['footer']  = $issue_reposrt_view->footer();

                    break;

                  case WPXtremeIssueReport::RESPONSE_UNPROCESSABLE_ENTITY:

                    $cErrors = json_decode( $request['body'] );
                    $sError  = '';

                    foreach ( $cErrors as $cError ) {
                      foreach ( $cError as $sKey => $aValue ) {
                        $sError .= $sKey . ' ' . $aValue[0] . PHP_EOL;
                      }
                    }

                    $response['message'] =
                      __( 'Warning: Can not send the report to server!', WPXTREME_TEXTDOMAIN ) . PHP_EOL .
                      __( 'Unprocessable request:', WPXTREME_TEXTDOMAIN ) . PHP_EOL . $sError;

                    break;

                  default:
                    $response['message'] = __( 'Warning: Can\'t send the report to server!', WPXTREME_TEXTDOMAIN ) .
                      PHP_EOL . sprintf( "%s: %d", __( 'Status code', WPXTREME_TEXTDOMAIN ), $iResponseCode );

                } // end switch

              } // is_array
              else {

                if ( is_wp_error( $request ) ) {
                  $response['message'] = __( 'Warning: Can\'t send the report to server!', WPXTREME_TEXTDOMAIN );
                  $response['message'] .= PHP_EOL;
                  $aErrors = $request->get_error_messages();
                  foreach ( $aErrors as $sMessage ) {
                    $response['message'] .= $sMessage . PHP_EOL;
                  }

                }

              }

            } // ! empty report
            else {
              $response['message'] = __( 'Warning: the report seems to be empty! Please, repeat the process.', WPXTREME_TEXTDOMAIN );
            }
          }
          else {
            $response['message'] = __( 'Warning: can\'t send your report without a title for the Issue!', WPXTREME_TEXTDOMAIN );
          }
        }
        else {
          $response['message'] = __( 'Warning: your email address is invalid. Can\'t send the issue report.', WPXTREME_TEXTDOMAIN );
        }
      }
      else {
        $response['message'] = __( 'Warning: can\'t send the report without your name!', WPXTREME_TEXTDOMAIN );
      }

      echo json_encode( $response );
      die();
    }

    // -------------------------------------------------------------------------------------------------------------
    // Secure sub-channel for iframe
    // -------------------------------------------------------------------------------------------------------------

    /**
     * Used to ask to the server what happen when the iframe is reload
     *
     * @brief Refresh iframe
     */
    public function wpxtreme_action_get_remote_status()
    {
      if ( isset( $_POST['client_id'] ) && !empty( $_POST['client_id'] ) ) {
        echo $this->api->status( $_POST['client_id'] );
      }
      die();
    }


  } // WPXtremeAjax
}