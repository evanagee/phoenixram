<?php
/**
 * Manage and send request to wpXtreme Server
 *
 * @class              WPXtremeAPI
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-06-11
 * @version            1.0.13
 *
 */

class WPXtremeAPI extends WPXAPI {

  /**
   * Create an instance of WPXtremeAPI class
   *
   * @brief Construct
   *
   * @return WPXtremeAPI
   */
  public function __construct()
  {
    parent::__construct( 'wpXtreme' );
  }

  // TODO
  public function wpxstore_plugins()
  {
    /* Get behaviour from GET. */
    $query    = json_encode( $_GET );
    $response = $this->request( 'wpxstore/plugins', $query );

    return $response->html;
  }

  // TODO
  public function wpxstore_plugin_card( $slug = null )
  {
    if ( is_null( $slug ) ) {
      /* Get behaviour from GET. */
      $product = isset( $_POST['product'] ) ? $_POST['product'] : '';
      if ( !empty( $product ) ) {
        $_GET['product'] = json_decode( base64_decode( $product ) );
        $query           = json_encode( $_GET );
        $response        = $this->request( 'wpxstore/plugin_card', $query );
        return $response->html;
      }
    }
    else {
      $_GET['product'] = $slug;
      $query           = json_encode( $_GET );
      $response        = $this->request( 'wpxstore/plugin_card', $query );
      return $response->html;
    }
    return false;
  }
}