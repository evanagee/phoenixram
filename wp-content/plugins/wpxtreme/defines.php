<?php
/**
 * Defines
 *
 * @author             =undo= <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-05-22
 * @version            1.1.0
 *
 */

/* Short hand for text domain. */
define( 'WPXTREME_VERSION', $this->version );
define( 'WPXTREME_TEXTDOMAIN', $this->textDomain );

define( 'WPXTREME_URL_ASSETS', $this->assetsURL );
define( 'WPXTREME_URL_CSS', $this->cssURL );
define( 'WPXTREME_URL_JAVASCRIPT', $this->url_javascript );
define( 'WPXTREME_URL_IMAGES', $this->url_images );

/*
* Path unix: /var/
*/

/* Set constant path: plugin directory. */
define( 'WPXTREME_PATH', $this->path );
define( 'WPXTREME_PATH_CLASSES', $this->classesPath );

/* Standard message when and error/warning occours. */
define( 'WPXTREME_SUPPORT', __( '<p>If the problem persist, please contact our <a href="mailto:support@wpxtre.me">support team</a></p>', WPXTREME_TEXTDOMAIN ) );

// -----------------------------------------------------------------------------------------------------------------
// Development
// -----------------------------------------------------------------------------------------------------------------

define( 'WPXTREME_DEBUG', false );
define( 'WPXTREME_IMPROVE_DEBUG_OUTPUT', false );