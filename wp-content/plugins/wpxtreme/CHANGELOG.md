# wpXtreme CHANGELOG

---

## Versioning

For transparency and insight into our release cycle, and for striving to maintain backward compatibility, this code will be maintained under the Semantic Versioning guidelines as much as possible.

Releases will be numbered with the following format:

`<major>.<minor>.<patch>`

And constructed with the following guidelines:

* Breaking backward compatibility bumps the major (and resets the minor and patch)
* New additions without breaking backward compatibility bumps the minor (and resets the patch)
* Bug fixes and misc changes bumps the patch

For more information on SemVer, please visit http://semver.org/.

---

## Version 1.0.2
### 2013-07-09

#### Bugs

* Fixed Potentially incorrect cache clear while upgrade plugin
* Fixed back to the store from the single product card
* Minor fixes

#### Enhancements

* Updated WPDK framework v1.1.2 (see https://github.com/wpXtreme/wpdk/blob/master/CHANGELOG.md )

#### Improvements

* Minor several improvements and stable



## Version 1.0.1
### 2013-06-14

#### Bugs

* Fixed/Improved the row action enhancer
* Remove debug-bug icon from control panel
* Minor fixes



## Version 1.0.0
### 2013-06-11

#### Enhancements

* Added WPDK URL and version in admin footer
* Makes WPDK as Open Source (removed from this private repo)
* Several improvements to UI
* New API engine

#### Bugs

* Fixed wrong ajax post send in enhancer preferences when swipe changed
* Minor fixes



## Version 1.0.0 beta 4
### 2013-04-19

#### Enhancements

* Added amazing autoload PHP class with over 50% performance increment on loading
* Added WPDKMail core class to make easy compose and send mail to an user
* Added WPDKMenu engine with WPDKSubMenu and new divider to make easy compose you backend area menu
* Added WPDKMetaBoxView to manage the WordPress meta box
* Added recursive scan in WPDKFilesystem
* Improved WPDKUI
* Improved enhancer users by adding a new combo menu filters for roles and capabilities
* Improved Appearance general and posts
* Improved CSS rules
* Improved internal documentation
* Cosmetic code
* Added deprecated filder
* Cleanup code
* Removed `bootstrap.php` and `config.php` and added new `kickstart.php` file
* Removed Tools menu and Maintenance. Checkout for Maintenance Pro plugin
* Removed embed Custom Post Mail. Checkout for Mail Manager plugin
* Removed User signin/signup/profile, etc... Checkout for Users Manager plugin
* Cleanup deprecated shortcode for user
* Removed embed security. Checkout for Deflector plugin
* Removed WPDKWidget and move into wpXtreme framework as WPXtremeWidget

#### Experimental

* Added WPDK progress bar like Twitter Bootstrap


#### Bugs

* Added CA certs handling for SSL transactions towards WPX Store. Following a signal from [Walter Franchetti](http://walterfranchetti.it/)
* Fixed bug in saving user extra-fields checkbox value
* Fixed CSS enhance rules
* Minor fixes in WPDK



## Version 1.0.0 beta 3
### 2013-01-21

#### Enhancements

* Added a new online Amazing Issue Report accesible from Control Panel or from new admin backend area footer
* Removed Breaking News from menu item and Control Panel
* Added a new WordPress dashboard Widgets with more information, breaking news and system updates
* Added a new admin backend area footer with some useful links and... see issue report above
* Added a back to Control Panel in the main navigation bar of WPX Store

#### Bugs

* Fixed WPDK shortcodes registration - moved registration from `wp_head()` hook to `template_redirect()`
* Fixed and complete map for WPDKHTMLTagTextarea
* Several Fixes on new CSS box model for WordPress 3.5
* Fixed configuration delta
* Fixed empty date in formatFromFormat() method
* Fixed wrong WordPress re-inclusion Plugin_Upgrader classes
* Minor labels fixes



## Version 1.0.0 beta 2
### 2013-01-14

#### Enhancements

* Added sorting by 'Enabled' columns in backend users list table
* Added new shortcode [wpdk_user_resend_unlock] to display a form to resend the unlock code
* Added new shortcode [wpdk_is_user_logged_in] to display a content only for logged in user
* Added new shortcode [wpdk_is_user_not_logged_in] to display a content only for NOT logged in user
* Added new Drag & Drop sorter for Custom Post Type of type page

#### Experimental

* Added WPDK Cron Jobs method prototype in WPDKWordPressPlugin class

#### Bugs

* Fixed #75 Fatal error in DateTime::createFromFormat()
* Fixed and improved update plugin procedure from standard WordPress plugin list
* Several improves and fixes in enhancer user list like add column for signup and sorting
* Improved install/update plugin layout from WPX Store
* Fixed enable/disable enhanced wpXtreme theme
* Improved and fixes for Custom Post Type Mail
* Minor fixes for WordPress.org submit



## Version 1.0.0 beta 1
### 2012-12-21

* First public release



## Version 0.9.12
### 2012-12-20

#### Enhancements

* Updated icons and header view icons

#### Bugs

* Minor fixes on maintenance mode date range check
* Fixed WPDKListTableViewController checkbox column name
* Align internal log
* Fixed: #57 - Hidden security badge



## Version 0.9.1
### 2012-12-12

#### Enhancements

* Added new WPDKShortcode class to make easy to write a shortcode own class
* Improved class WPDKWatchDog - added automatic removal for old zero log file
* Several improvement and fixes to WPDKListTableViewController class

#### Experimental

* Starting refactoring of WPDKDBTable
* Introducing WPDKTwitterBootstrapButton, WPDKTwitterBootstrapButtonType, WPDKTwitterBootstrapButtonSize

#### Bugs
* Fixed registration process
* Several fixes on WPX Store, API communications



## Version 0.8.0
### 2012-11-30

#### Enhancements

* Rewrite API services - introducing WPDKAPI, WPDKAPIErrorCode, WPDKAPIMethod, WPDKAPIResource, WPDKAPIResponse
* Added WPDKAjax a new class to make easy to write Ajax gateway class
* Updated logo plugin

#### Bugs

* Minor bugs fixes
* Minor Improvements
* Revision version and date in documentation



## Version 0.7.5
### 2012-11-27

#### Bugs

* Fixed textarea controls on set value
* Several fixes to prepare WordPress 3.5 compatibility
* Minor improvments
* Minor fixes



## Version 0.7.0
### 2012-11-20

#### Enhancements

* Updated bootstrap.php to fix invalid index
* Improved WPDK main class and removed manual WPDK version setting
* Improved Plugin loading and info
* Major updated in WPDKWordPressPlugin class
* Added nonce chceck in saving configuration view

#### Experimental

* Introducing new WPDKPlugin

#### Bugs

* Minor improvments in Control Panel
* Minor fixes in Control Panel
* Fixed wrong saving configuration in the queue



## Version 0.6.2
### 2012-11-16

#### Enhancements

* Updated bootstrap.php to v2.0.1 to avoid wrong WPDK path load

#### Bugs

* Fixed WPDKConfiguration wrong singleton init
* Minor docs updated
* Fixed minor bugs



## Version 0.6.1
### 2012-11-14

#### Bugs

* Fixed anonymous function (notice by Glenn Tate) in wpdk-sdf.php and WPDKForm.php



## Version 0.6.0
### 2012-11-13

#### Enhancements

* Updated WPDK Swipe control
* Updated Icons

#### Experimental
* Enabled beta of Control Panel

#### Bugs

* Fixed foreground color in form fields
* Minor fixes
