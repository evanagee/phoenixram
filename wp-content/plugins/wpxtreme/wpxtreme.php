<?php
/* Include the main plugin class */
require_once( trailingslashit( dirname( __FILE__ ) ) . 'classes/framework/wpx-plugin.php' );

/**
 * wpXtreme main Plugin Class.
 * This is the main class of plugin. This class extends WPDKWordPressPlugin in order to make easy several WordPress
 * funtions.
 *
 * @class              WPXtreme
 * @author             wpXtreme, Inc. <info@wpxtre.me>
 * @copyright          Copyright (C) 2012-2013 wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-28
 * @version            1.2.2
 *
 */
final class WPXtreme extends WPXPlugin {

  //-------------------------------------------------------------------------------------------
  // Properties
  //-------------------------------------------------------------------------------------------

  /**
   * Path of CA file in PEM format used for SSL certificating in shared hosting.
   *
   * @brief Path of CA file in PEM format
   *
   * @var string $_caCertPath
   *
   * @since 0.9.0
   */
  private $_caCertPath;

  /**
   * Value of ssl_verify flag into cURL transaction sent to the WPX Store
   *
   * @brief Value of ssl_verify flag
   *
   * @var string $_flagSSLVerify;
   *
   * @since 0.9.0
   */
  private $_flagSSLVerify;

  /**
   * Create and return a singleton instance of WPXtreme class
   *
   * @brief Boot wpXtreme plugin
   *
   * @param string $file The main file of this plugin. Usually __FILE__ (main.php)
   *
   * @return WPXtreme
   */
  public static function boot( $file ) {
    static $instance = null;
    if ( is_null( $instance ) ) {
      $instance = new WPXtreme( $file );
      do_action( __CLASS__ );
    }
    return $instance;
  }

  /**
   * Create an instance of WPXtreme class
   *
   * @brief Construct
   *
   * @param string $file The main file of this plugin. Usually __FILE__ (main.php)
   *
   * @return WPXtreme
   */
  public function __construct( $file ) {

    parent::__construct( $file );

    // Set CA cert file path needed by some shared hosting ( i.e. Azure )
    $this->_caCertPath    = $this->path . 'cacert.crt';
    $this->_flagSSLVerify = true;

    /* Definisco una serie di costanti di comodità. */
    $this->defines();

    /* Includes */
    $this->registerClasses();

    /* Dope WordPress */
    add_action( 'init', array( $this, 'enhancing' ) );

    //-----------------------------------------------------------------------------------------
    // cURL patch for shared hosting having 'SSL certificate problem, verify that the CA cert is OK' issue;
    // Without this patch, some shared hosting with WP and wpXtreme cannot see WPX Store, because
    // they don't have a default bundle of trusted certificates installed in their environment.
    //-----------------------------------------------------------------------------------------

    add_filter( 'https_local_ssl_verify', array( $this, 'curlGetFlagSSLVerify' ) );
    add_filter( 'https_ssl_verify', array( $this, 'curlGetFlagSSLVerify' ) );
    add_action( 'http_api_curl', array( $this, 'curlEnableSSLOnSharedHosting' ), 10, 2 );

    //-----------------------------------------------------------------------------------------
    // Activate wpXtreme issue report mode if requested;
    // WARNING: there is no need to join this activation to a WP action; simply, I start to log
    //          PHP error(s) DIRECTLY FROM HERE, that is, even before plugins activation
    //-----------------------------------------------------------------------------------------

    /* Get configuration instance. */
    $configuration     = WPXtremeConfiguration::init();
    $sub_configuration = $configuration->issueReport;

    if ( WPXtremeConfigurationIssueReport::ENABLED == $sub_configuration->issueReportMode ) {
      $cIssueInstance = WPXtremeIssueReport::init();
      $cIssueInstance->enablePHPErrorLog();
    }

    /* Put WPXtreme as first plugin in WordPress queue */
    add_action( 'activated_plugin', array( $this, 'activated_plugin' ), 10, 2 );

    /* Do a backtrace for deprecated notice. */
    if ( defined( 'WPXTREME_IMPROVE_DEBUG_OUTPUT' ) && WPXTREME_IMPROVE_DEBUG_OUTPUT ) {
      add_action( 'deprecated_function_run', array( $this, 'deprecated_function_run' ) );
      set_error_handler( array( $this, 'errorHandler' ) );
    }
  }

  /**
   * Include the external defines file
   *
   * @brief Defines
   */
  private function defines() {
    require_once( $this->path . 'defines.php' );
  }

  /**
   * Register all autoload classes
   *
   * @brief Autoload classes
   * @since 1.0.0.b4
   *
   */
  private function registerClasses() {

    $includes = array(
    	$this->classesPath . 'admin/wpxtreme-about-vc.php' => 'WPXtremeAboutViewController',

    	$this->classesPath . 'admin/wpxtreme-admin.php' => 'WPXtremeAdmin',

    	$this->classesPath . 'admin/wpxtreme-control-panel.php' => array( 'WPXtremeControlPanelViewController',
    'WPXtremeControlPanelView',
    'WPXtremeControlPanelWidgets',
    'WPXtremeControlPanelWidget',
    'WPXtremeControlPanelWidgetView' ),

    	$this->classesPath . 'admin/wpxtreme-dashboard.php' => array( 'WPXtremeDashboard',
    'WPXtremeDashboardView' ),

    	$this->classesPath . 'admin/wpxtreme-issue-report.php' => array( 'WPXtremeIssueReport',
    'WPXtremeIssueReportView' ),

    	$this->classesPath . 'configuration/wpxtreme-configuration.php' => array( 'WPXtremeConfiguration',
    'WPXtremeConfigurationIssueReport',
    'WPXtremeConfigurationEnhancerAppearanceGeneral',
    'WPXtremeConfigurationEnhancerAppearancePosts' ),

    	$this->classesPath . 'core/wpxtreme-ajax.php' => 'WPXtremeAjax',

    	$this->classesPath . 'core/wpxtreme-api.php' => 'WPXtremeAPI',

    	$this->classesPath . 'core/wpxtreme-shortcode.php' => 'WPXtremeShortcode',

    	$this->classesPath . 'enhancers/appearance/wpxtreme-appearance-vc.php' => array( 'WPXtremeConfigurationEnhancerAppearanceViewController',
    'WPXtremeConfigurationEnhancerAppearanceGeneralView',
    'WPXtremeConfigurationEnhancerAppearancePostView' ),

    	$this->classesPath . 'enhancers/appearance/wpxtreme-enhancer-post.php' => 'WPXtremeEnhancerPost',

    	$this->classesPath . 'enhancers/users/wpxtreme-enhancer-user.php' => 'WPXtremeEnhancerUser',

    	$this->classesPath . 'framework/wpx-about-viewcontroller.php' => 'WPXAboutViewController',

    	$this->classesPath . 'framework/wpx-api.php' => array( 'WPXAPI',
    'WPXAPIResponse',
    'WPXAPIMethod',
    'WPXAPIErrorCode'),

    	$this->classesPath . 'framework/wpx-plugin.php' => 'WPXPlugin',

    	$this->classesPath . 'framework/wpx-update.php' => array( 'WPXUpdate',
    'WPXPluginUpgrader',
    'WPXPluginUpgraderSkin' ),

    	$this->classesPath . 'frontend/wpxtreme-frontend.php' => 'WPXtremeFrontend',

    	$this->classesPath . 'store/wpxtreme-store-view-controller.php' => array(
        'WPXtremeStoreViewController',
        'WPXtremeStoreView'
      ),

    	$this->classesPath . 'store/wpxtreme-store-themes-view-controller.php' => array(
        'WPXtremeStoreThemesViewController',
        'WPXtremeStoreThemesView'
      )
    );

    $this->registerAutoloadClass( $includes );

  }

  // -----------------------------------------------------------------------------------------------------------------
  // Standard WordPress hook methods to override
  // -----------------------------------------------------------------------------------------------------------------

  /**
   * Catch for ajax
   *
   * @brief Ajax
   */
  public function ajax() {
    WPXtremeAjax::init();
  }

  /**
   * Catch for admin
   *
   * @brief Admin backend
   */
  public function admin() {
    new WPXtremeAdmin( $this );
  }

  /**
   * Catch for frontend
   *
   * @brief Frontend
   */
  public function theme() {
    /* To override. */
    // $frontend = new WPXtremeFrontend( $this );
  }

  /**
   * Ready to init configuration plugin
   *
   * @brief Configuration
   */
  public function configuration() {
    WPXtremeConfiguration::init();
  }

  /**
   * Add to 'http_api_curl' WordPress action a patch for shared hosting with some SSL certificate problems.
   * Thanks to Walter Franchetti: http://walterfranchetti.it/2013/01/wp_http-and-the-ssl-cert-problem/
   *
   * @brief Add to 'http_api_curl' WordPress filter a patch for shared hosting
   *
   * @param array &$aCurlHandler The cURL actual handler initialized by WordPress. Note that this parameter is passed
   *                             by reference, as the action needs.
   *
   * @since 0.9.0
   *
   */
  public function curlEnableSSLOnSharedHosting( &$aCurlHandler ) {

    // Add proper handling of CA certificate only if I'm in ssl_verify mode
    if ( true == $this->_flagSSLVerify ) {
      curl_setopt( $aCurlHandler, CURLOPT_CAINFO, $this->_caCertPath );
    }

  }

  /**
   * Hook to 'https(_local)_ssl_verify' WordPress filter in order to catch if cURL transaction has ssl_verify flag
   * enabled.
   *
   * @brief Hook to 'https(_local)_ssl_verify' WordPress filter
   *
   * @param boolean $bSSLFlag The current value of ssl_verify flag into cURL transaction
   *
   * @return boolean Current value of ssl_verify flag, without any changing.
   *
   * @since 0.9.0
   *
   */
  public function curlGetFlagSSLVerify( $bSSLFlag ) {
    $this->_flagSSLVerify = $bSSLFlag;
    return $bSSLFlag;
  }

  // -----------------------------------------------------------------------------------------------------------------
  // Activation/Deactivation
  // -----------------------------------------------------------------------------------------------------------------

  /**
   * Put wpXtreme plugin on the top of plugin list
   *
   * @brief Activate plugin
   *
   * @param $plugin
   * @param $network_wide
   */
  public function activated_plugin( $plugin, $network_wide ) {

    if ( $plugin == $this->pluginBasename ) {

      /* Put me at the top */
      $active_plugins = get_option( 'active_plugins' );
      $position       = array_search( $plugin, $active_plugins );
      if ( $position > 0 ) {
        array_splice( $active_plugins, $position, 1 );
        array_unshift( $active_plugins, $plugin );
        update_option( 'active_plugins', $active_plugins );
      }

      remove_action( 'activated_plugin', array( $this, 'activated_plugin' ), 10, 2 );
    }
  }

  /**
   * Catch for activation. This method is called one shot.
   *
   * @brief Activation
   */
  public function activation() {
    /* Do a delta (update) of configuration. */
    WPXtremeConfiguration::init()->delta();
  }

  /**
   * Catch for deactivation. This method is called when the plugin is deactivate.
   *
   * @brief Deactivation
   */
  public function deactivation() {
  }

  // -----------------------------------------------------------------------------------------------------------------
  // Enhancer
  // -----------------------------------------------------------------------------------------------------------------

  /**
   * Do a several enhancing to WordPress. Init all enhancing procedures.
   *
   * @brief Enanching
   */
  public function enhancing() {
    
    /* Enhancing Posts. */
    WPXtremeEnhancerPost::init();

    /* Enhancing users. */
    /* @todo Improved with add_action( 'load-users.php', array( 'WPXtremeEnhancerUser', 'init' ) ); */
    WPXtremeEnhancerUser::init();

  }

  // -----------------------------------------------------------------------------------------------------------------
  // Deep debug tool
  // -----------------------------------------------------------------------------------------------------------------

  /**
   * Enabled backtrace on deprecated function
   *
   * @brief Enable backtrace
   *
   * @since 0.7.5
   */
  public function deprecated_function_run() {
    if ( WPXTREME_IMPROVE_DEBUG_OUTPUT && apply_filters( 'deprecated_function_trigger_error', true ) ) {
      $functions = wp_debug_backtrace_summary( null, 0, false );
      $functions = array_slice( $functions, 3 );
      $index     = 1;
      $output    = WPDK_CRLF . '--- START BACKTRACE ------------------------------------------------' . WPDK_CRLF;
      foreach ( $functions as $func ) {
        $output .= sprintf( '  %d) %s %s', $index++, $func, WPDK_CRLF );
      }
      $output .= '--- END BACKTRACE ------------------------------------------------' . WPDK_CRLF;
      trigger_error( $output );
    }
  }

  /**
   * Called when debug is on.
   *
   * @brief Custom error handler
   *
   * @param $errno
   * @param $errstr
   * @param $errfile
   * @param $errline
   */
  public function errorHandler( $errno, $errstr, $errfile, $errline ) {
    if ( !( error_reporting() & $errno ) ) {
      /* This error code is not included in error_reporting. */
      return;
    }

    $functions = wp_debug_backtrace_summary( null, 0, false );
    $functions = array_slice( $functions, 3 );
    $index     = 1;
    $output    = PHP_EOL . '<pre>--- START BACKTRACE ------------------------------------------------' . PHP_EOL;
    foreach ( $functions as $func ) {
      $output .= sprintf( '  %d) %s %s', $index++, $func, PHP_EOL );
    }
    $output .= '--- END BACKTRACE ------------------------------------------------</pre>';
    //            $output .= sprintf( '[%s] in %s at line %s - ERRNO [%d]', $errstr, $errfile, $errline, $errno );
    trigger_error( $output );

    //---------------------------------------------------------------------
    // There is no need to output error line if this function returns FALSE:
    // in this case, all stuffs are normally handled by PHP core, and message is displayed anyway;
    // see http://it2.php.net/manual/en/function.set-error-handler.php.
    //---------------------------------------------------------------------

    return false;

  }

  /**
   * Do log for wpXtreme in easy way
   *
   * @brief Helper for log
   *
   * @param mixed  $txt
   * @param string $title Optional. Any free string text to context the log
   *
   */
  public static function log( $txt, $title = '' ) {
    /**
     * @var WPXtreme $me
     */
    $me = $GLOBALS[__CLASS__];
    $me->log->log( $txt, $title );
  }

}