<?php
/*
Plugin Name: WooCommerce Advanced Product Manager
Plugin URI: http://www.evanagee.com
Description: A powerful plugin to make product management within WooCommerce a more enjoyable experience.
Version: 0.1
Author: Evan Agee
Author URI: http://www.presscoders.com
*/

include_once( 'admin/admin-init.php' );