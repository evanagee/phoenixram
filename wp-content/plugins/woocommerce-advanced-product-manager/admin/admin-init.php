<?php
/**
 * Setup the Admin menu in WordPress
 *
 */
function wapm_admin_menu() {
    global $menu, $woocommerce;
	
    if ( current_user_can( 'manage_woocommerce' ) ) {
    	//$menu[] = array( '', 'read', 'separator-wapm', '', 'wp-menu-separator wapm' );
		//$main_page = add_menu_page( 'WooCommerce Product Manager', 'WooCommerce Product Manager', 'manage_woocommerce', 'wapm' , 'wapm_index_page', null, '58' );
	}
    //$reports_page = add_submenu_page( 'woocommerce', __( 'Reports', 'woocommerce' ),  __( 'Reports', 'woocommerce' ) , 'view_woocommerce_reports', 'woocommerce_reports', 'woocommerce_reports_page' );

    //add_submenu_page( 'edit.php?post_type=product', __( 'Manage Products', 'woocommerce' ), __( 'Manage Products', 'woocommerce' ), 'manage_product_terms', 'wapm', 'woocommerce_attributes_page');

    //add_action( 'load-' . $main_page, 'woocommerce_admin_help_tab' );
    //add_action( 'load-' . $reports_page, 'woocommerce_admin_help_tab' );

    //$wc_screen_id = strtolower( __( 'WooCommerce', 'wapm' ) );

    //$print_css_on = apply_filters( 'woocommerce_screen_ids', array( 'toplevel_page_' . $wc_screen_id, $wc_screen_id . '_page_woocommerce_settings', $wc_screen_id . '_page_woocommerce_reports', 'toplevel_page_woocommerce', 'woocommerce_page_woocommerce_settings', 'woocommerce_page_woocommerce_reports', 'woocommerce_page_woocommerce_status', 'product_page_woocommerce_attributes', 'edit-tags.php', 'edit.php', 'index.php', 'post-new.php', 'post.php' ) );

    //foreach ( $print_css_on as $page )
    	//add_action( 'admin_print_styles-'. $page, 'woocommerce_admin_css' );
}

//add_action('admin_menu', 'wapm_admin_menu', 9);

/**
 * Include and display the settings page.
 *
 * @access public
 * @return void
 */
function wapm_index_page() {
	include_once( 'admin-index.php' );
	woocommerce_settings();
}

function wapm_products_by_type() {
    global $typenow, $wp_query;
    if ($typenow=='product') :
		
		$taxonomies = get_taxonomies(null,'objects');
		$taxons = array();
		foreach ($taxonomies as $t) {
			if (substr($t->name,0,3) == 'pa_') $taxons[$t->name] = $t->label;
		}
		/*
echo '<pre>';
		print_r($taxons);
		echo '</pre>';
*/
		$output = '';
		
		foreach($taxons as $k => $v) :
	    	// Types
			$terms = get_terms($k);
			//print_r($terms);
			//echo $k . ' - '. count($terms).'<br>';
			if (count($terms) == 0 || !count($terms)) {
				continue;
			}
			
			$output .= "<select name='" . $k . "' id='dropdown_" . $k . "'>";
			$output .= '<option value="">Filter by '.$v.'</option>';
			foreach($terms as $term) :
				$output .="<option value='" . sanitize_title( $term->name ) . "' ";
				if ( isset( $wp_query->query[$k] ) ) $output .=selected($term->slug, $wp_query->query[$k], false);
				$output .=">";
				$output .= ucwords($term->name);
	
				$output .=" ($term->count)</option>";
			endforeach;
			$output .="</select>";
		endforeach;

		echo $output;
    endif;
}

add_action('restrict_manage_posts', 'wapm_products_by_type');