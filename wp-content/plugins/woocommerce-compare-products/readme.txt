=== Woocommerce Compare Products ===
Contributors: a3rev, A3 Revolution Software Development team
Tags: WooCommerce, WooCommerce Plugins, WooCommerce compare products, compare products plugin, compare products
Requires at least: 2.92
Tested up to: 3.6
Stable tag: 2.1.6
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Add a World Class Compare Products Feature to your WooCommerce store today with the WooCommerce Compare Products plugin..

== Description ==

The Compare Products extension for WooCommerce gives a product comparison feature that you'd only expect to find on the big corporate e-commerce sites. 

Compare Products uses your existing WooCommerce Product Categories and Product Attributes to create Product Feature Sets that can be assigned to each and every product to create a feature comparison table. 

This allows users to firstly add products to a compare widget basket, then at the click of a mouse the chosen products can be viewed in a state-of-the-art comparison table. 

Chosen products are compared side-by-side, feature by feature, price-by-price. Discard products from the table at the click of a mose as you hone in on the product that is the one for you. Save the comparison as a PDF or print it. 
 
= Key Features =
* In Version 2.1.0 we launch our the brand new feature, In-Plugin Custom Styling 
* Get a taste for what it is like to be able to style and set the layout of every element of this plugins front end output from the admin panel.
* Create the perfect layout and look for your compare features without touching the theme or plugin code.
* Theme updates and changing a theme does not affect the layout and styles you create. It is all in the plugin, independent of the theme.
* In Window Comparison page and the scrolling comparison table that will wow your users.
* Create unique Comparison feature set for a group of products by using existing WooCommerce Product Categories, Attributes and Attribute terms.
* Add or create completely new comparable feature sets independent to your existing WooCommerce Product categories, Attributes and terms.. 
* Works with any Theme that has the WooCommerce plugin installed and activated.
* On install all of your existing Product Categories are auto created as Compare Categories
* On install all of your existing Product Attributes are auto created as Compare Features. Each Attribute's Terms are auto created as Feature values (the data that is compared) 
* Add custom Comparable Features and values independent of your Product Attributes and terms. 
* Fully tested and optimized for all legacy browsers on IOS and WindowsXP to Windows8, including IE7 to IE10.
* Fully tested and optimized for all iPads, Android, Kindle and Google Tablets
* Fully tested and optimized for IOS and Android Mobile platforms.

= Why We Built it! =

We love the Compare Products feature that the big e-commerce sites have. We have looked at them all and over 12 months have developed what we believe is a product comparison extension for WooCommerce that is better than any of them.

We distribute the plugin in 2 versions - This FREE lite version and an advanced featured Pro Version.  Today thousands of WooCommerce stores owners successfully use the Free lite version to provide the Product Comparison feature we love - to their customers.

= Support =

If you have any problem with setting up the Free lite version please post your support request here on the WordPress support forum. PLEASE if you have a problem DO NOT just give the plugin a bad star rating and review without first requesting support. Giving the plugin a bagging without affording us the opportunity to help solve the issue is in our opinion just incredibly unfair.

Once you have the plugin installed and activated please refer to the plugins comprehensive [Online Documentation](http://docs.a3rev.com/user-guides/woocommerce/compare-products/) and guide to setting up the Compare Products plugin on your WooCommerce store.If you have questions - agin please post them to the support forum here.

= PRO Version Upgrade =

* When you install WooCommerce Compare Products you will see all the added functionality that the PRO Version offers right there in the admin panel.
* The plugin is designed so that the upgrade is completely seamless. 
* When you upgrade nothing changes except all the features of Compare Products you see on the lite version are activated. 
* This means you can activate, set up and use the free version and then if you find you want the added features and functionality you can upgrade, completely risk free.  


= Pro Version Features =

* All of the In-Plugin Custom Style and layout settings are activated.
* Product Express Manager - worth the price of the Pro Version upgrade on its own - Saves you hours, days or even weeks of work on larger stores
* Activates the 'View Cart' feature on Grid View. 
* Activates all Grid View layout and display styling function.
* Activates full Compare Widget layout and styling functions.
* Activates 'Add to Cart', 'View Cart' feature on the comparison table.
* Activates full Comparison Table layout and style functions.   


View these features on the [Pro Version Demo site here](http://store.a3dev.net/shop/) Add some products to the Compare basket and try the comparison table. It is a beautiful presentation.
View all the Pro Version features [here on the a3rev site](http://a3rev.com/shop/woocommerce-compare-products/)


= Localization =
* English (default) - always include.
* Turkish - added thanks to ManusH
* .pot file (woo_cp.pot) in languages folder for translations.
* Your translation? Please [send it to us](http://www.a3rev.com/contact/) We'll acknowledge your work and link to your site.
Please [Contact us](http://www.a3rev.com/contact/) if you'd like to provide a translation or an update. 


== Screenshots ==

1. screenshot-1.png

2. screenshot-2.png

3. screenshot-3.png

4. screenshot-4.png

5. screenshot-5.png

== Installation ==

1. Upload the folder woocommerce-compare-products to the /wp-content/plugins/ directory

2. Activate the plugin through the Plugins menu in WordPress

== Usage ==

1. Open WooCommerce > Compare Settings

2. Opens to the SETTINGS tab

 * Style your Compare Products Fly-Out screen - upload header image and set screen dimensions.
 * Select to show Compare Feature on Product Pages as Button or Hyperlink Text.
 * Set text to show in Button or Link.
 * Set Compare Products Tab to show in WooCommerce Product Page Navigation Tabs.
 * Save Settings to save your work. You are now ready to add the Compare features data for each product.

3. Click the FEATURES tab 

4. Click the dropdown arrow at the end of the Master Category tab. You'll see that all of your parent product Attributes have been auto created as Compare Features.

5. Add any Custom Compare categories or features you require.

6  Edit or deactivate the Compare feature for any products edit page.

Celebrate the extra sales Compare Products brings you !  

== Frequently Asked Questions ==

= When can I use this plugin? =

You can use this plugin when you have installed the WooCommerce plugin.


== Changelog ==

= 2.1.6 - 2013/08/20 =
* Tweaks :
	* Tested for full compatibility with WordPress v3.6.0
	* Added PHP Public Static to functions in Class. Done so that Public Static warnings don't show in WP__DEBUG mode.
	* Tweaked dashboard Yellow sidebar info and added link to the Pro Version 7 day FREE trail offer.

= 2.1.5 - 2013/06/15 =
* Fix: Compare Features Tab on Product Page, Enable / Disable function. The 2 functions worked but the opposite of what they where supposed to.
* Tweak: Updated support URL link to the plugins wordpress.org support forum.

= 2.1.4 - 2013/04/26 =
* Fixed: Text colour pickers not saving and displaying the hex number and hex colour in Font Colour, Font Link Colour and Font Hover colour selectors after editing and updating. The bug affected all font colour setting that use theme colours on install. These are the settings show an empty colour selector field on install.

= 2.1.3 - 2013/04/23 =
* Fixed: External / Affiliates link on Compare Window is now backward compatible from WooCommerce Version 2.0. Was showing fatal Error for older versions.

= 2.1.2 - 2013/04/17 =
* Feature: Added when install and activate plugin link redirects to WooCommerce Compare Products admin panel instead of the wp-plugins dashboard.
* Fixed: Compare Feature search, search term is no longer case sensitive.
* Fixed: Updated all JavaScript functions so that the plugin is compatible with jQuery Version1.9 and backwards to version 1.6. WordPress still uses jQuery version 1.8.3. In themes that use Google js Library instead of the WordPress jQuery then there was trouble because Google uses the latest jQuery version 1.9. There are a number of functions in jQuery Version 1.9 that have been depreciated and hence this was causing errors with the jQuery function in the plugin.

= 2.1.1 - 2013/04/10 =
* Fixed: Compare on screen widow not opening in IE9. Missed an _ in command and IE9 don't recognize space. Thanks to Nicola from yithemes.com for picking that up
* Fixed: Added correct Chosen script URL so admin panel uses plugin chosen script instead of defaulting to WooCommerce Chosen script.
* Fixed: Added correct Tool Tip tipTip minimum script file .min so admin panel uses plugin Tool Tip script instead of defaulting to WooCommerce script.

= 2.1.0 - 2013/04/09 =
* Feature: Added Compare Categories functionality for Lite Version - previously only available in the Pro Version upgrade.
* Feature: Replaced Compare Fancybox and Lightbox pop-up fly out with Comparisons page opening in a new browser window. WooCommerce Version 2.0 uses new WordPress PrettyPhoto as the pop-up tool and in testing we found supporting 3 pop-up scripts, PrettyPhoto, Fancybox and Lightbox was very buggy and troublesome. Removed all 3 pop-up scripts and replaced with open in browser window. 
* Feature: Added 2 Comparison widow opening options. Option 1. Show the product Comparison in an on-screen pop-up window or Option 2. Show Comparisons in a new window.
* Feature: Added Comparison Table Fixed Features Title column. Products in comparison table scroll left under a fixed Features Title column - always see what product features are being compared.
* Feature: Added Comparison Table scrolls horizontal and vertical via the window scrollers and not by scrollers on an inner container.
* Feature: Completely reworked the Print Product Comparison function. The Print function now prints the entire tall of 3 products Comparison columns regardless of the tall of the columns.
* Feature: In-Plugin Custom Styling - Style every element of the compare feature that front end users see and interface with all without touching the theme or plugin code.
* Feature: Theme updates and changing a theme does not affect the layout and styles you create for your Compare feature because it is all in the plugin, independent of any theme.
* Feature: Removed the old Settings tab and replaced with 4 new tabs, Product Page | Widget Style | Grid View Style | Comparison Page
* Feature: Separated all Product page and Grid View page layout and style settings for fine grain control of how the feature shows on any theme.
* Feature: Product Page Main tab.  Full custom layout and style features for the compare feature on product pages under 4 new sub tabs.
* Feature: Added Activate / Deactivate the Compare Feature on single product pages.
* Feature: Added full custom Compare Button and Linked Text style options.
* Feature: Added Product page 'successfully added' to compare icon. Auto shows after button is clicked. Default icon is a green tick, includes option to remove or upload a custom icon.
* Feature: Added 'View Compare' feature. Just like the 'View Cart' feature. Fully customizable Button or Linked Text. Ideal for full wide product pages that have no sidebar.
* Feature: Widget Style main tab. Full custom styling of everything that the plugin outputs in the Compare WordPress widget. (Pro Version Feature)
* Feature: Added option to show product feature image with items added to the Compare widget. Pro Version allows set thumbnail size, position (Right | Left) and custom image border.
* Feature: Added option in Pro Version to show Widget Compare Button as Button or Linked Text, full custom styling, set position (Left | Center | Right)
* Feature: Added option in Pro Version to create a fully customized Widget Title including background colour and border style.
* Feature: Added option in Pro Version to show 'Clear All' feature as Button or Link text, full custom styling, set position relative to the compare button (Above | Below) and horizontal align (Left | Right | Center)
* Feature: Added option in Pro Version to fully Customize the 'Nothing to Compare' text message and font style that shows when widget is empty.
* Feature: Added option in Pro Version to upload a custom 'Remove' Single item icon from Compare Widget. 
* Feature: Added 'Remove' single item icon function auto position (Pro Version). Always displays opposite side of product title to thumbnail. If thumbnail feature not activated - shows to the right as default.
* Feature: Grid View Style main tab. Created full custom style settings (Pro Version) for the Compare feature for product extracts on the WooCommerce Shop, Product Categories and Product Tags pages.
* Feature: Added option to set the position that the Compare feature shows relative to the 'add to cart' button (Above | Below) independent of Product page settings.
* Feature: Added Activate / Deactivate Compare Feature option on Grid View (Shop page, Product Categories and Product Tag archives pages)
* Feature: Added option in Pro Version for full custom styling of Compare Button / Text Link on Grid View pages independent of Product page settings.
* Feature: Added Grid View 'successfully added' to compare icon. Auto shows after button is clicked. Default icon is a green tick, includes option to remove or upload a custom icon.
* Feature: Added in Pro Version 'View Compare' feature on Grid View Product listings. Auto shows under Compare button after click. Fully customizable Link text. 
* Feature: Comparison Page main tab. In this upgrade we launch full table customization features.
* Feature: Added all new Page Header image uploader script to replace the old and clunky show image by URL from WordPress media library.
* Feature: Added Shortcode now shows as an image in the page visual text editor instead of the shortcode [product_comparison_page]. Added set page from the admin panel.
* Feature: Added customize Comparison page Header background colour, body background colour and the Comparison Empty Window Message text font and style.
* Feature: Added option in Pro Version to show Print Page function as a button or Linked text with full style options for both.
* Feature: Added option in Pro Version to edit text and font style for print page message.
* Feature: Added option in Pro Version to show Close Window function as a button or Linked text with full style options for both.
* Feature: Added option in Pro Version to custom style Table header and Alternate rows background colours. Custom style options for Table borders and row padding.
* Feature: Added custom style options in Pro Version for Compare Feature Titles font (Left fixed column), Table Rows Feature Values font and Product Name font.
* Feature: Added text edit and font style options and custom cell background colour in Pro Version to replace the default N/A text for Compare features that have no value.
* Feature: Added Product Prices option in Pro Version - Activate / Deactivate option. Custom font styling. Position options to display prices Top and Bottom | Top Only | Bottom Only.
* Feature: Added 'Add to Cart' options in Pro Version - Activate / Deactivate option. Added show Add to Cart as button or Linked text with full style options.
* Feature: Added 'successfully added' to cart icon in Pro Version. Displays after a Simple Product is added to cart. Default icon is a green tick, includes option to remove or upload a custom icon.
* Feature: Added custom 'View Cart' text link including text edit and font style options in Pro Version. The "View Cart' link shows under add to cart button when a Simple Product is added to the cart from the comparison table.
* Feature: Added support for External / Affiliate Products in Pro Version. Button or link shows the same text as the product page listing with link to external source.
* Feature: Added chosen script for select Compare categories on add and edit Compare features.
* Feature: Tested and optimization in Windows XP, IE 7, IE8, Windows 7, IE8 and IE9 and Windows 8, IE10 and IE10 Desktop.
* Feature: Tested and optimization for all 3 Windows operating systems - for these legacy browsers - Firefox, Safari, Chrome and Opera.
* Feature: Tested and optimization for Apple OS X operating systems. Snow leopard, Lion and Mountain Lion using these legacy Browsers - Firefox, Safari, Chrome and Opera 
* Feature: Tested and optimization for Apple IOS Mobile Safari in iPhones and all iPads.
* Feature: Tested and optimization for Android Browser on all models of these manufacturers tablets that use the Android operating system - Amazon Kindle Fire, Google Nexus 7, Samsung Galaxy Note, Samsung Galaxy Tab 2
* Feature: Tested and optimization for Android Browser on all models of these manufacturers phone that use the Android operating system (to many to list) mobile phones that support - Samsung Galaxy, Motorola, HTC, Sony and LG
* Feature: Tested and optimization for Opera Mobile - Samsung Galaxy Tablet and Mobiles HTC Wildfire. Nokia 5800, Samsung Galaxy S II, Motorola Droid X and Motorola Atrix 4G 
* Fixed: Full WP_DEG run. All Uncaught exceptions fixed.
* Fixed: Bug for users who have https: (SSL) on their sites wp-admin but have http on sites front end. This was causing a -1 to show when products added to Compare Widget wp-admin with SSL applied only allows https: but the url of admin-ajax.php is http: and it is denied hence returning the ajax -1 error. Fixed by writing a filter to recognize when https is configured on wp-admin and parsing correctly.  
* Tweak: Compare product page meta only shows open if the feature is activated.
* Tweak: Moved the Un-Assigned Compare Features on Features tab to the top of Compare categories for ease of use.
* Tweak: Updated admin error messages that displays when creating a Compare Category or Feature that already exists.
* Tweak: Updated admin error message that displays when plugin cannot connect to a3API on the Amazon cloud upon activation of the license.
* Tweak: Jumped version from 2.0.4 to 2.1.0 to keep in synch with Pro Version.
* Tweak: Updated plugin wiki docs to include new custom styling and layout features.

= 2.0.4 - 2013/02/25 =
* Feature: All plugin code updated to be 100% compatible with WooCommerce version 2.0 and backwards.
* Feature: Added PrettyPhoto pop-up tool script for the Compare products pop-up.
* Tweak: Updated plugin description and some support URL links.

= 2.0.3 - 2013/01/08 = 
* Feature: Added support for Chinese Characters
* Tweak: UI tweak - changed the order of the admin panel tabs so that the most used Features tab is moved to first tab.
* Tweak: Added links to all other a3rev wordpress.org plugins from the Features tab
* Tweak: Updated Support and Pro Version link URL's on wordpress.org description, plugins and plugins dashboard. Links were returning 404 errors since the launch of the all new a3rev.com mobile responsive site as the base e-commerce permalinks is changed.

= 2.0.2 - 2012/12/14 =
* Fixed: Updated depreciated custom database collator $wpdb->supports_collation() with new WP3.5 function $wpdb->has_cap( 'collation' ). �Supports backward version compatibility.
* Fixed: When Product attributes are auto created as Compare Features, if the Attribute has no terms then the value input field is created as Input Text - Single line instead of a Checkbox.
* Fixed: On Compare Products admin tab, ajax not able to load the products list again after saving a product edit

= 2.0.1 - 2012/08/14 =
* Changed - attributes terms where auto created as Compare Feature input type 'dropdown' (single select). Changed to input type 'check box' (multi-select)
* Documentation - Added comprehensive extension documentation to the [a3rev wiki](http://docs.a3rev.com/user-guides/woocommerce/compare-products/).
* Localization - Added Turkish translation (thanks to ManusH & Keremidi)
* Video - Added demo video to extensions home page
* Tweak: Set database table name to be created the same as WordPress table type
* Tweak - Change localization file path from actual to base path
* Tweak - Corrected typo on Compare pop-up window

= 2.0 - 2012/08/03 =
* Feature - All Product Categories and Sub Categories are auto created as Compare Categories when plugin is activated. Feature is activated on upgrade.
* Feature - All Product Parent Variations auto added to Master Category as Compare Features when the plugin is first activated.
* Feature - When Product Categories or Sub categories are created they are auto created as Compare categories. The plugin only listens to Create new so edits to Product categories are ignored.
* Feature: When parent product variations are created they are auto created as Compare Features. Child product variations and edits are ignored. 
* Feature - Complete rework of admin user interface - Combined Features and Categories tabs into a single tab - Added Products Tab. Greatly enhanced user experience and ease of use.
* Feature - Moved Create New Categories and Features to a single edit on-page assessable from an 'add new' button on Features tab.
* Feature - Added Features search facility for ease of finding and editing Compare Features.
* Feature - Added support for use of special characters in Feature Fields.
* Feature - Added support for use of Cyrillic Symbols in Feature Fields.
* Feature - Added support for use of sup tags in Feature Fields.
* Feature - Language file added to support localization � looking for people to do translations.
* Feature - Created plugin documents on a3rev wiki.
* Fixed - Can't create Compare Feature if user does not have a default value set in SQL. Changed INSERT INTO SQL command output to cater for this relatively rare occurrence.
* Tweak - Replaced all Category Edit | Delete icons with WordPress link text. Replace edit icon on Product Update table with text link.
* Tweak - Edited Product update table to fit 100% wide on page so that the horizontal scroll bar does not auto show.
* Tweak - Edited the way that Add Compare Features shows on product edit page - now same width as the page content.
* Tweak - Show Compare Featured fields on products page - added support for theme table css styling.
* Tweak - Adding padding between Product name and the Clear All - Compare button in sidebar widget.
* Tweak - Added yellow Pro update frame and dialogue box so its clear what features are activated on upgrade.
* Other - Create script to facilitate seamless upgrade from Version 1.04 to Major upgrade Version 2.0
* Other - Created and added WooCommerce Compare Products video to Wordpress plugins page

= 1.0.5 - 2012/05/22 =
* Feature: Set Compare Button position above or below and padding from the WooCommerce Add to Cart Button. 
* Fix: This feature is a workaround for site owners who use the WooCommerce Premium Catalog Visibility extension that removes the WooCommerce hook that Compare needs to show the button. Set the Compare button to show below the Add to Cart button in your theme and it will still show when that plugin removes the cart functionality.

= 1.0.4 - 2012/04/17 =
* Fixed: Print this page feature not working on Fly-Out screen
* Fixed: Column alignment problem on Features admin panel on smaller screens.
* Tweak: Re-designed admin panel style for improve UX and in line with the PRO version
* Tweak: Changed blue border in pop-up screen to square corners and gray colour.
* Tweak: Change alignment of Compare button to 'align right' in sidebar widget.
* Tweak: Code re-organized into folders with all files commented on and appropriate names as per WordPress Coding standards.

= 1.0.3 - 2012/04/05 =
* Tweak: Compare Settings page now on 2 tabs - SETTINGS | FEATURES in the same style as WooCommerce setting page for familiarity and greater ease of use.

= 1.0.2 - 2012/04/04 =
* Feature: Add default WooCommerce Fancybox  Fly-Out screen option. Retained Lightbox and now have option to use Fancybox or Lightbox to power Fly-Out window.
* Feature: Greatly improved the layout and ease of use of the single page admin panel.
* Feature: Added comprehensive admin page plugin setup instructions and added Tool Tips
* Fix : Auto add Compare Widget to sidebar when plugin is activated.
* Fix: Feature Unit of Measurement is added in brackets after Feature Name and if nothing added it does not show.
* Fix: Use $woocommerce->add_inline_js for inline javascript to add it to the footer (after scripts/fancybox are loaded) to prevent errors
* Tweak: Changed Fly-Out window from - include( '../../../wp-config.php') to show content using wp_ajax
* Tweak: Run WP_DEBUG and fix plugins 'undefined...' errors
* Tweak: Removed fading update messages and animation and replaced with default wordpress 'updated' messages.
* Tweak: Replace custom ajax handlers  with wp_ajax and wp_ajax_nopriv 
* Tweak: Add pop up window when deleting feature fields to ask you to check if you are sure? 

= 1.0.1 - 2012/03/22 =
* Tweak: Remove validation script never use for this plugin 

= 1.0.0 - 2012/03/15 =
* First working release of the modification


== Upgrade Notice ==

= 2.1.6 =
Upgrade your your plugin now for full compatibility with WordPress 3.6.0

= 2.1.4 =
Install this upgrade to fix colour picker bug in your plugin

= 2.1.3 =
Upgrade now to fix a backward compatibility issue with External / Affiliate Links.

= 2.1.2 =
This update includes 1 new activation feature and 2 fixes.