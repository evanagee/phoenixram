=== Woocomerce HTML5 Video ===

Contributors: webilop
Tags: woocomerce add-on, online store, product video, html5 video, mp4, ogg
Requires at least: 3.0.1
Tested up to: 3.5.1
Stable tag: 1.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Woocomerce HTML5 Video is a woocommerce add-on which allows you to add videos to your products in your online store. 
This plugin uses HTML5 to render videos in your products description page. The supported video formats are MP4 and Ogg.

= Localization =

*English (default).

*Spanish

*Persian

If you want to contribute with the localization of this plugin, you can send us your .mo and .po files to contact[at]webilop.com

= Documentation =

To access the plugin documentation in English, visit http://www.webilop.com/products/woocommerce-html5-video/
The documentation in Spanish is also availale at: http://www.webilop.com/es_ES/productos/woocommerce-html5-video-2/

== Installation ==

1. Install and activate the woocommerce plugin available at http://www.woothemes.com/woocommerce/
1. Upload the `woocommerce-html5-video` folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Create your products through 'the Products' menu option
4. Go to the Edit page of one of the created products. Under the 'Product Data' box, select the 'Video' optoin at the bottom.
Select the video source: Embedded code or uploaded video.
5. Update the product.

== Frequently Asked Questions ==

= What video format can I upload? =
The supported video formats are MP4 and Ogg.

= I cannot see the video in the frontend =
Make sure your browser supports HTML5 video. A list of browsers that have this feature can be found at http://www.w3schools.com/html/html5_video.asp

== Screenshots ==

1. screenshot1.jpg Illustrates the video options available in the 'Edit Product' pages in WP backend.

== Changelog ==

= 1.0 =
* First general availability plug-in version

= 1.1 =
* Settings page was added.

= 1.2 =
*Persian localization was added. Special thanks to Khalil Delavaran (khalil.delavaran[at]gmail.com) for this contribution.