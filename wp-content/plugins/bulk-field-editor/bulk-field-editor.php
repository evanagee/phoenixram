<?php /*

**************************************************************************
Plugin Name: Bulk Field Editor
Plugin URI: http://www.evanagee.com
Description: Allows you to edit your custom fields in bulk. Works with custom post types.
Author: Evan Agee
Version: 1.7
Author URI: http://www.evanagee.com
**************************************************************************/


//Init
add_action('admin_menu', 'bfe_init');
function bfe_init() {

	//Create or Set Settings
	global $bfe_post_types;
	$bfe_post_types = get_option("bfe_post_types");

	//Check for Double Serialization
	if (is_serialized($bfe_post_types)) {
		$bfe_post_types = unserialize($bfe_post_types);
		update_option("bfe_post_types", $bfe_post_types);
	}

	//Create Settings if New
	if (!is_array($bfe_post_types)) bfe_create_settings();

	//Create Menus
	$post_types = get_post_types();
	$skip_array = array("revision", "attachment", "nav_menu_item");
	foreach ($post_types as $post_type ) {
		if (in_array($post_type, $skip_array)) continue;
		if (in_array($post_type, $bfe_post_types)) add_submenu_page("edit.php".($post_type != "post" ? "?post_type=".$post_type : ""), __('Bulk Edit Fields'), __('Bulk Edit Fields'), apply_filters('bfe_menu_display_' . $post_type, 'manage_options'), 'bfe_editor-'.$post_type, 'bfe_editor');
	}

	if (isset($_REQUEST['page'])) {
		if (strpos($_REQUEST['page'], "bfe_editor-") !== false) {
			wp_enqueue_style('bfe-style', WP_PLUGIN_URL."/bulk-field-editor/bfe-style.css");
		}
	}
}


//Main Editor Menu
function bfe_editor() { }


//Save Custom Field
add_action('admin_init', 'bfe_save');
function bfe_save() {
	global $post_id, $wpdb;

	//Bail if not called or authenticated
	$actionkey = (isset($_POST['bfe_save']) ? $_POST['bfe_save'] : "");
	if ($actionkey != "1" || !check_admin_referer('bfe-save')) return;

	//Setup
	$post_type = (isset($_POST['bfe_post_type']) ? $_POST['bfe_post_type'] : "");
	$posts = (isset($_POST['post']) ? $_POST['post'] : array());
	$edit_mode = $_POST['edit_mode'] == "multi" ? "multi" : "single";

	//Multi-value Method Array Setup
	$multi_value_mode = isset($_POST['multi_value_mode']) ? $_POST['multi_value_mode'] : 'single';
	$arr_names = array();
	$arr_values = array();
	if ($multi_value_mode == "bulk") {
		$lines1 = preg_split("/(\r\n|\n|\r)/", trim($_POST['multi_bulk_name']));
		$lines2 = preg_split("/(\r\n|\n|\r)/", trim($_POST['multi_bulk_value']));
		for ($i = 0; $i < count($lines1); $i++) {
			$arr_names[$i] = isset($lines1[$i]) ? $lines1[$i] : '';
			$arr_values[$i] = isset($lines2[$i]) ? $lines2[$i] : '';
		}
	}

	//Loop Through Each Saved Post
	$current_record_count = 0;
	foreach ($posts AS $post) {
		$post_id = $post;

		//Multi Value
		if ($edit_mode == "multi") {

			//Bulk Edit Mode
			if ($multi_value_mode == "bulk") {

				$skip = 0;
				if (!isset($lines1[$current_record_count]) || !isset($lines2[$current_record_count])) $skip = 1;
				if (!$skip) if (!$lines1[$current_record_count] || !$lines2[$current_record_count]) $skip = 1;
				if (!$skip) {
					//echo $post_id . " write: " . $arr_names[$current_record_count] . " = " . $arr_values[$current_record_count] . "<br>\n";
					bfe_save_meta_data($arr_names[$current_record_count], $arr_values[$current_record_count]);
				}



			//Multi-edit Mode
			} elseif (!empty($_POST['bfe_multi_fieldname_'.$post_id])) {
				//echo 'EDIT ' . $post_id . ': ' . $_POST['bfe_multi_fieldname_'.$post_id] . ' = ' . $_POST['bfe_multi_fieldvalue_'.$post_id] . '<br />';
				bfe_save_meta_data($_POST['bfe_multi_fieldname_'.$post_id], $_POST['bfe_multi_fieldvalue_'.$post_id]);
			}


		//Single Value
		} else {

			do_action('bfe_save_fields', $post_type, $post_id);
			for($i=1; $i<=$_POST['bfe_current_max']; $i++) {
				if (!empty($_POST['bfe_name_'.$i])) {
					//echo 'EDIT ' . $post_id . ': ' . $_POST['bfe_name_'.$i] . ' = ' . $_POST['bfe_value_'.$i] . '<br />';
					bfe_save_meta_data($_POST['bfe_name_'.$i], $_POST['bfe_value_'.$i]);
				}
			}
		}

		//Change Field Name
		if (isset($_POST['bfe_fieldname_1']) && isset($_POST['bfe_fieldname_2'])) {
			if ($_POST['bfe_fieldname_1'] && $_POST['bfe_fieldname_2']) {
				$sql = "UPDATE $wpdb->postmeta SET meta_key = '" . mysql_real_escape_string($_POST['bfe_fieldname_2']) . "' WHERE post_id = $post_id AND meta_key = '" . mysql_real_escape_string($_POST['bfe_fieldname_1']) . "'";
				$wpdb->query("UPDATE $wpdb->postmeta SET meta_key = '" . mysql_real_escape_string($_POST['bfe_fieldname_2']) . "' WHERE post_id = $post_id AND meta_key = '" . mysql_real_escape_string($_POST['bfe_fieldname_1']) . "'");
			}
		}

		$current_record_count++;
	}

	$post_link = $post_type != "post" ? "post_type=$post_type&" : "";
	$bfe_add_new_values = isset($_POST['bfe_add_new_values']) ? '&bfe_add_new_values=1' : '';
	header("Location: edit.php?" . $post_link . "page=bfe_editor-$post_type&edit_mode=$edit_mode&saved=1&multi_value_mode=" . $multi_value_mode . $bfe_add_new_values);
	die;
}


//Settings Menu
add_action('admin_menu', 'bfe_settings_menu');
function bfe_settings_menu() {
	add_submenu_page('options-general.php', __('Bulk Field Editor Settings'), __('Bulk Field Editor Settings'), 'manage_options', 'bfe_settings', 'bfe_settings');
}
function bfe_settings() {
	global $bfe_post_types;

	echo '<div class="wrap">';
	echo '<div class="icon32" id="icon-options-general"><br></div>';
	echo '<h2>' . __('Bulk Field Editor Settings') . '</h2>';

	//Saved Notice
	if (isset($_GET['saved'])) echo '<div class="updated"><p>' . __('Your Settings Have Been Saved.') . '</p></div>';

	echo '<form action="options.php" name="bfe_form_1" id="bfe_form_1" method="post">';
	echo '<input type="hidden" name="bfe_settings_save" value="1" />'."\n";
	wp_nonce_field('bfe-settings-save');

	?>
	<br />

	<table class="widefat bfe_table">
		<thead>
			<tr>
				<th><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAAAsSAAALEgHS3X78AAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAA0RJREFUeNo8k8trnFUchp9zzjfJhMxkZqBJGh3NxVprbk1DFSGJWg1utXYliGIC2iglVSi4cCEYUHBhQoMu2qLoQrEgtP+AeEWULmpajZmmSZrbTM2k6SSd23d+57io6bt/H3gWj/Les7uJqck9wOZ74yfdx599+nM8FhuIx+MUCoXy2Cuv1k1MTRorfs/777yd2/2oXcDE1OQ+Y8xfCnasyLAx5sfRN16vB/ji7DmM1s+UyuUzJjAPxurqB06MjPxxDzAxNdlhjJk9+uLRyOyVK2SuL7jWdFrvbWpGa1jL5lheXaOjrbXyaHd37cULF3Bie989MT4TAGith40xwfqNFVKJFI/3J7X34LzDi6K5sZGmxkaA2uzyMiYwVKrh08DMPYUPp09fS7e0PHR/y32gwAPee8RagiCCUnedV9fX2dzakvGR0QBAfTD5SQSIaK3z/b29UWMMALdu32Ytm60opQpG62TrA+lItDaKtZY/r14l0dDQtLiyVtRa63w8Ftvu7umOesCKUCqXuL6wWAnDMD0+MtpUKpefXVpeCa0IoOjq6qJaDf+J1gbbGtAdbe1aicdawYrlTrGI937u1PGxDYBTx8d+siLFahgiTvDiaG9rS3nxSnvQ67kshZ0CVgQrgjEBSqv2s998HQH4/Py3nUCd8x5rLdt3tsnezOE0BE4kVROJ1C0uLm3sf3i/UQq00SQTifp8frPw0fT0DpBsiMcCsRYPLCwt0fXIgVRgDMHBzs6KE1+54VcXNvIb+1KpFApIJZMqFo9HrbXRmkgEow0iwq2tLWojNZKqT2wl6urRDs+lmcs9Ym1HPB5HxP2v4lBAJAjw3mPFYp0jFotRKpfM97//MnRkaBDtQ4f3/oC1VqwVqmGFbC6HiMU5hziHtUIulyMMQ0SEMLTFYrHcDqAFT39Pz3kPo3OZOZeZy4Sb+fx3f8/OumoY4sSRuZahWC5fymQyW/Pz806hTg4PPfUlgA5tFRQ8dujQV2JtsxVJHO7rO2aM0UoprFgAnjjYd9h5ly5VKukjA4Nnnnty8G6NK2vr/PDbr2hjeOn5F9qAGLD3tbfefLm5peUYSql/b2YvnpuaPg1sAzve+8XdnP8bADKEsbGi0fzfAAAAAElFTkSuQmCC" alt="" /><?php _e('Enable on Post Types'); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php
			$post_types = get_post_types();
			$skip_array = array("revision", "attachment", "nav_menu_item");
			foreach ($post_types as $post_type ) :
				if (in_array($post_type, $skip_array)) continue;
				$obj = get_post_type_object($post_type);
			?>
			<tr>
				<td>
					<input type="checkbox" name="bfe_post_type_<?php echo $post_type; ?>" id="bfe_post_type_<?php echo $post_type; ?>"<?php if (in_array($post_type, $bfe_post_types)) echo ' checked="checked"'; ?> />
					<label for="bfe_post_type_<?php echo $post_type; ?>"><?php echo $obj->labels->name; ?></label>
					<div style="clear: both;"></div>

				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<p><input type="submit" class="button-primary" value="<?php _e('Save Settings'); ?>" /></p>
	</form>

	<div style="clear: both;"></div>
	<?php
}

//Save Settings
add_action('admin_init', 'bfe_save_settings');
function bfe_save_settings() {

	//Bail if not called or authenticated
	$actionkey = (isset($_POST['bfe_settings_save']) ? $_POST['bfe_settings_save'] : "");
	if ($actionkey != "1" || !check_admin_referer('bfe-settings-save')) return;

	//Save Settings
	$bfe_post_types = array();
	$post_types = get_post_types();
	$skip_array = array("revision", "attachment", "nav_menu_item");
	foreach ($post_types as $post_type ) {
		if (isset($_POST['bfe_post_type_'.$post_type])) $bfe_post_types[] = $post_type;
	}
	update_option("bfe_post_types", $bfe_post_types);

	header("Location: options-general.php?page=bfe_settings&saved=1");
	die;
}


//Saving Functions
function bfe_save_meta_data($fieldname,$input) {
	global $post_id;
	$current_data = get_post_meta($post_id, $fieldname, TRUE);
 	$new_data = $input;
 	if (!$new_data || $new_data == "") $new_data = NULL;
 	bfe_meta_clean($new_data);

	if ($current_data && is_null($new_data)) {
		delete_post_meta($post_id,$fieldname);
	} elseif ($current_data && !isset($_POST['bfe_add_new_values'])) {
		update_post_meta($post_id,$fieldname,$new_data);
	} elseif (!is_null($new_data)) {
		add_post_meta($post_id,$fieldname,$new_data);
	}
}

function bfe_meta_clean(&$arr) {
	if (is_array($arr)) {
		foreach ($arr as $i => $v) {
			if (is_array($arr[$i]))  {
				bfe_meta_clean($arr[$i]);
				if (!count($arr[$i])) unset($arr[$i]);
			} else  {
				if (trim($arr[$i]) == '') unset($arr[$i]);
			}
		}
		if (!count($arr)) $arr = NULL;
	}
}


//Display Settings Link on Plugin Screen
add_filter('plugin_action_links', 'bfe_plugin_action_links', 10, 2);
function bfe_plugin_action_links($links, $file) {
	static $this_plugin;
	if (!$this_plugin) $this_plugin = "bulk-field-editor/bulk-field-editor.php";
	if ($file == $this_plugin) {
		$settings_link = '<a href="' . get_bloginfo('wpurl') . '/wp-admin/options-general.php?page=bfe_settings">Settings</a>';
		array_unshift($links, $settings_link);
	}
	return $links;
}

//Create Settings for Post Type Selection
function bfe_create_settings() {
	global $bfe_post_types;
	$bfe_post_types = array();
	$post_types = get_post_types();
	$skip_array = array("revision", "attachment", "nav_menu_item");
	foreach ($post_types as $post_type ) {
		if (in_array($post_type, $skip_array)) continue;
		$bfe_post_types[] = $post_type;
	}
	update_option("bfe_post_types", $bfe_post_types);
}

/** Step 2 (from text above). */
add_action( 'admin_menu', 'bfe_menu' );
/** Step 1. */
function bfe_menu() {
	global $bfe_post_types;
	//print_r($bfe_post_types);
	
	//add_submenu_page('edit.php?post_type=product', __('Category Data'), __('Category Data'), 'get_category_data', 'bfe_category_data', 'bfe_category_data');

	add_management_page( __('Category Data'), __('Category Data'), 'manage_options', 'bfe_categories', 'bfe_category_data');
}

/** Step 3. */
function bfe_category_data() {
	global $bfe_post_types;
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	//list terms in a given taxonomy using wp_list_categories (also useful as a widget if using a PHP Code plugin)

	$taxonomy     = 'product_cat';
	$orderby      = 'id';
	$show_count   = 1;      // 1 for yes, 0 for no
	
	$args = array(
	  'taxonomy'     => $taxonomy,
	  'orderby'      => $orderby,
	  'show_count'   => $show_count,
	  'hide_empty' => 0
	);
	
	echo '<h1>Category Data</h1><table class="wp-list-table widefat fixed tags ui-sortable" width="100%">
			<thead>
				<tr>
					<th align="left">Name</th>
					<th align="left">ID</th>
					<th align="left">Parent</th>
					<th align="left">Count</th>
				</tr>
			</thead>';
	$list = get_categories( $args );
	
	foreach ($list as $cat) {
		if ($cat->parent !== '0') {
			//echo '&nbsp&mdash;';
		} else {
			//echo '</td><td>';
		}
		echo '<tr>' ;
			
			if ($cat->parent == '0') {
				echo '<td><b><a href="/wp-admin/edit-tags.php?action=edit&taxonomy=product_cat&tag_ID=' . $cat->term_id . '&post_type=product">' . $cat->name . '</a></b></td>';
			} else {
				echo '<td>&nbsp&mdash; <a href="/wp-admin/edit-tags.php?action=edit&taxonomy=product_cat&tag_ID=' . $cat->term_id . '&post_type=product">' . $cat->name . '</a></td>';
			}
			
		echo '<td>' . $cat->term_id . '</td>
			<td>' . $cat->parent . '</td>
			<td>' . $cat->count . '</td>
		</tr>';
	}
	//print_r($list);
	echo '
		<tfoot>
			<tr>
				<th align="left">Name</th>
				<th align="left">ID</th>
				<th align="left">Parent</th>
				<th align="left">Count</th>
			</tr>
		</tfoot>
	</table>';
}