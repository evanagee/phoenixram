<div id="nsu-sidebar">

	<div id="nsu-donate-box">
			<h3>Donate $10, $20 or $50</h3>
			<p>If you like Newsletter Sign-Up and the free time I put into it, please consider donating a token of your appreciation.</p>
					
			<form class="donate" action="https://www.paypal.com/cgi-bin/webscr" method="post">
                <input type="hidden" name="cmd" value="_donations">
                <input type="hidden" name="business" value="AP87UHXWPNBBU">
                <input type="hidden" name="lc" value="US">
                <input type="hidden" name="item_name" value="Danny van Kooten">
                <input type="hidden" name="item_number" value="Newsletter Sign-Up">
                <input type="hidden" name="currency_code" value="USD">
                <input type="hidden" name="bn" value="PP-DonationsBF:btn_donateCC_LG.gif:NonHosted">
                <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                <img alt="" border="0" src="https://www.paypalobjects.com/nl_NL/i/scr/pixel.gif" width="1" height="1">
            </form>

			<p>Alternatively, you can: </p>
            <ul>
                <li><a href="http://wordpress.org/support/view/plugin-reviews/newsletter-sign-up?rate=5#postform" target="_blank">Give a 5&#9733; rating on WordPress.org</a></li>
                <li><a href="http://dannyvankooten.com/wordpress-plugins/newsletter-sign-up/" target="_blank">Blog about it and link to the plugin page</a></li>
                <li><a href="http://twitter.com/?status=I%20manage%20my%20%23WordPress%20sign-up%20forms%20using%20%40DannyvanKooten%20%27s%20Newsletter%20Sign-Up%20plugin%20and%20I%20love%20it%20-%20check%20it%20out!%20http%3A%2F%2Fwordpress.org%2Fplugins%2Fnewsletter-sign-up%2F" target="_blank">Tweet about Newsletter Sign-Up</a></li>
            </ul>
    </div>

	<div>
		<h3>Other WordPress plugins by Danny</h3>
		<ul class="">
			<li class="mc4wp"><a href="http://dannyvankooten.com/wordpress-plugins/mailchimp-for-wordpress/">MailChimp for WordPress</a></li>
			<li><a href="http://dannyvankooten.com/wordpress-plugins/wysiwyg-widgets/">WYSIWYG Widgets</a></li>
			<li class="rfb"><a href="http://dannyvankooten.com/wordpress-plugins/recent-facebook-posts/">Recent Facebook Posts</a></li>

			<li class="dvk-email"><a href="http://dannyvankooten.com/newsletter/">Subscribe to Danny's newsletter</a> by e-mail</li>
			<li class="dvk-twitter">You should follow me on twitter <a href="http://twitter.com/dannyvankooten">here</a></li>
		</ul>
	</div>

	<div>
		<h3>Need support?</h3>
		<p>Please use the <a href="http://wordpress.org/support/plugin/newsletter-sign-up">WordPress.org support forums for Newsletter Sign-Up</a>.</p>
	</div>
      			
</div>