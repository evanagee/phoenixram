<?php
/**
 * WPXDeflector is the main class of this plugin.
 * This class extends WPDKWordPressPlugin in order to inherits several WordPress functions.
 *
 * @class              WPXDeflector
 * @author             wpXtreme <info@wpxtre.me>
 * @copyright          Copyright (C) wpXtreme <info@wpxtre.me>.
 * @date               2012-11-28
 * @version            1.0.0
 *
 */
final class WPXDeflector extends WPXPlugin {

  //---------------------------------------------------------------------------
  // PROPERTIES
  //---------------------------------------------------------------------------

  /**
   * Deflector kernel configuration
   *
   * @brief Deflector kernel configuration
   *
   * @var WPXDeflectorConfiguration $_config
   *
   * @since 1.0.0
   */
  private $_config;

  /**
   * Deflector whole rules environment. It is an array, structured in this way:
   *
   *   $rulesEnvironment['tab_instance'] = tab object instance
   *     $rulesEnvironment['tab_instance'][0..n] = collection of rules
   *
   * @brief Deflector whole rules environment
   *
   * @var array $rulesEnvironment
   *
   * @since 1.0.0
   */
  public $rulesEnvironment;

  /**
   * Counter of rules with status = OFF - used in showing badge
   *
   * @brief Counter of rules with status = OFF
   *
   * @var int $numberOfRulesOFF;
   *
   * @since 1.0.0
   */
  public $numberOfRulesOFF;

  //---------------------------------------------------------------------------
  // METHODS
  //---------------------------------------------------------------------------

  /**
   * Create and return a singleton instance of WPXDeflector class
   *
   * @brief Init
   *
   * @param string $file The main file of this plugin. Usually __FILE__ (main.php). If missing, this function is used only
   *                     to get current instance, if it exists.
   *
   * @return WPXDeflector
   */
  public static function boot( $file = '' ) {
    static $instance = null;
    if( is_null( $instance ) && ( ! empty( $file )) ) {
      $instance = new WPXDeflector( $file );
      do_action( __CLASS__ );
    }
    return $instance;
  }

  /**
   * Return the singleton instance of WPXDeflector class
   *
   * @brief Return the singleton instance of WPXDeflector class
   *
   * @return WPXDeflector|NULL
   */
  public static function getInstance() {
    return self::boot();
  }

  /**
   * Create an instance of WPXDeflector class
   *
   * @brief Construct
   *
   * @param string $file The main file of this plugin. Usually __FILE__ (main.php)
   *
   * @return WPXDeflector object instance
   */
  public function __construct( $file ) {

    parent::__construct( $file );

    $this->_defines();

    // Build environment of autoload classes - this is ALWAYS the first thing to do
    $this->_registerClasses();

    // Get configuration
    $this->_config = WPXDeflectorConfiguration::init();

    // Build ALL rules MVC environment
    $this->numberOfRulesOFF = 0;
    $this->_buildDeflectorRulesEnvironment();

  }

  /**
   * Include the external defines file
   *
   * @brief Defines
   */
  private function _defines() {
    include_once( 'defines.php' );
  }

  /**
   * Build the entire Basic Shields Rules Model
   *
   * @brief Build the entire Basic Shields Rules Model
   *
   * @since 1.0.0
   *
   */
  private function _initBasicShields() {

    //--------------------------------------------------------------------------
    // INIT BASIC SHIELDS RULE 1
    //--------------------------------------------------------------------------

    // Basic Shields rule 1 model
    $bsRule1 = new WPXDeflectorBasicShieldsRule1(
      'wpxdeflector-basic-shields-rule-1',
      __( 'Make your WordPress admin area inaccessible to unknown users.', WPXDEFLECTOR_TEXTDOMAIN )
    );

    // If rule status is OFF, increment Deflector rule counter in order to show Deflector badge with proper value
    if( WPXDeflectorRule::RULE_OFF == $bsRule1->status() ) {
      $this->numberOfRulesOFF++;
    }

    // Set rule help
    $bsRule1->help(
      'wpxdevcenter://make-your-wordpress-admin-area-inaccessible-to-unknown-users',
      __('wpXtreme Deflector Basic Shields - Info about this rule', WPXDEFLECTOR_TEXTDOMAIN )
    );

    // Hook rule execution to proper WordPress action - the method engages execution internally only if rule status = ON
    $bsRule1->hookExecutionToAction( 'init' );

    //--------------------------------------------------------------------------
    // INIT BASIC SHIELDS RULE 2
    //--------------------------------------------------------------------------

    // Basic Shields rule 2 model
    $bsRule2 = new WPXDeflectorBasicShieldsRule2(
      'wpxdeflector-basic-shields-rule-2',
      __( 'Make your WordPress admin area inaccessible to users having specific WordPress roles.', WPXDEFLECTOR_TEXTDOMAIN )
    );

    // If rule status is OFF, increment Deflector rule counter in order to show Deflector badge with proper value
    if( WPXDeflectorRule::RULE_OFF == $bsRule2->status() ) {
      $this->numberOfRulesOFF++;
    }

    // Set rule help
    $bsRule2->help(
      'wpxdevcenter://make-your-wordpress-admin-area-inaccessible-to-users-having-specific-wordpress-roles',
      __('wpXtreme Deflector Basic Shields - Info about this rule', WPXDEFLECTOR_TEXTDOMAIN )
    );

    // Hook rule execution to proper WordPress action - the method engages execution internally only if rule status = ON
    $bsRule2->hookExecutionToAction( 'init' );

    //--------------------------------------------------------------------------
    // BASIC SHIELDS RULES COLLECTION
    //--------------------------------------------------------------------------

    // Create admin area basic shields collection
    $bsAdminAreaCollection = new WPXDeflectorRulesCollection(
      'wpxdeflector-basic-shields-admin-area-rules',
      __( 'Secure your WordPress admin area', WPXDEFLECTOR_TEXTDOMAIN )
    );

    // Set description for admin area basic shields collection
    $bsAdminAreaCollection->description( __( 'Here you can activate/deactivate some basic security rules in your WordPress installation, in order to protect your admin area. For any details, we suggest you to read detailed info about the rules of your interest.', WPXDEFLECTOR_TEXTDOMAIN ) );

    // Add Basic Shields rule 1 to admin area collection
    $bsAdminAreaCollection->addRule( $bsRule1 );

    // Add Basic Shields rule 2 to admin area collection
    $bsAdminAreaCollection->addRule( $bsRule2 );

    //--------------------------------------------------------------------------
    // BASIC SHIELDS TAB
    //--------------------------------------------------------------------------

    // Create basic-shields tab
    $keyTab                                   = 'wpxdeflector-basic-shields';
    $this->rulesEnvironment[$keyTab]['title'] = __( 'Basic Shields', WPXDEFLECTOR_TEXTDOMAIN );

    // Add rules collections to this tab
    $this->rulesEnvironment[$keyTab]['rules-collection'][]  = $bsAdminAreaCollection;

  }

  /**
   * Build the entire Environment Scanning Rules Model
   *
   * @brief Build the entire Environment Scanning Rules Model
   *
   * @since 1.0.0
   *
   */
  private function _initEnvironmentScanning() {

    //--------------------------------------------------------------------------
    // INIT DEFLECTOR TIPS RULE 1
    //--------------------------------------------------------------------------

    // Deflector Tips rule 1 model
    $tipsRule1 = new WPXDeflectorTipsRule1(
      'wpxdeflector-tips-rule-1',
      __( 'Check database username.', WPXDEFLECTOR_TEXTDOMAIN )
    );

    // This is a CHECK_ONLY rule: execute rule check DIRECTLY AND BEFORE handling the Deflector global badge
    $tipsRule1->execute();

    // If rule status is OFF, increment Deflector rule counter in order to show Deflector badge with proper value
    // Also set properly help message
    $helpInfo = __( 'Your Wordpress environment is working with a secure database username.', WPXDEFLECTOR_TEXTDOMAIN );
    if( WPXDeflectorRule::RULE_OFF == $tipsRule1->status() ) {
      $this->numberOfRulesOFF++;
      $helpInfo = __( 'Your Wordpress environment is working with an insecure database username.<br/><br/>Maybe you\'re using a database user account that has many privileges and can do many undesirable things within your DB and with your data; or you\'re using a database username too short. So it is <strong>STRONGLY RECOMMENDED</strong> that you change <em>AS SOON AS POSSIBLE</em> your internal database credential for your Wordpress environment, in order to change this database user. Please contact your technical administrator for details about how to proceed.', WPXDEFLECTOR_TEXTDOMAIN );
    }

    // Set rule help
    $tipsRule1->help( $helpInfo, __('wpXtreme Deflector Tips - Info about this rule', WPXDEFLECTOR_TEXTDOMAIN ) );

    //--------------------------------------------------------------------------
    // INIT DEFLECTOR TIPS RULE 2
    //--------------------------------------------------------------------------

    // Deflector Tips rule 2 model
    $tipsRule2 = new WPXDeflectorTipsRule2(
      'wpxdeflector-tips-rule-2',
      __( 'Check database password.', WPXDEFLECTOR_TEXTDOMAIN )
    );

    // This is a CHECK_ONLY rule: execute rule check DIRECTLY AND BEFORE handling the Deflector global badge
    $tipsRule2->execute();

    // If rule status is OFF, increment Deflector rule counter in order to show Deflector badge with proper value
    // Also set properly help message
    $helpInfo = __( 'Your Wordpress environment is working with a secure database password.', WPXDEFLECTOR_TEXTDOMAIN );
    if( WPXDeflectorRule::RULE_OFF == $tipsRule2->status() ) {
      $this->numberOfRulesOFF++;
      $helpInfo = __( 'Your Wordpress environment is working with an insecure database password.<br/><br/>This can lead to very serious security problems: a malicious user could access to your WordPress database, and be granted all privileges your WordPress has ( changing and deleting posts, pages, users, ecc. ). So it is <strong>STRONGLY RECOMMENDED</strong> that you change <em>AS SOON AS POSSIBLE</em> your internal database credential for your Wordpress environment, in order to create a strong and secure password for your database user. We suggest you to create a database password almost 8 characters long, with letters, digits, and one or more character not in these set ( for example \'#\', or \'=\' ). If you don\'t know how to set a new database password, please contact your technical administrator for details about how to proceed.', WPXDEFLECTOR_TEXTDOMAIN );
    }

    // Set rule help
    $tipsRule2->help( $helpInfo, __('wpXtreme Deflector Tips - Info about this rule', WPXDEFLECTOR_TEXTDOMAIN ) );

    //--------------------------------------------------------------------------
    // INIT DEFLECTOR TIPS RULE 3
    //--------------------------------------------------------------------------

    // Deflector Tips rule 3 model
    $tipsRule3 = new WPXDeflectorTipsRule3(
      'wpxdeflector-tips-rule-3',
      __( 'Check global database prefix of WordPress tables.', WPXDEFLECTOR_TEXTDOMAIN )
    );

    // This is a CHECK_ONLY rule: execute rule check DIRECTLY AND BEFORE handling the Deflector global badge
    $tipsRule3->execute();

    // If rule status is OFF, increment Deflector rule counter in order to show Deflector badge with proper value
    // Also set properly help message
    $helpInfo = __( 'Your Wordpress environment is working with a more secure global database prefix than default.', WPXDEFLECTOR_TEXTDOMAIN );
    if( WPXDeflectorRule::RULE_OFF == $tipsRule3->status() ) {
      $this->numberOfRulesOFF++;
      $helpInfo = __( 'Your Wordpress environment is using the default table prefix that WordPress defines for its database tables.<br/><br/>This can lead to security problems, because hackers know this default table prefix and thus can use it from its malicious purposes. It is recommended that you change this table prefix with a different, less obvious prefix, in order to further enhance your security. We suggest you to create a new table prefix with a complex, not easy to catch, sequence of characters. Please contact your technical administrator for details about how to change this prefix.', WPXDEFLECTOR_TEXTDOMAIN );
    }

    // Set rule help
    $tipsRule3->help( $helpInfo, __('wpXtreme Deflector Tips - Info about this rule', WPXDEFLECTOR_TEXTDOMAIN ) );

    //--------------------------------------------------------------------------
    // INIT DEFLECTOR TIPS RULE 4
    //--------------------------------------------------------------------------

    // Deflector Tips rule 4 model
    $tipsRule4 = new WPXDeflectorTipsRule4(
      'wpxdeflector-tips-rule-4',
      __( 'Check auto-organization year/month option about media files.', WPXDEFLECTOR_TEXTDOMAIN )
    );

    // This is a CHECK_ONLY rule: execute rule check DIRECTLY AND BEFORE handling the Deflector global badge
    $tipsRule4->execute();

    // If rule status is OFF, increment Deflector rule counter in order to show Deflector badge with proper value
    // Also set properly help message
    $helpInfo = __( 'Your Wordpress environment is using a more secure handling of uploaded media directories.', WPXDEFLECTOR_TEXTDOMAIN );
    if( WPXDeflectorRule::RULE_OFF == $tipsRule4->status() ) {
      $aUploadDir = wp_upload_dir();
      $this->numberOfRulesOFF++;
      $helpInfo = __('Your Wordpress environment is using the auto-organization year/month option in storing uploaded media files.<br/><br/>This can lead to security problems, because every single directory created by WordPress in upload phase could have additional write permissions that can be used by malicious users to hack your system. It is recommended that you remove this auto-organization option, unchecking <em>Organize my uploads into month- and year- based folders</em> option in <strong>Settings->Media</strong> page. Please contact your technical administrator for further details.', WPXDEFLECTOR_TEXTDOMAIN ) .
                  '<br/><br/>' .
                  sprintf( __( 'NOTE: if this option is unchecked, then all media files will be uploaded <strong>only</strong> in directory <em>%s</em>. This behaviour does not affect in any way normal WordPress working, and files uploaded before this new setting.', WPXDEFLECTOR_TEXTDOMAIN ), trailingslashit( $aUploadDir['baseurl'] ) );
    }

    // Set rule help
    $tipsRule4->help( $helpInfo, __('wpXtreme Deflector Tips - Info about this rule', WPXDEFLECTOR_TEXTDOMAIN ) );

    //--------------------------------------------------------------------------
    // TIPS RULES COLLECTION
    //--------------------------------------------------------------------------

    // Create admin area tips collection
    $tipsCollection = new WPXDeflectorRulesCollection(
      'wpxdeflector-tips-admin-area-rules',
      __( 'Basic configuration scanning of your WordPress admin area', WPXDEFLECTOR_TEXTDOMAIN )
    );

    // Set description for admin area tips collection
    $tipsCollection->description( __( 'This is the result of basic configuration scanning of your WordPress admin area. For any details, we suggest you to read detailed info about the checking line of your interest.', WPXDEFLECTOR_TEXTDOMAIN ) );

    // Add tips rules to tips collection
    $tipsCollection->addRule( $tipsRule1 );
    $tipsCollection->addRule( $tipsRule2 );
    $tipsCollection->addRule( $tipsRule3 );
    $tipsCollection->addRule( $tipsRule4 );

    //--------------------------------------------------------------------------
    // ENVIRONMENT SCANNING TAB
    //--------------------------------------------------------------------------

    // Create config-scanning tab
    $keyTab                                   = 'wpxdeflector-environment-scanning';
    $this->rulesEnvironment[$keyTab]['title'] = __( 'Environment Scanning', WPXDEFLECTOR_TEXTDOMAIN );

    // Add rules collections to this tab
    $this->rulesEnvironment[$keyTab]['rules-collection'][]  = $tipsCollection;

  }

  /**
   * Build the entire Theme Cleaning Rules Model
   *
   * @brief Build the entire Theme Cleaning Rules Model
   *
   * @since 1.0.0
   *
   * @note Any theme cleaning rules does not increment global badge in case of status = OFF.
   *
   */
  private function _initThemeCleaning() {

    //--------------------------------------------------------------------------
    // INIT THEME CLEANING RULE 3
    //--------------------------------------------------------------------------

    $tcRule3 = new WPXDeflectorThemeCleaningRule3(
      'wpxdeflector-theme-cleaning-rule-3',
      __( 'Hide the link to the RSD ( Really Simple Discovery ) service endpoint from HTML HEAD section.', WPXDEFLECTOR_TEXTDOMAIN )
    );

    // Set rule help
    if( WPXDeflectorRule::RULE_OFF == $tcRule3->status() ) {
      $this->numberOfRulesOFF++;
      $helpInfo = __( 'A common mechanism for XML-RPC clients to find out information about your WordPress environment is to look for the Really Simple Discovery ( RSD ) URL. The RSD contains information about the available APIs that WordPress supports. Your Wordpress theme are showing this RSD URL in the HEAD section of any page/post HTML source code.<br/><br/>By activating this rule, you can hide this information in any theme page/post, if you don\'t need it; this operation is safe and increases your security.', WPXDEFLECTOR_TEXTDOMAIN );
    }
    else {
      $helpInfo = __( 'Your Wordpress theme are hiding the RSD URL in the HEAD section of any page/post HTML source code. This is a behaviour that increases your security. Deactivate this rule if you want to restore default.', WPXDEFLECTOR_TEXTDOMAIN ) .
                  '<br/><br/>' .
                  __( '<strong>WARNING</strong>: if this rule remains active even after deactivation, it means that another plugin, or another function in your WordPress environment, is activating this rule again.', WPXDEFLECTOR_TEXTDOMAIN );
    }
    $tcRule3->help( $helpInfo, __('wpXtreme Deflector Tips - Info about this rule', WPXDEFLECTOR_TEXTDOMAIN ) );

    // Hook rule execution to proper WordPress action - the method engages execution internally only if rule status = ON
    $tcRule3->hookExecutionToAction( 'after_setup_theme' );

    //--------------------------------------------------------------------------
    // INIT THEME CLEANING RULE 6
    //--------------------------------------------------------------------------

    $tcRule6 = new WPXDeflectorThemeCleaningRule6(
      'wpxdeflector-theme-cleaning-rule-6',
      __( 'Hide the meta generator tag with WordPress version from HTML HEAD section.', WPXDEFLECTOR_TEXTDOMAIN )
    );

    // If this rule is active
    if( WPXDeflectorRule::RULE_ON == $tcRule6->status() ) {
      $helpInfo = __('Your Wordpress theme are hiding WordPress XHTML meta generator tag containing current WordPress version. This is a behaviour that increases your security. Deactivate this rule if you want to restore default.', WPXDEFLECTOR_TEXTDOMAIN );
    }
    else {
      $this->numberOfRulesOFF++;
      $helpInfo = __('In any page/post HTML source code, and also in RSS Feed, the WordPress XHTML meta generator tag displays your website\'s WordPress version for anyone to see. This can be a security risk for your website, because an attacker can use this information to easily get the WordPress version you\'re using, and then trying to hack your WordPress environment.', WPXDEFLECTOR_TEXTDOMAIN ) .
                    '<br/><br/>' .
                    __( 'By activating this rule, you can hide this XHTML meta generator tag anywhere in your HEAD section of your WordPress pages and posts AND in your RSS feed; this operation is safe and increases your security.', WPXDEFLECTOR_TEXTDOMAIN );
    }

    // Set help about the rule
    $tcRule6->help( $helpInfo, __('wpXtreme Deflector Tips - Info about this rule', WPXDEFLECTOR_TEXTDOMAIN ) );

    // Hook rule execution to proper WordPress action - the method engages execution internally only if rule status = ON
    $tcRule6->hookExecutionToFilters( 'the_generator' );

    //--------------------------------------------------------------------------
    // INIT THEME CLEANING RULE 8
    //--------------------------------------------------------------------------

    $tcRule8 = new WPXDeflectorThemeCleaningRule8(
      'wpxdeflector-theme-cleaning-rule-8',
      __( 'Hide version from any enqueued style and script.', WPXDEFLECTOR_TEXTDOMAIN )
    );

    // Set rule help
    if( WPXDeflectorRule::RULE_OFF == $tcRule8->status() ) {
      $this->numberOfRulesOFF++;
      $helpInfo = __('WordPress adds by default the current version to any enqueued script and CSS style; if a script or style does not specify a version number when enqueued, the current version of WordPress is used. This means that all of your WordPress, CSS and script versions of your site becomes easily readable for anyone, thus leading to a serious security risk, because an attacker can use them to hack your WordPress environment.<br/><br/>By activating this rule, you can hide the version added by default in any enqueued CSS style and script; this operation is safe and increases your security.', WPXDEFLECTOR_TEXTDOMAIN );
    }
    else {
      $helpInfo = __('Deflector are hiding the version added by default in any enqueued CSS style and script in your Wordpress theme.  This is a behaviour that increases your security. Deactivate this rule if you want to restore default.', WPXDEFLECTOR_TEXTDOMAIN );
    }
    $tcRule8->help( $helpInfo, __('wpXtreme Deflector Tips - Info about this rule', WPXDEFLECTOR_TEXTDOMAIN ) );

    // Hook rule execution to proper WordPress filters - the method engages execution internally only if rule status = ON
    $tcRule8->hookExecutionToFilters( array( 'style_loader_src', 'script_loader_src' ) );

    //--------------------------------------------------------------------------
    // THEME CLEANING RULES COLLECTION
    //--------------------------------------------------------------------------

    // Create theme cleaning rules collection
    $themeCleaningCollection = new WPXDeflectorRulesCollection(
      'wpxdeflector-theme-cleaning-rules',
      __( 'Clean your theme pages/posts from confidential data', WPXDEFLECTOR_TEXTDOMAIN )
    );

    // Set description for theme cleaning rules collection
    $themeCleaningCollection->description( __( 'In any page/post HTML source code of your theme, WordPress publishes automatically many information about the environment that are almost unnecessary for your theme and to end users. This information could be taken by malicious users and then used for a scope that can lead to security inconveniences. ', WPXDEFLECTOR_TEXTDOMAIN ) .
  __( 'Here you can set specific data to hide or leave in any HTML pages generated by your current theme, in order to increase your security. ', WPXDEFLECTOR_TEXTDOMAIN ) .
    '<br/>' .
  __( 'For any details, we suggest you to read detailed info about the rules of your interest.', WPXDEFLECTOR_TEXTDOMAIN ) );

    // Add rules to theme cleaning rules collection
    $themeCleaningCollection->addRule( $tcRule3 );
    $themeCleaningCollection->addRule( $tcRule6 );
    $themeCleaningCollection->addRule( $tcRule8 );

    //--------------------------------------------------------------------------
    // THEME CLEANING TAB
    //--------------------------------------------------------------------------

    // Create tab
    $keyTab                                   = 'wpxdeflector-theme-cleaning';
    $this->rulesEnvironment[$keyTab]['title'] = __( 'Theme Cleaning', WPXDEFLECTOR_TEXTDOMAIN );

    // Add rules collections to this tab
    $this->rulesEnvironment[$keyTab]['rules-collection'][]  = $themeCleaningCollection;

  }

  /**
   * Build the entire Deflector default rules environment.
   *
   * @brief Build the entire Deflector default rules environment
   *
   * @since 1.0.0
   *
   */
  private function _buildDeflectorRulesEnvironment() {

    //--------------------------------------------------------------------------
    // INIT BASIC SHIELDS
    //--------------------------------------------------------------------------

    $this->_initBasicShields();

    //--------------------------------------------------------------------------
    // INIT ENVIRONMENT SCANNING
    //--------------------------------------------------------------------------

    $this->_initEnvironmentScanning();

    //--------------------------------------------------------------------------
    // INIT THEME CLEANING
    //--------------------------------------------------------------------------

    $this->_initThemeCleaning();

  }

  /**
   * Register all autoload classes
   *
   * @brief Autoload classes
   */
  private function _registerClasses() {

    //------------------------------------------------------------------
    // NOTE: if you're including class definitions, don't insert any require|include statement here!
    // Follow WPDK SPL autoload login embedded instead.
    //------------------------------------------------------------------

    $includes = array(
    	$this->classesPath . 'admin/wpxdeflector-about-vc.php' => 'WPXDeflectorAboutViewController',

    	$this->classesPath . 'admin/wpxdeflector-admin.php' => 'WPXDeflectorAdmin',

    	$this->classesPath . 'configuration/wpxdeflector-configuration-vc.php' => 'WPXDeflectorConfigurationViewController',

    	$this->classesPath . 'configuration/wpxdeflector-configuration.php' => 'WPXDeflectorConfiguration',

    	$this->classesPath . 'core/wpxdeflector-ajax.php' => 'WPXDeflectorAjax',

    	$this->classesPath . 'frontend/wpxdeflector-frontend.php' => 'WPXDeflectorFrontEnd',

    	$this->classesPath . 'rules/wpxdeflector-basic-shield-rule1.php' => array( 'WPXDeflectorBasicShieldsRule1',
    'WPXDeflectorBasicShieldsRule1View' ),

    	$this->classesPath . 'rules/wpxdeflector-basic-shield-rule2.php' => array( 'WPXDeflectorBasicShieldsRule2',
    'WPXDeflectorBasicShieldsRule2View' ),

    	$this->classesPath . 'rules/wpxdeflector-rule.php' => array( 'WPXDeflectorRule',
    'WPXDeflectorRuleView' ),

    	$this->classesPath . 'rules/wpxdeflector-rules-collection.php' => array( 'WPXDeflectorRulesCollection',
    'WPXDeflectorRulesCollectionView' ),

    	$this->classesPath . 'rules/wpxdeflector-tab.php' => 'WPXDeflectorTabView',

    	$this->classesPath . 'rules/wpxdeflector-theme-cleaning-rule3.php' => 'WPXDeflectorThemeCleaningRule3',

    	$this->classesPath . 'rules/wpxdeflector-theme-cleaning-rule6.php' => 'WPXDeflectorThemeCleaningRule6',

    	$this->classesPath . 'rules/wpxdeflector-theme-cleaning-rule8.php' => 'WPXDeflectorThemeCleaningRule8',

    	$this->classesPath . 'rules/wpxdeflector-tips-rule1.php' => 'WPXDeflectorTipsRule1',

    	$this->classesPath . 'rules/wpxdeflector-tips-rule2.php' => 'WPXDeflectorTipsRule2',

    	$this->classesPath . 'rules/wpxdeflector-tips-rule3.php' => 'WPXDeflectorTipsRule3',

    	$this->classesPath . 'rules/wpxdeflector-tips-rule4.php' => 'WPXDeflectorTipsRule4' );

    $this->registerAutoloadClass( $includes );

  }

  /**
   * Catch for ajax
   *
   * @brief Ajax
   */
  public function ajax() {
    WPXDeflectorAjax::init();
  }

  /**
   * Catch for admin
   *
   * @brief Admin backend
   */
  public function admin() {
    WPXDeflectorAdmin::init( $this );
  }

  /**
   * Catch for frontend
   *
   * @brief Frontend
   */
  public function theme() {
    WPXDeflectorFrontEnd::init( $this );
  }

  /**
   * Ready to init plugin configuration
   *
   * @brief Init configuration
   */
  public function configuration() {
    WPXDeflectorConfiguration::init();
  }

  /**
   * Catch for activation. This method is called one shot.
   *
   * @brief Activation
   */
  public function activation() {
    WPXDeflectorConfiguration::init()->delta();
  }

  /**
   * Catch for deactivation. This method is called when the plugin is deactivate.
   *
   * @brief Deactivation
   */
  public function deactivation() {

    // Deactivate ALL Deflector rules, both Deflector and Deflector extensions plugins. See also extended description
    // of method disableRuleOnDeflectorDeactivation of class WPXDeflectorRule for details
    $arrayTabKeys = array_keys( $this->rulesEnvironment );
    foreach( $arrayTabKeys as $tabKey ) {
      /**
       * @var WPXDeflectorRulesCollection $ruleCollectionInstance
       */
      foreach( $this->rulesEnvironment[$tabKey]['rules-collection'] as $ruleCollectionInstance ) {
        /**
         * @var WPXDeflectorRule $rule
         */
        foreach( $ruleCollectionInstance->rules() as $rule ) {
          $rule->disableRuleOnDeflectorDeactivation();
        }
      }
    }

  }

  /**
   * Do log in easy way
   *
   * @brief Helper for log
   *
   * @param mixed  $txt
   * @param string $title Optional. Any free string text to context the log
   *
   */
  public static function log( $txt, $title = '' ) {
    /**
     * @var WPXDeflector $me
     */
    $me = $GLOBALS[ __CLASS__ ];
    $me->log->log( $txt, $title );
  }

}