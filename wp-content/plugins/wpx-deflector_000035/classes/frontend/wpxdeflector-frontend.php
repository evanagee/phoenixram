<?php
/**
 * Front-End interface of Deflector
 *
 * @class              WPXDeflectorFrontEnd
 * @author             yuma <info@wpxtre.me>
 * @copyright          Copyright (C) 2013- wpXtreme Inc. All Rights Reserved.
 * @date               2013-01-22
 * @since              1.0.0
 *
 */

class WPXDeflectorFrontEnd extends WPDKWordPressTheme {

  /**
   * Create and return a singleton instance of WPXDeflectorFrontEnd class
   *
   * @brief Init
   *
   * @param WPXDeflector $plugin The main class of this plugin.
   *
   * @return WPXDeflectorFrontEnd
   */
  public static function init( WPXDeflector $plugin ) {
    static $instance = null;
    if ( is_null( $instance ) ) {
      $instance = new WPXDeflectorFrontEnd( $plugin );
    }
    return $instance;
  }


  /**
   * Create an instance of WPXDeflectorFrontEnd class
   *
   * @brief Construct
   *
   * @param WPXDeflector $plugin
   *
   * @return WPXDeflectorFrontEnd
   */
  public function __construct( WPXDeflector $plugin ) {

    parent::__construct( $plugin );

    //-------------------------------------------------------------------------------
    // FILTERS AND ACTIONS RELATED _ONLY_ TO FRONTEND
    //-------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------
    // END FILTERS AND ACTIONS RELATED _ONLY_ TO FRONTEND
    //-------------------------------------------------------------------------------

  }


}