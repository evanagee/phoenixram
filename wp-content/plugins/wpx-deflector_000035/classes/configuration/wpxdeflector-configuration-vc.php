<?php
/**
 * Settings View controller
 *
 * @class              WPXDeflectorConfigurationViewController
 * @author             wpXtreme <info@wpxtre.me>
 * @copyright          Copyright 2013- wpXtreme inc.
 * @date               2013-02-15 15:15:04
 * @version            0.1.0
 *
 */

class WPXDeflectorConfigurationViewController extends WPDKjQueryTabsViewController {

  /**
   * Create an instance of WPXDeflectorConfigurationViewController class
   *
   * @brief Construct
   *
   * @return WPXDeflectorConfigurationViewController
   */
  public function __construct() {

    /* Prepare tabs. */
    $tabs = array();

    // Get main class instance
    $deflectorInstance = WPXDeflector::getInstance();
    if( NULL == $deflectorInstance ) return;

    /* Create each single tab container */
    foreach( $deflectorInstance->rulesEnvironment as $key => $arrayValues) {

      // Create the main tab container
      $tabContainer = new WPXDeflectorTabView( $key );

      // Build all collections views for any related rules collection model
      $collectionContent = '';
      foreach( $arrayValues['rules-collection'] as $rulesCollectionModel) {
        $collectionView = new WPXDeflectorRulesCollectionView( $rulesCollectionModel );
        $collectionContent .= $collectionView->html();
      }

      // Add global rules collection view to tab container
      $tabContainer->content = $collectionContent;

      // Add the tab to the view
      $tabs[] = new WPDKjQueryTab( $key, $arrayValues['title'], $tabContainer->html() );

    }

    // Create the global view
    $view = new WPDKjQueryTabsView( 'wpxdeflector-shields', $tabs );

    parent::__construct( 'wpxdeflector-console', __( 'Deflector Console', WPXDEFLECTOR_TEXTDOMAIN ), $view );

  }


  /**
   * This static method is called when the head of this view controller is loaded by WordPress.
   *
   * @brief Head
   */
  public static function didHeadLoad() {
  }

}
