<?php
/**
 * Sample configuration class. In this class you define your tree configuration.
 *
 * @class              WPXDeflectorConfiguration
 * @author             wpXtreme <info@wpxtre.me>
 * @copyright          Copyright 2013- wpXtreme inc.
 * @date               2013-02-15 15:15:04
 * @since              1.0.0
 */

class WPXDeflectorConfiguration extends WPDKConfiguration {

    /**
     * The configuration name used on database
     *
     * @brief Configuration name
     *
     * @var string
     */
    const CONFIGURATION_NAME = 'wpx-deflector-configuration';

    /**
     * Your own configuration property
     *
     * @brief Configuration version
     *
     * @var string $version
     */
    public $version = WPXDEFLECTOR_VERSION;

    /**
     * This is the first entry pointer to your own tree configuration
     *
     * @brief Settings
     *
     * @var WPXDeflectorSettingsConfiguration $settings
     */
//    public $settings;

    /**
     * Return an instance of WPXDeflectorConfiguration class from the database or onfly.
     *
     * @brief Get the configuration
     *
     * @return WPXDeflectorConfiguration
     */
    public static function init() {
        $instance = parent::init( self::CONFIGURATION_NAME, __CLASS__ );

        /* Or if the obfly version is different from stored version. */
        if( version_compare( $instance->version, WPXDEFLECTOR_VERSION ) < 0 ) {
            /* For i.e. you would like update the version property. */
            $instance->version = WPXDEFLECTOR_VERSION;
            $instance->update();
        }

        return $instance;
    }

    /**
     * Create an instance of WPXDeflectorConfiguration class
     *
     * @brief Construct
     *
     * @return WPXDeflectorConfiguration
     */
    public function __construct() {
        parent::__construct( self::CONFIGURATION_NAME );

        /* Init my tree settings. */
//        $this->settings = new WPXDeflectorSettingsConfiguration();
    }

}
