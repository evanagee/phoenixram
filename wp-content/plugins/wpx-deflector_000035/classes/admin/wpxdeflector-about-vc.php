<?php
/**
 * Standard about view controller
 *
 * @class           WPXDeflectorAboutViewController
 * @author          =undo= <info@wpxtre.me>
 * @copyright       Copyright (C) 2013 wpXtreme Inc. All Rights Reserved.
 * @date            2013-02-21
 * @version         1.0.0
 *
 */
class WPXDeflectorAboutViewController extends WPXAboutViewController {

  /**
   * Create an instance of WPXDeflectorAboutViewController class
   *
   * @brief Construct
   *
   * @return WPXDeflectorAboutViewController
   */
  public function __construct() {
    parent::__construct( $GLOBALS['WPXDeflector'] );
  }
}
