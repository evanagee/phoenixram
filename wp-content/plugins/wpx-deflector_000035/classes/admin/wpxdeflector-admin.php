<?php
/**
 * Brief Description here
 *
 * ## Overview
 * Markdown here
 *
 * @class              WPXDeflectorAdmin
 * @author             wpXtreme <info@wpxtre.me>
 * @copyright          Copyright 2013- wpXtreme inc.
 * @date               2013-02-15 15:15:04
 * @version            0.1.0
 *
 */

class WPXDeflectorAdmin extends WPDKWordPressAdmin {

  /**
   * This is the minumun capability required to display admin menu item
   *
   * @brief Menu capability
   */
  const MENU_CAPABILITY = 'manage_options';


  /**
   * Create and return a singleton instance of WPXDeflectorAdmin class
   *
   * @brief Init
   *
   * @param WPXDeflector $plugin The main class of this plugin.
   *
   * @return WPXDeflectorAdmin
   */
  public static function init( WPXDeflector $plugin ) {
    static $instance = null;
    if ( is_null( $instance ) ) {
      $instance = new WPXDeflectorAdmin( $plugin );
    }
    return $instance;
  }



  /**
   * Create an instance of WPXDeflectorAdmin class
   *
   * @brief Construct
   *
   * @param WPXDeflector $plugin Main class of your plugin
   *
   * @return WPXDeflectorAdmin
   */
  public function __construct( WPXDeflector $plugin ) {

    parent::__construct( $plugin );

    /* Loading Script & style for backend */
    add_action( 'admin_head', array( $this, 'admin_head' ) );

    /* Choose my plugin widget link. */
    add_filter( 'wpxm_control_panel_widget_url-' . $this->plugin->name, array( $this, 'wpxm_control_panel_url' ) );

    // Hook to plugin list in order to catch deactivation click
    add_action( 'plugin_action_links_' . $this->plugin->pluginBasename, array( $this, 'plugin_action_links' ) );

    /* Control panel badge */
    add_filter( 'wpxm_widget_badge_' . sanitize_key( $plugin->folderName ), array( $this, 'wpxm_widget_badge' ) );

  }

  /**
   * Called by WPDKWordPressAdmin parent when the admin head is loaded
   *
   * @brief Admin head
   */
  public function admin_head() {

    /* Scripts */

    wp_enqueue_script( 'wpxdeflector-admin', WPXDEFLECTOR_URL_JAVASCRIPT . 'wpxdeflector-admin.js', array( 'jquery' ), WPXDEFLECTOR_VERSION, true );

    // Enqueue javascript abstraction layer
    // This javascript contains also code related to the rules embedded into Deflector by default
    wp_enqueue_script( 'wpxdeflector-standard.settings', WPXDEFLECTOR_URL_JAVASCRIPT . 'wpxdeflector-standard-settings.js', array( 'jquery' ), WPXDEFLECTOR_VERSION, true );

    wp_enqueue_style( 'wpxdeflector-plugins-list', WPXDEFLECTOR_URL_CSS . 'wpxdeflector-plugins-list.css' );
    wp_enqueue_style( 'wpxdeflector-admin', WPXDEFLECTOR_URL_CSS . 'wpxdeflector-admin.css' );
  }

  /**
   * Called when WordPress is ready to build the admin menu.
   * Sample hot to build a simple menu.
   *
   * @brief Admin menu
   */
  public function admin_menu() {

    // Plugin icon
    $icon_menu = $this->plugin->imagesURL . 'logo-16x16.png';

    // Create the badge with the global number of rules with status = OFF
    $badge = WPDKUI::badge( WPXDeflector::getInstance()->numberOfRulesOFF, 'wpxdeflector-badge' );

    $menus = array(
      'wpxdeflector_main' => array(
        'menuTitle'  => __( 'Deflector', WPXDEFLECTOR_TEXTDOMAIN ) . $badge,
        'pageTitle'  => __( 'Deflector', WPXDEFLECTOR_TEXTDOMAIN ),
        'capability' => self::MENU_CAPABILITY,
        'icon'       => $icon_menu,
        'subMenus'   => array(

          array(
            'menuTitle'      => __( 'Console', WPXDEFLECTOR_TEXTDOMAIN ),
            'capability'     => self::MENU_CAPABILITY,
            'viewController' => 'WPXDeflectorConfigurationViewController',
          ),

          WPDKSubMenuDivider::DIVIDER,

          array(
            'menuTitle'      => __( 'About', WPXDEFLECTOR_TEXTDOMAIN ),
            'viewController' => 'WPXDeflectorAboutViewController',
          ),
        )
      )
    );

    WPDKMenu::renderByArray( $menus );

  }

  /**
   * Display the badge in Control Panel
   *
   * @brief Badge
   *
   * @param string $badge Defaul is empty
   *
   * @return string
   */
  public function wpxm_widget_badge( $badge ) {
    $badge = WPDKUI::badge( WPXDeflector::getInstance()->numberOfRulesOFF, 'wpxdeflector-badge' );
    return $badge;
  }

  /**
   * This hook is called when the control panel draw the link of a plugin widget
   *
   * @brief Control panel URL
   *
   * @return string|void
   */
  public function wpxm_control_panel_url() {
    return WPDKMenu::url( 'WPXDeflectorConfigurationViewController' );
  }

  /**
   * Handle Deflector deactivation through a confirm dialog.
   *
   * @brief Handle Deflector deactivation through a confirm dialog.
   *
   * @param array $links
   *
   * @return array
   */
  public function plugin_action_links( $links ) {

    $l10n = array(
      'warning_confirm_deactive_plugin' => __( "WARNING! If you deactivate Deflector plugin, _ALL_ security rules currently active, in Deflector and in Deflector extensions installed, will be disabled and reset to OFF status.\n\nDo you really want to continue?", WPXDEFLECTOR_TEXTDOMAIN )
    );

    wp_enqueue_script( 'wpxdeflector-plugin-list', $this->plugin->javascriptURL . 'wpxdeflector-plugin-list.js', $this->plugin->version, true );
    wp_localize_script( 'wpxdeflector-plugin-list', 'WPXDeflectorPluginListL10n', $l10n );

    return $links;

  }

}
