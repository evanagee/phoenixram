<?php
/**
 * Theme Cleaning Rule #8 Model
 *
 * @class              WPXDeflectorThemeCleaningRule8
 * @author             yuma <info@wpxtre.me>
 * @copyright          Copyright (C) 2013- wpXtreme Inc. All Rights Reserved.
 * @date               2013-03-12
 * @since              1.0.0
 *
 */

class WPXDeflectorThemeCleaningRule8 extends WPXDeflectorRule {

  //---------------------------------------------------------------------------
  // METHODS
  //---------------------------------------------------------------------------

  /**
   * Create an instance of WPXDeflectorThemeCleaningRule8 class
   *
   * @brief Construct
   *
   * @param $id
   * @param $title
   *
   * @return WPXDeflectorThemeCleaningRule8
   */
  function __construct( $id, $title ) {

    // Engage parent constructor ( and restore settings as they have been saved )
    parent::__construct( $id, $title );

  }

  /**
   * This function executes this rule. Overridden parent method.
   *
   * @brief Execute this rule
   *
   * @since 1.0.0
   *
   */
  public function execute( ) {

    // Get first argument from param: it is the url in which remove version
    $arrayFilterArguments = func_get_args();
    $url = $arrayFilterArguments[0];

    // Cut version from url
    if( strpos( $url, 'ver=' ) ) {
      $url = remove_query_arg( 'ver', $url );
    }

    // Return back the url without version in query args
    return $url;

  }

  /**
   * This is the Ajax action invoked in handling this rule. This method overrides correctly parent ajaxGateway method,
   * that should be an abstract.
   *
   * @brief This AJAX action is invoked in handling this rule
   *
   * @since 1.0.0
   *
   */
  public function ajaxGateway() {

    // Execute exactly default Ajax handling
    // Override default to make unique the Ajax key for this rule
    parent::ajaxGateway();

  }

}
