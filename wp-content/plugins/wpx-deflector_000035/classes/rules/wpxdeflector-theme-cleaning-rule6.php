<?php
/**
 * Theme Cleaning Rule #6 Model
 *
 * @class              WPXDeflectorThemeCleaningRule6
 * @author             yuma <info@wpxtre.me>
 * @copyright          Copyright (C) 2013- wpXtreme Inc. All Rights Reserved.
 * @date               2013-03-12
 * @since              1.0.0
 *
 */

class WPXDeflectorThemeCleaningRule6 extends WPXDeflectorRule {

  //---------------------------------------------------------------------------
  // METHODS
  //---------------------------------------------------------------------------

  /**
   * Create an instance of WPXDeflectorThemeCleaningRule6 class
   *
   * @brief Construct
   *
   * @param $id
   * @param $title
   *
   * @return WPXDeflectorThemeCleaningRule6
   */
  function __construct( $id, $title ) {

    // Engage parent constructor ( and restore settings as they have been saved )
    parent::__construct( $id, $title );

    // This rule needs an alignment check - add it on proper WP action
    add_action( 'wp_loaded', array( $this, 'alignStatus' ) );

  }

  /**
   * Deflector theme rules are very general, and can be set also in another plugin or even in theme. This function checks
   * if another plugin has already set the execution of this rule, and if it's so, set the rule status to ON because
   * it is already active.
   *
   * @brief Align this rule to environment
   *
   * @since 1.0.0
   *
   */
  public function alignStatus( ) {

    if( false === has_action( 'wp_head', 'wp_generator' )) {

      $this->_status = self::RULE_ON;
      $this->_saveSettings();

      // Properly set info about the rule
      $this->_helpContent = __( 'Your Wordpress theme are hiding WordPress XHTML meta generator tag containing current WordPress version in all your pages/posts.', WPXDEFLECTOR_TEXTDOMAIN ) .
                            '<br/><br/>' .
                            __( 'This is a behaviour activated by another plugin or theme that increases your security; but it could leave this information untouched in your RSS feed.', WPXDEFLECTOR_TEXTDOMAIN ) .
                            '<br/><br/>' .
                            __( 'For increasing your security, you should disable this rule in plugin or theme that activate it, and then leave this rule enable into Deflector.', WPXDEFLECTOR_TEXTDOMAIN );

    }

  }

  /**
   * This function executes this rule. Overridden parent method.
   *
   * @brief Execute this rule
   *
   * @since 1.0.0
   *
   */
  public function execute( ) {
    return '';
  }

  /**
   * This is the Ajax action invoked in handling this rule. This method overrides correctly parent ajaxGateway method,
   * that should be an abstract.
   *
   * @brief This AJAX action is invoked in handling this rule
   *
   * @since 1.0.0
   *
   */
  public function ajaxGateway() {

    // Execute exactly default Ajax handling
    // Override default to make unique the Ajax key for this rule
    parent::ajaxGateway();

  }

}
