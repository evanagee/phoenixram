<?php
/**
 * Theme Cleaning Rule #3 Model
 *
 * @class              WPXDeflectorThemeCleaningRule3
 * @author             yuma <info@wpxtre.me>
 * @copyright          Copyright (C) 2013- wpXtreme Inc. All Rights Reserved.
 * @date               2013-03-11
 * @since              1.0.0
 *
 */

class WPXDeflectorThemeCleaningRule3 extends WPXDeflectorRule {

  //---------------------------------------------------------------------------
  // METHODS
  //---------------------------------------------------------------------------

  /**
   * Create an instance of WPXDeflectorThemeCleaningRule3 class
   *
   * @brief Construct
   *
   * @param $id
   * @param $title
   *
   * @return WPXDeflectorThemeCleaningRule3
   */
  function __construct( $id, $title ) {

    // Engage parent constructor ( and restore settings as they have been saved )
    parent::__construct( $id, $title );

    // This rule needs an alignment check - add it on proper WP action
    add_action( 'wp_loaded', array( $this, 'alignStatus' ) );

  }

  /**
   * Deflector theme rules are very general, and can be set also in another plugin or even in theme. This function checks
   * if another plugin has already set the execution of this rule, and if it's so, set the rule status to ON because
   * it is already active.
   *
   * @brief Align this rule to environment
   *
   * @since 1.0.0
   *
   */
  public function alignStatus( ) {

    if( false === has_action( 'wp_head', 'rsd_link' )) {

      $this->_status = self::RULE_ON;
      $this->_saveSettings();

      // Properly set info about the rule
      $this->_helpContent = __( 'Your Wordpress theme are hiding the RSD URL in the HEAD section of any page/post HTML source code. This is a behaviour that increases your security. Deactivate this rule if you want to restore default.', WPXDEFLECTOR_TEXTDOMAIN ) .
                            '<br/><br/>' .
                            __( '<strong>WARNING</strong>: if this rule remains active even after deactivation, it means that another plugin, or another function in your WordPress environment, is activating this rule again.', WPXDEFLECTOR_TEXTDOMAIN );

    }

  }

  /**
   * This function executes this rule. Overridden parent method.
   *
   * @brief Execute this rule
   *
   * @since 1.0.0
   *
   */
  public function execute( ) {
    remove_action( 'wp_head', 'rsd_link' );
  }

  /**
   * This is the Ajax action invoked in handling this rule. This method overrides correctly parent ajaxGateway method,
   * that should be an abstract.
   *
   * @brief This AJAX action is invoked in handling this rule
   *
   * @since 1.0.0
   *
   */
  public function ajaxGateway() {

    // Execute exactly default Ajax handling
    // Override default to make unique the Ajax key for this rule
    parent::ajaxGateway();

  }

}
