<?php
/**
 * Tips Rule #4 Model
 *
 * @class              WPXDeflectorTipsRule4
 * @author             yuma <info@wpxtre.me>
 * @copyright          Copyright (C) 2013- wpXtreme Inc. All Rights Reserved.
 * @date               2013-03-11
 * @since              1.0.0
 *
 * @note This rule model will use default rule view
 *
 */

class WPXDeflectorTipsRule4 extends WPXDeflectorRule {

  //---------------------------------------------------------------------------
  // METHODS
  //---------------------------------------------------------------------------

  /**
   * Create an instance of WPXDeflectorTipsRule4 class
   *
   * @brief Construct
   *
   * @return WPXDeflectorTipsRule4
   */
  function __construct( $id, $title ) {

    // Set custom properties to default - nothing to do here

    // Engage parent constructor ( and restore settings as they have been saved )
    parent::__construct( $id, $title, self::RULE_CHECK_ONLY );

  }

  /**
   * This function executes tips rule 4 check. If check passed, rule status becomes RULE_ON. Overridden parent method.
   *
   * @brief Execute tips rule 4 check.
   *
   * @since 1.0.0
   *
   * @note Rule status is set directly here, because this rule is check only ( no user changeable ).
   *
   */
  public function execute( ) {

    if( '1' != get_option( 'uploads_use_yearmonth_folders' )) {
      $this->status( self::RULE_ON );
    }

  }

}
