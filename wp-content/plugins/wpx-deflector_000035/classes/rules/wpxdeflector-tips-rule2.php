<?php
/**
 * Tips Rule #2 Model
 *
 * @class              WPXDeflectorTipsRule2
 * @author             yuma <info@wpxtre.me>
 * @copyright          Copyright (C) 2013- wpXtreme Inc. All Rights Reserved.
 * @date               2013-03-08
 * @since              1.0.0
 *
 * @note This rule model will use default rule view
 *
 */

class WPXDeflectorTipsRule2 extends WPXDeflectorRule {

  //---------------------------------------------------------------------------
  // INTERNAL CONSTANTS
  //---------------------------------------------------------------------------

  /**
   * int - Min length of database password for considering it OK
   *
   * @brief Min length of database password for considering it OK
   *
   * @since 1.0.0
   */
  const MIN_DBPASSWD_LENGTH = 8;

  //---------------------------------------------------------------------------
  // METHODS
  //---------------------------------------------------------------------------

  /**
   * Create an instance of WPXDeflectorTipsRule2 class
   *
   * @brief Construct
   *
   * @return WPXDeflectorTipsRule2
   */
  function __construct( $id, $title ) {

    // Set custom properties to default - nothing to do here

    // Engage parent constructor ( and restore settings as they have been saved )
    parent::__construct( $id, $title, self::RULE_CHECK_ONLY );

  }

  /**
   * This function executes tips rule 2 check. If check passed, rule status becomes RULE_ON. Overridden parent method.
   *
   * @brief Execute tips rule 2 check.
   *
   * @since 1.0.0
   *
   * @note Rule status is set directly here, because this rule is check only ( no user changeable ).
   *
   */
  public function execute( ) {

    $test = DB_PASSWORD;
    if( ( strlen( $test ) >= self::MIN_DBPASSWD_LENGTH ) ) {
      $this->status( self::RULE_ON );
    }

  }

}
