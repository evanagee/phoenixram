<?php
/**
 * Deflector Rules Collection Model
 *
 * @class              WPXDeflectorRulesCollection
 * @author             yuma <info@wpxtre.me>
 * @copyright          Copyright (C) 2013- wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-28
 * @since              1.0.0
 *
 */

class WPXDeflectorRulesCollection {

  //---------------------------------------------------------------------------
  // PROPERTIES
  //---------------------------------------------------------------------------

  /**
   * Rules collection as array of registered rules
   *
   * @brief Rules collection
   *
   * @var mixed $_arrayRules
   *
   * @since 1.0.0
   */
  private $_arrayRules;

  /**
   * The id of the collection.
   *
   * @brief Collection id
   *
   * @var string $_id
   *
   * @since 1.0.0
   */
  private $_id;

  /**
   * The title of this rules collection
   *
   * @brief Rules collection title
   *
   * @var string $_title
   *
   * @since 1.0.0
   */
  private $_title;

  /**
   * The extended description of this rules collection
   *
   * @brief Rules collection description
   *
   * @var string $_description
   *
   * @since 1.0.0
   */
  private $_description;

  //---------------------------------------------------------------------------
  // METHODS
  //---------------------------------------------------------------------------

  /**
   * Return an instance of WPXDeflectorRulesCollection class
   *
   * @brief Construct
   *
   * @return WPXDeflectorRulesCollection
   */
  public function __construct( $id, $title ) {

    $this->_id          = $this->id( $id );
    $this->_title       = $this->title( $title );
    $this->_arrayRules  = array();

  }

  /**
   * This function is the set/get interface to title property.
   *
   * @brief Set/get interface to title property.
   *
   * @param string $newTitle - New rule title to set. If missing, function return current title.
   *
   * @return string The current title, even on set.
   *
   * @since 1.0.0
   *
   */
  public function title( $newTitle = '' ) {

    // Set
    if( ! empty( $newTitle ) ) {
      $this->_title = strip_tags( $newTitle );
    }

    return $this->_title;

  }

  /**
   * This function is the set/get interface to the description of rules collection.
   *
   * @brief Set/get interface to description property.
   *
   * @param string $newTitle - New rule title to set. If missing, function return current title.
   *
   * @return string The current title, even on set.
   *
   * @since 1.0.0
   *
   */
  public function description( $newDesc = '' ) {

    // Set
    if( ! empty( $newDesc ) ) {
      $this->_description = $newDesc;
    }

    return $this->_description;

  }

  /**
   * Add a rule in this collection. The rule MUST BE an object instance related to iUnknown class WPXDeflectorRule, or
   * directly, or as a child of.
   *
   * @brief Add a rule in the collection.
   *
   * @param object $rule - The rule to register. It MUST BE an object instance related to iUnknown class WPXDeflectorRule, or
   * directly, or as a child of.
   *
   * @return boolean TRUE if rule has been successfully added to this collection, FALSE otherwise.
   *
   * @since 1.0.0
   *
   */
  public function addRule( $rule ) {

    // Check rule instance
    if( is_object( $rule ) && ( TRUE == is_a( $rule, 'WPXDeflectorRule') ) ) {
      $this->_arrayRules[] = $rule;
      return TRUE;
    }

    return FALSE;

  }

  /**
   * Return rules in this collection.
   *
   * @brief Return rules in the collection.
   *
   * @since 1.0.0
   *
   */
  public function rules() {
    return $this->_arrayRules;
  }

  /**
   * This function is the set/get interface to id property.
   *
   * @brief Set/get interface to id property.
   *
   * @param string $newID - New rule id to set. If missing, function return current id.
   *
   * @return string The current id, even on set.
   *
   * @since 1.0.0
   *
   */
  public function id( $newID = '' ) {

    // Set
    if( ! empty( $newID ) ) {
      $this->_id = $newID;
    }

    return $this->_id;

  }

}

/**
 * Deflector Rules Collection View
 *
 * @class              WPXDeflectorRulesCollectionView
 * @author             yuma <info@wpxtre.me>
 * @copyright          Copyright (C) 2013- wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-28
 * @since              1.0.0
 *
 */
class WPXDeflectorRulesCollectionView extends WPDKView {

  //---------------------------------------------------------------------------
  // PROPERTIES
  //---------------------------------------------------------------------------

  /**
   * The rules collection model this view is related to. This is an instance of a class WPXDeflectorRulesCollection.
   *
   * @brief The rules collection model
   *
   * @var WPXDeflectorRulesCollection $_model
   *
   * @since 1.0.0
   */
  private $_model;

  //---------------------------------------------------------------------------
  // METHODS
  //---------------------------------------------------------------------------

  /**
   * Return an instance of WPXDeflectorRulesCollectionView class
   *
   * @brief Construct
   *
   * @param string $model
   * @param string $class
   *
   * @return WPXDeflectorRulesCollectionView
   */
  public function __construct( $model, $class = '') {

    // Link this view to its model
    if( is_object( $model ) && ( true == is_a( $model, 'WPXDeflectorRulesCollection') ) ) {
      $this->_model = $model;
    }

    // Call parent constructor with params got from model
    parent::__construct( $this->_model->id(), $class );

    // Generate rules view
    $this->generateContent();

  }

  /**
   * Prepare content to be drawn. This content is the collection of rules related to this instance.
   *
   * @brief Prepare content to be drawn
   *
   * @since 1.0.0
   *
   */
  public function generateContent() {

    // Get properties from collection model
    $id     = $this->_model->id();
    $title  = $this->_model->title();
    $desc   = $this->_model->description();

    // Create collection container
    $rulesContainer = array( $title => array() );
    $sKey = key( $rulesContainer );
    $rulesContainer[$sKey][] = $desc;

    // Create rules view
    $rulesView = <<<HTML
<table id="wpxdeflector-rules-collection-table-$id" class="wpxdeflector-rules-collection-table" cellpadding="4" cellspacing="0" width="100%">
  <tbody>
HTML;

    // Foreach rule in the collection, engage the view
    foreach( $this->_model->rules() as $rule ) {

      // Build and instance the view
      $ruleModelInstance = get_class( $rule );
      $ruleViewClassName = $ruleModelInstance . 'View';

      // If rule hasn't a specific view, use default
      if( ! class_exists( $ruleViewClassName ) ) {
        $ruleViewClassName = 'WPXDeflectorRuleView';
      }
      /** @var WPXDeflectorRuleView $ruleViewInstance  */
      $ruleViewInstance = new $ruleViewClassName( $rule );

      // Concat single rule view to global collection view
      $rulesView .= '<tr><td>' . $ruleViewInstance->html() . '</td></tr>';
    }

    // Footer
    $rulesView .= <<<HTML
  </tbody>
</table>
HTML;

    // Add rules view to collection container
    $rulesContainer[$sKey][] = array(
      array(
        'type'    => WPDKUIControlType::CUSTOM,
        'content' => $rulesView
      )
    );

    // Generate final collection content
    $layout = new WPDKUIControlsLayout( $rulesContainer );
    $this->content = '<div id="wpxdeflector-rules-collection-container-'. $id . '" ' .
                        'class="wpxdeflector-rules-collection-container">' .
                     $layout->html() .
                     '</div>';

  }

}
