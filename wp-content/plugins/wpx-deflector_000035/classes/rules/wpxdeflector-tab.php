<?php
/**
 * Deflector Tab View
 *
 * @class              WPXDeflectorTabView
 * @author             yuma <info@wpxtre.me>
 * @copyright          Copyright (C) 2013- wpXtreme Inc. All Rights Reserved.
 * @date               2013-03-01
 * @since              1.0.0
 *
 */

class WPXDeflectorTabView extends WPDKView {

  //---------------------------------------------------------------------------
  // METHODS
  //---------------------------------------------------------------------------

  /**
   * Return an instance of WPXDeflectorTabView class
   *
   * @brief Construct
   *
   * @param string $id
   *
   * @return WPXDeflectorTabView
   */
  public function __construct( $id ) {

    parent::__construct( $id );

  }

}
