<?php
/**
 * Basic Shields Rule #2 Model
 *
 * @class              WPXDeflectorBasicShieldsRule2
 * @author             yuma <info@wpxtre.me>
 * @copyright          Copyright (C) 2013- wpXtreme Inc. All Rights Reserved.
 * @date               2013-03-07
 * @since              1.0.0
 *
 */

class WPXDeflectorBasicShieldsRule2 extends WPXDeflectorRule {

  //---------------------------------------------------------------------------
  // INTERNAL CONSTANTS
  //---------------------------------------------------------------------------

  /**
   * int - Max length of HTML output for this rule
   *
   * @brief HTML output max length
   *
   * @since 1.0.0
   */
  const MAX_LENGTH_HTML_OUTPUT = 512;

  /**
   * string - Default HTTP Header used in this rule
   *
   * @brief Default HTTP header
   *
   * @since 1.0.0
   */
  const DEFAULT_HTTP_HEADER = '403 Forbidden';

  /**
   * string - Default html output used in this rule
   *
   * @brief Default html output
   *
   * @since 1.0.0
   */
  const DEFAULT_HTML_OUTPUT = 'Your credentials do not grant the permission to access this resource.';

  //---------------------------------------------------------------------------
  // PROPERTIES
  //---------------------------------------------------------------------------

  /**
   * The HTTP header to send to client if this rule is executed
   *
   * @brief HTTP header sent to client in rule execution
   *
   * @var string $_httpHeader
   *
   * @since 1.0.0
   *
   * @note IT MUST BE PROTECTED, NOT PRIVATE, ELSE sync to DB DOES NOT SET IT!
   *
   */
  protected $_httpHeader;

  /**
   * The HTML output displayed if this rule is executed
   *
   * @brief HTML output displayed in rule execution
   *
   * @var string $_htmlOutput
   *
   * @since 1.0.0
   *
   * @note IT MUST BE PROTECTED, NOT PRIVATE, ELSE parent sync to DB DOES NOT SET IT!
   *
   */
  protected $_htmlOutput;

  /**
   * The array of WordPress roles blocked when this rule is executed
   *
   * @brief WordPress roles blocked when this rule is executed
   *
   * @var array $_blockedRoles
   *
   * @since 1.0.0
   *
   * @note IT MUST BE PROTECTED, NOT PRIVATE, ELSE parent sync to DB DOES NOT SET IT!
   *
   */
  protected $_blockedRoles;

  //---------------------------------------------------------------------------
  // METHODS
  //---------------------------------------------------------------------------

  /**
   * Create an instance of WPXDeflectorBasicShieldsRule1 class
   *
   * @brief Construct
   *
   * @return WPXDeflectorBasicShieldsRule1
   */
  function __construct( $id, $title ) {

    // Set custom properties to default
    $this->_httpHeader    = self::DEFAULT_HTTP_HEADER;
    $this->_htmlOutput    = self::DEFAULT_HTML_OUTPUT;
    $this->_blockedRoles  = array();

    // Engage parent constructor ( and restore settings as they have been saved )
    parent::__construct( $id, $title, self::RULE_WITH_STATUS_CHANGEABLE );

  }

  /**
   * This method returns a title => value array with custom settings of this rule, in the form
   * 'setting title to display' => 'its value'. It is used in view to show current settings of this rule when its status is ON.
   *
   * @brief Return settings array
   *
   * @since 1.0.0
   *
   */
  public function settingsData() {

    // Generate string from array of interdicted roles
    $interdictedRoles = '';
    $roles = new WP_Roles();
    $arrayRoles = $roles->get_names();
    foreach( $this->_blockedRoles as $role ) {
      if( ! empty( $interdictedRoles )) $interdictedRoles .= ', ';
      $interdictedRoles .= $arrayRoles[$role];
    }

    // Generate settings data
    $arraySettings = array (
      'Interdicted Roles' => $interdictedRoles,
      'HTTP Header'       => $this->_httpHeader,
      'HTML Output'       => htmlentities( $this->_htmlOutput )
    );

    return $arraySettings;

  }

  /**
   * This function executes basic shields rule 1. Overridden parent method.
   *
   * @brief Execute basic shields rule 1
   *
   * @since 1.0.0
   *
   */
  public function execute( ) {

    // Is this request related to admin area?
    if(is_admin()) {

      // If I'm already logged in
      if( TRUE == is_user_logged_in() ) {

        // Get my user data
        $cUser = new WP_User( get_current_user_id() );

        // Do I belong to some WP role?
        if( ! empty( $cUser->roles ) ) {

          // Do I belong to an interdicted users?
          if( in_array( $cUser->roles[0], $this->_blockedRoles ) ) {

            // Generate HTML output with HTTP header as set in rule 2 options
            header( 'HTTP/1.0 ' . $this->_httpHeader );
            echo '<h1>' . $this->_httpHeader . '</h1>';
            echo $this->_htmlOutput;
            exit(1);
          }
        }
      }
    }

  }

  /**
   * This is the Ajax action invoked in handling this rule. This method overrides correctly parent ajaxGateway method,
   * that should be an abstract.
   *
   * @brief This AJAX action is invoked in handling this rule
   *
   * @since 1.0.0
   *
   */
  public function ajaxGateway() {

    // Out of that if I'm not in Ajax channel!
    if( FALSE == wpdk_is_ajax() ) {
      echo '<h3>Unable to execute the method ' . __METHOD__ .'in this way</h3>';
      die();
    }

    // Init Ajax response
    $response = array();

    switch( $_POST['command'] ) {

      case self::ENGAGE_RULE:

        // Build parameter = value array
        $parameters = array();
        foreach( explode('&', $_POST['data']) as $chunk ) {
          $single = explode( '=', $chunk );
          $parameters[ urldecode( $single[0] ) ][] = urldecode( $single[1] );
        }

        // Filter WP Roles
        $roles = new WP_Roles();
        $arrayRoles = $roles->get_names();
        foreach( $parameters[ $this->id() . '-roles'] as $key => $role ) {
          if( ! array_key_exists( $role, $arrayRoles )) {
            // Received an unknown role ( ?? )
            unset( $parameters[ $this->id() . '-roles'][$key] );
          }
        }

        // Filter http_header
        $regexpCheck = '~' . '[0-9]{3} [a-zA-Z]+' . '~';
        $httpHeader  = filter_var( $parameters[ $this->id() . '-http-header'][0], FILTER_VALIDATE_REGEXP,
           array( 'options' => array( 'regexp' => $regexpCheck ) ) );
        if( empty( $httpHeader ) ) {
          // Autoset default
          $httpHeader = self::DEFAULT_HTTP_HEADER;
        }

        // Filter html_output
        $htmlOutput = $parameters[ $this->id() . '-html-output'][0];
        $htmlOutput = strip_tags( $htmlOutput, '<p><div><span><strong><em><br>');
        if( empty( $htmlOutput ) ) {
          // Autoset default
          $htmlOutput = self::DEFAULT_HTML_OUTPUT;
        }

        // Enable rule 2 in configuration
        $this->status( self::RULE_ON );
        $this->_httpHeader = $httpHeader;
        $this->_htmlOutput = $htmlOutput;
        $this->_blockedRoles = array_values( $parameters[ $this->id() . '-roles'] );

        break;

      case self::DISABLE_RULE:

        // Disable rule 2 in configuration
        $this->status( self::RULE_OFF );
        $this->_httpHeader    = self::DEFAULT_HTTP_HEADER;
        $this->_htmlOutput    = self::DEFAULT_HTML_OUTPUT;
        $this->_blockedRoles  = array();

        break;

      default:

        $response['message'] = __( 'General error in Deflector Rule 2', WPXDEFLECTOR_TEXTDOMAIN );

    }

    // Store new configuration into DB
    $this->_saveSettings();

    // Return response to caller
    echo json_encode( $response );
    die();

  }

  /**
   * This function is the get interface to htmlOutput property.
   *
   * @brief Get interface to htmlOutput property.
   *
   * @return string The current value of htmlOutput.
   *
   * @since 1.0.0
   *
   */
  public function htmlOutput() {
    return $this->_htmlOutput;
  }

}



/**
 * Basic Shields Rule #2 View
 *
 * @class              WPXDeflectorBasicShieldsRule2View
 * @author             yuma <info@wpxtre.me>
 * @copyright          Copyright (C) 2013- wpXtreme Inc. All Rights Reserved.
 * @date               2013-03-07
 * @version            1.0.0
 *
 */
class WPXDeflectorBasicShieldsRule2View extends WPXDeflectorRuleView {

  //---------------------------------------------------------------------------
  // METHODS
  //---------------------------------------------------------------------------

  /**
   * Return an instance of WPXDeflectorBasicShieldsRule2View class
   *
   * @brief Construct
   *
   * @param string $model
   * @param string $classes
   *
   * @return WPXDeflectorBasicShieldsRule2View
   */
  public function __construct( $model, $classes = '') {

    // Create the standard rule view, in a single line, with swipe and icon.
    parent::__construct( $model, $classes );

  }

  /**
   * Build the help content view
   *
   * @brief Help content view
   *
   * @param string $title - The title of help view.
   * @param string $message - The help message to show.
   *
   * @since 1.0.0
   *
   */
  public function helpContent( $title, $message ) {

    $content = <<<CONTENT
<div class="wpxdeflector-help-box">
<div class="wpxdeflector-help-title">
CONTENT;

    $content .= $title;

    $content .= <<<CONTENT
<span class="wpxdeflector-accordion-selector">Read more...</span>
</div>
<div id="wpxdeflector-accordion-help">
  <p>
CONTENT;

    $content .= $message;

    $content .= <<<CONTENT
  </p>
</div>
</div>
CONTENT;

  return $content;

  }

  /**
   * Twitter Bootstrap Modal to attach to this rule
   *
   * @brief Twitter Bootstrap Modal to attach to this rule
   *
   * @since 1.0.0
   *
   */
  public function modalContent() {

    $fields = array( __( 'Basic Shields Rule Settings', WPXDEFLECTOR_TEXTDOMAIN ) => array() );
    $sKey = key( $fields );

    //--------------------------------------------------
    // ROLES LIST
    //--------------------------------------------------

    $roles = new WP_Roles();

    $helpContent = $this->helpContent(  __( 'Choose the WordPress roles to block.', WPXDEFLECTOR_TEXTDOMAIN ),
                                        __( 'Any user belonging to one of the roles you select from list below <strong>will be completely unable to browse WordPress admin area from the moment you enable this security rule on</strong>. Users belonging to Administrator role are ALWAYS enabled, since the Administrator role is not listed at all.', WPXDEFLECTOR_TEXTDOMAIN ) );

    $fields[$sKey][] = array(
      array(
        'type'    => WPDKUIControlType::CUSTOM,
        'content' => $helpContent
      )
    );

    $fields[$sKey][] =  array(

      array(
        'table_title' => __( 'WordPress roles', WPXDEFLECTOR_TEXTDOMAIN ),
        'type'        => WPDKUIControlType::SELECT_LIST,
        'name'        => $this->_model->id() . '-roles',
        'title'       => __( 'Choose WordPress roles to block', WPXDEFLECTOR_TEXTDOMAIN ),
        'label' => array(
          'value' => __( 'WP roles to block', WPXDEFLECTOR_TEXTDOMAIN ),
          'data' => array( 'placement' => 'right' )
        ),
        'options'     => array_diff( $roles->get_names(), array( 'administrator' => 'Administrator' ) ),
        'value'       => 'subscriber'
      )

    );

    //--------------------------------------------------
    // HEADER HTTP
    //--------------------------------------------------

    $helpContent = $this->helpContent(  __( 'Choose the HTTP header to send to client.', WPXDEFLECTOR_TEXTDOMAIN ),
                                        __( 'Choose the HTTP header to send to client, when an unauthorized user tries to browse a page of your WordPress admin area. If you don\'t know what this field means, you can safely leave the default selected.', WPXDEFLECTOR_TEXTDOMAIN ) );

    $fields[$sKey][] = array(
      array(
        'type'    => WPDKUIControlType::CUSTOM,
        'content' => $helpContent
      )
    );

    $aHTTPHeaders = array(
      __( '401 Unauthorized', WPXDEFLECTOR_TEXTDOMAIN ) => __( '401 Unauthorized', WPXDEFLECTOR_TEXTDOMAIN ),
      __( '403 Forbidden', WPXDEFLECTOR_TEXTDOMAIN )    => __( '403 Forbidden', WPXDEFLECTOR_TEXTDOMAIN )
       );

    $fields[$sKey][] =  array(

      array(
        'table_title' => __( 'HTTP Header', WPXDEFLECTOR_TEXTDOMAIN ),
        'type'        => WPDKUIControlType::SELECT,
        'name'        => $this->_model->id() . '-http-header',
        'title'       => __( 'Choose HTTP Header', WPXDEFLECTOR_TEXTDOMAIN ),
        'label' => array(
          'value' => __( 'HTTP header', WPXDEFLECTOR_TEXTDOMAIN ),
          'data' => array( 'placement' => 'right' )
        ),
        'options'     => $aHTTPHeaders,
        'value'       => $aHTTPHeaders[__( '403 Forbidden', WPXDEFLECTOR_TEXTDOMAIN )]
      )

    );

    //--------------------------------------------------
    // HTML OUTPUT
    //--------------------------------------------------

    $helpContent = $this->helpContent(  __( 'Enter the HTML output you want to show.', WPXDEFLECTOR_TEXTDOMAIN ),
                                        __( 'Enter the HTML output you want to show, when an unauthorized user tries to browse a page of your WordPress admin area. You can use also this HTML tags:', WPXDEFLECTOR_TEXTDOMAIN ) .
                                        ' ' . htmlentities('<p><div><span><strong><em><br>') . '. ' .
                                        __( 'If you don\'t know what this field means, you can safely leave the default selected.', WPXDEFLECTOR_TEXTDOMAIN ) );

    $fields[$sKey][] = array(
      array(
        'type'    => WPDKUIControlType::CUSTOM,
        'content' => $helpContent
      )
    );

    $fields[$sKey][] =  array(
      array(
        'type'  => WPDKUIControlType::TEXTAREA,
        'name'  => $this->_model->id() . '-html-output',
        'rows'  => 6,
        'cols'  => 32,
        'attrs' => array( 'maxlength' => WPXDeflectorBasicShieldsRule2::MAX_LENGTH_HTML_OUTPUT ),
        'label' => array(
          'value' => __( 'HTML output', WPXDEFLECTOR_TEXTDOMAIN ),
          'data' => array( 'placement' => 'right' )
        ),
        'title' => __( 'Enter the HTML output you want to show, when an unauthorized user tries to browse a page of your WordPress admin area. If you don\'t know what this field means, you can safely leave the default selected.', WPXDEFLECTOR_TEXTDOMAIN ),
        // Default is always the last HTML choosen, even after rule deactivation
        'value' => $this->_model->htmlOutput()
      )
    );

    $layout = new WPDKUIControlsLayout( $fields );

    // Build output buffer
    $outputBuffer = '<form name="' . $this->_model->id() . '-form" method="POST" action="">' .
                    $layout->html() .
                    '</form>';

    return $outputBuffer;

  }

}
