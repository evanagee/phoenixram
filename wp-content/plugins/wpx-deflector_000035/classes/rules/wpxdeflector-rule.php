<?php
/**
 * iUnknown Deflector Rule Model
 *
 * @class              WPXDeflectorRule
 * @author             yuma <info@wpxtre.me>
 * @copyright          Copyright (C) 2013- wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-28
 * @since              1.0.0
 *
 */

class WPXDeflectorRule {

  //---------------------------------------------------------------------------
  // INTERNAL CONSTANTS
  //---------------------------------------------------------------------------

  /**
   * string - Engage rule command
   *
   * @brief Engage rule command
   *
   * @since 1.0.0
   */
  const ENGAGE_RULE = 'WPXDEFLECTOR-ENGAGE-RULE';

  /**
   * string - Disable rule command
   *
   * @brief Disable rule command
   *
   * @since 1.0.0
   */
  const DISABLE_RULE = 'WPXDEFLECTOR-DISABLE-RULE';

  /**
   * string - Constant related to rule activation state
   *
   * @brief Rule activation state.
   *
   * @since 1.0.0
   */
  const RULE_ON = 'on';

  /**
   * string - Constant related to rule deactivation state
   *
   * @brief Rule deactivation state.
   *
   * @since 1.0.0
   */
  const RULE_OFF = 'off';

  /**
   * string - Constant related to a rule that can be directly (a|de)ctivated
   *
   * @brief Rule with swipe control
   *
   * @since 1.0.0
   */
  const RULE_WITH_STATUS_CHANGEABLE = 'rule-changeable';

  /**
   * string - Constant related to a rule that cannot be directly (a|de)ctivated
   *
   * @brief Rule with no swipe control
   *
   * @since 1.0.0
   */
  const RULE_CHECK_ONLY = 'rule-check-only';

  /**
   * int - Max length of a rule help info in direct content, in bytes
   *
   * @brief Max length of a rule help info in direct content
   *
   * @since 1.0.0
   */
  const MAX_HELP_LENGTH = 1024;

  //---------------------------------------------------------------------------
  // PROPERTIES
  //---------------------------------------------------------------------------

  /**
   * Rule type: it can be a rule that can be (a|de)ctivated, or a simple check
   *
   * @brief Rule type
   *
   * @var string $_type
   *
   * @since 1.0.0
   */
  protected $_type;

  /**
   * Rule status: it can be set to one of the constant specified above.
   *
   * @brief Rule status
   *
   * @var string $_status
   *
   * @since 1.0.0
   */
  protected $_status;

  /**
   * The id of the rule.
   *
   * @brief Rule id
   *
   * @var string $_id
   *
   * @since 1.0.0
   */
  protected $_id;

  /**
   * The unique ID of the internal method used as Ajax gateway for the rule.
   *
   * @brief Ajax gateway unique ID for this rule.
   *
   * @var string $_ajaxGatewayID
   *
   * @since 1.0.0
   */
  protected $_ajaxGatewayID;

  /**
   * The title of the rule.
   *
   * @brief Rule title
   *
   * @var string $_title
   *
   * @since 1.0.0
   */
  protected $_title;

  /**
   * Help rule type. It can be 'external-link', 'wpx-devcenter-slug', 'direct-content'
   *
   * @brief Help rule type.
   *
   * @var string $_helpType
   *
   * @since 1.0.0
   */
  protected $_helpType;

  /**
   * Link to detailed help about the rule. It can be slug to a guide stored in wpXtreme Developer Center
   *
   * @brief Link to detailed help about the rule.
   *
   * @var string $_helpLink
   *
   * @since 1.0.0
   */
  protected $_helpLink;

  /**
   * Help content. It can be NULL, if help is in an external resource.
   *
   * @brief Help content.
   *
   * @var string $_helpContent
   *
   * @since 1.0.0
   */
  protected $_helpContent;

  /**
   * Title of modal window showing detailed help about the rule.
   *
   * @brief Title of help window.
   *
   * @var string $_helpWindowTitle;
   *
   * @since 1.0.0
   */
  protected $_helpWindowTitle;

  //---------------------------------------------------------------------------
  // METHODS
  //---------------------------------------------------------------------------

  /**
   * Return an instance of WPXDeflectorRule class
   *
   * @brief Construct
   *
   * @param        $id
   * @param        $title
   * @param string $type
   *
   * @return WPXDeflectorRule
   */
  public function __construct( $id, $title, $type = self::RULE_WITH_STATUS_CHANGEABLE ) {

    // The id is used by _syncWithDB as option DB key
    $this->_id          = $this->id( $id );

    // Set default basic settings
    $this->_status  = self::RULE_OFF;

    // This is a FIRST THING THE CONSTRUCTOR MUST DO: SYNC WITH DB TO GET SETTINGS!
    // This sync acts ONLY if this rule has an Ajax interface to set status, i.e. if this rule is
    // of type RULE_WITH_STATUS_CHANGEABLE. Because only Ajax interface save settings to DB.
    // If this rule is RULE_CHECK_ONLY, this sync does nothing because nothing is stored on DB.
    $this->_syncWithDB();

    // Set new rule property values as requested by code
    $this->_title = $this->title( $title );
    $this->_type  = $type;

    // Open the Ajax Gateway and save its ID for using by the view
    $ajax = WPXDeflectorAjax::getInstance();
    $this->_ajaxGatewayID = $ajax->registerAjaxGateway( $this, 'ajaxGateway', false );

  }

  /**
   * This function is the prototype of rule execution, and must be override by any single rule with specific code.
   *
   * @brief Rule execution function prototype
   *
   * @since 1.0.0
   *
   */
  public function execute() {

    // To override by any single rule!

  }

  /**
   * This method returns a title => value array with custom settings of this rule, in the form
   * 'setting title to display' => 'its value'. It is used in rule view to show current custom settings of a rule
   * when its status is ON. Any single rule has to override this method in order to show its custom settings under rule title.
   *
   * @brief Return settings array
   *
   * @since 1.0.0
   *
   */
  public function settingsData() {
    return( array() );
  }

  /**
   * This method reset the rule status to OFF when Deflector is going to deactivate. This behaviour is necessary to leave
   * a clean environment status when plugin is deactivated.
   * Deflector disables _ALL_ rules, even rules added by any Deflector extension. So any single rule has to override this
   * method with its custom cleaning, if needed, and then call the parent.
   * For example, a rule that set some specific behaviours in .htaccess must clean them on Deflector deactivation
   * as well as the rule status, because .htaccess is checked _ALWAYS_ in an Apache environment, no matter if Deflector is
   * active or not.
   *
   * @brief Reset the rule status to OFF when Deflector is going to deactivate.
   *
   * @since 1.0.0
   *
   */
  public function disableRuleOnDeflectorDeactivation() {

    $this->status( self::RULE_OFF );
    $this->_saveSettings();

  }

  /**
   * This function is the prototype of rule Ajax gateway, and must be override by any single rule with specific code, if
   * necessary. If this method is not overriden, this method will be the default Ajax gateway for the rule, and just handles
   * rule status.
   *
   * @brief Default Ajax gateway for a Deflector rule
   *
   * @since 1.0.0
   *
   */
  public function ajaxGateway() {

    // Out of that if I'm not in Ajax channel!
    if( false == wpdk_is_ajax() ) {
      echo '<h3>Unable to execute the method ' . __METHOD__ .'in this way</h3>';
      die();
    }

    // Init Ajax response
    $response = array();

    switch( $_POST['command'] ) {

      case self::ENGAGE_RULE:

        // Enable rule
        $this->status( self::RULE_ON );

        break;

      case self::DISABLE_RULE:

        // Disable rule
        $this->status( self::RULE_OFF );

        break;

      default:

        $response['message'] = __( 'General error in Deflector Default Ajax Gateway.', WPXDEFLECTOR_TEXTDOMAIN );

    }

    // Store new configuration into DB
    $this->_saveSettings();

    // Return response to caller
    echo json_encode( $response );
    die();

  }

  /**
   * This function hooks the rule execution to a specific WordPress hook, but only if rule status is ON.
   *
   * @brief Hook rule execution to WordPress hook.
   *
   * @param string $hook - WordPress action to hook in.
   *
   * @return boolean TRUE if hook has been completed successfully, FALSE otherwise.
   *
   * @since 1.0.0
   *
   */
  public function hookExecutionToAction( $hook ) {
    if( self::RULE_ON == $this->status() ) {
      return add_action( $hook, array( $this, 'execute' ) );
    }
    return false;
  }

  /**
   * This function hooks the rule execution to a specific WordPress filter(s), but only if rule status is ON.
   *
   * @brief Hook rule execution to specific WordPress filter(s).
   *
   * @param string|array $filters - WordPress filters to hook in. It can be a string, or an array of filters name.
   *
   * @return boolean TRUE if hook has been completed successfully, FALSE otherwise.
   *
   * @since 1.0.0
   *
   */
  public function hookExecutionToFilters( $filters ) {

    // If current rule is active
    if( self::RULE_ON == $this->status() ) {

      // If $filters is an array
      if( is_array( $filters ) ) {
        foreach( $filters as $filter ) {
          add_filter( $filter, array( $this, 'execute' ) );
        }
      }
      // or it is string
      elseif( is_string( $filters ) ) {
        add_filter( $filters, array( $this, 'execute' ) );
      }
    }

  }

  /**
   * This method syncs this instance with rule settings saved in DB. It is invoked in constructor of rule model.
   *
   * @brief Sync this instance with DB.
   *
   * @return boolean TRUE if sync has been completed successfully, FALSE otherwise.
   *
   * @since 1.0.0
   *
   */
  private function _syncWithDB() {

    // Array of special properties
    $arraySpecialProp = array( '_status', '_id' );

    // Try to get the rule settings from DB
    $ruleInDB = get_option( $this->id() );
    if( false !== $ruleInDB) {

      // Is the DB object in confidence with $this?
      if( true == is_a( $ruleInDB, 'WPXDeflectorRule' ) ) {

        // Sync DB properties --> $this properties
        foreach($ruleInDB as $property => $value) {

          // Is this property yet existent in $this?
          if( property_exists( $this, $property ) ) {

            // Don't assign special properties to NULL value - any other prop can be NULL
            if( false == ( in_array( $property, $arraySpecialProp ) && ( null == $value ) ) ) {
              $this->$property = $value;
            }

          }

        }

        return true;

      }

    }

    return false;

  }

  /**
   * This method sets the help of a rule, that will be linked to 'info' icon of the rule.
   *
   * Help can be one of these:
   *
   * 1) an external link to a web page
   * 2) a link to a page into wpXtreme Developer Center
   * 3) a specific content directly from input.
   *
   * [1] has the form http://.....
   *
   * [2] has the form wpxdevcenter://slug-to-page-in-developer-center
   * In this case, this method cuts 'wpxdevcenter://' and leave only slug, so WPDK can get it.
   *
   * [3] has a normal HTML syntax.
   *
   * @brief Set the help of the rule.
   *
   * @param string $content - The help content. It can be a web link, a wpXtreme Developer Center link, or a simple HTML
   *                          content ( see method description ). If missing, method returns an array of help data.
   * @param string $windowTitle - The title of modal window containing help. Optional.
   *
   * @return array|boolean TRUE if help has been correctly set; an array of help data if all parameters are omitted.
   *
   * @since 1.0.0
   *
   */
  public function help( $content = '', $windowTitle = '' ) {

    // Do I need to get help data?
    if( empty( $content ) ) {
      $arrayHelp = array(
        'type'    => $this->_helpType,
        'link'    => $this->_helpLink,
        'content' => $this->_helpContent,
        'title'   => $this->_helpWindowTitle
      );
      return $arrayHelp;
    }

    // Default
    $this->_helpType        = 'direct-content';
    $this->_helpContent     = '';
    $this->_helpLink        = '#';
    $this->_helpWindowTitle = '';

    // Case [1] - external link
    if( 1 == preg_match( '|^http(s?)://|', $content ) ) {
      // Sanitize and save URL
      $this->_helpLink = esc_url( $content, array( 'http', 'https' ) );
      $this->_helpType = 'external-link';
    }

    // Case [2] - wpXtreme Developer Center internal url
    elseif( 1 == preg_match( '|^wpxdevcenter://|', $content ) ) {
      // Sanitize and save URL with modal window title, if present
      $this->_helpLink        = sanitize_key( substr( $content, strlen('wpxdevcenter://') ) );
      $this->_helpWindowTitle = $windowTitle;
      $this->_helpType        = 'wpx-devcenter-slug';
    }

    // Case [3] - content IS the help
    else {
      // Sanitize and save HTML content (max bytes check) with modal window title, if present
      $this->_helpContent     = substr( strip_tags( $content, '<p><div><span><a><strong><em><br><br/>'),
                                                    0, self::MAX_HELP_LENGTH );
      $this->_helpWindowTitle = $windowTitle;
      $this->_helpType        = 'direct-content';
    }

    return true;

  }

  /**
   * This method saves current instance in DB. It is used to save all settings about rule.
   *
   * @brief Save this instance with DB.
   *
   * @return boolean TRUE if saving has been completed successfully, FALSE otherwise.
   *
   * @since 1.0.0
   *
   */
  protected function _saveSettings() {
    return update_option( $this->id(), $this );
  }

  /**
   * This function is the set/get interface to status property.
   *
   * @brief Set/get interface to status property.
   *
   * @param string $newStatus - New rule status to set. If missing, function return current status.
   *
   * @return string The current status, even on set.
   *
   * @since 1.0.0
   *
   */
  public function status( $newStatus = '' ) {

    // Set
    if( ! empty( $newStatus ) ) {
      $arrayAllowedValues = array( self::RULE_OFF, self::RULE_ON );
      if( in_array( $newStatus, $arrayAllowedValues ) ) {
        $this->_status = $newStatus;
      }
    }

    return $this->_status;

  }

  /**
   * This function is the set/get interface to title property.
   *
   * @brief Set/get interface to title property.
   *
   * @param string $newTitle - New rule title to set. If missing, function return current title.
   *
   * @return string The current title, even on set.
   *
   * @since 1.0.0
   *
   */
  public function title( $newTitle = '' ) {

    // Set
    if( ! empty( $newTitle ) ) {
      $this->_title = strip_tags( $newTitle );
    }

    return $this->_title;

  }

  /**
   * This function is the get interface to Ajax gateway property.
   *
   * @brief Get interface to Ajax gateway property.
   *
   * @return string The current Ajax gateway ID
   *
   * @since 1.0.0
   *
   */
  public function ajaxGatewayID() {
    return $this->_ajaxGatewayID;
  }

  /**
   * This function is the get interface to type property.
   *
   * @brief Get interface to type property.
   *
   * @return string The current rule type
   *
   * @since 1.0.0
   *
   */
  public function type() {
    return $this->_type;
  }

  /**
   * This function is the set/get interface to id property.
   *
   * @brief Set/get interface to id property.
   *
   * @param string $newID - New rule id to set. If missing, function return current id.
   *
   * @return string The current id, even on set.
   *
   * @since 1.0.0
   *
   */
  public function id( $newID = '' ) {

    // Set
    if( ! empty( $newID ) ) {
      $this->_id = sanitize_key( $newID );
    }

    return $this->_id;

  }

}

/**
 * iUnknown Deflector Rule View
 *
 * @class              WPXDeflectorRuleView
 * @author             yuma <info@wpxtre.me>
 * @copyright          Copyright (C) 2013- wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-28
 * @since              1.0.0
 *
 */
class WPXDeflectorRuleView extends WPDKView {

  //---------------------------------------------------------------------------
  // PROPERTIES
  //---------------------------------------------------------------------------

  /**
   * The rule model this view is related to. This is an instance of a class WPXDeflectorRule, or any of its children.
   *
   * @brief The rule model
   *
   * @var WPXDeflectorRule $_model
   *
   * @since 1.0.0
   */
  protected $_model;

  //---------------------------------------------------------------------------
  // METHODS
  //---------------------------------------------------------------------------

  /**
   * Return an instance of WPXDeflectorRuleView class
   *
   * @brief Construct
   *
   * @param string $model
   * @param string $classes
   *
   * @return WPXDeflectorRuleView
   */
  public function __construct( $model, $classes = '' ) {

    // Link this view to its model
    if( is_object( $model ) && ( true == is_a( $model, 'WPXDeflectorRule') ) ) {
      $this->_model = $model;

      // Engage parent constructor with right params
      parent::__construct( $this->_model->id(), $classes );

      // Generate view content with a line
      $this->generateContent();

    }

  }

  /**
   * Prepare content to be drawn. This content is a single rule line, with proper icons and styles.
   *
   * @brief Prepare content to be drawn
   *
   * @since 1.0.0
   *
   */
  public function generateContent() {

    // Get data from model
    $type           = $this->_model->type();
    $status         = $this->_model->status();
    $title          = $this->_model->title();
    $id             = $this->_model->id();
    $ajaxGatewayID  = $this->_model->ajaxGatewayID();
    $modalContent   = $this->modalContent();

    // Set class icon related to rule status
    if( WPXDeflectorRule::RULE_ON == $status ) {
      $classIcon      = 'wpxdeflector-rule-line-icon-enable';
      $statusTip      = __( 'This rule is currently active', WPXDEFLECTOR_TEXTDOMAIN );
      $statusMessage  = __( 'Status: Active', WPXDEFLECTOR_TEXTDOMAIN );
    }
    else {
      $classIcon      = 'wpxdeflector-rule-line-icon-disable';
      $statusTip      = __( 'This rule is currently NOT active', WPXDEFLECTOR_TEXTDOMAIN );
      $statusMessage  = __( 'Status: Not Active', WPXDEFLECTOR_TEXTDOMAIN );
    }

    // Build default swipe data attributes related to rule
    // Attach custom data about modal and ajax action related to this rule,
    // so javascript deflector kernel can use it, if exists, in javascript side.
    $arrayData = array(
      'placement' => 'left',
      'action'    => $ajaxGatewayID,
    );
    // If this rule has a modal, then attach modal id to the swipe for javascript handling
    if( ! empty( $modalContent ) ) {
      $arrayData['id-modal'] = $id . '-modal';
    }

    // Create swipe data only if rule type allows (a|de)activation
    $swipeContent = '';
    if( WPXDeflectorRule::RULE_WITH_STATUS_CHANGEABLE == $type ) {
      $swipeData = array(
        'type'            => WPDKUIControlType::SWIPE,
        'name'            => $id . '-swipe',
        'label_placement' => 'left',
        'data'            => $arrayData,
        'title'           => $title,
        'value'           => ( WPXDeflectorRule::RULE_ON == $this->_model->status() ) ? 'TRUE' : '',
      );
      $swipe        = new WPDKUIControlSwipe( $swipeData );
      $swipeContent = $swipe->html();
    }

    // Create HTML of custom settings
    $settingsHTML = '';
    // Display it only if rule status = ON
    if( WPXDeflectorRule::RULE_ON == $status ) {
      $arraySettings = $this->_model->settingsData();
      // If there is something to show
      if( ! empty( $arraySettings ) ) {
        $settingsHTML = '<div id="wpxdeflector-settings-rule-' . $id . '" class="wpxdeflector-current-params">';
        $flag = true;
        foreach( $arraySettings as $setting => $value ) {
          // Internal magheggi
          if( true == $flag )
            {$settingsHTML .= __( 'Current settings: ', WPXDEFLECTOR_TEXTDOMAIN );$flag=false;}
          else
            {$settingsHTML .= ' - ';}
          // Concat custom setting
          $settingsHTML .= '<strong>' . $setting . '</strong> = ' . $value;
        }
        $settingsHTML .= '</div>';
      }
    }

    // Create HTML help about rule
    $arrayHelpData = $this->_model->help();
    $dataTitle     = ( 'external-link' != $arrayHelpData['type'] ) ? 'data-title="' . $arrayHelpData['title'] . '"' : '';
    $windowTarget  = ( 'external-link' == $arrayHelpData['type'] ) ? 'target="_blank"' : '';
    $dataContent   = ( 'direct-content' == $arrayHelpData['type'] ) ? 'data-content="' . $arrayHelpData['content'] . '"' : '';
    $wpdkGuide     = ( 'external-link' != $arrayHelpData['type'] ) ? 'class="wpdk-guide button button-secondary wpdk-tooltip"' : 'class="button button-secondary wpdk-tooltip"';
    $help          = __( 'Help', WPXDEFLECTOR_TEXTDOMAIN );
    $helpHTML = <<< HTML
<a href="{$arrayHelpData['link']}" $windowTarget $dataTitle $dataContent $wpdkGuide id="wpxdeflector-rule-line-info" title="Read detailed info">{$help}</a>
HTML;

    // Reset the view content
    $this->content = '';

    // Build a twitter bootstrap modal with custom content for attaching it to swipe button.
    // But IF AND ONLY IF method modalContent returns a not empty HTML content!
    if( ! empty ( $modalContent ) ) {
      $this->content .= $this->modal( $modalContent );
    }

    // Build content line
    $this->content .= <<<HTML
<table id="wpxdeflector-rule-table-$this->id" class="wpxdeflector-rule-line-table" cellpadding="4" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td class="wpxdeflector-rule-line-icon" id="wpxdeflector-rule-line-icon-$id" ><span class="$classIcon wpdk-tooltip" title="$statusTip" id="wpxdeflector-rule-cell-icon-$id" >$statusMessage</span></td>
      <td class="wpxdeflector-rule-line-title" id="wpxdeflector-rule-line-title-$id" >$title $settingsHTML</td>
HTML;

    // Echo swipe cell only if rule allows changing status
    if( WPXDeflectorRule::RULE_WITH_STATUS_CHANGEABLE == $type ) {
      $this->content .= '<td class="wpxdeflector-rule-line-swipe" id="wpxdeflector-rule-line-swipe-$id" >' .
                        $swipeContent .
                        '</td>';
    }

    $this->content .= <<<HTML
      <td class="wpxdeflector-rule-line-icon" id="wpxdeflector-rule-line-icon-help-$id" >$helpHTML</td>
    </tr>
  </tbody>
</table>
HTML;


  }

  /**
   * This function is the method that ALL rules have to define in order to init a twitter bootstrap modal window.
   * If a rule view extending this base class implements this method with a custom HTML, then this rule will have
   * automatically attached a twitter bootstrap modal window that handles rule options.
   *
   * @brief The abstract method to implements if I need a twitter bootstrap modal window.
   *
   * @return string - The modal HTML content. If empty, no modal window will be created and attached to the rule.
   *
   */
  public function modalContent() {
    return '';
  }


  /**
   * Return the HTML markup for a twitter bootstrap modal
   *
   * @brief Return the HTML markup for a twitter bootstrap modal
   *
   * @param string $modalContent - The HTML modal content
   *
   * @return string - The HTML code of complete modal window.
   */
  public function modal( $modalContent ) {

    // Get data from model
    $id = $this->_model->id();

    // Init modal
    $modal = new WPDKTwitterBootstrapModal( $id . '-modal', __( 'Rule Settings', WPXDEFLECTOR_TEXTDOMAIN ));
    $modal->width = 600;

    // Hide close button in topright of window
    $modal->dismissButton = false;

    // Do not automatically close modal if I click outside of it
    $modal->backdrop = 'static';

    // Disable automatic ESC closing
    $modal->keyboard = 'false';

    // Add buttons
    $modal->addButton( $id . '-close', __( 'Close this window', WPXDEFLECTOR_TEXTDOMAIN ), true, 'button-primary alignleft' );
    $modal->addButton( $id . '-go', __( 'Enable this security rule!', WPXDEFLECTOR_TEXTDOMAIN ), false, 'button-primary alignright' );

    // Set modal content with custom handling of options
    $modal->content .= $modalContent;

    // return HTML of modal
    return $modal->html();

  }

}
