<?php
/**
 * Tips Rule #1 Model
 *
 * @class              WPXDeflectorTipsRule1
 * @author             yuma <info@wpxtre.me>
 * @copyright          Copyright (C) 2013- wpXtreme Inc. All Rights Reserved.
 * @date               2013-03-08
 * @since              1.0.0
 *
 * @note This rule model will use default rule view
 *
 */

class WPXDeflectorTipsRule1 extends WPXDeflectorRule {

  //---------------------------------------------------------------------------
  // INTERNAL CONSTANTS
  //---------------------------------------------------------------------------

  /**
   * int - Min length of database user for considering it OK
   *
   * @brief Min length of database user for considering it OK
   *
   * @since 1.0.0
   */
  const MIN_DBUSER_LENGTH = 6;

  //---------------------------------------------------------------------------
  // METHODS
  //---------------------------------------------------------------------------

  /**
   * Create an instance of WPXDeflectorTipsRule1 class
   *
   * @brief Construct
   *
   * @return WPXDeflectorTipsRule1
   */
  function __construct( $id, $title ) {

    // Set custom properties to default - nothing to do here

    // Engage parent constructor ( and restore settings as they have been saved )
    parent::__construct( $id, $title, self::RULE_CHECK_ONLY );

  }

  /**
   * This function executes tips rule 1 check. If check passed, rule status becomes RULE_ON. Overridden parent method.
   *
   * @brief Execute tips rule 1 check.
   *
   * @since 1.0.0
   *
   * @note Rule status is set directly here, because this rule is check only ( no user changeable ).
   *
   */
  public function execute( ) {

    if( ('root' != DB_USER ) &&
        ( strlen( DB_USER ) >= self::MIN_DBUSER_LENGTH ) ) {
      $this->status( self::RULE_ON );
    }

  }

}
