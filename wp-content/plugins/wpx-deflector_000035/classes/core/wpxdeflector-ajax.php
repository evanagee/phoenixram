<?php

/**
 * Ajax gateway engine
 *
 * @class              WPXDeflectorAjax
 * @author             yuma <info@wpxtre.me>
 * @copyright          Copyright (C) 2013- wpXtreme Inc. All Rights Reserved.
 * @date               2013-02-01
 * @since              1.0.0
 *
 */
class WPXDeflectorAjax {

  /**
   * Create or return a singleton instance of WPXtremeAjax
   *
   * @brief Create or return a singleton instance of WPXtremeAjax
   *
   * @return WPXDeflectorAjax
   */
  public static function getInstance() {
    static $instance = null;
    if ( is_null( $instance ) ) {
      $instance      = new WPXDeflectorAjax();
    }
    return $instance;
  }

  /**
   * Alias of getInstance();
   *
   * @brief Init the ajax register
   *
   * @return WPXtremeAjax
   */
  public static function init() {
    return self::getInstance();
  }

  /**
   * No register of any Ajax action by default in Deflector. Return an empty array because every rule is responsible
   * of registering its Ajax gateway.
   *
   * @brief No ajax action registered
   *
   * @return array
   */
  public function actions() {
    return array();
  }

  /**
   * Register a function as Ajax gateway
   *
   * @brief Register the function in input as Ajax gateway
   *
   * @param object $objectInstance - The instance of object that owns the method to register
   * @param string $method - The method name to register as Ajax gateway
   * @param boolean $forAllUsers - if TRUE, the Ajax gateway is open for both visitors and logged-in users
   *
   * @return string - The unique ID for this Ajax channel; default Ajax channel has always ID = 'WPXDeflectorRule-ajaxGateway'
   *
   * @since 1.0.0
   *
   */
  public function registerAjaxGateway( $objectInstance, $method, $forAllUsers ) {

    // Generate unique ID for this Ajax channel
    $uniqueID = get_class( $objectInstance ) . '-' . $method;

    // In case of more rule models with the same class type, create an even more unique ID
    while( TRUE == has_action( 'wp_ajax_' . $uniqueID ) ) {
      $uniqueID .= '_n';  // again... :-)
    }

    // Create a unique Ajax action for this class
    add_action( 'wp_ajax_' . $uniqueID, array( $objectInstance, $method ) );
    if( TRUE == $forAllUsers ) {
      add_action( 'wp_ajax_nopriv_' . $uniqueID, array( $objectInstance, $method ) );
    }

    // return unique ID for this Ajax channel
    return $uniqueID;

  }

} // WPXDeflectorAjax
