<?php
/**
 * Defines
 *
 * @author             wpXtreme <info@wpxtre.me>
 * @copyright          Copyright 2013- wpXtreme inc.
 * @date               2013-02-15 15:15:04
 * @version            0.1.0
 *
 */

/**
 * @var WPXDeflector $this
 */

define( 'WPXDEFLECTOR_VERSION',           $this->version );
define( 'WPXDEFLECTOR_TEXTDOMAIN',        $this->textDomain );
define( 'WPXDEFLECTOR_TEXTDOMAIN_PATH',   $this->textDomainPath );

define( 'WPXDEFLECTOR_URL_ASSETS',        $this->assetsURL );
define( 'WPXDEFLECTOR_URL_CSS',           $this->cssURL );
define( 'WPXDEFLECTOR_URL_JAVASCRIPT',    $this->javascriptURL );
define( 'WPXDEFLECTOR_URL_IMAGES',        $this->imagesURL );



