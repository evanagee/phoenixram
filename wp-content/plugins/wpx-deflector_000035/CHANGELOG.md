# CHANGELOG

## Version 1.0.2
### 2013-06-11

#### Enhancers

* Updated kickstart.php



## Version 1.0.1
### 2013-04-22

#### Bugs

* Ticket #103 - Fixed wrong used of self (in uppercase)



## Version 1.0.0
### 2013-04-15

* First public release of plugin
