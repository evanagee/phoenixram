<?php
// Silence is golden or you can use this file for extra doxygen documentation

/**
 * Welcome to **Deflector**
 *
 * @mainpage   Welcome
 *
 * @section    introduction Introduction
 *             Deflector is a Secure your WordPress installation from hackers and attackers, adding tons of security features in your system. Deflector is a plugin by wpXtreme..
 *
 *
 * @page       page_1 Getting Started
 *
 * @section    section_1_1 The Section One
 *
 * This is the content of section one for page Getting Started. Use markdown as you like.
 *
 *
 */