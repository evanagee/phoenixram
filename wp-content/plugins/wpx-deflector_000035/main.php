<?php
/// @cond private
/**
 * Plugin Name:     Deflector
 * Plugin URI:      https://wpxtre.me
 * Description:     Surrounds your WordPress installation to protect against enemy attacks or natural hazards :-). Secure your WordPress installation from hackers and attackers, adding tons of security features in your system.
 * Version:         1.0.2
 * Author:          wpXtreme, Inc.
 * Author URI:      https://www.wpxtre.me
 * Text Domain:     wpx-deflector
 * Domain Path:     localization
 *
 * WPX PHP Min: 5.2.4
 * WPX WP Min: 3.5
 * WPX MySQL Min: 5.0
 * WPX wpXtreme Min: 1.0.0.b4
 *
 */
/// @endcond

/* Avoid directly access. */
if ( !defined( 'ABSPATH' ) ) {
  exit;
}

// wpXtreme kickstart logic
require_once( trailingslashit( dirname( __FILE__ ) ) . 'kickstart.php' );

// Engage this WPX plugin
wpxtreme_kickstart( __FILE__, 'WPXDeflector', 'wpx-deflector.php' );

