# Deflector

## Overview

Deflector is a plugin that surrounds your WordPress installation to protect against enemy attacks or natural hazards :-). Secure your WordPress installation from hackers and attackers, adding tons of security features in your system. Deflector is a plugin by wpXtreme.

## System Requirements

* wpXtreme framework 1.0.0.b4 or higher (last version is suggested)
* WPDK framework 1.0.0.b4 or higher (last version is suggested)
* Wordpress 3.5 or higher (last version is suggest)
* PHP version 5.2.4 or greater
* MySQL version 5.0 or greater

## Browser compatibility

* We suggest to update your browser always at its last version.
