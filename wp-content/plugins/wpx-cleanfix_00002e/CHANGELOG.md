# CHANGELOG

## Version 1.0.8
### 2013-06-11

* Updated kickstart.php
* Minor alignments


## Version 1.0.7
### 2013-04-29

* Improved get url of main view controller


## Version 1.0.6
### 2013-04-22

#### Enhancements

* Renamed main styles filename

#### Bugs

* Fixed wrong icon render in about view controller
* Fixed some documentation
* Fixed refuse strings


## Version 1.0.5
### 2013-01-24

#### Enhancements

* wpXtreme framework 1.0.0.b4 compatibility
* Added link to Issue Report in Info view
* Added and Improved localization

#### Bugs

* Fixed #11 - Replaced deprecated `WPDKUI::badged()` with `WPDKUI::badge()`
* Minor fixes


## Version 1.0.2
### 2013-01-11

#### Enhancements

* Added right Icon in Info view

#### Bugs

* Minor fixed in info view
* Fixed hidden icon in standard WordPress plugin list
* Fixes #10 - Warning in headMenuMain action


## Version 1.0.1
### 2013-01-09

#### Bugs

* Minor fixes


## Version 1.0.0
### 2012-12-10

#### Bugs

* First release as porting from WP CleanFix