<?php
/// @cond private
/**
 * Plugin Name: CleanFix
 * Plugin URI: https://wpxtre.me
 * Description: Clean and fix tools! Repair corrupted data and clean up your database
 * Version: 1.0.8
 * Author: wpXtreme, Inc.
 * Author URI: https://wpxtre.me
 * Text Domain: wpx-cleanfix
 * Domain Path: localization
 *
 * WPX PHP Min: 5.2.4
 * WPX WP Min: 3.5
 * WPX MySQL Min: 5.0
 * WPX wpXtreme Min: 1.0.0.b4
 *
 */
// @endcond

/* Avoid directly access. */
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

// wpXtreme kickstart logic
require_once( trailingslashit( dirname( __FILE__ ) ) . 'kickstart.php' );

wpxtreme_kickstart( __FILE__, 'WPXCleanFix', 'wpx-cleanfix.php' );