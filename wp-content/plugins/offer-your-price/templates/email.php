<?php if (!defined('ABSPATH')) exit; ?>

<?php woocommerce_get_template('emails/email-header.php', array( 'email_heading' => $email_heading )); ?>

<?php 

echo $message . "<br /><br />";

$link = '<a href="' . get_permalink( $product->id ) . '">link</a>';

$quantity_string = ( $offer_action['quantity'] > 1 ) ? 'each on <strong>' . $offer_action['quantity'] . '</strong> qty. of' : 'on';

echo sprintf(__("You have received this coupon worth %s %s following product and this coupon is valid till %s. Click on the product $link to buy it.", "woocommerce"), '<strong>' . woocommerce_price($offer_action['coupon_amount']) . '</strong>', $quantity_string, '<strong>' . $coupon_expiry_date . '</strong>' ); ?>

<br /><br />

<center><a href="<?php echo get_permalink( $product->id ); ?>">

<?php 

$size = 'shop_thumbnail';
if ( isset( $product->variation_id ) && $product->variation_id && has_post_thumbnail( $product->variation_id ) ) {
        $image = get_the_post_thumbnail( $product->variation_id, $size );
} elseif ( has_post_thumbnail( $product->id ) ) {
        $image = get_the_post_thumbnail( $product->id, $size );
} elseif ( $parent_id = wp_get_post_parent_id( $product->id ) && has_post_thumbnail( $parent_id ) ) {
        $image = get_the_post_thumbnail( $parent_id, $size );
} else {
        $image = '';
}

echo $image;

?>

<br /><strong><?php echo ( isset( $product->variation_id ) && $product->variation_id > 0 ) ? get_product_title( $product->variation_id ) : get_the_title( $product->id ); ?></strong>

</a></center>

<p><?php echo sprintf(__("To redeem your discount use the following coupon code during checkout:", 'woocommerce'), $blogname); ?></p>

<strong style="margin: 10px 0; font-size: 2em; line-height: 1.2em; font-weight: bold; display: block; text-align: center;"><?php echo $offer_coupon_code; ?></strong>

<div style="clear:both;"></div>

<?php woocommerce_get_template('emails/email-footer.php'); ?>