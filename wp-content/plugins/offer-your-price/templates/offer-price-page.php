<?php
//if ( !function_exists( 'woocommerce_get_template' ) )    require_once ABSPATH . 'wp-content/plugins/woocommerce/woocommerce-core-functions.php';
//if ( !function_exists( 'woocommerce_quantity_input' ) )    require_once ABSPATH . 'wp-content/plugins/woocommerce/woocommerce-template.php';
$colors = (array) get_option( 'woocommerce_frontend_css_colors' );
$size = 'shop_thumbnail';

if ( isset( $product->variation_id ) && $product->variation_id && has_post_thumbnail( $product->variation_id ) ) {
        $image = get_the_post_thumbnail( $product->variation_id, $size );        
} elseif ( has_post_thumbnail( $product->id ) ) {
        $image = get_the_post_thumbnail( $product->id, $size );
} elseif ( $parent_id = wp_get_post_parent_id( $product->id ) && has_post_thumbnail( $parent_id ) ) {
        $image = get_the_post_thumbnail( $parent_id, $size );       
} else {
        $image = '';
}
$input_name  = 'quantity';
$input_value  = '1';
$max_value  = '';
$min_value  = '0';

?>
<style type="text/css">
    td img {
        padding-top: 4%;
        max-height: 90px;
        max-width: 90px;
    }
    
    .modal {
        position:   absolute;
        z-index:    1000;
        top:        0;
        left:       0;
        height:     100%;
        width:      100%;
        background: rgba( 255, 255, 255, .8 ) 
                    url('<?php echo get_option( 'siteurl' ); ?>/wp-content/plugins/woocommerce/assets/images/ajax-loader.gif') 
                    50% 50% 
                    no-repeat;
    }
    
</style>
<script type="text/javascript">
//    jQuery.noConflict();
    jQuery(function() {
        
        jQuery('div#offer_your_price_form').on({
            ajaxStart: function() { 
                jQuery('div.modal').css('display', 'block');
            },
            ajaxStop: function() { 
                jQuery('div.modal').css('display', 'none');
                jQuery('input[name=my_offer]').focus();
                jQuery('input[name=quantity]').attr('step', '1');
            }
        });
        
        jQuery('div.quantity:not(.buttons_added)').addClass('buttons_added').append('<input type="button" value="+" id="add1" class="plus" />').prepend('<input type="button" value="-" id="minus1" class="minus" />');
        
        jQuery('input#submit_my_offer').click( function(){
            
            var regex_price = /^\d*\.?\d+$/;
            var regex_email = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            
            var error_msg = 'Invalid value for';
            var display_error = false;
            
            var offer_price = Number(jQuery('input[name=my_offer]').val());         
            var current_price = Number('<?php echo $product->get_price(); ?>');            
            
            if ( ! regex_price.test( offer_price ) || offer_price >= current_price || offer_price == '' || offer_price == 0 ) {
                error_msg += ' "My Offer"';
                display_error = true;
                jQuery('input[name=my_offer]').css('border-color', 'red');
            } else {
                jQuery('input[name=my_offer]').css('border-color', '');
            }
            
            var email = jQuery('input[name=my_email]').val();
            
            if ( ! regex_email.test( email ) ) {
                error_msg += ' "My E-mail"';
                display_error = true;
                jQuery('input[name=my_email]').css('border-color', 'red');
            } else {
                jQuery('input[name=my_email]').css('border-color', '');
            }
            
            if ( display_error == true ) {
                jQuery('label#error_msg').text( error_msg );                
                return false;
            } else {
                jQuery('label#error_msg').text('');
            }
            
            jQuery.ajax({
                type: 'POST',
                url: '<?php echo admin_url('admin-ajax.php'); ?>',
                data: {
                    action:         'on_submit_my_offer',
                    product_id:     jQuery('input#product_id').val(),
                    current_price:  <?php echo $product->get_price(); ?>,
                    my_offer:       jQuery('input[name=my_offer]').val(),
                    quantity:       jQuery('input#quantity').val(),
                    my_email:       jQuery('input[name=my_email]').val()
                },
                success: function( response ) {
                
                    if( response == 'coupon_already_sent' ) {
                        var html_string = '<center>';
                            html_string += '<strong>';
                            html_string += '<label style="color: green;"><?php _e( 'Please Check Your Email again (', 'woocommerce' ); ?>';
                            html_string += jQuery('input[name=my_email]').val();
                            html_string += '<?php _e( ') Your Offer is already been sent.', 'woocommerce' ); ?></label>';
                            html_string += '</strong><br /><br />';
                            html_string += '<a id="close_offer_your_price_form" class="button">Close</a>';
                            html_string += '</center>';
                    } else {
                        var html_string = '<center>';
                            html_string += '<strong>';
                            html_string += '<label style="color: green;"><?php _e( 'Please Check Your Email (', 'woocommerce' ); ?>';
                            html_string += jQuery('input[name=my_email]').val();
                            html_string += '<?php _e( ') to Receive Your Offer', 'woocommerce' ); ?></label>';
                            html_string += '</strong><br /><br />';
                            html_string += '<a id="close_offer_your_price_form" class="button">Close</a>';
                            html_string += '</center>';
                        
                    }
                        jQuery('div#offer_your_price_form').html( html_string );
                        jQuery('<script type="text/javascript">\
                                    jQuery(function() {\
                                        jQuery("a#close_offer_your_price_form").click(function(){\
                                            jQuery("div#fancybox-wrap").css( "display", "none" );\
                                            jQuery("div#fancybox-overlay").css( "display", "none" );\
                                        });\
                                    });\
                                <\/script>').appendTo('div#offer_your_price_form');
                    
                }

            });
            
        });
        
    });
</script>
<div id="offer_your_price_form" style="overflow: hidden;">
    <center id="">
        <table style="min-width: 500px;">
            <tr style="background-color: <?php echo ( empty( $colors['primary'] ) ) ? '#ad74a2' : $colors['primary']; ?><?php if( !empty( $colors['primary'] ) ) echo 'color: ' . ( $colors['primary'] == '#ffffff' ) ? '#000000' : '#ffffff'; ?>; height: 3.5em;">
                <th colspan="3" style="text-align: center; vertical-align: middle; border-bottom-style: solid; border-bottom-width: 1em; <?php if( !empty( $colors['content_bg'] ) ) echo 'border-bottom-color: ' . $colors['content_bg']; ?>"><h2 style="font-size: 1.2em;"><?php echo get_option('offer_your_price_button_text'); ?></h2></th>
            </tr>
            <tr>
                <td><?php echo __('Current Price', 'woocommerce') . ': '; ?></td>
                <td><input type="text" readonly="readonly" name="current_price" value="<?php echo $product->get_price(); ?>" /></td>
                <td rowspan="3" style="vertical-align: top;"><?php echo $image; ?></td>
            </tr>
            <tr>
                <td><?php echo __('My Offer', 'woocommerce') . ': '; ?></td>
                <td><input type="text" name="my_offer" value="" onkeyup="this.value=this.value.replace(/[^\d.]/,'')" /></td>
            </tr>
            <tr>
                <td><?php echo __('Quantity', 'woocommerce') . ': '; ?></td>                
                <td><input type="number" id="quantity" value="<?php echo $_REQUEST['quantity']; ?>" min="1" /></td>
            </tr>
            <tr>
                <td><?php echo __('My E-mail ', 'woocommerce') . ': '; ?></td>
                <td colspan="2"><input type="text" name="my_email" value="<?php echo $user_email; ?>" style="width: 95%;"/></td>
            </tr>
            <tr>
                <td colspan="2" style="vertical-align: middle;"><label id="error_msg" style="color: red;"></label></td>
                <td><input class="button" type="submit" id="submit_my_offer" value="Submit" style="float: right; margin-top: 3%;"/></td>
            </tr>
        </table>
        <input type="hidden" id="product_id" value="<?php echo $_REQUEST['product_id']; ?>" />
    </center>
    <div class="modal"></div>
</div>
