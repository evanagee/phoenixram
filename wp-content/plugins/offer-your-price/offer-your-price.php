<?php
/*
Plugin Name: Offer Your Price
Plugin URI: http://www.storeapps.org/product/offer-your-price/
Description: Allows customer to offer their price. 
Version: 1.0.2
Author: Store Apps
Author URI: http://www.storeapps.org/
Copyright (c) 2012 Store Apps All rights reserved.
*/

/**
 * Required functions
 **/
if ( ! function_exists( 'oyp_is_woocommerce_active' ) ) require_once( 'sa-includes/sa-functions.php' );

if ( oyp_is_woocommerce_active() ) {
    
    /**
     * Localisation
     **/
    load_plugin_textdomain( 'offer_your_price', false, dirname( plugin_basename( __FILE__ ) ) . '/languages');

    require 'classes/offer-your-price.class.php';

    global $offer_your_price_coupon;
    $offer_your_price_coupon = new Offer_Your_Price( __FILE__ );
    
    add_action( 'woocommerce_after_add_to_cart_button', 'display_offer_your_price' );
    add_action( 'wp_ajax_nopriv_on_submit_my_offer', 'on_submit_my_offer' );
    add_action( 'wp_ajax_on_submit_my_offer', 'on_submit_my_offer' );
    add_action( 'wp_ajax_nopriv_is_offer_your_price', 'is_offer_your_price' );
    add_action( 'wp_ajax_is_offer_your_price', 'is_offer_your_price' );
    add_action( 'init', 'offer_your_price_init' );
    
    //
    function get_product_instance( $product_id ) {
        $parent_id = wp_get_post_parent_id ( $product_id );
    
//        if ( !class_exists( 'WC_Product' ) ) {
//            require_once ABSPATH . 'wp-content/plugins/woocommerce/classes/class-wc-product.php';
//        }
//        if ( !class_exists( 'WC_Product_Variation' ) ) {
//            require_once ABSPATH . 'wp-content/plugins/woocommerce/classes/class-wc-product-variation.php';
//        }

        if ( $parent_id > 0 ) {
            $_product = new WC_Product_Variation( $product_id );
        } else {
            $_product = function_exists( 'get_product' ) ? get_product( $product_id ) : new WC_Product( $product_id );
        }
        
    return $_product;
    }
    
    // Function to get formatted Product's Name
    function get_product_title ( $product_id ) {
            $parent_id = wp_get_post_parent_id ( $product_id );

            if ( $parent_id > 0 ) {
                $_product = new WC_Product_Variation( $product_id );
                $product_title = substr( get_the_title( $product_id ), strpos( get_the_title( $product_id ), 'of ')+3);
            } else {
                $_product = function_exists( 'get_product' ) ? get_product( $product_id ) : new WC_Product( $product_id );
                $product_title = get_the_title( $product_id );
            }

            if ( !empty( $_product->variation_data ) && woocommerce_get_formatted_variation( $_product->variation_data, true ) != '' ) $product_title .= ' ( ' . woocommerce_get_formatted_variation( $_product->variation_data, true ) . ' )';

            return $product_title;
    }

    //Function to send e-mail containing coupon code to customer
    function send_offer_coupon( $offer_action = array(), $product_id = 0, $customer_email = '', $offer_coupon_code = '' ) {
        global $woocommerce, $product;

        $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

        $offer_type = $offer_action['offer_type'];
        
        $subject = $email_heading = $message = '';
        
        switch ( $offer_type ) {

                case 'accepted':
                        $subject    = apply_filters( 'woocommerce_email_subject_offer_coupon', sprintf( '[%s] %s', $blogname, __( 'Offer Accepted!!!', 'woocommerce' ) ) );
                        $email_heading  = sprintf(__( 'Your Offer Has Been Accepted!', 'woocommerce' ) );
                        $message        = sprintf(__( "Your Offer has been accepted for %s. See below on how to redeem your discount.", 'woocommerce' ), '<strong>' . woocommerce_price( $offer_action['offer_price'] ) . '</strong>' );
                        break;

                case 'counter':
                        $subject	= apply_filters( 'woocommerce_email_subject_offer_coupon', sprintf( '[%s] %s', $blogname, __( 'Counter Offer!!!', 'woocommerce' ) ) );
                        $email_heading 	= sprintf(__( 'Your offer has been Counter Offered', 'woocommerce' ) );
                        $message        = sprintf(__( "Your Offer has been counter offered for %s. This is our lowest available price. See below on how to redeem your discount.", 'woocommerce' ), '<strong>' . woocommerce_price( $offer_action['offer_price'] ) . '</strong>' );
                        break;

        }
        
        $coupon = new WC_Coupon( $offer_coupon_code );
        
        $coupon_expiry_date = date( 'jS M Y', $coupon->expiry_date );
        
        // Start collecting content for e-mail
        ob_start();

        $product = get_product_instance( $product_id );
        
        include(apply_filters('woocommerce_offer_your_price_email_template', 'templates/email.php'));

        // Get contents of the e-mail to be sent
        $message = ob_get_clean();

        woocommerce_mail( $customer_email, $subject, $message );

    }
    
    //
    function generate_coupon( $offer_action = array(), $product_id = 0, $customer_email = '' ) {
        
        $offer_coupon_code = strtoupper( uniqid( substr( preg_replace('/[^a-z0-9]/i', '', sanitize_title( $customer_email ) ), 0, 5 ) ) );
                    
    $offer_coupon_args = array(
            'post_title'    => $offer_coupon_code,
            'post_content'  => '',
            'post_status'   => 'publish',
            'post_author'   => 1,
            'post_type'     => 'shop_coupon'
    );
                    
    $offer_coupon_id = wp_insert_post( $offer_coupon_args );
        
        $validity = date('Y-m-d', strtotime('+' . get_option( 'offer_your_price_coupon_validity' ) . ' days', time()));
                    
    // Add meta for Gift Certificate
    update_post_meta( $offer_coupon_id, 'discount_type', 'fixed_product' );
    update_post_meta( $offer_coupon_id, 'coupon_amount', $offer_action['coupon_amount'] );
    update_post_meta( $offer_coupon_id, 'individual_use', get_option('offer_your_price_individual_use') );
//    update_post_meta( $offer_coupon_id, 'minimum_amount', $offer_action['minimum_purchase'] );
    update_post_meta( $offer_coupon_id, 'product_ids', $product_id );
    update_post_meta( $offer_coupon_id, 'exclude_product_ids', '' );
    update_post_meta( $offer_coupon_id, 'usage_limit', 1 );
    update_post_meta( $offer_coupon_id, 'expiry_date', $validity );
    update_post_meta( $offer_coupon_id, 'customer_email', $customer_email );
    update_post_meta( $offer_coupon_id, 'apply_before_tax', get_option('offer_your_price_apply_before_tax') );
    update_post_meta( $offer_coupon_id, 'free_shipping', 'no' );
        update_post_meta( $offer_coupon_id, 'coupon_generated_from', 'offer_your_price' );
                    
    return $offer_coupon_code;
        
    }
    
    //
    function is_offer_your_price() {
        if ( get_post_meta( $_POST['post_id'], 'is_offer_your_price', true ) == 'yes' ) {
            echo json_encode('true');
        } else {
            echo json_encode('false');
        }
        die();
    }
    
    //
    function on_submit_my_offer() {

        $product_id     = $_POST['product_id'];
        $current_price  = $_POST['current_price'];
        $offer_price    = $_POST['my_offer'];
        $quantity       = $_POST['quantity'];
        $customer_email = $_POST['my_email'];
       
        if( is_coupon_already_sent( $product_id, $customer_email ) ) {            
            ob_clean();
            echo 'coupon_already_sent';
            die();
        }   

        $_product = get_product_instance( $product_id );
        
        if ( get_post_meta( $product_id, 'is_offer_your_price', true ) == 'no' ) return;
        
        $discount_rule = get_post_meta( $product_id, 'discount_rule', true );

        switch ( $discount_rule ) {
            
            case 'custom':
                
                $minimum_allowed_price = get_post_meta( $product_id, 'manual_minimum_price', true );

                break;
            
            case 'default':
            default :
                
                $from_price = maybe_unserialize( get_option( 'offer_your_price_discount_rule_from' ) );
                $to_price = maybe_unserialize( get_option( 'offer_your_price_discount_rule_to' ) );
                $discount_rates = maybe_unserialize( get_option( 'offer_your_price_discount_rule_discount_rate' ) );
                
                $discount = '';
                
                for ( $i = 0; $i < count( $from_price ); $i++ ) {
                    if ( $current_price >= $from_price[$i] && $current_price <= $to_price[$i] ) {
                        $discount = ( $discount_rates[$i] / 100 ) * $current_price;
                        break;
                    }
                }
                
                $minimum_allowed_price = $current_price - $discount;
                
                break;
            
        }
        
        if ( $offer_price < $minimum_allowed_price ) {
            $offer_action = array(
                'coupon_amount' => ( $current_price - $minimum_allowed_price ),
                'offer_type' => 'counter',
                'offer_price' => $minimum_allowed_price,
                'quantity' => $quantity,
                'minimum_purchase' => $current_price * $quantity
            );
        } elseif ( $offer_price >= $minimum_allowed_price ) {
            $offer_action = array(
                'coupon_amount' => ( $current_price - $offer_price ),
                'offer_type' => 'accepted',
                'offer_price' => $offer_price,
                'quantity' => $quantity,
                'minimum_purchase' => $current_price * $quantity
            );
        }

        if ( !empty( $offer_action['offer_type'] ) && ( $offer_action['coupon_amount'] > 0 ) ) {
            $offer_coupon_code = generate_coupon( $offer_action, $product_id, $customer_email );
            send_offer_coupon( $offer_action, $product_id, $customer_email, $offer_coupon_code );
        }
        
        ob_clean();
        echo 'coupon_generated';
        die();
        
    }

    function is_coupon_already_sent( $product_id, $customer_email ) {
        global $wpdb;
        
        $query = "SELECT MAX( post_id ) 
                FROM {$wpdb->postmeta} 
                WHERE meta_key = 'coupon_generated_from' 
                AND meta_value = 'offer_your_price'
                AND post_id IN( SELECT post_id 
                                FROM {$wpdb->postmeta} 
                                WHERE meta_key = 'product_ids' 
                                AND meta_value = $product_id
                                AND post_id IN( SELECT post_id 
                                                FROM {$wpdb->postmeta} 
                                                WHERE meta_key = 'customer_email' 
                                                AND meta_value = '$customer_email'
                                                )
                                )";

        $post_id = $wpdb->get_col( $query );
        
        if( isset( $post_id[0] ) ) {

            $expiry_date = get_post_meta( $post_id[0], 'expiry_date', true );
            $post_status = get_post_status( $post_id[0] );

            if(  get_post_meta( $post_id[0], 'usage_count', true ) || date( "Y-m-d" ) > $expiry_date  || $post_status != 'publish' ) {
                return false;
            } 
            return true;

        } else {
            return false;

        }
        
    }
    
    //
    function display_offer_your_price() {
        global $product, $woocommerce;
        
        if ( did_action( 'woocommerce_before_single_product' ) < 1 ) return;

        if ( $product->product_type == 'grouped' || $product->product_type == 'external' ) return;
        
        if ( $product->product_type == 'simple' ) {
            if ( get_post_meta( $product->id, 'is_offer_your_price', true ) != 'yes' )  return;
            if ( get_post_meta( $product->id, 'discount_rule', true ) == '' ) return;
        }
        
        if ( is_user_logged_in() ) {
            $current_user = wp_get_current_user();
        }
        
        $user_email_query_string = ( !empty( $current_user->data->user_email ) ) ? '&user_email=' . $current_user->data->user_email : '';
        $product_id_query_string = ( $product->product_type == 'simple' ) ? '&product_id=' . $product->id : '';
        
        $result = wp_script_is( 'fancybox', 'queue' );

        if ( !$result ) {
            $suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
            // wp_enqueue_script( 'fancybox', '/wp-content/plugins/woocommerce/assets/js/fancybox/fancybox' . $suffix . '.js', array( 'jquery' ), '', true );
            wp_enqueue_script( 'fancybox', '/wp-content/plugins/offer-your-price/assets/js/fancybox/fancybox' . $suffix . '.js', array( 'jquery' ), '', true );
            // wp_enqueue_style( 'woocommerce_fancybox_styles', '/wp-content/plugins/woocommerce/assets/css/fancybox.css' );
            wp_enqueue_style( 'woocommerce_fancybox_styles', '/wp-content/plugins/offer-your-price/assets/css/fancybox.css' );
        }
        ?>
            <div class="oyp_single_variation_wrap offer-price-wrapper" style="display: <?php echo ( $product->product_type == 'simple' ) ? 'block' : 'none'; ?>;">
                <?php
                    $woocommerce->add_inline_js( "
                        jQuery(function(){

                                    jQuery('.variations select').change(function(){
                                            setTimeout( function() {
                                                var current_href = jQuery('a#offer_your_price_lightbox').attr('href');
                                                var index_of_product_id = current_href.indexOf('&product_id=');
                                                var product_id = jQuery('input[name=variation_id]').val();
                                                var quantity = jQuery('input[name=quantity]').val();
                                                
                                                if ( product_id != '' ) {
                                                    jQuery('a#offer_your_price_lightbox').hide();
                                                    jQuery.ajax({
                                                        type: 'POST',
                                                        url: '" . admin_url('admin-ajax.php') . "',
														dataType: 'json',
                                                        data: {
                                                            action:     'is_offer_your_price',
                                                            post_id:    product_id
                                                        },
                                                        success: function( response ) {

                                                            if ( response == 'true' ) {
                                                                jQuery('div.oyp_single_variation_wrap').show();
                                                                jQuery('a#offer_your_price_lightbox').show();
                                                            } else if ( response == 'false' ) {
                                                                jQuery('div.oyp_single_variation_wrap').hide();
                                                                jQuery('a#offer_your_price_lightbox').hide();
                                                            }
                                                        }

                                                    });
                                                    
                                                    if ( index_of_product_id > -1 ) {
                                                        current_href = current_href.substr( 0, index_of_product_id );
                                                    }
                                                    current_href += '&product_id=' + product_id + '&quantity=' + quantity;
                                                    jQuery('a#offer_your_price_lightbox').attr('href', current_href);
                                                }
                                            }, 0);
                                    });                                   
                                   
                                var variation_id = jQuery('input[name=variation_id]').val();

                                if ( variation_id != '' ) {
                                    jQuery('.variations select').trigger('change');
                                }
                                                                      
                                jQuery('input[name=quantity]').change(function(){
                                    var current_href = jQuery('a#offer_your_price_lightbox').attr('href');
                                    var index_of_quantity = current_href.indexOf('&quantity=');
                                    var quantity = jQuery(this).val();
                                            
                                    if ( quantity != '' ) {
                                        if ( index_of_quantity > -1 ) {
                                            current_href = current_href.substr( 0, index_of_quantity );
                                        }
                                        current_href += '&quantity=' + quantity;
                                        jQuery('a#offer_your_price_lightbox').attr('href', current_href);
                                    }
                                });
                                
                                var quantity = jQuery('input[name=quantity]').val();
                                
                                if ( quantity != '' ) {
                                    jQuery('input[name=quantity]').trigger('change');
                                }
                                
                                // Lightbox
                                jQuery('a.offer_your_price_button').fancybox({
                                        'transitionIn'  :   'elastic',
                                        'transitionOut' :   'elastic',
                                        'speedIn'       :   600, 
                                        'speedOut'      :   200, 
                                        'overlayShow'   :   true
                                });

                        });
                    " );
                ?>
                <a id="offer_your_price_lightbox" type="button" href="<?php echo home_url() . '?page=offer_your_price_page' . $user_email_query_string . $product_id_query_string; ?>" class="offer_your_price_button button single_make_offer_button alt"><?php echo get_option( 'offer_your_price_button_text' ); ?></a>
            </div>
        <?php
        
    }
    
    function offer_your_price_init() {
            
            if ( isset( $_GET['page'] ) && $_GET['page'] == 'offer_your_price_page' ) {

                $user_email = ( !empty( $_REQUEST['user_email'] ) ) ? $_REQUEST['user_email'] : '';

                $product = get_product_instance( $_REQUEST['product_id'] );

                include WP_CONTENT_DIR . '/plugins/offer-your-price/templates/offer-price-page.php';

                die();
            }
    }
    
}
?>