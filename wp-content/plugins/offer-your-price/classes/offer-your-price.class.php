<?php
if ( ! class_exists( 'Offer_Your_Price' ) ) {

    class Offer_Your_Price {

        var $general_settings;
        var $base_name;
        var $check_update_timeout;
        var $plugin_data;
        var $sku = 'oyp';
        var $license_key;
        var $download_url;
        var $installed_version;
        var $live_version;
        var $changelog;

        public function __construct( $file ) {

            $this->check_update_timeout = (24 * 60 * 60); // 24 hours
            
            if (! function_exists( 'get_plugin_data' )) {
                require_once ABSPATH . 'wp-admin/includes/plugin.php';
            }
            $this->plugin_data = get_plugin_data( $file );
            $this->base_name = plugin_basename( $file );
            
            add_site_option( 'offer_your_price_license_key', '' );
            add_site_option( 'offer_your_price_download_url', '' );
            add_site_option( 'offer_your_price_installed_version', '' );
            add_site_option( 'offer_your_price_live_version', '' );
            add_site_option( 'offer_your_price_changelog', '' );

            if (get_site_option( 'offer_your_price_installed_version' ) != $this->plugin_data ['Version']) {
                update_site_option( 'offer_your_price_installed_version', $this->plugin_data ['Version'] );
            }

            if ( ( get_site_option( 'offer_your_price_live_version' ) == '' ) || ( get_site_option( 'offer_your_price_live_version' ) < get_site_option( 'offer_your_price_installed_version' ) ) ) {
                update_site_option( 'offer_your_price_live_version', $this->plugin_data['Version'] );
            }

            if ( empty( $this->license_key ) ) {
                $this->license_key = get_site_option( 'offer_your_price_license_key' );
            }

            if ( empty( $this->changelog ) ) {
                $this->changelog = get_site_option( 'offer_your_price_changelog' );
            }

            // Actions for License Validation & Upgrade process
            add_action( 'admin_footer', array (&$this, 'offer_your_price_support_ticket_content' ) );
            add_action( "after_plugin_row_".$this->base_name, array (&$this, 'offer_your_price_update_row' ), 10, 2 );
            add_action( 'wp_ajax_offer_your_price_validate_license_key', array (&$this, 'offer_your_price_validate_license_key' ) );
            add_action( 'wp_ajax_offer_your_price_force_check_for_updates', array (&$this, 'offer_your_price_force_check_for_updates' ) );
            add_action( 'install_plugins_pre_plugin-information', array (&$this, 'offer_your_price_overwrite_plugin_information' ) );

            // Filters for pushing Offer Your Price plugin details in Plugins API of WP
            add_filter( 'plugins_api', array (&$this, 'overwrite_wp_plugin_api_for_offer_your_price' ), 10, 3 );
            add_filter( 'site_transient_update_plugins', array (&$this, 'offer_your_price_overwrite_site_transient' ), 10, 2 );
            
            add_filter( 'plugin_row_meta', array( &$this, 'offer_your_price_add_support_link' ), 10, 4 );

            // Offer Your Price Settings to be displayed under WooCommerce->Settings
            $this->general_settings = array(
                array(
                    'name' => __('Offer Your Price - Settings', 'woocommerce'),
                    'type' => 'title',
                    'desc' => __('The following options are specific to "Offer Your Price".', 'woocommerce'),
                    'id' => 'offer_your_price_options'
                ),
                array(
                    'name' => __('Text for "Offer Your Price" button', 'woocommerce'),
                    'desc' => __('This text will appear on "Offer Your Price" button', 'woocommerce'),
                    'id'   => 'offer_your_price_button_text',
                    'type' => 'text',
                    'std'  => esc_attr(get_option('offer_your_price_button_text'))
                ),
                array(
                    'name' => __('Validity of generated coupons', 'woocommerce'),
                    'desc' => __('The generated coupon will be valid for these many days', 'woocommerce'),
                    'id' => 'offer_your_price_coupon_validity',
                    'type' => 'number',
                    'std'  => esc_attr(get_option('offer_your_price_coupon_validity'))                
                ),
                array(
                    'name' => __('Discount Rules', 'woocommerce'),
                    'desc' => '',
                    'id' => 'offer_your_price_rules_table',
                    'type' => 'offer_your_price_rules_table'
                ),
                array(
                    'name' => __('Default Coupon options', 'woocommerce'),
                    'desc' => __('Apply before tax', 'woocommerce'),
                    'desc_tip' => __('Check this box if the coupon should be applied before calculating cart tax.', 'woocommerce'),
                    'id' => 'offer_your_price_apply_before_tax',
                    'type' => 'checkbox',
                    'default' => 'no',
                    'checkboxgroup' => 'start'
                ),
                array(
                    'desc' => __('Individual use', 'woocommerce'),
                    'id' => 'offer_your_price_individual_use',
                    'desc_tip' => __('Check this box if the coupon cannot be used in conjunction with other coupons. Recommended: Keep it checked/enabled to avoid misuse.', 'woocommerce'),
                    'type' => 'checkbox',
                    'default' => 'no',
                    'checkboxgroup' => ''
                ),
                array(
                    'type' => 'sectionend',
                    'id' => 'offer_your_price_options'
                ),
            );

            add_option( 'offer_your_price_button_text', 'Offer Your Price' );
            add_option( 'offer_your_price_coupon_validity', 30 );
            add_option( 'offer_your_price_discount_rule_from', array('0.00001') );
            add_option( 'offer_your_price_discount_rule_to', array('10000') );
            add_option( 'offer_your_price_discount_rule_discount_rate', array('10') );
            add_option( 'offer_your_price_apply_before_tax', 'no' );
            add_option( 'offer_your_price_individual_use', 'yes' );

            // Actions for Gift Certificate settings
            add_action( 'woocommerce_settings_digital_download_options_after', array(&$this, 'offer_your_price_admin_settings') );
            add_action( 'woocommerce_update_options_general', array(&$this, 'save_offer_your_price_admin_settings') );
            add_action( 'woocommerce_admin_field_offer_your_price_rules_table', array(&$this, 'offer_your_price_rules_table') );

            //
            add_action( 'woocommerce_product_after_variable_attributes', array( &$this, 'offer_your_price_product_setting_variable' ), 10, 2 );
            add_action( 'woocommerce_product_options_general_product_data', array( &$this, 'offer_your_price_product_setting_simple' ) );

            // Actions to save option to generate serial key
            add_action( 'woocommerce_process_product_meta', array( &$this, 'offer_your_price_process_product_meta_simple' ));
            add_action( 'woocommerce_process_product_meta_variable', array( &$this, 'offer_your_price_process_product_meta_variable' ));

        }

        //
        function offer_your_price_overwrite_plugin_information() {
            _e( 'Changelog: ', 'offer_your_price' );
            echo $this->changelog;
            exit();
        }

        //
        function offer_your_price_overwrite_admin_url( $url = '' ) {

            $slug = substr( $this->base_name, 0, strpos( $this->base_name, '/' ) );

            if ( strpos( $url, 'plugin-install.php?tab=plugin-information&plugin=' . $slug ) ) {
                return $this->plugin_data['PluginURI'];
            }
            return $url;
        }

        //
        function offer_your_price_force_check_for_updates() {
            $current_transient = get_site_transient( 'update_plugins' );
            $new_transient = apply_filters( 'site_transient_update_plugins', $current_transient, true );
            set_site_transient( 'update_plugins', $new_transient, $this->check_update_timeout );
                echo json_encode( 'checked' );
            exit();
        }

        //
        function offer_your_price_check_for_updates() {
            
            $this->live_version = get_site_option( 'offer_your_price_live_version' );
            $this->installed_version = get_site_option( 'offer_your_price_installed_version' );
            
            if (version_compare( $this->installed_version, $this->live_version, '<=' )) {
            
                $result = wp_remote_post( 'http://www.storeapps.org/wp-admin/admin-ajax.php?action=get_products_latest_version&sku=' . $this->sku );
                
                if (is_wp_error($result)) {
                        return;
                }
                
                $response = json_decode( $result ['body'] );
                $live_version = $response->version;
                
                if ($this->live_version == $live_version || $response == 'false') {
                        return;
                }
                
                $this->changelog = $response->changelog;

                update_site_option( 'offer_your_price_live_version', $live_version );
                update_site_option( 'offer_your_price_changelog', $response->changelog );
            }
        }

        //
        function offer_your_price_overwrite_site_transient($plugin_info, $force_check_updates = false) {

            if (empty( $plugin_info->checked ))
                    return $plugin_info;

            $time_not_changed = isset( $plugin_info->last_checked ) && $this->check_update_timeout > ( time() - $plugin_info->last_checked );

            if ( $force_check_updates || !$time_not_changed ) {
                $this->offer_your_price_check_for_updates();
            }

            $plugin_base_file = $this->base_name;
            $live_version = get_site_option( 'offer_your_price_live_version' );
            $installed_version = get_site_option( 'offer_your_price_installed_version' );

                if (version_compare( $live_version, $installed_version, '>' )) {
                        $plugin_info->response [$plugin_base_file] = new stdClass();
                        $plugin_info->response [$plugin_base_file]->slug = substr( $plugin_base_file, 0, strpos( $plugin_base_file, '/' ) );
                        $plugin_info->response [$plugin_base_file]->new_version = $live_version;
                        $plugin_info->response [$plugin_base_file]->url = 'http://www.storeapps.org';
                        $plugin_info->response [$plugin_base_file]->package = get_site_option( 'offer_your_price_download_url' );
            }

            return $plugin_info;
        }

        //
        function overwrite_wp_plugin_api_for_offer_your_price($api = false, $action = '', $args = '') {

                if ($args->slug != 'offer-your-price')
                        return $api;

                if ('plugin_information' == $action || false === $api || $_REQUEST ['plugin'] == $args->slug) {
                $api->name = 'Offer Your Price';
                $api->version = get_site_option( 'offer_your_price_live_version' );
                $api->download_link = get_site_option( 'offer_your_price_download_url' );
            }

            return $api;
        }

        //
        function offer_your_price_validate_license_key() {
                $this->license_key = (isset( $_REQUEST ['license_key'] ) && ! empty( $_REQUEST ['license_key'] )) ? $_REQUEST ['license_key'] : '';
                $storeapps_validation_url = 'http://www.storeapps.org/wp-admin/admin-ajax.php?action=woocommerce_validate_serial_key&serial=' . urlencode( $this->license_key ) . '&is_download=true&sku=' . $this->sku;
                $resp_type = array ('headers' => array ('content-type' => 'application/text' ) );
                $response_info = wp_remote_post( $storeapps_validation_url, $resp_type ); //return WP_Error on response failure

                if (is_array( $response_info )) {
                        $response_code = wp_remote_retrieve_response_code( $response_info );
                        $response_msg = wp_remote_retrieve_response_message( $response_info );

                    if ($response_code == 200 && $response_msg == 'OK') {
                                $storeapps_response = wp_remote_retrieve_body( $response_info );
                            $decoded_response = json_decode( $storeapps_response );
                                if ($decoded_response->is_valid == 1) {
                                update_site_option( 'offer_your_price_license_key', $this->license_key );                
                                update_site_option( 'offer_your_price_download_url', $decoded_response->download_url );
                            } else {
                                $this->remove_license_download_url();
                            }
                            echo $storeapps_response;
                            exit();
                    }
                    $this->remove_license_download_url();
                    echo json_encode( array ('is_valid' => 0 ) );
                    exit();
            }
            $this->remove_license_download_url();
                echo json_encode( array ('is_valid' => 0 ) );
            exit();
        }

        //
        function remove_license_download_url() {
            update_site_option( 'offer_your_price_license_key', '' );                
            update_site_option( 'offer_your_price_download_url', '' );
        }

        //
        function offer_your_price_update_row($file, $plugin_data) {
            global $woocommerce;
            $license_key = get_site_option( 'offer_your_price_license_key' );
            $valid_color = '#AAFFAA';
            $invalid_color = '#FFAAAA';
                $color = ($license_key != '') ? $valid_color : $invalid_color;
            ?>
                <style>
                    div#TB_window {
                        background: lightgrey;
                    }
                </style>
                <?php
                    $woocommerce->add_inline_js( "
                        jQuery(function(){
                            jQuery('input#offer_your_price_validate_license_button').click(function(){
                                jQuery('img#offer_your_price_license_validity_image').show();
                                jQuery.ajax({
                                    url: '" . admin_url('admin-ajax.php') . "',
                                    type: 'post',
                                    dataType: 'json',
                                    data: {
                                        'action': 'offer_your_price_validate_license_key',
                                        'license_key': jQuery('input#offer_your_price_license_key').val()
                                    },
                                    success: function( response ) {
                                        if ( response.is_valid == 1 ) {
                                            jQuery('tr.offer_your_price_license_key').css('background', '" . $valid_color . "');
                                        } else {
                                            jQuery('tr.offer_your_price_license_key').css('background', '" . $invalid_color . "');
                                            jQuery('input#offer_your_price_license_key').val('');
                                        }
                                        location.reload();
                                    }
                                });
                            });

                            jQuery('input#offer_your_price_check_for_updates').click(function(){
                                jQuery('img#offer_your_price_license_validity_image').show();
                                jQuery.ajax({
                                    url: '" . admin_url('admin-ajax.php') . "',
                                    type: 'post',
                                    dataType: 'json',
                                    data: {
                                        'action': 'offer_your_price_force_check_for_updates'
                                    },
                                    success: function( response ) {
                                        if ( response == 'checked' ) {
                                            location.reload();
                                        } else {
                                            jQuery('img#offer_your_price_license_validity_image').hide();
                                        }
                                    }
                                });
                            });

                            jQuery(document).ready(function(){
                                var loaded_url = jQuery('a.offer_your_price_support_link').attr('href');
                                
                                if ( loaded_url != undefined && ( loaded_url.indexOf('width') == -1 || loaded_url.indexOf('height') == -1 ) ) {
                                    var width = jQuery(window).width();
                                    var H = jQuery(window).height();
                                    var W = ( 720 < width ) ? 720 : width;
                                    var adminbar_height = 0;

                                    if ( jQuery('body.admin-bar').length )
                                        adminbar_height = 28;

                                    jQuery('a.offer_your_price_support_link').each(function(){
                                        var href = jQuery(this).attr('href');
                                        if ( ! href )
                                                return;
                                        href = href.replace(/&width=[0-9]+/g, '');
                                        href = href.replace(/&height=[0-9]+/g, '');
                                        jQuery(this).attr( 'href', href + '&width=' + ( W - 80 ) + '&height=' + ( H - 85 - adminbar_height ) );
                                    });

                                }

                            });

                        });
                    " );
                    ?>
                <tr class="offer_your_price_license_key" style="background: <?php echo $color; ?>">
                    <td></td>
                    <td style="vertical-align: middle;"><label for="offer_your_price_license_key"><strong><?php _e( $this->plugin_data['Name'] . ' License Key', 'offer_your_price' ); ?></strong></label></td>
                    <td>
                        <input type="text" id="offer_your_price_license_key" name="offer_your_price_license_key" value="<?php echo ( ( $license_key != '' ) ? $license_key : '' ); ?>" size="50" style="text-align: center;" />
                        <input type="button" class="button" id="offer_your_price_validate_license_button" name="offer_your_price_validate_license_button" value="<?php _e( 'Validate', 'offer_your_price' ); ?>" />
                        <input type="button" class="button" id="offer_your_price_check_for_updates" name="offer_your_price_check_for_updates" value="Check for updates" />
                        <img id="offer_your_price_license_validity_image" src="<?php echo esc_url( admin_url( 'images/wpspin_light.gif' ) ); ?>" style="display: none; vertical-align: middle;" />

                    </td>
                </tr>
            <?php
        }

        //
        function offer_your_price_support_ticket_content() {
            global $current_user, $wpdb, $woocommerce, $pagenow;

            if ( $pagenow != 'plugins.php' ) return;

            if ( !( $current_user instanceof WP_User ) ) return;

            if ( isset( $_GET['result'] ) ) {
                if ( $_GET['result'] == 'success' ) {
                    ?>
                        <div id="message" class="updated fade">
                            <p><?php _e('Support query has been successfully submitted', 'offer_your_price'); ?>. <a href="http://members.appsmagnet.com/viewticket.php?tid=<?php echo $_GET['tid']; ?>&c=<?php echo $_GET['c']; ?>" target="_blank"><?php _e('View sent query'); ?></a></p>
                        </div>
                    <?php
                } else {
                    ?>
                        <div id="notice" class="error">
                            <p><?php _e('Query submission failed', 'offer_your_price'); ?>. <?php _e('Reason: ' . $_GET['message'], 'offer_your_price'); ?></p>
                        </div>
                    <?php
                }
            }
            ?>
            <div id="offer_your_price_post_query_form" style="display: none;">
                <style>
                    table#offer_your_price_post_query_table {
                        padding: 5px;
                    }
                    table#offer_your_price_post_query_table tr td {
                        padding: 5px;
                    }
                    input.oyp_text_field {
                        padding: 5px;
                    }
                    label {
                        font-weight: bold;
                    }
                </style>
                <?php

                    if ( !wp_script_is('jquery') ) {
                        wp_enqueue_script('jquery');
                        wp_enqueue_style('jquery');
                    }

                    $first_name = get_user_meta($current_user->ID, 'first_name', true);
                    $last_name = get_user_meta($current_user->ID, 'last_name', true);
                    $name = $first_name . ' ' . $last_name;
                    $customer_name = ( !empty( $name ) ) ? $name : $current_user->data->display_name;
                    $customer_email = $current_user->data->user_email;
                    $license_key = $this->license_key;
                    $ecom_plugin_version = 'WooCommerce ' . ( ( defined( 'WOOCOMMERCE_VERSION' ) ) ? WOOCOMMERCE_VERSION : $woocommerce->version );
                    $wp_version = ( is_multisite() ) ? 'WPMU ' . get_bloginfo('version') : 'WP ' . get_bloginfo('version');
                    $admin_url = admin_url();
                    $php_version = ( function_exists( 'phpversion' ) ) ? phpversion() : '';
                    $wp_max_upload_size = wp_convert_bytes_to_hr( wp_max_upload_size() );
                    $server_max_upload_size = ini_get('upload_max_filesize');
                    $server_post_max_size = ini_get('post_max_size');
                    $wp_memory_limit = WP_MEMORY_LIMIT;
                    $wp_debug = ( defined( 'WP_DEBUG' ) && WP_DEBUG === true ) ? 'On' : 'Off';
                    $this_plugins_version = $this->plugin_data['Name'] . ' ' . $this->plugin_data['Version'];
                    $ip_address = $_SERVER['REMOTE_ADDR'];
                    $additional_information = "===== [Additional Information] =====
                                               [E-Commerce Plugin: $ecom_plugin_version] =====
                                               [WP Version: $wp_version] =====
                                               [Admin URL: $admin_url] =====
                                               [PHP Version: $php_version] =====
                                               [WP Max Upload Size: $wp_max_upload_size] =====
                                               [Server Max Upload Size: $server_max_upload_size] =====
                                               [Server Post Max Size: $server_post_max_size] =====
                                               [WP Memory Limit: $wp_memory_limit] =====
                                               [WP Debug: $wp_debug] =====
                                               [" . $this->plugin_data['Name'] . " Version: $this_plugins_version] =====
                                               [License Key: $license_key] =====
                                               [IP Address: $ip_address] =====
                                              ";

                ?>
                <form id="offer_your_price_form_post_query" method="POST" action="http://www.storeapps.org/api/supportticket.php" enctype="multipart/form-data">
                <?php
                    $woocommerce->add_inline_js( "
                        jQuery(function(){
                            jQuery('input#offer_your_price_submit_query').click(function(e){
                                var error = false;

                                var client_name = jQuery('input#client_name').val();
                                if ( client_name == '' ) {
                                    jQuery('input#client_name').css('border-color', 'red');
                                    error = true;
                                } else {
                                    jQuery('input#client_name').css('border-color', '');
                                }

                                var client_email = jQuery('input#client_email').val();
                                if ( client_email == '' ) {
                                    jQuery('input#client_email').css('border-color', 'red');
                                    error = true;
                                } else {
                                    jQuery('input#client_email').css('border-color', '');
                                }

                                var subject = jQuery('table#offer_your_price_post_query_table input#subject').val();
                                if ( subject == '' ) {
                                    jQuery('input#subject').css('border-color', 'red');
                                    error = true;
                                } else {
                                    jQuery('input#subject').css('border-color', '');
                                }

                                var message = jQuery('table#offer_your_price_post_query_table textarea#message').val();
                                if ( message == '' ) {
                                    jQuery('textarea#message').css('border-color', 'red');
                                    error = true;
                                } else {
                                    jQuery('textarea#message').css('border-color', '');
                                }

                                if ( error == true ) {
                                    jQuery('label#error_message').text('* All fields are compulsory.');
                                    e.preventDefault();
                                } else {
                                    jQuery('label#error_message').text('');
                                }

                            });

                            jQuery(\"span.offer_your_price_support a.thickbox\").click( function(){                                    
                                setTimeout(function() {
                                    jQuery('#TB_ajaxWindowTitle strong').text('Send your query');
                                }, 0 );
                            });

                            jQuery('div#TB_ajaxWindowTitle').each(function(){
                               var window_title = jQuery(this).text(); 
                               if ( window_title.indexOf('Send your query') != -1 ) {
                                   jQuery(this).remove();
                               }
                            });

                            jQuery('input,textarea').keyup(function(){
                                var value = jQuery(this).val();
                                if ( value.length > 0 ) {
                                    jQuery(this).css('border-color', '');
                                    jQuery('label#error_message').text('');
                                }
                            });

                        });
                    " );
                    ?>
                    <table id="offer_your_price_post_query_table">
                        <tr>
                            <td><label for="client_name"><?php _e('Name', 'offer_your_price'); ?>*</label></td>
                            <td><input type="text" class="regular-text oyp_text_field" id="client_name" name="client_name" value="<?php echo $customer_name; ?>" /></td>
                        </tr>
                        <tr>
                            <td><label for="client_email"><?php _e('E-mail', 'offer_your_price'); ?>*</label></td>
                            <td><input type="email" class="regular-text oyp_text_field" id="client_email" name="client_email" value="<?php echo $customer_email; ?>" /></td>
                        </tr>
                        <tr>
                            <td><label for="current_plugin"><?php _e('Product', 'offer_your_price'); ?></label></td>
                            <td><input type="text" class="regular-text oyp_text_field" id="current_plugin" name="current_plugin" value="<?php echo $this_plugins_version; ?>" readonly /></td>
                        </tr>
                        <tr>
                            <td><label for="subject"><?php _e('Subject', 'offer_your_price'); ?>*</label></td>
                            <td><input type="text" class="regular-text oyp_text_field" id="subject" name="subject" value="<?php echo ( !empty( $subject ) ) ? $subject : ''; ?>" /></td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; padding-top: 12px;"><label for="message"><?php _e('Message', 'offer_your_price'); ?>*</label></td>
                            <td><textarea id="message" name="message" rows="10" cols="60"><?php echo ( !empty( $message ) ) ? $message : ''; ?></textarea></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><label id="error_message" style="color: red;"></label></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="submit" class="button" id="offer_your_price_submit_query" name="submit_query" value="Send" /></td>
                        </tr>
                    </table>
                    <input type="hidden" name="license_key" value="<?php echo $license_key; ?>" />
                    <input type="hidden" name="sku" value="<?php echo $this->sku; ?>" />
                    <input type="hidden" class="hidden_field" name="ecom_plugin_version" value="<?php echo $ecom_plugin_version; ?>" />
                    <input type="hidden" class="hidden_field" name="wp_version" value="<?php echo $wp_version; ?>" />
                    <input type="hidden" class="hidden_field" name="admin_url" value="<?php echo $admin_url; ?>" />
                    <input type="hidden" class="hidden_field" name="php_version" value="<?php echo $php_version; ?>" />
                    <input type="hidden" class="hidden_field" name="wp_max_upload_size" value="<?php echo $wp_max_upload_size; ?>" />
                    <input type="hidden" class="hidden_field" name="server_max_upload_size" value="<?php echo $server_max_upload_size; ?>" />
                    <input type="hidden" class="hidden_field" name="server_post_max_size" value="<?php echo $server_post_max_size; ?>" />
                    <input type="hidden" class="hidden_field" name="wp_memory_limit" value="<?php echo $wp_memory_limit; ?>" />
                    <input type="hidden" class="hidden_field" name="wp_debug" value="<?php echo $wp_debug; ?>" />
                    <input type="hidden" class="hidden_field" name="current_plugin" value="<?php echo $this_plugins_version; ?>" />
                    <input type="hidden" class="hidden_field" name="ip_address" value="<?php echo $ip_address; ?>" />
                    <input type="hidden" class="hidden_field" name="additional_information" value='<?php echo $additional_information; ?>' />
                </form>
            </div>
            <?php
        }

        //
        function offer_your_price_add_support_link( $plugin_meta, $plugin_file, $plugin_data, $status ) {
            
            if ( $this->base_name == $plugin_file ) {
                $query_char = ( strpos( $_SERVER['REQUEST_URI'], '?' ) !== false ) ? '&' : '?';
                $plugin_meta[] = '<a href="#TB_inline'.$query_char.'inlineId=offer_your_price_post_query_form" class="thickbox offer_your_price_support_link" title="' . __( 'Submit your query', 'offer_your_price' ) . '">' . __( 'Support', 'offer_your_price' ) . '</a>';
                $plugin_meta[] = '<a href="http://www.storeapps.org/support/documentation/offer-your-price/" target="_blank" title="' . __( 'Documentation', 'offer_your_price' ) . '">' . __( 'Docs', 'offer_your_price' ) . '</a>';
            }
            
            return $plugin_meta;
            
        }

        //
        function offer_your_price_product_setting_simple( $post_id ) {
            global $post, $woocommerce;

            $term = wp_get_object_terms( $post->ID, 'product_type', array( 'fields' => 'names' ) );

            if ( is_wp_error( $term ) || empty( $term ) || $term[0] != 'simple' ) return;
            ?>
            <?php
                $woocommerce->add_inline_js( "
                    jQuery(function(){
                        if ( jQuery('input#is_offer_your_price').is(':checked') ) {
                            jQuery('p.discount_rule').css( 'display', 'block' );
                        } else {
                            jQuery('p.discount_rule').css( 'display', 'none' );
                        }

                        jQuery('input#is_offer_your_price').click(function(){
                            jQuery('p.discount_rule').slideToggle();
                        });
                    });
                " );
            ?>
            <div class="options_group">
                <p class="form-field show_if_simple" style="width: 100%;">
                    <label><?php _e('Offer Your Price?', 'woocommerce'); ?></label>
                    <input type="checkbox" id="is_offer_your_price" name="is_offer_your_price" title="<?php _e('Allow customer to Offer their Price?', 'woocommerce'); ?>" style="width: 3%;" <?php checked( get_post_meta( $post->ID, 'is_offer_your_price', true ), 'yes' ); ?> value="yes" />
                    <label for="is_offer_your_price"><?php _e('Allow customer to Offer their Price', 'woocommerce'); ?></label>
                    <span class="description">(meta:is_offer_your_price &rarr; <?php _e('allowed values', 'woocommerce'); ?> [yes OR no])</span>
                </p>
                <?php
                    if ( get_post_meta( $post->ID, 'is_offer_your_price', true ) == 'yes' ) {
                        $discount_rule_style = 'style="display: block;"';
                    } else {
                        $discount_rule_style = 'style="display: none;"';
                    }
                ?>
                <p class="form-field show_if_simple discount_rule" <?php echo $discount_rule_style; ?>>
                    <?php $current_discount_rule = get_post_meta( $post->ID, 'discount_rule', true ); ?>
                    <label><?php _e('Discount Rule', 'woocommerce'); ?></label>
                    <input type="radio" id="offer_your_price_default_rule" name="discount_rule" value="default" style="width: 3%;" <?php echo ( checked( $current_discount_rule, 'default', false ) == '' && checked( $current_discount_rule, 'custom', false ) == '' ) ? "checked='checked'": checked( $current_discount_rule, 'default', false ); ?>/><label for="offer_your_price_default_rule" ><?php _e('Use Global / Admin setting', 'woocommerce'); ?></label>
                    <span class="description">(meta:discount_rule &rarr; <?php _e('value for this option', 'woocommerce'); ?> [default])</span>
                </p>
                <p class="form-field show_if_simple discount_rule" <?php echo $discount_rule_style; ?>>
                    <label>&nbsp;</label>
                    <input type="radio" id="offer_your_price_custom_rule" name="discount_rule" value="custom" style="width: 3%;" <?php echo checked( $current_discount_rule, 'custom', false ); ?>/><label for="offer_your_price_custom_rule" ><?php _e('Minimum allowed Product Price', 'woocommerce'); ?></label>
                    <span class="description">(meta:discount_rule &rarr; <?php _e('value for this option', 'woocommerce'); ?> [custom])</span>
                </p>
                <p class="form-field show_if_simple discount_rule" <?php echo $discount_rule_style; ?>>
                    <label>&nbsp;</label>
                    <input type="text" name="manual_minimum_price" value="<?php echo get_post_meta( $post->ID, 'manual_minimum_price', true ); ?>" style="width: 25%;"/>
                    <span class="description">(meta:manual_minimum_price)</span>
                </p>
            </div>
            <?php
        }

        // Function for saving checkbox value for generating serial key for simple product
        function offer_your_price_process_product_meta_simple( $post_id ) {
        $product_type_term = wp_get_object_terms( $post_id, 'product_type', array( 'fields' => 'names' ) );
            if ( isset( $product_type_term[0] ) && $product_type_term[0] == 'variable' ) return;

            if (isset($_POST['is_offer_your_price']) && !empty($_POST['is_offer_your_price']) ) {
                update_post_meta($post_id, 'is_offer_your_price', 'yes');
            } else {
                update_post_meta($post_id, 'is_offer_your_price', 'no');
            }

            if (isset($_POST['discount_rule']) && !empty($_POST['discount_rule'])) {
                update_post_meta($post_id, 'discount_rule', $_POST['discount_rule']);
                update_post_meta($post_id, 'manual_minimum_price', $_POST['manual_minimum_price']);
            }

        }

        //
        function offer_your_price_product_setting_variable( $loop, $variation_data ) {
            global $woocommerce;
           
                    $woocommerce->add_inline_js( "
                        jQuery(function() {
                            jQuery('input#is_offer_your_price_" . $variation_data['variation_post_id'] . "').click( function(){
                                jQuery('tr." . $variation_data['variation_post_id'] . "').slideToggle();
                            });
                        });
                    " );
            ?>
            <tr>
                <td colspan="2">
                    <div>
                        <label>
                            <input id="is_offer_your_price_<?php echo $variation_data['variation_post_id']; ?>" type="checkbox" class="checkbox" name="is_offer_your_price[<?php echo $loop; ?>]" <?php if (isset($variation_data['is_offer_your_price'][0])) checked($variation_data['is_offer_your_price'][0], 'yes'); ?> /> <?php _e('Allow customer to Offer their Price', 'woocommerce'); ?> 
                            <a class="tips" data-tip="<?php _e('Enable this option to allow customer to offer their price.', 'woocommerce'); ?> (meta:is_offer_your_price &rarr; <?php _e('allowed values', 'woocommerce'); ?> [yes OR no])" href="#">[?]</a>
                        </label>
                    </div>
                </td>
            </tr>
            <?php
                $current_discount_rule = get_post_meta( $variation_data['variation_post_id'], 'discount_rule', true );
            ?>
            <tr class="<?php echo $variation_data['variation_post_id']; ?>" style="overflow: hidden; display: <?php echo ( isset( $variation_data['is_offer_your_price'][0] ) && $variation_data['is_offer_your_price'][0] == 'yes' ) ? 'table-row' : 'none'; ?> ">
                <td>
                    <label><?php _e('Discount Rule', 'woocommerce'); ?></label>
                </td>
                <td>
                    <div>
                        <label>
                            <input type="radio" name="discount_rule[<?php echo $loop; ?>]" value="default" <?php echo ( checked( $current_discount_rule, 'default', false ) == '' && checked( $current_discount_rule, 'custom', false ) == '' ) ? "checked='checked'": checked( $current_discount_rule, 'default', false );  ?> style="max-width: 12%;" /> <?php _e('Use Global / Admin setting', 'woocommerce'); ?> 
                            <a class="tips" data-tip="<?php _e('Check if you want to use setting from admin panel', 'woocommerce'); ?> (meta:discount_rule &rarr; <?php _e('value for this option', 'woocommerce'); ?> [default])" href="#">[?]</a>
                        </label>
                    </div>
                </td>
            </tr>

            <tr class="<?php echo $variation_data['variation_post_id']; ?>" style="overflow: hidden; display: <?php echo ( isset( $variation_data['is_offer_your_price'][0] ) && $variation_data['is_offer_your_price'][0] == 'yes' ) ? 'table-row' : 'none'; ?> ">
                <td></td>
                <td>
                    <div>
                        <label>
                            <input type="radio" name="discount_rule[<?php echo $loop; ?>]" value="custom" <?php echo checked( $current_discount_rule, 'custom', false ) ?> style="max-width: 12%;" /> <?php _e('Minimum allowed Product Price', 'woocommerce'); ?> 
                            <a class="tips" data-tip="<?php _e('Check if you want to enter a minimum price for the product', 'woocommerce'); ?> (meta:discount_rule &rarr; <?php _e('value for this option', 'woocommerce'); ?> [custom])" href="#">[?]</a>
                            <input type="text" name="manual_minimum_price[<?php echo $loop; ?>]" value="<?php echo get_post_meta( $variation_data['variation_post_id'], 'manual_minimum_price', true ); ?>"/>
                        </label>
                        <a class="tips" data-tip="(meta:manual_minimum_price)" href="#">[?]</a>
                    </div>
                </td>
            </tr>

            <?php
        }

        // Function for saving checkbox value for generating serial key for variable product
        function offer_your_price_process_product_meta_variable( $post_id ) {

            $max_loop = max(array_keys($_POST['variable_post_id']));

            for ($i = 0; $i <= $max_loop; $i++) {

                if (isset($_POST['variable_post_id'][$i])) {

                    if (isset($_POST['is_offer_your_price'][$i]) && isset($_POST['is_offer_your_price'][$i])) {
                        update_post_meta($_POST['variable_post_id'][$i], 'is_offer_your_price', 'yes');
                    } else {
                        update_post_meta($_POST['variable_post_id'][$i], 'is_offer_your_price', 'no');
                    }

                    if (isset($_POST['discount_rule'][$i]) && isset($_POST['discount_rule'][$i])) {
                        update_post_meta($_POST['variable_post_id'][$i], 'discount_rule', $_POST['discount_rule'][$i]);
                        update_post_meta($_POST['variable_post_id'][$i], 'manual_minimum_price', $_POST['manual_minimum_price'][$i]);
                    } else {
                        update_post_meta($_POST['variable_post_id'][$i], 'discount_rule', 'no');
                    }
                }
            }
        }

        //
        function offer_your_price_rules_table( $value ) {
            global $woocommerce;

            $from = maybe_unserialize( get_option( 'offer_your_price_discount_rule_from' ) );
            $to = maybe_unserialize( get_option( 'offer_your_price_discount_rule_to' ) );
            $discount_rate = maybe_unserialize( get_option( 'offer_your_price_discount_rule_discount_rate' ) );

            ?><tr valign="top">
                    <th scope="row" class="titledesc">
                        <label for="<?php echo esc_attr( $value['id'] ); ?>"><?php echo $value['name']; ?></label>
                    </th>
                    <td class="forminp">
                        <table class="discountrows widefat" cellspacing="0">
                            <thead>
                                    <tr>
                                            <th class="check-column"><input type="checkbox"></th>
                                            <th><?php _e('Prices From', 'woocommerce'); ?> <a class="tips" data-tip="<?php _e('From Product Prices', 'woocommerce'); ?>">[?]</a></th>
                                            <th><?php _e('Prices To', 'woocommerce'); ?> <a class="tips" data-tip="<?php _e('To Product Prices', 'woocommerce'); ?>">[?]</a></th>
                                            <th><?php _e('Maximum Discount', 'woocommerce'); ?>&nbsp;<a class="tips" data-tip="<?php _e('This discount will be applied on Product whose prices are between this price range.', 'woocommerce'); ?>">[?]</a></th>
                                    </tr>
                            </thead>
                            <tfoot>
                                    <tr>
                                            <th colspan="2"><a href="#" class="add_discount_rule button"><?php _e('+ Add Discount Rule', 'woocommerce'); ?></a></th>
                                            <th><small><?php _e('Prices in above rule should not overlap.', 'woocommerce'); ?></small></th>
                                            <th><a href="#" class="remove button"><?php _e('Delete selected rules', 'woocommerce'); ?></a></th>
                                    </tr>
                            </tfoot>
                            <tbody id="discount_rules">

                                    <?php 

                                        if ($discount_rate && is_array($discount_rate)) 
                                            for ( $i = 0; $i < count( $discount_rate ) ; $i++ ) : 
                                        ?>
                                    <tr class="offer_your_price_discount_rule">
                                        <th class="check-column"><input type="checkbox" name="select" /></th>
                                        <td class="from">
                                            <input type="text" class="text" value="<?php echo esc_attr( $from[$i] ); ?>" name="from[<?php echo $i; ?>]" title="<?php _e('From', 'woocommerce'); ?>" placeholder="<?php _e('From', 'woocommerce'); ?>" />
                                        </td>
                                        <td class="to">
                                            <input type="text" class="text" value="<?php echo esc_attr( $to[$i] ); ?>" name="to[<?php echo $i; ?>]" title="<?php _e('To', 'woocommerce'); ?>" placeholder="<?php _e('To', 'woocommerce'); ?>" />
                                        </td>
                                        <td class="discount_rate">
                                            <input type="text" class="text" value="<?php echo esc_attr( $discount_rate[$i] ); ?>" name="discount_rate[<?php echo $i; ?>]" title="<?php _e('Discount Rate', 'woocommerce'); ?>" placeholder="<?php _e('Discount Rate', 'woocommerce'); ?>" /> % off
                                        </td>
                                    </tr>
                                    <?php endfor; ?>

                            </tbody>
                        </table>
                    </td>
              </tr>
              
              <?php
                    $woocommerce->add_inline_js( "
                        jQuery(function() {

                            jQuery('.discountrows a.add_discount_rule').live('click', function(){
                                    var size = jQuery('#discount_rules tr').size();

                                    // Add the row
                                    jQuery('<tr class=\"offer_your_price_discount_rule new_rule\">\
                                            <th class=\"check-column\"><input type=\"checkbox\" name=\"select\" /></th>\
                                            <td class=\"from\">\
                                                    <input type=\"text\" class=\"text\" name=\"from[' + size + ']\" title=\"" . __('From', 'woocommerce') . "\" placeholder=\"" . __('From', 'woocommerce') . "\" maxlength=\"10\" size=\"10\" />\
                                            </td>\
                                            <td class=\"to\">\
                                                    <input type=\"text\" class=\"text\" name=\"to[' + size + ']\" title=\"" . __('To', 'woocommerce') . "\" placeholder=\"" . __('To', 'woocommerce') . "\" maxlength=\"10\" size=\"10\" />\
                                            </td>\
                                            <td class=\"discount_rate\">\
                                                    <input type=\"text\" class=\"text\" name=\"discount_rate[' + size + ']\" title=\"" . __('Discount Rate', 'woocommerce') . "\" placeholder=\"" . __('Discount Rate', 'woocommerce') . "\" maxlength=\"10\" size=\"10\" /> % off\
                                            </td>\
                                    </tr>').appendTo('#discount_rules');

                                    jQuery(\".new_rule\").removeClass('new_rule');

                                    return false;
                            });

                            // Remove row
                            jQuery('.discountrows a.remove').live('click', function(){
                                    var answer = confirm(\"" . __('Delete the selected rules?', 'woocommerce') . "\");
                                    if (answer) {
                                            var rates = jQuery(this).closest('.discountrows').find('tbody');

                                            rates.find('tr th.check-column input:checked').each(function(i, el){
                                                    jQuery(el).closest('tr').find('input.text, input.checkbox, select.select').val('');
                                                    jQuery(el).closest('tr').hide();
                                            });
                                    }
                                    return false;
                            });

                            jQuery('#offer_your_price_coupon_validity').attr('min', '1');
                        });
                    " );
                ?>   

              <?php

        }

        // Function for saving settings for Gift Certificate
        function save_offer_your_price_admin_settings() {

            if ( isset( $_POST['offer_your_price_button_text'] ) && !empty( $_POST['offer_your_price_button_text'] ) ) {
                update_option( 'offer_your_price_button_text', $_POST['offer_your_price_button_text'] );
            }

            if ( isset( $_POST['offer_your_price_coupon_validity'] ) && !empty( $_POST['offer_your_price_coupon_validity'] ) ) {
                update_option( 'offer_your_price_coupon_validity', $_POST['offer_your_price_coupon_validity'] );
            }

            if ( isset( $_POST['from'] ) && !empty( $_POST['from'] ) ) {
                $from = array_values( array_filter( $_POST['from'] ) );
                update_option( 'offer_your_price_discount_rule_from', $from );
            }

            if ( isset( $_POST['to'] ) && !empty( $_POST['to'] ) ) {
                $to = array_values( array_filter( $_POST['to'] ) );
                update_option( 'offer_your_price_discount_rule_to', $to );
            }

            if ( isset( $_POST['discount_rate'] ) && !empty( $_POST['discount_rate'] ) ) {
                $discount_rate = array_values( array_filter( $_POST['discount_rate'] ) );
                update_option( 'offer_your_price_discount_rule_discount_rate', $discount_rate );
            }

            if ( isset( $_POST['offer_your_price_apply_before_tax'] ) && !empty( $_POST['offer_your_price_apply_before_tax'] ) ) {
                update_option( 'offer_your_price_apply_before_tax', 'yes' );
            } else {
                update_option( 'offer_your_price_apply_before_tax', 'no' );
            }

            if ( isset( $_POST['offer_your_price_individual_use'] ) && !empty( $_POST['offer_your_price_individual_use'] ) ) {
                update_option( 'offer_your_price_individual_use', 'yes' );
            } else {
                update_option( 'offer_your_price_individual_use', 'no' );
            }

        }

        // Function to display fields for configuring settings for Gift Certificate 
        function offer_your_price_admin_settings() {
            woocommerce_admin_fields($this->general_settings);
        }

    }

}
?>