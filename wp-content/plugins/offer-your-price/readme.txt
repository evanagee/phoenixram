=== Offer Your Price ===
Contributors: niravmehta, ratnakar.dubey
Donate link: http://www.storeapps.org/
Tags: woocommerce, offer, coupons, bid, auction
Requires at least: 3.3
Tested up to: 3.4.2
Stable tag: 1.0.1

Allows your customer to offer their price.

== Description ==

Offer Your Price will enable your customer to interact with your store about prices of the product. They can offer a price of their choice & this tool will respond on the basis of pre-defined rules created by Administrator.

There are situations when a customer doesn't purchase a product due to its price. So, this tool will give them option to 'Offer their Price' & this tool will create a discount coupon based on some pre-defined rules.
These rules are customizable, so Store Admin can define their own rule. In the rule, store admin actually defines "discount on price range".

Store admin have the option to set rules at 2 places - Global level & product level. The global level setting will allow you to set 'maximum allowed percent discount' on product's price range.
The product level setting will allow you to set minimum allowed product's price for that product. So, customer will get coupons on offering their price and this tool will ensure that store admin will get at least 'minimum allowed price' which is pre-defined by the admin.

Coupons are created in following 2 cases:
1. If the price entered by customer is greater than or equal to minimum allowed price set by admin, then a coupon with amount equal to difference in price of the product & price entered by customer, will be generated, And this coupon code will be send to customer through e-mail on his/her e-mail id.
2. If the price entered by customer is less than minimum allowed pice set by admin, then a coupon with amount equal to difference in price of product & minimum allowed price set by admin, will be generated, And this coupon code will be send to customer through e-mail on his/her e-mail id.

== Installation ==

1. Ensure you have latest version of [WooCommerce](http://www.storeapps.org/woocommerce/) plugin installed
2. Unzip and upload the folder 'offer-your-price' to your `/wp-content/plugins/` directory
3. Activate 'Offer Your Price' through the 'Plugins' menu in WordPress

== Usage ==

1. Go to 'WooCommerce->Settings', look for 'Offer Your Price - Settings' under 'General' tab.
2. Set discount rules under 'Discount Rules', you can add rules, if you want, using 'Add Discount Rules' button and also can delete rules by selecting & deleting using 'Delete selected rules' button.
3. Set maximum discount that can be applied on the products, in column 'Maximum Discount'.
4. You can also apply other settings like - Text for "Offer Your Price" button, Validity of generated coupons, Default Coupon options.
5. Save these settings by clicking 'Save changes'. These are Global setting for 'Offer Your Price'.
6. Open a product on which you want to enable 'Offer Your Price'.
7. For 'Simple Products': Look for 'Offer Your Price?' under 'General' tab of 'Product data' and for 'Variable Product': in each variation under 'Variation' tab , check it to enable 'Offer Your Price' for that product.
8. After checking it, look for 'Discount Rule' under it, you'll have 2 option - Use Global / Admin setting or Minimum allowed Product Price.
9. Select 'Use Global / Admin setting', if you want to use 'Offer Your Price' global setting OR select 'Minimum allowed Product Price' to set minimum allowed price manually, enter minimum price in field provided.
10. Save the product. That's it! Now, you are ready to go!

== Frequently Asked Questions ==

= How & when the coupons are generated & send? =

Coupons are created in following 2 cases:
1. If the price entered by customer is greater than or equal to minimum allowed price set by admin, then a coupon with amount equal to difference in price of the product & price entered by customer, will be generated, And this coupon code will be send to customer through e-mail on his/her e-mail id.
2. If the price entered by customer is less than minimum allowed pice set by admin, then a coupon with amount equal to difference in price of product & minimum allowed price set by admin, will be generated, And this coupon code will be send to customer through e-mail on his/her e-mail id.

= Is it possible to set expiry for the discount coupons generated from 'Offer Your Price'? If yes, how? =

Yes, it is possible, you can set it from 'WooCommerce->Settings'. Look for 'Offer Your Price - Settings' under 'General' tab. You can set number of days under 'Validity of generated coupons', after which the generated coupon will get expired.

= Is it possible to set text for 'Offer Your Price' button? =

Yes, it is possible, you can set it from 'WooCommerce->Settings'. Look for 'Offer Your Price - Settings' under 'General' tab. You can set text for 'Offer Your Price' button under 'Text for "Offer Your Price" button'.

= What does 'Prices in above rule should not overlap' means? =

The price range should not be conflicting, for example - you've set 2 rules having price range as:
1. From 0 to 100
2. From 100 to 200. Suppose a product has its price set as 100, then which rule should get applied.

So, the preferred rules should be:
1. From 0 to 99.99
2. From 100 to 199.99 and so on...
